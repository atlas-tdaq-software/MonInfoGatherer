tdaq_package()

#
# Some definitions (useless mostly)
#
add_definitions(-DMonInfVersion=\"${TDAQ_PACKAGE_VERSION}\")
cmake_host_system_information(RESULT MIGBUILD_HOST QUERY FQDN)
add_definitions(-DMIGBuildHost=\"${MIGBUILD_HOST}\")
# change to 1 to enable timing info to be collected/saved
add_definitions(-DMIG_TIMING_ENABLE=1)
#remove_definitions(-DERS_NO_DEBUG)

#set(CMAKE_CXX_FLAGS_RELEASE "-O0 ${TDAQ_OPT_FLAGS} -ftree-vectorize -fomit-frame-pointer")

# Extername libs that we use
set(tdaqlibs ipc is ZLIB ohroot daq-core-dal)
set(rootlibs ROOT::Core m -dl -rdynamic)

#
# DAL
#
tdaq_generate_dal(data/MonInfoGatherer.schema.xml
  NAMESPACE MIG::dal
  INCLUDE MIG/dal
  INCLUDE_DIRECTORIES dal DFConfiguration
  CPP_OUTPUT dal_cpp_srcs)

tdaq_add_schema(data/MonInfoGatherer.schema.xml)

tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/MIG/dal/ DESTINATION MonInfoGatherer/MonInfoGatherer PATTERN *.h)

#===========
# Libraries
#===========

# Basic/common stuff
tdaq_add_library(MIGCommon
  src/common/*.cxx
  OPTIONS PRIVATE -rdynamic
  LINK_LIBRARIES oh is)

# DAL classes
tdaq_add_library(MIGDAL DAL
  ${dal_cpp_srcs}
  LINK_LIBRARIES daq-core-dal config)

# all standard algorithms
tdaq_add_library(MIGAlgorithms
  src/algos/*.cxx
  OPTIONS PRIVATE -rdynamic $<$<CXX_COMPILER_ID:GNU>:-ftree-vectorize -ftree-vectorizer-verbose=1> -fassociative-math
      -fno-signed-zeros -fno-trapping-math
  LINK_LIBRARIES MIGCommon oh is
  )

# all standard handlers/containers
tdaq_add_library(MIGContainers
  src/containers/*.cxx
  OPTIONS PRIVATE -rdynamic
  LINK_LIBRARIES MIGCommon oh is TBB LZ4::LZ4
  )

tdaq_add_executable(MonInfoGatherer
  src/*.cxx
  LINK_LIBRARIES rc_ItemCtrl rc_CmdLine
  MIGDAL MIGContainers MIGCommon MIGAlgorithms
  is oh ipc daq-core-dal tdaq-common::ers Boost::program_options)

tdaq_add_executable(MIGHistPublisher NOINSTALL
  test/MIGHistPublisher.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options oh rc_ItemCtrl rc_CmdLine ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGTestISData NOINSTALL
  test/MIGTestISData.cxx src/InfoHandlerWrapper.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options Boost::filesystem MIGContainers MIGAlgorithms MIGCommon TBB tdaq-common::ers)

tdaq_add_executable(MIGBench
  test/MIGBench.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options oh MIGContainers MIGAlgorithms MIGCommon TBB ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGPublisher
  test/MIGPublisher.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options oh MIGContainers MIGAlgorithms MIGCommon TBB ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGSimple
  test/MIGSimple.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options oh MIGContainers MIGAlgorithms MIGCommon TBB ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGHReplay
  test/MIGHReplay.cxx
  LINK_LIBRARIES ${tdaqlibs} monsvcpub monsvc oh rc_ItemCtrl rc_CmdLine ${rootlibs} TTCInfo_ISINFO tdaq-common::ers)

# need patched oh
#tdaq_add_executable(MIGTestHandler
#  test/MIGTestHandler.cxx
#  LINK_LIBRARIES ${tdaqlibs} Boost::program_options oh MIGContainers MIGAlgorithms MIGCommon TBB ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGHistCompare NOINSTALL
  test/MIGHistCompare.cxx
  INCLUDE_DIRECTORIES tools
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options ${rootlibs} tdaq-common::ers)

tdaq_add_executable(MIGHistReceive NOINSTALL
  test/MIGHistReceive.cxx
  LINK_LIBRARIES ${tdaqlibs} Boost::program_options tdaq-common::ers is ipc owl cmdline)

#tdaq_add_executable(MIGMockHLT
#  test/MIGMockHLT.cxx src/InfoHandlerWrapper.cxx
#  LINK_LIBRARIES is_srv ${tdaqlibs} Boost::program_options Boost::filesystem oh MIGContainers MIGAlgorithms MIGCommon TBB ${rootlibs} tdaq-common::ers)

# Test app used to produce results for ADHI-4678. If you want to build it
# then uncomment and install all needed packages on your system. On CentOS
# system you may need to install: lz4-devel zlib-devel libzstd-devel snappy-devel
#tdaq_add_executable(test_compression NOINSTALL
#  test/test_compression.cxx
#  LINK_LIBRARIES Boost::program_options z lz4 zstd snappy)

tdaq_add_data(data/MonInfoGatherer.view.xml DESTINATION view)
tdaq_add_python_package(MonInfoGatherer)

# general unit tests
foreach(unit_test MIG_unit_config MIG_unit_smallintset MIG_unit_periodfinder)
    tdaq_add_executable(${unit_test} NOINSTALL
      test/unit/${unit_test}.cxx
      LINK_LIBRARIES ${tdaqlibs} Boost::unit_test_framework MIGCommon tdaq-common::ers)
    tdaq_add_test(NAME ${unit_test} POST_INSTALL COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${unit_test})
endforeach(unit_test)

foreach(unit_test MIG_unit_libmanager MIG_unit_handlermanager)
    tdaq_add_executable(${unit_test} NOINSTALL
      test/unit/${unit_test}.cxx
      LINK_LIBRARIES ${tdaqlibs} Boost::unit_test_framework MIGContainers MIGAlgorithms MIGCommon tdaq-common::ers)
    tdaq_add_test(NAME ${unit_test} POST_INSTALL COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${unit_test})
endforeach(unit_test)

# algorithm unit tests
foreach(unit_test
  MIG_unit_axistools MIG_unit_bindatamerge MIG_unit_algo_is MIG_unit_algo_add
  MIG_unit_algo_merge MIG_unit_algo_lblmerge MIG_unit_algo_normadd MIG_unit_algo_normmerge)
    tdaq_add_executable(${unit_test} NOINSTALL
      test/unit/${unit_test}.cxx
      LINK_LIBRARIES ${tdaqlibs} Boost::unit_test_framework MIGContainers MIGAlgorithms MIGCommon tdaq-common::ers rt)
    tdaq_add_test(NAME ${unit_test} POST_INSTALL COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${unit_test})
endforeach(unit_test)

# containers unit tests
foreach(unit_test
  MIG_unit_annotations
  MIG_unit_cont_histogram
  MIG_unit_cont_isdynany
  MIG_unit_cont_isdynanyng
  MIG_unit_cont_perlb
  MIG_unit_handler_histogram
  MIG_unit_handler_isdynany
  MIG_unit_histo_holder
  MIG_unit_scheduler)
    tdaq_add_executable(${unit_test} NOINSTALL
      test/unit/${unit_test}.cxx
      LINK_LIBRARIES ${tdaqlibs} Boost::unit_test_framework MIGContainers MIGAlgorithms MIGCommon tdaq-common::ers)
    tdaq_add_test(NAME ${unit_test} POST_INSTALL COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${unit_test})
endforeach(unit_test)

# unit tests which use ROOT
foreach(unit_test
  MIG_unit_histo_resize)
    tdaq_add_executable(${unit_test} NOINSTALL
      test/unit/${unit_test}.cxx
      LINK_LIBRARIES ${tdaqlibs} Boost::unit_test_framework MIGContainers MIGAlgorithms MIGCommon ${rootlibs} tdaq-common::ers)
    tdaq_add_test(NAME ${unit_test} POST_INSTALL COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${unit_test})
endforeach(unit_test)
