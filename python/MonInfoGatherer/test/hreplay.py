"""RC.Controllable which replays histograms from MDA files.

To make a run control application out of this module use `rc_pyrunner`
executable, e.g.:

  rc_pyrunner -M MDAReplay@MonInfoGatherer.test.hreplay

or

    <obj class="RunControlApplication" id="MDAReplay">
     <attr name="InterfaceName" type="string">"rc/commander"</attr>
     <attr name="ProbeInterval" type="s32">60</attr>
     <attr name="Parameters" type="string">"-M MDAReplay@MonInfoGatherer.test.hreplay"</attr>
     <attr name="RestartParameters" type="string">"-M MDAReplay@MonInfoGatherer.test.hreplay"</attr>
     <attr name="InitTimeout" type="u32">30</attr>
     <attr name="ExitTimeout" type="u32">5</attr>
     <rel name="Program">"Binary" "rc_pyrunner"</rel>
     <rel name="Uses" num="1">
      "SW_Repository" "MIG-Repository"
     </rel>
     <rel name="ProcessEnvironment" num="1">
      "Variable" "REPLAY_FILE"
     </rel>
     ...
    </obj>

Make sure that PYTHONPATH is set correctly for this application (see Uses relation
above) in case hreplay module is no in tdaq release.
"""

from __future__ import division

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
import os
import random
import re
import threading
import time

#-----------------------------
# Imports for other modules --
#-----------------------------
from ipc import IPCPartition
from ispy import IS
import ROOT
from oh import OHRootProvider
from pyolc2hlt.LogHandler import initLogging
from rcpy import Controllable

#----------------------------------
# Local non-exported definitions --
#----------------------------------

initLogging()

_LOG = logging.getLogger(__name__[__name__.find('.') + 1:])

#------------------------
# Exported definitions --
#------------------------

#---------------------
#  Class definition --
#---------------------
class MDAReplay(Controllable):
    """
    Implementation of controllable interface from RunControl.

    On every "publish" call reads corresponding histograms from a MDA ROOT file
    and publishes them in OH server. The name of the ROOT file to use comes from
    REPLAY_FILE environment variable, it has to be on a local disk.

    Note: potentially one can use root://eosatlas// syntax for file name, but
    current TDAQ setup is broken and does not include xrootd directory into
    LD_LIBRARY_PATH, so the only working option now is not to use xrootd.
    """

    partition = os.environ.get("TDAQ_PARTITION")
    is_lb_object = "RunParams.LumiBlock"  # IS object to check for LBN (attr LumiBlockNumber)
    mda_server = "Histogramming"  # server name in MDA file
    mda_provider = "TopMIG-OH_HLT"  # provider name in MDA file
    oh_server = "Histograming-IN"  # OH server name for publishing
    oh_provider = os.environ["TDAQ_APPLICATION_NAME"]  # provider name for publishing
    filter = re.compile("(/LB)?/(EXPERT|DEBUG)/.*")
    num_buckets = 8               # number of publishing buckets
    publication_period = 80       # seconds, must be wholly divisible by num_buckets
    fast_filter = re.compile("/SHIFT/.*")  # names published in every bucket

    #----------------
    #  Constructor --
    #----------------
    def __init__(self):
        Controllable.__init__(self)

        self._tfile = None
        self._provider = None
        self._is_lb_obj = None
        self._histos = {}
        self._fast_histos = {}
        self._buckets = []
        self._publisher = None
        self._event = threading.Event()

    def configure(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in configure: %s", cmd)

    def connect(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def prepareForRun(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in prepareForRun: %s", cmd)

        try:
            part = IPCPartition(self.partition)
            _LOG.info('instantiating provider: %s', self.oh_provider)
            self._provider = OHRootProvider(part, self.oh_server, self.oh_provider, None)
        except Exception as exc:
            _LOG.error('Error instantiating OH provider', exc_info=True)
            return

        try:
            _LOG.info("Instantiating IS object \"%s!%s\"",
                      self.partition, self.is_lb_object)
            self._is_lb_obj = IS.LumiBlock(self.partition, self.is_lb_object)
        except Exception as exc:
            _LOG.error('Error instantiating IS object', exc_info=True)
            return

        fname = os.environ["REPLAY_FILE"]
        self._open_file(fname)

        # start publisher thread
        _LOG.info("Starting publisher thread")
        self._event.clear()
        self._publisher = threading.Thread(target=self._replay, name="hreplay")
        self._publisher.start()

    def stopROIB(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in stopROIB: %s", cmd)

        # stop publisher thread
        _LOG.info("Stopping publisher thread")
        self._event.set()
        self._publisher.join()
        _LOG.info("Stopped publisher thread")

        # reset everything
        self._tfile = None
        self._is_lb_obj = None

    def stopDC(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopHLT(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopRecording(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopGathering(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopArchiving(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def disconnect(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """

    def unconfigure(self, cmd):
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in unconfigure: %s", cmd)

    def user(self, cmd):
        """
        @param cmd: instance of UserCmd type
        """
        _LOG.info("in user, cmd: %s", cmd)

    def onExit(self, state):
        """
        @param state: instance of FSM_STATE type
        """
        _LOG.info("in onExit: %s", state)

    def publish(self):
        """
        Publish (incremental) statistics, may be called periodically
        """
        _LOG.info("in publish")

    def _open_file(self, fname):
        """open MDA file for given run
        """

        # iterate over all histogram names in a file
        def histo_names(tdir):
            for key in tdir.GetListOfKeys():
                name = str(key.GetName())
                if name.startswith("."):
                    # skip special folders
                    continue
                if key.IsFolder():
                    for hname in histo_names(tdir.Get(name)):
                        yield name + '/' + hname
                else:
                    yield name

        _LOG.info('Opening MDA file %s', fname)
        self._tfile = ROOT.TFile.Open(fname)
        if not self._tfile:
            _LOG.error('Failed to open MDA file %s', fname)
            return

        # read all histogram names and split them into groups
        self._histos = {}
        self._fast_histos = {}
        for hpath in histo_names(self._tfile):

            _LOG.debug('hpath: %s', hpath)

            pieces = hpath.split("/", 2)
            if len(pieces) < 3:
                # odd number of components
                continue

            # first strip run_NN/lb_MM part and remember LBN
            lbn = -1
            if pieces[0].startswith("run_"):
                if not pieces[1].startswith("lb_"):
                    continue
                lbn = int(pieces[1][3:])
                pieces = pieces[2].split("/", 2)
                if len(pieces) < 3:
                    # odd number of components
                    continue

            # path should start with server name and provider name
            if pieces[:2] == [self.mda_server, self.mda_provider]:
                hname = "/" + pieces[2]
                if self.fast_filter.match(hname):
                    _LOG.debug('adding: %d -> %s (fast)', lbn, hpath)
                    self._fast_histos.setdefault(hname, {})[lbn] = hpath
                elif self.filter.match(hname):
                    _LOG.debug('adding: %d -> %s', lbn, hpath)
                    self._histos.setdefault(hname, {})[lbn] = hpath

        # split all names into buckets
        self._buckets = [set() for x in range(self.num_buckets)]
        for hname in self._histos.keys():
            bucket = hash(hname) % self.num_buckets
            self._buckets[bucket].add(hname)
        _LOG.info('Bucket sizes: %s', [len(x) for x in self._buckets])

    def _replay(self):
        """Generate histograms for given LBN.
        """

        bucket_period = self.publication_period // self.num_buckets
        while True:

            # sleep until next cycle
            now = time.time()
            next_stop = ((now + 1) // bucket_period + 1) * bucket_period
            wait = next_stop - now
            _LOG.info('will wait for %.3f seconds', wait)
            self._event.wait(wait)
            if self._event.is_set():
                # done here
                break

            # guess bucket number
            now = time.time()
            bucket = int((now + 1) // bucket_period) % self.num_buckets
            _LOG.info('start publishing bucket: %s', bucket)

            lbn = None
            if self._is_lb_obj:
                try:
                    self._is_lb_obj.checkout()
                    lbn = self._is_lb_obj.LumiBlockNumber
                    if lbn == 0:
                        lbn = 1
                    _LOG.info('Current LB number: %s', lbn)
                except Exception as exc:
                    _LOG.info('Cannot read LBN IS object: %s', exc)

            # randomize histogram name order
            hnames = list(self._buckets[bucket])
            random.shuffle(hnames)

            # combine fast and regular, fast published first
            histos = list(self._fast_histos.items())
            random.shuffle(histos)
            histos += [(hname, self._histos[hname]) for hname in hnames]

            count = 0
            for hname, hdict in histos:
                _LOG.debug('will publish name: %s', hname)
                if len(hdict) == 1 and -1 in hdict:
                    # non-LB histogram, just publish it
                    pubs = [(-1, hdict[-1])]
                    _LOG.debug('single-version histo')
                else:
                    # per-LB histo, publish two of them
                    hlbn = lbn
                    if hlbn not in hdict:
                        # find anything usable
                        hlbn = max(hdict.keys())
                    prevlbn = hlbn - 1
                    if prevlbn not in hdict:
                        prevlbn = hlbn
                    _LOG.debug('multi-version histo %s %s', prevlbn, hlbn)
                    pubs = [(lbn-1, hdict[prevlbn]), (lbn, hdict[hlbn])]

                for tag, hpath in pubs:
                    # find histogram
                    histo = self._tfile.Get(hpath)
                    if not histo:
                        _LOG.error('Failed to find histogram %s', hpath)
                        continue
                    # publish it
                    _LOG.debug('publishing <tag=%d>: %s', tag, hpath)
                    self._provider.publish(histo, hname, tag)
                    count += 1

            _LOG.info("published %d histos", count)
