#ifndef MONINFOGATHERER_CONFIGPARSER_H
#define MONINFOGATHERER_CONFIGPARSER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace MIG {

/// @addtogroup MonInfoGatherer

/**
 *  @ingroup MonInfoGatherer
 *
 *  Class which parses OKS configuration and builds MIG config objects out of it.
 */

class ConfigParser  {
public:

    /**
     *  Parse application configuration and build MIG config obejcts.
     */
    std::vector<Config> parse() const;
};

} // namespace MIG

#endif // MONINFOGATHERER_CONFIGPARSER_H
