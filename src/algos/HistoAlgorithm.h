// -*- c++ -*-
#ifndef MONINFOGATHERER_ALGOS_HISTOALGORITHM_H
#define MONINFOGATHERER_ALGOS_HISTOALGORITHM_H

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"
#include "oh/core/Axis.h"
#include "oh/core/Histogram.h"
#include "oh/core/Efficiency.h"
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class BinDataMerge;
class LabelMap;
}

namespace MIG {

/**
 *  Merge algorithm for histograms.
 *
 *  This is a new universal merging algorithm that can handle all
 *  possible kinds of histogram axes.
 */
class HistoAlgorithm {
public:

    /**
     *  Algorithm constructor.
     *
     *  @param algoConfig Algorithm configuration parameters.
     */
    HistoAlgorithm(const ConfigParameters& cfg = ConfigParameters());

    void operate(oh::ObjectBase<oh::Histogram>& src,
                 oh::ObjectBase<oh::Histogram>& dst,
                 LabelMap& labelMap) const;

    void operate(oh::ObjectBase<oh::Efficiency>& src,
                 oh::ObjectBase<oh::Efficiency>& dst,
                 LabelMap& labelMap) const;

private:

    void resize(oh::ObjectBase<oh::Histogram>& hist,
                const std::vector<oh::Axis>& newAxes,
                BinDataMerge& binMerge) const;
    void merge(oh::ObjectBase<oh::Histogram>& src,
               oh::ObjectBase<oh::Histogram>& dst,
               BinDataMerge& binMerge) const;

    bool m_normalized = false;  // Do normalized merging
};
}

#endif // MONINFOGATHERER_ALGOS_HISTOALGORITHM_H
