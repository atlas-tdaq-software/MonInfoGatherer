#ifndef MIG_HISTOALGOBASE_H
#define MIG_HISTOALGOBASE_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"
#include "oh/core/DataTypes.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {
namespace HistoTypeDispatch {

/**
 *  Method that casts histogram object to its actual type and passes it
 *  to callable using the static type of the histogram.
 *
 *  This method accepts single histogram object.
 *
 *  Callable is supposed to have oveloaded operator() which
 *  handles all known histogram types.
 */
template <typename Callable>
void dispatch1(oh::ObjectBase<oh::Histogram>& src, Callable& callable)
{
    // always call type() on ISInfo, never on its subclass!
    auto&& currType = src.ISInfo::type();
    // ISType only supports equal/non-equal operator, have to check against each supported type
    if (currType == oh::HistogramData<float>::type()) {
        using HType = oh::HistogramData<float>;
        callable(static_cast<HType&>(src));
    } else if (currType == oh::HistogramData<int>::type()) {
        using HType = oh::HistogramData<int>;
        callable(static_cast<HType&>(src));
    } else if (currType == oh::HistogramData<double>::type()) {
        using HType = oh::HistogramData<double>;
        callable(static_cast<HType&>(src));
    } else if (currType == oh::HistogramData<char>::type()) {
        using HType = oh::HistogramData<char>;
        callable(static_cast<HType&>(src));
    } else if (currType == oh::ProfileData::type()) {
        using HType = oh::ProfileData;
        callable(static_cast<HType&>(src));
    } else if (currType == oh::HistogramData<short>::type()) {
        using HType = oh::HistogramData<short>;
        callable(static_cast<HType&>(src));
    } else {
        throw UnsupportedType(ERS_HERE, src.get_title(), currType.name());
    }
}

/**
 *  Method that casts histogram objects to their actual type and passes them
 *  to callable using the static type of the histogram.
 *
 *  This method accepts two histogram objects, they must have the same dynamic
 *  type (this is not checked).
 *
 *  Callable is supposed to have oveloaded operator() which
 *  handles all known histogram types.
 */
template <typename Callable>
void dispatch2(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst, Callable& callable)
{
    // always call type() on ISInfo, never on its subclass!
    auto&& currType = src.ISInfo::type();
    // ISType only supports equal/non-equal operator, have to check against each supported type
    if (currType == oh::HistogramData<float>::type()) {
        using HType = oh::HistogramData<float>;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else if (currType == oh::HistogramData<int>::type()) {
        using HType = oh::HistogramData<int>;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else if (currType == oh::HistogramData<double>::type()) {
        using HType = oh::HistogramData<double>;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else if (currType == oh::HistogramData<char>::type()) {
        using HType = oh::HistogramData<char>;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else if (currType == oh::ProfileData::type()) {
        using HType = oh::ProfileData;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else if (currType == oh::HistogramData<short>::type()) {
        using HType = oh::HistogramData<short>;
        callable(static_cast<HType&>(src), static_cast<HType&>(dst));
    } else {
        throw UnsupportedType(ERS_HERE, src.get_title(), currType.name());
    }
}

}}

#endif
