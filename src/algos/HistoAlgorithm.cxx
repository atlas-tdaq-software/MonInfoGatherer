//-----------------------
// This Class's Header --
//-----------------------
#include "HistoAlgorithm.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <unordered_map>
#include <unordered_set>
#include "boost/math/special_functions/round.hpp"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "AxisTools.h"
#include "BinDataMerge.h"
#include "HistoTypeDispatch.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"
#include "oh/core/DataTypes.h"

using namespace MIG;


namespace {

/**
 *  Count-limited reporting of the problems with dynamic lables.
 */
void reportNonEmptyBins(oh::Object& hist) {

    unsigned const max_warnings = 10;
    static std::unordered_set<std::string> titles;  // we already complained about these
    static std::mutex titles_mutex;

    {
        std::lock_guard<std::mutex> lock(titles_mutex);
        if (titles.size() >= max_warnings) {
            // don't annoy people too much
            return;
        }

        if (titles.count(hist.id.title) > 0) {
            // already warned about this
            return;
        }

        titles.insert(hist.id.title);
    }

    ers::warning(MIG::NonEmptyDynLabelled(ERS_HERE, hist.id.title));
}


/**
 * Generic method to resize and move histogram. It is used to resize and
 * resample destination histogram into new axes. It is only called if resize
 * is needed, otherwise destination histogram is not touched at all.
 *
 * Template method is for non-profile histograms, non-template overload
 * is for ProfileData.
 */
template <typename T>
inline
int resize(oh::HistogramData<T>& hist,
           const std::vector<oh::Axis>& newAxes,
           BinDataMerge& binMerge) {

    unsigned countBins = hist.get_bins_size();
    int numFinalBins = 1;
    for (int i = 0; i != hist.dimension; ++i) {
        numFinalBins *= newAxes[i].nbins + 2;
    }

    // check that there is no unexpected data loss for dynamic labels
    if (not binMerge.checkEmptyBins(hist.get_bins_array())) {
        ::reportNonEmptyBins(hist);
    }

    //copy data to new bins
    T *newBins = new T[numFinalBins];
    std::fill_n(newBins, numFinalBins, 0.);
    binMerge.merge(hist.get_bins_array(), newBins);
    hist.set_bins_array(newBins, numFinalBins, true);

    // copy errors
    if (!hist.errors.empty()) {
        if (hist.errors.size() != countBins) {
            throw UnexpectedBinCount(ERS_HERE, "errors", countBins, hist.errors.size());
        }
        std::vector<double> newErrs(numFinalBins, 0);
        binMerge.merge(hist.errors.data(), newErrs.data());
        newErrs.swap(hist.errors);
    }

    // copy new axes
    hist.axes = newAxes;

    return numFinalBins;
}

inline
int resize(oh::ProfileData& hist,
           const std::vector<oh::Axis>& newAxes,
           BinDataMerge& binMerge) {

    unsigned countBins = hist.get_bins_size();

    // resize regular part (base class)
    int numFinalBins = ::resize(static_cast<oh::HistogramData<double>&>(hist), newAxes, binMerge);

    // copy bin entries to new bins, assumption is that bin_entries is never empty
    if (hist.bin_entries.size() != countBins) {
        throw UnexpectedBinCount(ERS_HERE, "bin_entries", countBins, hist.bin_entries.size());
    }
    std::vector<double> newBinEntries(numFinalBins, 0);
    binMerge.merge(hist.bin_entries.data(), newBinEntries.data());
    newBinEntries.swap(hist.bin_entries);

    return numFinalBins;
}

/**
 * Generic method to merge histograms. This method merges source histogram
 * with destination, but does not re-sample destination histogram.
 * If destination histogram needs to be resampled before mergng then call
 * resize() method first for destiantion histogram.
 *
 * Template method is for non-profile histograms, non-template overload
 * is for ProfileData.
 */
template <typename T>
inline
void merge(oh::HistogramData<T>& src,
           oh::HistogramData<T>& dst,
           BinDataMerge& binMerge) {

    // check that there is no unexpected data loss for dynamic labels
    if (not binMerge.checkEmptyBins(src.get_bins_array())) {
        ::reportNonEmptyBins(src);
    }

    // copy data to new bins
    binMerge.merge(src.get_bins_array(), dst.get_bins_array());

    // merge errors, one or both can be empty
    if (!src.errors.empty()) {

        // check consistency
        if (src.errors.size() != src.get_bins_size()) {
            throw UnexpectedBinCount(ERS_HERE, "errors", src.get_bins_size(), src.errors.size());
        }

        if (dst.errors.empty()) {
            dst.errors.resize(dst.get_bins_size());
        } else if (dst.errors.size() != dst.get_bins_size()) {
            throw UnexpectedBinCount(ERS_HERE, "errors", dst.get_bins_size(), dst.errors.size());
        }
        binMerge.merge(src.errors.data(), dst.errors.data());
    }

    // merge statistics
    auto src_itr = src.statistics.begin();
    auto dst_itr = dst.statistics.begin();
    while (src_itr != src.statistics.end() and dst_itr != dst.statistics.end()) {
        *dst_itr++ += *src_itr++;
    }
}

template <typename T>
inline
void merge(oh::HistogramData<T>& src,
           oh::HistogramData<T>& dst,
           BinDataMerge& binMerge,
           double wSrc, double wDst) {

    // check that there is no unexpected data loss for dynamic labels
    if (not binMerge.checkEmptyBins(src.get_bins_array())) {
        ::reportNonEmptyBins(src);
    }

    // copy data to new bins
    binMerge.merge(src.get_bins_array(), dst.get_bins_array(), wSrc, wDst);

    // merge errors, one or both can be empty
    if (!src.errors.empty()) {

        // check consistency
        if (src.errors.size() != src.get_bins_size()) {
            throw UnexpectedBinCount(ERS_HERE, "errors", src.get_bins_size(), src.errors.size());
        }

        if (dst.errors.empty()) {
            dst.errors.resize(dst.get_bins_size());
        } else if (dst.errors.size() != dst.get_bins_size()) {
            throw UnexpectedBinCount(ERS_HERE, "errors", dst.get_bins_size(), dst.errors.size());
        }

        binMerge.merge(src.errors.data(), dst.errors.data(), wSrc, wDst);

    } else if (!dst.errors.empty()) {

        if (dst.errors.size() != dst.get_bins_size()) {
            throw UnexpectedBinCount(ERS_HERE, "errors", dst.get_bins_size(), dst.errors.size());
        }

        // need to re-weight dst errors anyways, but for that we need non-empty
        // source array filled with zeroes. Potential for optimization here.
        std::vector<double> srcErr(src.get_bins_size());
        binMerge.merge(srcErr.data(), dst.errors.data(), wSrc, wDst);

    }

    // merge statistics
    auto src_itr = src.statistics.begin();
    auto dst_itr = dst.statistics.begin();
    while (src_itr != src.statistics.end() and dst_itr != dst.statistics.end()) {
        *dst_itr += *dst_itr * wDst + *src_itr * wSrc;
        ++ dst_itr;
        ++ src_itr;
    }
}

inline
void merge(oh::ProfileData& src,
           oh::ProfileData& dst,
           BinDataMerge& binMerge) {
    // resize regular part (base class)
    ::merge(static_cast<oh::HistogramData<double>&>(src), static_cast<oh::HistogramData<double>&>(dst), binMerge);

    // merge bin entries, assumption is that bin_entries are never empty for profile histo
    if (src.bin_entries.size() != src.get_bins_size()) {
        throw UnexpectedBinCount(ERS_HERE, "bin_entries", src.get_bins_size(), src.bin_entries.size());
    }
    if (dst.bin_entries.size() != dst.get_bins_size()) {
        throw UnexpectedBinCount(ERS_HERE, "bin_entries", dst.get_bins_size(), dst.bin_entries.size());
    }
    binMerge.merge(src.bin_entries.data(), dst.bin_entries.data());
}

inline
void merge(oh::ProfileData& src,
           oh::ProfileData& dst,
           BinDataMerge& binMerge,
           double wSrc, double wDst) {
    // resize regular part (base class)
    ::merge(static_cast<oh::HistogramData<double>&>(src),
            static_cast<oh::HistogramData<double>&>(dst),
            binMerge, wSrc, wDst);

    // merge bin entries, assumption is that bin_entries are never empty
    binMerge.merge(src.bin_entries.data(), dst.bin_entries.data(), wSrc, wDst);
}

}

HistoAlgorithm::HistoAlgorithm(const ConfigParameters& cfg)
        : m_normalized(cfg.Normalized())
{
}

void HistoAlgorithm::operate(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst,
                             LabelMap& labelMap) const
{
    //ERS_LOG("Running new algorithm title= "<<src.get_title());
    if (src.dimension != dst.dimension) {
        throw IncompatibleDimensions(ERS_HERE, src.get_title(), src.dimension, dst.dimension);
    }
    if (src.type() != dst.type()) {
        throw IncompatibleTypes(ERS_HERE, src.get_title(), src.type().name(), dst.type().name());
    }
    unsigned numAxes = src.dimension;

    try {
        // this will throw on errors, handled by caller
        AxisTools::BinMergeDescriptor descr =
                AxisTools::makeBinDataMerge(numAxes, src.axes, dst.axes, &labelMap);
        if (descr.resize) {
            // need to resize destination and copy its content
            resize(dst, descr.newDstAxes, *descr.dstBinMerge);
        }

        // copy source to a (new) destination
        merge(src, dst, *descr.srcBinMerge);

        // update non-binned stats counters
        dst.entries += src.entries;
    } catch (const IncompatibleAxisBinning& exc) {
        // translate this into expected type
        throw IncompatibleBinning(ERS_HERE, src.get_title(), "incompatible binning", exc);
    }
}

void HistoAlgorithm::operate(oh::ObjectBase<oh::Efficiency>& src, oh::ObjectBase<oh::Efficiency>& dst,
                             LabelMap& labelMap) const
{
    if (src.total.dimension != dst.total.dimension) {
        throw IncompatibleDimensions(ERS_HERE, src.get_title(), src.total.dimension, dst.total.dimension);
    }
    unsigned numAxes = src.total.dimension;

    try {
        // this will throw on errors, handled by caller
        AxisTools::BinMergeDescriptor descr =
                AxisTools::makeBinDataMerge(numAxes, src.total.axes, dst.total.axes, &labelMap);
        if (descr.resize) {
            // need to resize destination and copy its content
            resize(dst.total, descr.newDstAxes, *descr.dstBinMerge);
            resize(dst.passed, descr.newDstAxes, *descr.dstBinMerge);
        }

        // copy source to a (new) destination
        merge(src.total, dst.total, *descr.srcBinMerge);
        merge(src.passed, dst.passed, *descr.srcBinMerge);

        // update non-binned stats counters
        dst.total.entries += src.total.entries;
        dst.passed.entries += src.passed.entries;

        // update weights, like in TEfficiency::Add() method (does not make much sense to me)
        double sumw = src.weight + dst.weight;
        if (sumw != 0.) {
            dst.weight = src.weight * dst.weight / sumw;
        }

    } catch (const IncompatibleAxisBinning& exc) {
        // translate this into expected type
        throw IncompatibleBinning(ERS_HERE, src.get_title(), "incompatible binning", exc);
    }
}

void HistoAlgorithm::resize(oh::ObjectBase<oh::Histogram>& hist,
                            const std::vector<oh::Axis>& newAxes,
                            BinDataMerge& binMerge) const
{
    auto resize = [&](auto& src) {
        ::resize(src, newAxes, binMerge);
    };
    HistoTypeDispatch::dispatch1(hist, resize);
}

void HistoAlgorithm::merge(oh::ObjectBase<oh::Histogram>& src,
                           oh::ObjectBase<oh::Histogram>& dst,
                           BinDataMerge& binMerge) const
{
    if (m_normalized) {
        // maybe do weighted merge
        auto entries = src.entries + dst.entries;
        if (entries > 0) {
            double wSrc = double(src.entries) / entries;
            double wDst = double(dst.entries) / entries;
            auto merge_lambda = [&](auto& src, auto& dst) {
                ::merge(src, dst, binMerge, wSrc, wDst);
            };
            HistoTypeDispatch::dispatch2(src, dst, merge_lambda);
            return;
        }
    }
    // regular merge
    auto merge_lambda = [&](auto& src, auto& dst) {
        ::merge(src, dst, binMerge);
    };
    HistoTypeDispatch::dispatch2(src, dst, merge_lambda);
}
