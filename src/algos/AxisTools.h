// -*- c++ -*-
#ifndef MONINFOGATHERER_ALGOS_AXISTOOLS_H
#define MONINFOGATHERER_ALGOS_AXISTOOLS_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "oh/core/Axis.h"
#include "BinDataMerge.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class AxisLabelMap;
class LabelMap;
}

namespace MIG {
namespace AxisTools {


/**
 *  Struct with data returned from makeBinDataMerge().
 */
struct BinMergeDescriptor {
    bool resize = false;   // If true then resizing of destination bin array is needed
    std::unique_ptr<BinDataMerge> srcBinMerge;  // Merger for source histogram
    std::unique_ptr<BinDataMerge> dstBinMerge;  // Merger for destination histogram, nullptr if resize is false
    std::vector<oh::Axis> newDstAxes;           // New axes definitions, empty if resize is false
};

/**
 *  Compare source and destination axes and make best merger plan.
 *
 *  @param numAxes  number of axes in histogram, 1 to 3
 *  @param srcAxes  source histogram axes, number can be higher than numAxes
 *  @param dstAxes  destination histogram axes, number can be higher than numAxes
 *  @param labelMap pointer to label map, only used for dynaically labelled
 *                  histograms, can be nullptr for others.
 */
BinMergeDescriptor makeBinDataMerge(unsigned numAxes,
                                    const std::vector<oh::Axis>& srcAxes,
                                    const std::vector<oh::Axis>& dstAxes,
                                    LabelMap* labelMap=nullptr);


// detail namespace is only exposed in the header file for testing purposes
// and should not be used by regular clients.
namespace detail {

/**
 *  Constant defining tolerance when comparing bin edges (or bin widths).
 *  This should be understood as relave to the (single) bin width.
 *
 *  Note: this is an implementation detail and it is exposed in a header file
 *  only for the purpose of unit testing.
 */
const double binEdgeTolerance = 1e-6;

/**
 *  Check whether two axes have identical binning, ignoring labels.
 *
 *  In this method binning is identical if axes have the same kind (both are
 *  Fixed or both are Variable), same number of bins, and bin edges are
 *  matching (within some numerical precision).
 *
 *  Note: this is an implementation detail and it is exposed in a header file
 *  only for the purpose of unit testing.
 *
 *  @param axis1  first axis
 *  @param axis2  second axis
 *  @throw IncompatibleAxisBinning if binning is not the same
 */
void sameBinning(const oh::Axis& axis1, const oh::Axis& axis2);

// Labeling type used for an axis
enum class LabelType {NoLabels, StaticLabels, DynamicLabels};

/**
 *  Guess labelling type used for the axes.
 *
 *  @throw IncompatibleAxisBinning if binning/labelling is incompatible.
 */
LabelType labelType(const oh::Axis& axis1, const oh::Axis& axis2);

/**
 *  Helper class that keeps details about two axes that are needed to build
 *  BinMerge instances for one dimension. It is used to implement
 *  makeBinDataMerge().
 */
class MergeAxis {
public:
    MergeAxis() {}

    // Construct from two axis
    MergeAxis(const oh::Axis& axis1, const oh::Axis& axis2);

    /**
     *  Check whether merged axis is identical to axis1.
     */
    bool sameAs1() const {
        return mergeFactor1 == 1 && firstBin1 == 1 && lastBin1 == numBins + 1;
    }

    /**
     *  Check whether merged axis is identical to axis2.
     */
    bool sameAs2() const {
        return mergeFactor2 == 1 && firstBin2 == 1 && lastBin2 == numBins + 1;
    }

    oh::Axis::Kind kind = oh::Axis::NoAxis;
    int numBins = -1;       // number of bins in merged axis (without over/underflow)
    double binWidth = 0.;   // bin width of merged axis
    double lowerBound = 0.; // lower bound of merged axis
    double upperBound = 0.; // upper bound of merged axis
    int mergeFactor1 = 1;   // meging factor for axis1
    int firstBin1 = 0;      // bin of the merged axis into which first bin of axis1 maps
    int lastBin1 = 1;       // bin of the merged axis into which last bin of axis1 maps
    int mergeFactor2 = 1;   // meging factor for axis2
    int firstBin2 = 0;      // bin of the merged axis into which first bin of axis2 maps
    int lastBin2 = 1;       // bin of the merged axis into which last bin of axis2 maps
};

/**
 *  Helper class that works with dynamically labelled axes.
 *  It checks axes from two histograms and decides how they can be merged.
 */
class DynamicLabelledAxis {
public:

    DynamicLabelledAxis();

    /**
     *  Construct from two axes.
     *
     *  labelMap must correspond to current contents of dstAxis.
     */
    DynamicLabelledAxis(const oh::Axis& srcAxis, const oh::Axis& dstAxis, AxisLabelMap& labelMap);

    oh::Axis axis;                       // new axis, made from dstAxis plus necessary additions from srcAxis
    bool resize = false;                 // True if new axis is not the same as dstAxis
    std::vector<unsigned> srcIndices;    // maps source bins to destination bins, this only includes bins with
                                         //   labels and underflow bin=0
    std::vector<unsigned> dstIndices;    // maps destination bins to (new) destination bins, this only includes
                                         //   bins with labels and underflow bin=0
};

} // namespace detail

}} // namespace MIG::AxisTools

#endif // MONINFOGATHERER_ALGOS_AXISTOOLS_H
