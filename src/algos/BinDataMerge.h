// -*- c++ -*-
#ifndef MONINFOGATHERER_ALGOS_BINDATAMERGE_H
#define MONINFOGATHERER_ALGOS_BINDATAMERGE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Abstract interface for bin data merge operations.
 *
 *  Tehre are multiple implementation of this interface supporting various
 *  type of merge - simple add, bin remapping, many-to-one merge, etc.
 */
class BinDataMerge {
public:

    virtual ~BinDataMerge() = default;

    /**
     *  Methods to merge data from source array into destination array.
     *
     * There is one overload for each supported data type.
     */
    virtual void merge(const char* src, char* dst) const = 0;
    virtual void merge(const short* src, short* dst) const = 0;
    virtual void merge(const int* src, int* dst) const = 0;
    virtual void merge(const float* src, float* dst) const = 0;
    virtual void merge(const double* src, double* dst) const = 0;

    /**
     *  Methods to merge data from source array into destination array,
     *  these methods do weighted summing.
     *
     * There is one overload for each supported data type.
     */
    virtual void merge(const char* src, char* dst, double wSrc, double wDst) const = 0;
    virtual void merge(const short* src, short* dst, double wSrc, double wDst) const = 0;
    virtual void merge(const int* src, int* dst, double wSrc, double wDst) const = 0;
    virtual void merge(const float* src, float* dst, double wSrc, double wDst) const = 0;
    virtual void merge(const double* src, double* dst, double wSrc, double wDst) const = 0;

    /**
     * Check that empty bins have no contents.
     *
     * This mnethod is intended for dynamically labelled axes, which may have
     * some non-labelled bins that are supposed to be empty. When merging
     * such axes we sometimes drop non-labelled bins but we want to check that
     * they are indeed empty.
     *
     * There is one overload for each supported data type.
     *
     * @returns true if empty bins have no contents
     */
    virtual bool checkEmptyBins(const char* src) const = 0;
    virtual bool checkEmptyBins(const short* src) const = 0;
    virtual bool checkEmptyBins(const int* src) const = 0;
    virtual bool checkEmptyBins(const float* src) const = 0;
    virtual bool checkEmptyBins(const double* src) const = 0;

    /**
     *  Return merge object used for inner dimension
     *
     *  This is only used for testing, normal code should not need it.
     */
    virtual BinDataMerge* subMerge() const = 0;
};

/**
 *  Implementation of bin merging with direct bin mapping.
 *
 *  This should be used for X axis merging.
 *
 *  Note that this algorithm merges overflow/undeflow bins.
 */
class BinDataMergeDirectX : public BinDataMerge {
public:

    BinDataMergeDirectX(unsigned nbinsSrc) : m_nbinsSrc(nbinsSrc) {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return nullptr; }

private:
    unsigned m_nbinsSrc;        // number of bins in src in this dimension (including under/overflow)
};

/**
 *  Implementation of bin merging with direct bin mapping.
 *
 *  This should be used for Y or Z axis merging, and it takes
 *  pointer to a merger instance for nested level.
 *
 *  Note that this algorithm merges overflow/undeflow bins.
 */
class BinDataMergeDirect : public BinDataMerge {
public:

    BinDataMergeDirect(unsigned nbinsSrc, unsigned strideSrc, unsigned strideDst,
                       std::unique_ptr<BinDataMerge> subMerge)
      : m_nbinsSrc(nbinsSrc), m_strideSrc(strideSrc), m_strideDst(strideDst),
        m_subMerge(std::move(subMerge))
    {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return m_subMerge.get(); }

private:
    unsigned m_nbinsSrc;    // number of bins in src in this dimension (including under/overflow)
    unsigned m_strideSrc;   // stride (number of bins) for nested level
    unsigned m_strideDst;   // stride (number of bins) for nested level
    std::unique_ptr<BinDataMerge> m_subMerge;   // merge object for nested sub-array
};

/**
 *  Implementation of bin merging with many-to-one.
 *
 *  This should be used for X axis merging.
 *
 *  Note that this algorithm skips overflow/undeflow bins.
 */
class BinDataMergeManyX : public BinDataMerge {
public:

    BinDataMergeManyX(unsigned nbinsSrc, unsigned offsetDst, unsigned mergeFactor)
        : m_nbinsSrc(nbinsSrc), m_offsetDst(offsetDst), m_mergeFactor(mergeFactor)
    {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return nullptr; }

private:
    unsigned m_nbinsSrc;    // number of bins in src in this dimension (including under/overflow)
    unsigned m_offsetDst;   // bin number in dst for first (exclude undeflow) bin in src
    unsigned m_mergeFactor; // number of src bins merged into one dst bin
};

/**
 *  Implementation of bin merging with many-to-one.
 *
 *  This should be used for Y and Z axis merging, and it takes
 *  pointer to a merger instance for nested level.
 *
 *  Note that this algorithm skips overflow/undeflow bins.
 */
class BinDataMergeMany : public BinDataMerge {
public:

    BinDataMergeMany(unsigned nbinsSrc, unsigned offsetDst, unsigned mergeFactor,
                     unsigned strideSrc, unsigned strideDst, std::unique_ptr<BinDataMerge> subMerge)
        : m_nbinsSrc(nbinsSrc), m_offsetDst(offsetDst), m_mergeFactor(mergeFactor),
          m_strideSrc(strideSrc), m_strideDst(strideDst), m_subMerge(std::move(subMerge))
    {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return m_subMerge.get(); }

private:
    unsigned m_nbinsSrc;    // number of bins in src in this dimension (including under/overflow)
    unsigned m_offsetDst;   // bin number in dst for first (exclude undeflow) bin in src
    unsigned m_mergeFactor; // number of src bins merged into one dst bin
    unsigned m_strideSrc;   // stride (number of bins) for nested level
    unsigned m_strideDst;   // stride (number of bins) for nested level
    std::unique_ptr<BinDataMerge> m_subMerge;   // merge object for nested sub-array
};

/**
 *  Implementation of bin merging with arbitrary bin mapping.
 *
 *  This should be used for X axis merging.
 *
 *  Note that this algorithm skips overflow/undeflow bins.
 */
class BinDataMergeMapX : public BinDataMerge {
public:

    /**
     *  binMap maps src bins to dst bins, must have sensible numbers
     *  for every i in range [1, nbinsSrc-2] (inclusive), index 0 and
     *  nbinsSrc-1 correspond to under/overflov bins and are not used.
     */
    BinDataMergeMapX(unsigned nbinsSrc, std::vector<unsigned>&& binMap)
        : m_nbinsSrc(nbinsSrc), m_binMap(binMap)
    {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return nullptr; }

private:
    unsigned m_nbinsSrc;            // number of bins in src in this dimension (including under/overflow)
    std::vector<unsigned> m_binMap; // maps src bins to dst bins
};

/**
 *  Implementation of bin merging with arbitrary bin mapping.
 *
 *  This should be used for Y and Z axis merging, and it takes
 *  pointer to a merger instance for nested level.
 *
 *  Note that this algorithm skips overflow/undeflow bins.
 */
class BinDataMergeMap : public BinDataMerge {
public:

    /**
     *  binMap maps src bins to dst bins, must have sensible numbers
     *  for every i in range [1, nbinsSrc-2] (inclusive), index 0 and
     *  nbinsSrc-1 correspond to under/overflov bins and are not used.
     */
    BinDataMergeMap(unsigned nbinsSrc, std::vector<unsigned>&& binMap,
                    unsigned strideSrc, unsigned strideDst, std::unique_ptr<BinDataMerge> subMerge)
        : m_nbinsSrc(nbinsSrc), m_binMap(binMap),
          m_strideSrc(strideSrc), m_strideDst(strideDst), m_subMerge(std::move(subMerge))
    {}

    void merge(const char* src, char* dst) const override;
    void merge(const short* src, short* dst) const override;
    void merge(const int* src, int* dst) const override;
    void merge(const float* src, float* dst) const override;
    void merge(const double* src, double* dst) const override;
    void merge(const char* src, char* dst, double wSrc, double wDst) const override;
    void merge(const short* src, short* dst, double wSrc, double wDst) const override;
    void merge(const int* src, int* dst, double wSrc, double wDst) const override;
    void merge(const float* src, float* dst, double wSrc, double wDst) const override;
    void merge(const double* src, double* dst, double wSrc, double wDst) const override;
    bool checkEmptyBins(const char* src) const override;
    bool checkEmptyBins(const short* src) const override;
    bool checkEmptyBins(const int* src) const override;
    bool checkEmptyBins(const float* src) const override;
    bool checkEmptyBins(const double* src) const override;

    BinDataMerge* subMerge() const override { return m_subMerge.get(); }

private:
    unsigned m_nbinsSrc;            // number of bins in src in this dimension (including under/overflow)
    std::vector<unsigned> m_binMap; // maps src bins to dst bins, the map does not have to cover all
                                    // source bins, m_binMap.size() can be less than m_nbinsSrc
    unsigned m_strideSrc;   // stride (number of bins) for nested level
    unsigned m_strideDst;   // stride (number of bins) for nested level
    std::unique_ptr<BinDataMerge> m_subMerge;   // merge object for nested sub-array
};

} // namespace MIG

#endif // MONINFOGATHERER_ALGOS_BINDATAMERGE_H
