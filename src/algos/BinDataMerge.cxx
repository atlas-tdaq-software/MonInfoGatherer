//-----------------------
// This Class's Header --
//-----------------------
#include "BinDataMerge.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

using namespace MIG;

namespace {

template <typename T>
void dataMergeDirect(const T* src, T* dst, unsigned nbinsSrc) {
    // we just sum overflow/underflow here, no worries
    for (unsigned i = 0; i < nbinsSrc; ++i) {
        *dst++ += *src++;
    }
}

template <typename T>
void dataMergeDirect(const T* src, T* dst, unsigned nbinsSrc, const unsigned strideSrc,
                     const unsigned strideDst, const BinDataMerge& subMerge) {
    // we just sum overflow/underflow here, no worries
    for (unsigned i = 0; i < nbinsSrc; ++i) {
        subMerge.merge(src, dst);
        src += strideSrc;
        dst += strideDst;
    }
}

template <typename T>
void dataMergeMany(const T* src, T* dst, const unsigned nbinsSrc, const unsigned offsetDst, const unsigned mergeFactor) {
    // exclude under/overflow bins in src
    ++src;
    dst += offsetDst;
    unsigned const lastBinSrc = nbinsSrc - 1;
    for (unsigned i = 1; i < lastBinSrc; i += mergeFactor, ++dst) {
        unsigned const w = std::min(mergeFactor, lastBinSrc - i);
        for (unsigned j = 0; j < w; ++j) {
            *dst += *src++;
        }
    }
}

template <typename T>
void dataMergeMany(const T* src, T* dst, const unsigned nbinsSrc, const unsigned offsetDst, const unsigned mergeFactor,
                   const unsigned strideSrc, const unsigned strideDst, const BinDataMerge& subMerge) {
    // exclude under/overflow bins in src
    src += strideSrc;
    dst += offsetDst*strideDst;
    unsigned const lastBinSrc = nbinsSrc - 1;
    for (unsigned i = 1; i < lastBinSrc; i += mergeFactor, dst += strideDst) {
        unsigned const w = std::min(mergeFactor, lastBinSrc - i);
        for (unsigned j = 0; j < w; ++j) {
            subMerge.merge(src, dst);
            src += strideSrc;
        }
    }
}

template <typename T>
void dataMergeMap(const T* src, T* dst, const unsigned nbinsSrc, const std::vector<unsigned>& binMap) {
    // exclude under/overflow bins in src
    unsigned nbins = std::min(nbinsSrc-1, unsigned(binMap.size()));
    for (unsigned i = 1; i < nbins; ++i) {
        dst[binMap[i]] += src[i];
    }
}

template <typename T>
void dataMergeMap(const T* src, T* dst, const unsigned nbinsSrc, const std::vector<unsigned>& binMap,
                  const unsigned strideSrc, const unsigned strideDst, const BinDataMerge& subMerge) {
    // exclude under/overflow bins in src
    unsigned nbins = std::min(nbinsSrc-1, unsigned(binMap.size()));
    src += strideSrc;
    for (unsigned i = 1; i < nbins; ++i) {
        auto j = binMap[i];
        subMerge.merge(src, dst+j*strideDst);
        src += strideSrc;
    }
}

template <typename T>
void dataMergeDirectNorm(const T* src, T* dst, double wSrc, double wDst, unsigned nbinsSrc) {
    for (unsigned i = 0; i < nbinsSrc; ++i, ++dst, ++src) {
        double tmp = *dst * wDst;
        tmp += *src * wSrc;
        *dst = tmp;
    }
}

template <typename T>
void dataMergeDirectNorm(const T* src, T* dst, double wSrc, double wDst, unsigned nbinsSrc,
                         const unsigned strideSrc, const unsigned strideDst, const BinDataMerge& subMerge) {
    // we just sum overflow/underflow here, no worries
    for (unsigned i = 0; i < nbinsSrc; ++i) {
        subMerge.merge(src, dst, wSrc, wDst);
        src += strideSrc;
        dst += strideDst;
    }
}

template <typename T>
void dataMergeManyNorm(const T* src, T* dst, double wSrc, double wDst, const unsigned nbinsSrc,
                       const unsigned offsetDst, const unsigned mergeFactor) {
    // exclude under/overflow bins in src
    ++src;
    dst += offsetDst;
    unsigned const lastBinSrc = nbinsSrc - 1;
    for (unsigned i = 1; i < lastBinSrc; i += mergeFactor, ++dst) {
        double tmp = *dst * wDst;
        unsigned const w = std::min(mergeFactor, lastBinSrc - i);
        for (unsigned j = 0; j < w; ++j, ++src) {
            tmp += *src * wSrc;
        }
        *dst = tmp;
    }
}

template <typename T>
void dataMergeManyNorm(const T* src, T* dst, double wSrc, double wDst, const unsigned nbinsSrc,
                       const unsigned offsetDst, const unsigned mergeFactor,
                       const unsigned strideSrc, const unsigned strideDst, const BinDataMerge& subMerge) {
    // exclude under/overflow bins in src
    src += strideSrc;
    dst += offsetDst*strideDst;
    unsigned const lastBinSrc = nbinsSrc - 1;
    for (unsigned i = 1; i < lastBinSrc; i += mergeFactor, dst += strideDst) {
        unsigned const w = std::min(mergeFactor, lastBinSrc - i);
        for (unsigned j = 0; j < w; ++j) {
            subMerge.merge(src, dst, wSrc, wDst);
            src += strideSrc;
        }
    }
}

template <typename T>
void dataMergeMapNorm(const T* src, T* dst, double wSrc, double wDst,
                      const unsigned nbinsSrc, const std::vector<unsigned>& binMap) {
    // exclude under/overflow bins in src
    unsigned nbins = std::min(nbinsSrc-1, unsigned(binMap.size()));
    for (unsigned i = 1; i < nbins; ++i) {
        auto j = binMap[i];
        double tmp = dst[j] * wDst;
        tmp += src[i] * wSrc;
        dst[j] = tmp;
    }
}

template <typename T>
void dataMergeMapNorm(const T* src, T* dst, double wSrc, double wDst,
                      const unsigned nbinsSrc, const std::vector<unsigned>& binMap,
                      const unsigned strideSrc, const unsigned strideDst, const BinDataMerge& subMerge) {
    // exclude under/overflow bins in src
    unsigned nbins = std::min(nbinsSrc-1, unsigned(binMap.size()));
    for (unsigned i = 1; i < nbins; ++i) {
        auto j = binMap[i];
        subMerge.merge(src+i*strideSrc, dst+j*strideDst, wSrc, wDst);
    }
}

template <typename T>
bool checkEmptyBinsDirect(const T* src, const unsigned nbinsSrc, const unsigned strideSrc, const BinDataMerge& subMerge) {
    // ignore over/underflow
    for (unsigned i = 1; i < nbinsSrc-1; ++i) {
        if (not subMerge.checkEmptyBins(src + i*strideSrc)) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool checkEmptyBinsMap(const T* src, const unsigned nbinsSrc, const std::vector<unsigned>& binMap) {
    // Check all bins after the dynamically labelled ones. binMap can be empty if there
    // are no labels at all. Do not include overflow/underflow bins in test (but they
    // should be empty too).
    unsigned firstBin = binMap.empty() ? 1 : binMap.size();
    for (unsigned i = firstBin; i < nbinsSrc-1; ++i) {
        if (src[i] != 0) return false;
    }
    return true;
}

template <typename T>
bool checkEmptyBinsMap(const T* src, const unsigned nbinsSrc, const std::vector<unsigned>& binMap,
                       const unsigned strideSrc, const BinDataMerge& subMerge) {
    unsigned firstBin = binMap.empty() ? 1 : binMap.size();
    for (unsigned i = 1; i < firstBin; ++i) {
        // the data is allowed here but check sub-dimension too
        auto subsrc = src + i * strideSrc;
        if (not subMerge.checkEmptyBins(subsrc)) {
            return false;
        }
    }
    for (unsigned i = firstBin; i < nbinsSrc-1; ++i) {
        // Sub-dimensions can be non-labelled, for them checkEmptyBinsMap
        // does not work. So we just check whole sub-plane, this will include
        // under/overflows but we do not care.
        auto subsrc = src + i * strideSrc;
        for (unsigned j = 0; j < strideSrc; ++ j) {
            if (subsrc[j] != 0) return false;
        }
    }
    return true;
}

}

#define MERGE_IMPL_1(CLASS, TYPE, CODE) \
void CLASS::merge(const TYPE* src, TYPE* dst) const { CODE; }

#define MERGE_IMPL(CLASS, CODE) \
MERGE_IMPL_1(CLASS, char, CODE) \
MERGE_IMPL_1(CLASS, short, CODE) \
MERGE_IMPL_1(CLASS, int, CODE) \
MERGE_IMPL_1(CLASS, float, CODE) \
MERGE_IMPL_1(CLASS, double, CODE) \

MERGE_IMPL(BinDataMergeDirectX, ::dataMergeDirect(src, dst, m_nbinsSrc))

MERGE_IMPL(BinDataMergeDirect, ::dataMergeDirect(src, dst, m_nbinsSrc, m_strideSrc, m_strideDst, *m_subMerge))

MERGE_IMPL(BinDataMergeManyX, ::dataMergeMany(src, dst, m_nbinsSrc, m_offsetDst, m_mergeFactor))

MERGE_IMPL(BinDataMergeMany, ::dataMergeMany(src, dst, m_nbinsSrc, m_offsetDst, m_mergeFactor, m_strideSrc, m_strideDst, *m_subMerge))

MERGE_IMPL(BinDataMergeMapX, ::dataMergeMap(src, dst, m_nbinsSrc, m_binMap))

MERGE_IMPL(BinDataMergeMap, ::dataMergeMap(src, dst, m_nbinsSrc, m_binMap, m_strideSrc, m_strideDst, *m_subMerge))

#define MERGE_IMPL_2(CLASS, TYPE, CODE) \
void CLASS::merge(const TYPE* src, TYPE* dst, double wSrc, double wDst) const { CODE; }

#define MERGE_IMPL_NORM(CLASS, CODE) \
MERGE_IMPL_2(CLASS, char, CODE) \
MERGE_IMPL_2(CLASS, short, CODE) \
MERGE_IMPL_2(CLASS, int, CODE) \
MERGE_IMPL_2(CLASS, float, CODE) \
MERGE_IMPL_2(CLASS, double, CODE) \

MERGE_IMPL_NORM(BinDataMergeDirectX, ::dataMergeDirectNorm(src, dst, wSrc, wDst, m_nbinsSrc))

MERGE_IMPL_NORM(BinDataMergeDirect, ::dataMergeDirectNorm(src, dst, wSrc, wDst, m_nbinsSrc, m_strideSrc, m_strideDst, *m_subMerge))

MERGE_IMPL_NORM(BinDataMergeManyX, ::dataMergeManyNorm(src, dst, wSrc, wDst, m_nbinsSrc, m_offsetDst, m_mergeFactor))

MERGE_IMPL_NORM(BinDataMergeMany, ::dataMergeManyNorm(src, dst, wSrc, wDst, m_nbinsSrc, m_offsetDst, m_mergeFactor, m_strideSrc, m_strideDst, *m_subMerge))

MERGE_IMPL_NORM(BinDataMergeMapX, ::dataMergeMapNorm(src, dst, wSrc, wDst, m_nbinsSrc, m_binMap))

MERGE_IMPL_NORM(BinDataMergeMap, ::dataMergeMapNorm(src, dst, wSrc, wDst, m_nbinsSrc, m_binMap, m_strideSrc, m_strideDst, *m_subMerge))

#define CHECKEMPTYBINS_IMPL_1(CLASS, TYPE, CODE) \
bool CLASS::checkEmptyBins(const TYPE* src) const { CODE; }

#define CHECKEMPTYBINS_IMPL(CLASS, CODE) \
CHECKEMPTYBINS_IMPL_1(CLASS, char, CODE) \
CHECKEMPTYBINS_IMPL_1(CLASS, short, CODE) \
CHECKEMPTYBINS_IMPL_1(CLASS, int, CODE) \
CHECKEMPTYBINS_IMPL_1(CLASS, float, CODE) \
CHECKEMPTYBINS_IMPL_1(CLASS, double, CODE) \

CHECKEMPTYBINS_IMPL(BinDataMergeDirectX, return true)

CHECKEMPTYBINS_IMPL(BinDataMergeDirect, return ::checkEmptyBinsDirect(src, m_nbinsSrc, m_strideSrc, *m_subMerge))

CHECKEMPTYBINS_IMPL(BinDataMergeManyX, return true)

CHECKEMPTYBINS_IMPL(BinDataMergeMany, return ::checkEmptyBinsDirect(src, m_nbinsSrc, m_strideSrc, *m_subMerge))

CHECKEMPTYBINS_IMPL(BinDataMergeMapX, return ::checkEmptyBinsMap(src, m_nbinsSrc, m_binMap))

CHECKEMPTYBINS_IMPL(BinDataMergeMap, return ::checkEmptyBinsMap(src, m_nbinsSrc, m_binMap, m_strideSrc, *m_subMerge))
