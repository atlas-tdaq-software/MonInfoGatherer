// -*- c++ -*-
#ifndef MONINFOGATHERER_ALGOS_ISSUM_H
#define MONINFOGATHERER_ALGOS_ISSUM_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoDynAny;

namespace MIG {

/**
 *  Algorithm that sums numeric attributes of IS objects.
 *
 *  This algorithm is for non-histogram IS types, it does regular
 *  summing for numeric attributes and it can also concatenate
 *  array attributes if configured to do so.
 */
class ISSum {
public:

    /**
     *  Algorithm constructor.
     *
     *  @param algoConfig Algorithm configuration parameters.
     */
    ISSum(const ConfigParameters& cfg = ConfigParameters());

    void operate(ISInfoDynAny &src, ISInfoDynAny &dst) const;

private:

    bool _concatenate;
};
}
#endif // MONINFOGATHERER_ALGOS_ISSUM_H
