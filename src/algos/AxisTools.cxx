//-----------------------
// This Class's Header --
//-----------------------
#include "AxisTools.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <cmath>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"

using namespace MIG;

namespace {

bool sameEdgesFixed(const oh::Axis& axis1, const oh::Axis& axis2) {
    // Fixed axis, precision with which we compare edges is equal to 1e-6 of the bin width
    const double prec = axis1.range[1] * AxisTools::detail::binEdgeTolerance;
    const unsigned nEdges = axis1.range.size();
    for (unsigned i = 0; i < nEdges; ++ i) {
        if (abs(axis1.range[i] - axis2.range[i]) > prec) {
            return false;
        }
    }
    return true;
}

bool sameEdgesVariable(const oh::Axis& axis1, const oh::Axis& axis2) {
    // Variable binning.
    // Protect against degenerate case.
    if (axis1.nbins > 0) {

        // Unroll the loop for i = 0.
        // Precision with which we compare edges, it is equal to 1e-6 of
        // the adjacent bin width (bin above for i=0, bin below for all other edges)
        double binWidth = axis1.range[1] - axis1.range[0];
        double prec = binWidth * AxisTools::detail::binEdgeTolerance;
        if (abs(axis1.range[0] - axis2.range[0]) > prec) {
            return false;
        }

        const unsigned nEdges = axis1.range.size();
        for (unsigned i = 1; i < nEdges; ++ i) {
            double binWidth = axis1.range[i] - axis1.range[i-1];
            double prec = binWidth * AxisTools::detail::binEdgeTolerance;
            if (abs(axis1.range[i] - axis2.range[i]) > prec) {
                return false;
            }
        }
    }
    return true;
}

/**
 *  Compare bin edges, axes have to be of the same kind and have the same number of bins.
 */
bool sameEdges(const oh::Axis& axis1, const oh::Axis& axis2) {
    // compare bin edges, for optimization just try to do exact comparison first
    if (axis1.range == axis2.range) {
        return true;
    }
    // otherwise do comparison with tolerances
    if (axis1.kind == oh::Axis::Fixed) {
        return ::sameEdgesFixed(axis1, axis2);
    } else {
        return ::sameEdgesVariable(axis1, axis2);
    }
}

// Return count of labels for an axis
unsigned labelCount(const oh::Axis& axis) {
    // be paranoid and check for aligned arrays
    return std::min(axis.labels.size(), axis.indices.size());
}

// Return true if axis is labelled, i.e. at least one bin has a label.
bool isLabelled(const oh::Axis& axis) {
    return labelCount(axis) > 0;
}

// Return true if axis is dynamically labelled, i.e. there is a bunch of
// labelled bins followed by zero or more non0-lebelled bins.
bool isDynamicallyLabelled(const oh::Axis& axis)
{
    auto& indices = axis.indices;
    if (indices.empty()) {
        return false;
    }
    // To test is that all of the first N bins have labels just find highest
    // bin index, it should be equal to number of labels (indices start with 1,
    // 0th bin is underflow).
    auto max_it = std::max_element(indices.begin(), indices.end());
    return unsigned(*max_it) == indices.size();
}

} // namespace


// Check whether two axes have identical binning, ignoring labels.
void AxisTools::detail::sameBinning(const oh::Axis& axis1, const oh::Axis& axis2)
{
    if (axis1.kind != axis2.kind) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "different axes type");
    }
    if (axis1.nbins != axis2.nbins) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "different number of bins");
    }

    // then compare also by allowing some imprecision
    if (!::sameEdges(axis1, axis2)) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "different bin edges");
    }
}

AxisTools::detail::MergeAxis::MergeAxis(const oh::Axis& axis1, const oh::Axis& axis2)
{
    if (axis1.kind != axis2.kind) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "different axes type");
    }
    this->kind = axis1.kind;

    if (axis1.kind == oh::Axis::Variable) {
        // For variable binning we can only do 1-to-1 "merge", so we just need
        // same binning.
        sameBinning(axis1, axis2);
        this->numBins = axis1.nbins;
        this->binWidth = 0.;
        this->lowerBound = axis1.range.front();
        this->upperBound = axis1.range.back();
        // +1 here is to account for underflow bin
        this->firstBin1 = 1;
        this->firstBin2 = 1;
        this->lastBin1 = axis1.nbins+1;
        this->lastBin2 = axis2.nbins+1;
        return;
    }

    const double lowEdge1 = axis1.range[0];
    const double binWidth1 = axis1.range[1];
    const double lowEdge2 = axis2.range[0];
    const double binWidth2 = axis2.range[1];
    this->binWidth = std::max(binWidth1, binWidth2);
    double binRatio = binWidth1 > binWidth2 ? binWidth1/binWidth2 : binWidth2/binWidth1;
    int ibinRatio = std::lround(binRatio);
    if (std::abs(binRatio - ibinRatio) > AxisTools::detail::binEdgeTolerance) {
        // non-integer multiples
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "Axes have non-integer-multiple binning");
    }
    this->mergeFactor1 = std::lround(this->binWidth / binWidth1);
    this->mergeFactor2 = std::lround(this->binWidth / binWidth2);

    // check if axes are shifted, find common lower bound
    double shift = std::abs(lowEdge1 - lowEdge2); //shift between start of each histogram
    if (shift/binWidth < AxisTools::detail::binEdgeTolerance) {
        // lower bounds are the same
        this->lowerBound = lowEdge1;
    } else {
        // there is a shift in lower bounds
        this->lowerBound = std::min(lowEdge1, lowEdge2);
        double nbinshift = shift / binWidth;
        int ibinshift = std::lround(nbinshift);
        if (std::abs(nbinshift - ibinshift) > AxisTools::detail::binEdgeTolerance) {
            // bin shift is not integer multiple of binwidth
            throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "Shifts in axes have non-integer-multiple of bin widths");
        }
    }

    // find upper bound
    double upperBound1 = lowEdge1 + axis1.nbins * binWidth1;
    double upperBound2 = lowEdge2 + axis2.nbins * binWidth2;
    this->upperBound = std::max(upperBound1, upperBound2);
    this->numBins = std::lround((this->upperBound - this->lowerBound) / this->binWidth);

    // Check that upper bound aligns with new bin width.
    // Potentially we could extend upper bound but that brings a question of
    // how to handle overflow bins in that case.
    if (std::abs((this->upperBound - this->lowerBound) / this->binWidth - this->numBins) > AxisTools::detail::binEdgeTolerance) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "New axis extent is not an integer-multiple of bin widths");
    }

    // 0 bin is underflow, so we need +1 offset
    this->firstBin1 = std::lround((lowEdge1 - this->lowerBound) / this->binWidth) + 1;
    this->firstBin2 = std::lround((lowEdge2 - this->lowerBound) / this->binWidth) + 1;
    this->lastBin1 = std::lround((upperBound1 - this->lowerBound) / this->binWidth) + 1;
    this->lastBin2 = std::lround((upperBound2 - this->lowerBound) / this->binWidth) + 1;
}

//  Guess labelling type used for the axes.
AxisTools::detail::LabelType
AxisTools::detail::labelType(const oh::Axis& axis1, const oh::Axis& axis2)
{
    // Axes have to be of the same type
    if (axis1.kind != axis2.kind) {
        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "different axes type");
    }

    unsigned const nLabels1 = ::labelCount(axis1);
    unsigned const nLabels2 = ::labelCount(axis2);
    if (nLabels1 > 0 or nLabels2 > 0) {

        // Fixed labels have exactly the same binning and exactly the same set
        // of labelled bins (not all bins have to be labelled). This also
        // matches the case of dynamic labels with the same labels, but for that
        // case it is OK to handle dynamic labels as static.
        if (nLabels1 == nLabels2 and ::sameEdges(axis1, axis2)) {

            // check labelling, potentially order of labels can be different so
            // sort them first.
            using BinLabel = std::pair<unsigned, std::string>;
            std::vector<BinLabel> labels1, labels2;
            for (unsigned i = 0; i != nLabels1; ++ i) {
                labels1.emplace_back(axis1.indices[i], axis1.labels[i]);
            }
            for (unsigned i = 0; i != nLabels2; ++ i) {
                labels2.emplace_back(axis2.indices[i], axis2.labels[i]);
            }
            std::sort(labels1.begin(), labels1.end());
            std::sort(labels2.begin(), labels2.end());
            if (labels1 == labels2) {
                // they are indeed the same
                return LabelType::StaticLabels;
            }

        }

        // Dynamic labels exist for few initial bins followed optionally by
        // few non-labelled bins. It can also be the case that one of the two
        // histograms was not filled yet and has zero labelled bins (but we
        // know here that at least one of the two has labels).
        bool isDynamic1 = nLabels1 == 0 or ::isDynamicallyLabelled(axis1);
        bool isDynamic2 = nLabels2 == 0 or ::isDynamicallyLabelled(axis2);
        if (isDynamic1 and isDynamic2) {
            return LabelType::DynamicLabels;
        }

        throw IncompatibleAxisBinning(ERS_HERE, axis1.title, "incompatible labelling of axes");

    }

    return LabelType::NoLabels;
}

AxisTools::detail::DynamicLabelledAxis::DynamicLabelledAxis()
{
    axis.kind = oh::Axis::NoAxis;
    axis.nbins = -1;
}

AxisTools::detail::DynamicLabelledAxis::DynamicLabelledAxis(
        const oh::Axis& srcAxis, const oh::Axis& dstAxis, AxisLabelMap& labelMap)
{
    if (srcAxis.kind != dstAxis.kind) {
        throw IncompatibleAxisBinning(ERS_HERE, srcAxis.title, "different axes type");
    }

    // Check that we get reasonable input - if axis has labels then
    // it must be dynamic, and at least one of them must have labels.
    if ((not ::isLabelled(srcAxis) and not ::isLabelled(dstAxis)) or
        (::isLabelled(srcAxis) and not ::isDynamicallyLabelled(srcAxis)) or
        (::isLabelled(dstAxis) and not ::isDynamicallyLabelled(dstAxis))) {
        throw IncompatibleAxisBinning(ERS_HERE, srcAxis.title, "not dynamically labelled axes");
    }

    // check for labels, be paranoid and check for aligned arrays
    unsigned const nSrcLabels = ::labelCount(srcAxis);
    unsigned const nDstLabels = ::labelCount(dstAxis);

    this->axis = dstAxis;

    // fill mapping for dst->dst bins
    for (unsigned t = 0; t < nDstLabels; t++) {
        unsigned const bin = dstAxis.indices[t];
        if (this->dstIndices.size() <= bin) {
            this->dstIndices.resize(bin+1);
        }
        this->dstIndices[bin] = bin;
    }

    for (unsigned lbl = 0; lbl < nSrcLabels; ++ lbl) {
        auto& label = srcAxis.labels[lbl];
        int idx = labelMap.index(label);
        if (idx < 0) {
            // new label, add it to dstAxis and labelMap
            this->axis.labels.push_back(label);
            idx = labelMap.addLabel(label);
            if (idx != int(this->axis.labels.size())) {
                throw IncompatibleAxisBinning(ERS_HERE, srcAxis.title, "label map inconsistency");
            }
            this->axis.indices.push_back(idx);

            // Resizing of the axis is the only way today to make sure that
            // bin labels are updated. Potentially we do not need resizing if
            // destination has enough empty bins, we could just add labels to
            // destiantion axis but this is a bit complicated so we do it
            // with resizing instead.
            this->resize = true;
            this->axis.nbins = this->axis.labels.size();

        }

        unsigned const bin = srcAxis.indices[lbl];
        if (this->srcIndices.size() <= bin) {
            this->srcIndices.resize(bin+1);
        }
        this->srcIndices[bin] = idx;
    }
}


// Compare source and destination axes and make best merger plan.
AxisTools::BinMergeDescriptor
AxisTools::makeBinDataMerge(unsigned numAxes,
                            const std::vector<oh::Axis>& srcAxes,
                            const std::vector<oh::Axis>& dstAxes,
                            LabelMap* labelMap)
{
    AxisTools::BinMergeDescriptor descr;

    // strides for current dimension, updated as we loop over axes
    unsigned srcStride = 1;
    unsigned dstStride = 1;
    unsigned newStride = 1;
    for (unsigned axis = 0; axis < numAxes; axis++) {
        const auto& srcAxis = srcAxes[axis];
        const auto& dstAxis = dstAxes[axis];
        if (srcAxis.kind != dstAxis.kind) {
            throw IncompatibleAxisBinning(ERS_HERE, dstAxis.title, "different axes kind");
        }

        // The algorithm for deciding how to merge current dimension:
        //  - if both axes have fixed labels (identical binning and at least
        //    few non-empty labels and labels are identical) then do
        //    one-to-one simple-add merge
        //  - otherwise, if axes are dynamically labelled then do merge with
        //    bin remapping (ignoring non-labelled bins, but we need to check
        //    that those non-labelled bins are empty)
        //  - otherwise, if both labels are non-labelled then we do regular
        //    merge (potentially many-to-one merge)
        //  - all other cases cannot be handled and cause exception


        // check how it's labelled
        detail::LabelType labelType = detail::LabelType::NoLabels;
        try {
            labelType = AxisTools::detail::labelType(srcAxis, dstAxis);
        } catch (const IncompatibleAxisBinning& exc) {
            throw IncompatibleAxisBinning(ERS_HERE, dstAxis.title, "incompatible binning", exc);
        }
        if (labelType == detail::LabelType::DynamicLabels) {
            // do labelled merge
            if (labelMap == nullptr) {
                // need label map
                throw MissingLabelMap(ERS_HERE);
            }
            detail::DynamicLabelledAxis dynAxis(srcAxis, dstAxis, labelMap->axisMap(axis));
            if (dynAxis.resize and not descr.resize) {
                descr.resize = true;
                descr.newDstAxes = dstAxes;
            }
            if (dynAxis.resize) {
                // will need to merge dst into new dst
                descr.newDstAxes[axis] = dynAxis.axis;
                if (dstStride == 1) {
                    descr.dstBinMerge = std::make_unique<BinDataMergeMapX>(dstAxis.nbins+2, std::move(dynAxis.dstIndices));
                } else {
                    if (descr.dstBinMerge == nullptr) {
                        // need a sub-merger
                        descr.dstBinMerge = std::make_unique<BinDataMergeDirectX>(dstStride);
                    }
                    descr.dstBinMerge = std::make_unique<BinDataMergeMap>(dstAxis.nbins+2, std::move(dynAxis.dstIndices),
                                                                          dstStride, newStride, std::move(descr.dstBinMerge));
                }
            }
            // merge src into new dst
            if (srcStride == 1) {
                descr.srcBinMerge = std::make_unique<BinDataMergeMapX>(srcAxis.nbins+2, std::move(dynAxis.srcIndices));
            } else {
                if (descr.srcBinMerge == nullptr) {
                    // need a sub-merger
                    descr.srcBinMerge = std::make_unique<BinDataMergeDirectX>(srcStride);
                }
                descr.srcBinMerge = std::make_unique<BinDataMergeMap>(srcAxis.nbins+2, std::move(dynAxis.srcIndices),
                                                                      srcStride, newStride, std::move(descr.srcBinMerge));
            }
        } else {
            // check that binning is exactly the same
            if (srcAxis.nbins == dstAxis.nbins and sameEdges(srcAxis, dstAxis)) {
                // bin-by-bin copy, but may be complicated if resize is needed for inner dimension
                if (descr.dstBinMerge != nullptr) {
                    descr.dstBinMerge = std::make_unique<BinDataMergeDirect>(dstAxis.nbins+2, dstStride, newStride,
                                                                             std::move(descr.dstBinMerge));
                }
                if (descr.srcBinMerge != nullptr) {
                    descr.srcBinMerge = std::make_unique<BinDataMergeDirect>(srcAxis.nbins+2, srcStride, newStride,
                                                                             std::move(descr.srcBinMerge));
                } else {
                    // this is handled below
                }
            } else {
                // potentially a multi-bin merge
                detail::MergeAxis mergeAxis(srcAxis, dstAxis);
                bool resize = !mergeAxis.sameAs2();
                if (resize and not descr.resize) {
                    descr.resize = true;
                    descr.newDstAxes = dstAxes;
                }
                if (descr.resize) {
                    // update axis binning
                    descr.newDstAxes[axis].nbins = mergeAxis.numBins;
                    descr.newDstAxes[axis].range = {mergeAxis.lowerBound, mergeAxis.binWidth};
                    if (dstStride == 1) {
                        descr.dstBinMerge = std::make_unique<BinDataMergeManyX>(dstAxis.nbins+2, mergeAxis.firstBin2, mergeAxis.mergeFactor2);
                    } else {
                        if (descr.dstBinMerge == nullptr) {
                            // need a sub-merger
                            descr.dstBinMerge = std::make_unique<BinDataMergeDirectX>(dstStride);
                        }
                        descr.dstBinMerge = std::make_unique<BinDataMergeMany>(dstAxis.nbins+2, mergeAxis.firstBin2, mergeAxis.mergeFactor2,
                                                                               dstStride, newStride, std::move(descr.dstBinMerge));
                    }
                }
                // merge src into new dst
                if (srcStride == 1) {
                    descr.srcBinMerge = std::make_unique<BinDataMergeManyX>(srcAxis.nbins+2, mergeAxis.firstBin1, mergeAxis.mergeFactor1);
                } else {
                    if (descr.srcBinMerge == nullptr) {
                        // need a sub-merger
                        descr.srcBinMerge = std::make_unique<BinDataMergeDirectX>(srcStride);
                    }
                    descr.srcBinMerge = std::make_unique<BinDataMergeMany>(srcAxis.nbins+2, mergeAxis.firstBin1, mergeAxis.mergeFactor1,
                                                                           srcStride, newStride, std::move(descr.srcBinMerge));
                }
            }
        }

        // strides for next dimension
        srcStride *= srcAxis.nbins + 2;
        dstStride *= dstAxis.nbins + 2;
        newStride *= descr.newDstAxes.empty() ? dstAxis.nbins + 2 : descr.newDstAxes[axis].nbins + 2;
    }
    if (descr.srcBinMerge == nullptr) {
        // means that we have 1-to-1 bin mapping
        descr.srcBinMerge = std::make_unique<BinDataMergeDirectX>(srcStride);
    }

    return descr;
}