//-----------------------
// This Class's Header --
//-----------------------
#include "ISSum.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infodynany.h"
#include "is/types.h"

using namespace MIG;

namespace {

template<typename T>
inline
void sumAttr(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr)
{
    dst.getAttributeValue<T>(attr) += src.getAttributeValue<T>(attr);
}

// specialization for types that don't support addition
template<> inline void sumAttr<OWLDate>(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr) {}
template<> inline void sumAttr<OWLTime>(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr) {}
template<> inline void sumAttr<std::string>(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr) {}
// ISInfo will map to InfoObject type
template<> inline void sumAttr<ISInfo>(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr) {}

template<typename T>
inline
void concatAttr(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr)
{
    std::vector<T>& out = dst.getAttributeValue<std::vector<T>>(attr);
    std::vector<T> const& in = src.getAttributeValue<std::vector<T>>(attr);
    out.reserve(out.size() + in.size());
    out.insert(out.end(), in.begin(), in.end());
}

// specialization for types that don't support concatenation
// ISInfo will map to InfoObject type
template<> inline void concatAttr<ISInfo>(ISInfoDynAny& src, ISInfoDynAny& dst, unsigned attr) {}

}

ISSum::ISSum(const ConfigParameters& cfg)
    : _concatenate(cfg.Concatenate())
{
}

void ISSum::operate(ISInfoDynAny& src, ISInfoDynAny& dst) const
{
    for (size_t attr = 0; attr < src.getAttributesNumber(); attr++) {
        if (!src.isAttributeArray(attr)) {

#define CASE_SUM(CPPTYPE,ISTYPE) case ISType::ISTYPE: ::sumAttr<CPPTYPE>(src, dst, attr); break;
#define CASE_SUM_OBJECT_TYPE ISInfo
#define CASE_SUM_SEPARATOR

            switch (src.getAttributeType(attr)) {
            IS_TYPES(CASE_SUM)

            case ISType::Error:
                break;
            }

        } else if (_concatenate) {

#define CASE_CONCAT(CPPTYPE,ISTYPE) case ISType::ISTYPE: ::concatAttr<CPPTYPE>(src, dst, attr); break;
#define CASE_CONCAT_OBJECT_TYPE ISInfo
#define CASE_CONCAT_SEPARATOR

            switch (src.getAttributeType(attr)) {
            IS_TYPES(CASE_CONCAT)

            case ISType::Error:
                break;
            }
        }
    }
}
