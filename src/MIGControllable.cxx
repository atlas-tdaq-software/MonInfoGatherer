//
// Implementation of MIGControllable class.
//

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/MIGControllable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dal/InfrastructureTemplateApplication.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "ConfigParser.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/InfoHandlerWrapper.h"
#include "MonInfoGatherer/LibraryManager.h"
#include "MonInfoGatherer/Timers.h"
#include "RunControl/Common/OnlineServices.h"
#include "ers/ers.h"
#include "./containers/Scheduler.h"


using namespace MIG;

MIGControllable::MIGControllable(const std::string& providerName, unsigned int sleepFor)
        : daq::rc::Controllable(),
          m_sleep(sleepFor),
          m_providerName(providerName),
          m_isr()
{
}

MIGControllable::~MIGControllable() noexcept(true)
{
    ERS_LOG("Destroying MIGControllable");
    // try to cleanup in case UNCONFIGURE did not happen
    if (m_isr != nullptr) {
        for (auto&& handler: m_handlers) {
            try {
                handler->unsubscribe(*m_isr);
            } catch (const std::exception& exc) {
                // expect potential troubles, ignore everything
                ERS_LOG("Exception during cleanup: " << exc.what());
            }
        }
        // Stop scheduler before destroying handlers.
        Scheduler::instance().stop();
        m_handlers.clear();
        m_isr.reset();
    }
}

void MIGControllable::configure(const daq::rc::TransitionCmd & args)
{
    sleep (m_sleep);
    ERS_LOG("Got configure with arguments " << args.toString());

    // get partition
    auto& onlsvc = daq::rc::OnlineServices::instance();
    IPCPartition partition;
    try {
        partition = onlsvc.getIPCPartition();
    } catch (const std::exception& ex) {
        ERS_LOG("Can't create IPCPartition object");
        throw ConfigurationError(ERS_HERE, "Can't create IPCPartition object.", ex);
    }

    // make receiver instance
    try {
        ERS_LOG("Creating a new ISInfoReceiver");
        m_isr = std::make_unique<ISInfoReceiver>(partition, false);
    } catch (const std::exception& ex) {
        ERS_LOG("Can't create ISInfoReceiver object");
        throw ConfigurationError(ERS_HERE, "Can't create ISInfoReceiver object.", ex);
    }

    // try loading our standard libraries with handlers and algos
    const char* libs[] = {"libMIGContainers.so"};
    for (auto lib: libs) {
        if (!LibraryManager::instance().loadLibrary(lib)) {
            ERS_LOG("Failed to load " << lib);
            char *error = dlerror();
            ERS_LOG("Error message is \"" << error << "\"");
        } else {
            ERS_LOG(lib << " is loaded successfully");
        }
    }

    // parse configuration
    ConfigParser parser;
    auto configs = parser.parse();

    // make all handlers
    for (auto&& cfg: configs) {
        ERS_LOG("using configuration " << cfg);
        m_handlers.push_back(std::make_unique<InfoHandlerWrapper>(cfg, m_providerName, partition));
    }

    // check that we have at least one handler
    if (m_handlers.empty()) {
        ConfigurationError noValidConfig(ERS_HERE,
                                         "No valid configuration can be found. Can't continue!.");
        ers::fatal(noValidConfig);
        return;
    }

    // Start scheduler for periodic publishing.
    Scheduler::instance().start();
}

void MIGControllable::connect(const daq::rc::TransitionCmd & args)
{
    sleep (m_sleep);
    ERS_LOG("Got connect with arguments " << args.toString());
    if (m_isr != nullptr) {
        for (auto&& handler: m_handlers) {
            handler->subscribe(*m_isr);
        }
    }

    static const char* stats_file = getenv("MIG_STATS_FILE");
    if (stats_file != nullptr and stats_file[0] != '\0') {
        m_mon_fd = open(stats_file, O_WRONLY | O_APPEND | O_CREAT, 0666);
        if (m_mon_fd < 0) {
            ERS_LOG("Failed top open stats file " << stats_file << ", no stats will be saved");
        }
    }
}

void MIGControllable::prepareForRun(const daq::rc::TransitionCmd & args)
{
    sleep (m_sleep);
    ERS_LOG("Got prepareForRun with arguments " << args.toString());
}

void MIGControllable::stopGathering(const daq::rc::TransitionCmd & args)
{
    ERS_LOG("Got stopGathering with arguments " << args.toString());
    ERS_LOG("Sleeping for " << m_sleep << " seconds");
    sleep (m_sleep);
}

void MIGControllable::disconnect(const daq::rc::TransitionCmd & args)
{
    sleep (m_sleep);
    ERS_LOG("Got disconnect with arguments " << args.toString());
    if (m_mon_fd >= 0) {
        close(m_mon_fd);
        m_mon_fd = -1;
    }
}

void MIGControllable::unconfigure(const daq::rc::TransitionCmd & args)
{
    sleep (m_sleep);
    ERS_LOG("Got unconfigure with arguments " << args.toString());
    if (m_isr != nullptr) {
        for (auto&& handler: m_handlers) {
            handler->unsubscribe(*m_isr);
        }
    }

    // save timing info to file(s)
    static const char* timing_file = getenv("MIG_TIMING_FILE");
    if (timing_file != nullptr and timing_file[0] != '\0') {
        TimingSaver saver(timing_file);
        for (auto&& handler: m_handlers) {
            handler->saveTiming(saver);
        }
        saver.close();
    }

    // Stop scheduler before destroying handlers.
    Scheduler::instance().stop();
    m_handlers.clear();
}

void MIGControllable::publish()
{
    if (m_mon_fd >= 0) {
        std::lock_guard<std::mutex> lock(m_publishMutex);
        for (auto&& handler: m_handlers) {
            handler->saveStats(m_mon_fd);
        }
    }
}
