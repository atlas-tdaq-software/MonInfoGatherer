//
// Main executable of MonInfoGatherer
//

//-----------------
// C/C++ Headers --
//-----------------
#include <unistd.h>
#include <iostream>
#include <exception>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "ipc/core.h"
#include "MonInfoGatherer/MIGControllable.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/CmdLineParser.h"

extern char **environ;

namespace boostPOpt = boost::program_options;
using namespace MIG;

int main(int argc, char* argv[])
{

    unsigned int sleepFor;
    //try to initialize ipc core
    bool IPCFailed = false;
    try {
        IPCCore::init(argc, argv);
    } catch (const std::exception& ex) {
        IPCFailed = true;
    }
    boost::program_options::options_description desc("This program adds-up the histograms it receives.");
    try {
        desc.add_options()("sleepFor,S", boostPOpt::value<unsigned int>(&sleepFor)->default_value(2),
                           "Transition delay in seconds.")("version,v", "Print MonInfoGatherer version")(
                "help,h", "Print help message");

        std::cout << "Starting MonInfoGatherer " << MonInfVersion << std::endl;
        std::cout << "Dumping Environment " << std::endl;
        for (char **env = environ; *env != 0; env++) {
            std::cout << *env << std::endl;
        }

        std::cout << std::endl << "Input parameters are " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"" << std::endl;
        }
        std::cout << std::endl << "Command to run " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;

        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run(),
                         vm);
        boostPOpt::notify(vm);
        std::cout << "Got input parameters " << std::endl;
        boostPOpt::basic_parsed_options<char> bop =
                boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                << bop.options.at(t).value.at(k) << std::endl;
            }
        }

        if (vm.count("help")) {
            daq::rc::CmdLineParser cmdParser(argc, argv, true);
            std::cout << "Description from item control " << std::endl;
            std::cout << cmdParser.description() << std::endl << std::endl;
            std::cout << "MonInfoGatherer description" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if (vm.count("version")) {
            //std::cout << desc << std::endl;
            std::cout << MonInfVersion << std::endl;
            return EXIT_SUCCESS;
        }

        if (IPCFailed) {
            std::cerr << "Error in IPC core initialization. Exiting." << std::endl;
            return EXIT_FAILURE;
        }
        daq::rc::CmdLineParser cmdParser(argc, argv, true);
        daq::rc::ItemCtrl itemCtrl(
                cmdParser,
                std::shared_ptr < daq::rc::Controllable > (new MIGControllable(cmdParser.applicationName(),
                                                                               sleepFor)),
                std::shared_ptr < daq::rc::ControllableDispatcher > (new daq::rc::DefaultDispatcher()));
        itemCtrl.init();
        itemCtrl.run();
        ERS_LOG("Exiting application " << cmdParser.applicationName());
    } catch (const daq::rc::CmdLineHelp& ex) {
        std::cout << desc << std::endl;
        std::cout << ex.message() << std::endl;
    } catch (const ers::Issue& ex) {
        if (!IPCFailed)
            ers::fatal(ex);
        return EXIT_FAILURE;
    } catch (const boost::program_options::error& ex) {
        std::cerr << "Exception from program options " << ex.what() << std::endl;
        if (!IPCFailed)
            ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    } catch (const std::exception& ex) {
        //ers::error(daq::rc::CmdlParsing(ERS_HERE, "", ex));
        std::cerr << "Caught exception " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
