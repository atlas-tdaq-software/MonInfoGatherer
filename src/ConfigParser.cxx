//
// Implementation of MIGControllable class.
//

//-----------------------
// This Class's Header --
//-----------------------
#include "ConfigParser.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dal/InfrastructureTemplateApplication.h"
#include "MIG/dal/MIGApplication.h"
#include "MIG/dal/MIGConfiguration.h"
#include "MIG/dal/MIGMatchHandler.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LibraryManager.h"
#include "RunControl/Common/OnlineServices.h"
#include "ers/ers.h"


using namespace MIG;


std::vector<Config> ConfigParser::parse() const
{
    std::vector<Config> configs;

    auto& onlsvc = daq::rc::OnlineServices::instance();

    // find configuration for this instance in DB
    const dal::MIGApplication *me = onlsvc.getApplication().cast<dal::MIGApplication>();
    if (me == 0) {
        ERS_LOG("Can't find my configuration " << onlsvc.applicationName());
        std::string errMess = "Can't find configuration for the gatherer named \"" +
                onlsvc.applicationName() + "\" in config DB";
        throw ConfigurationError(ERS_HERE, errMess);
    }

    auto libPaths = me->get_LibraryPaths();
    for (auto& libPath : libPaths) {
        if (!libPath.empty()) {
            if (!LibraryManager::instance().loadLibrary(libPath)) {
                ers::error(ConfigurationError(ERS_HERE, "Failed to load library " + libPath));
            } else {
                ERS_LOG(libPath << " is loaded successfully");
            }
        }
    }

    //get list of configurations associated with this instance
    const std::vector<const dal::MIGConfiguration*> myConfigs = me->get_Configurations();
    if (myConfigs.size() == 0) {
        ERS_LOG("No gathering configuration specified!" << onlsvc.applicationName());
        std::string errMess = "No gathering configuration is specified for the gatherer named \""
                + onlsvc.applicationName() + "\" in config DB";
        throw ConfigurationError(ERS_HERE, errMess);
    }

    Configuration& partitionConfig = onlsvc.getConfiguration();

    for (auto&& migConfig: myConfigs) {

        // fill source/destination server names
        std::vector<std::string> enabledSrcs, enabledDsts;
        for (const auto& srv: migConfig->get_SourceServers()) {
            if (auto iapp = partitionConfig.cast<daq::core::InfrastructureTemplateApplication>(srv)) {
                if (not iapp->get_SegmentProcEnvVarName().empty()) {
                    if (char *sourceSName = getenv(iapp->get_SegmentProcEnvVarName().c_str())) {
                        enabledSrcs.push_back(sourceSName);
                    } else {
                        ERS_LOG("Warning environment Variable  \"" << iapp->get_SegmentProcEnvVarName()
                                << "\" is not set or empty. Skipping " << iapp->UID());
                    }
                } else {
                    ERS_LOG("Warning SegmentProcEnvVarName is empty for  " << iapp->UID() << ". Skipping");
                    ConfigurationError missingVarName(ERS_HERE,
                                "SegmentProcEnvVarName is empty for source server \""
                                + iapp->UID() + "\" skipping");
                    ers::warning(missingVarName);
                }
            } else {
                enabledSrcs.push_back(srv->UID());
            }
        }
        for (const auto& srv: migConfig->get_DestinationServers()) {
            if (auto iapp = partitionConfig.cast<daq::core::InfrastructureTemplateApplication>(srv)) {
                if (not iapp->get_SegmentProcEnvVarName().empty()) {
                    if (char * dstSName = getenv(iapp->get_SegmentProcEnvVarParentName().c_str())) {
                        enabledDsts.push_back(dstSName);
                    } else {
                        ERS_LOG("Warning environment Variable  \"" << iapp->get_SegmentProcEnvVarParentName()
                                << "\" is not set or empty. Skipping " << iapp->UID());
                    }
                } else {
                    ConfigurationError missingVarName(ERS_HERE,
                              "SegmentProcEnvVarParentName is empty for destination server \""
                              + iapp->UID() + "\" skipping");
                    ers::warning(missingVarName);
                    ERS_LOG(missingVarName.what());
                }
            } else {
                enabledDsts.push_back(srv->UID());
            }
        }
        if (enabledDsts.empty() || enabledSrcs.empty()) {
            ERS_LOG("Missing source or destination servers for configuration " << migConfig->UID()
                    << ". Skipping");
            ConfigurationError missingServers(ERS_HERE,
                    "Missing source or destination servers for configuration \"" + migConfig->UID()
                     + ". Configuration is ignored.");
            ers::warning(missingServers);
            continue; // if no input or output servers skip configuration
        }

        // convert all MIGMatchHandler configs into HandlerConfig
        std::vector<HandlerConfig> hConfigs;
        for (auto&& handler: migConfig->get_MatchHandlers()) {
            hConfigs.emplace_back(handler->get_ObjectRegExp(),
                                  handler->get_Name(),
                                  handler->get_Parameters());
        }

        if (hConfigs.empty()) {
            // if no handlers defined, add a catch-all handler
            ERS_LOG("Adding default catch-all handler");
            std::vector<std::string> hParams;
            hConfigs.emplace_back(".*", "", hParams);
        }

        Config cfg(migConfig->get_ProviderRegExp(), migConfig->get_ObjectRegExp(),
                   enabledSrcs, enabledDsts, hConfigs);
        configs.push_back(cfg);
    }

    return configs;
}
