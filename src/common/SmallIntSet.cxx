//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/SmallIntSet.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace MIG {

/// Return minimum number in the set or -1 if set is empty
int SmallIntSet::min_element() const
{
    int n_buckets = m_buckets.size();
    for (int i = 0; i != n_buckets; ++i) {
        auto word = m_buckets[i];
        if (word != 0) {
            // use "count trailing zeros" builtin
            return i * WORD_BITS + __builtin_ctzl(word);
        }
    }
    return -1;
}

/// Return maximum number in the set or -1 if set is empty
int SmallIntSet::max_element() const
{
    int n_buckets = m_buckets.size();
    for (int i = n_buckets; i != 0; --i) {
        auto word = m_buckets[i-1];
        if (word != 0) {
            // use "count leading zeros" builtin
            return i * WORD_BITS - __builtin_clzl(word) - 1;
        }
    }
    return -1;
}

void SmallIntSet::print(std::ostream& out) const
{
    int start = -1;
    bool first = true;
    int bit = 0;
    out << '{';
    for (auto word: m_buckets) {
        for (int wbit = 0; wbit != WORD_BITS; ++wbit, word >>= 1) {
            if (word & 1) {
                if (start < 0) {
                    start = bit;
                }
            } else {
                if (start >= 0) {
                    const int end = bit - 1;
                    if (first) {
                        first = false;
                    } else {
                        out << ',';
                    }
                    if (end == start) {
                        out << start;
                    } else {
                        out << start << '-' << end;
                    }
                    start = -1;
                }
            }
            ++ bit;
        }
    }
    out << '}';
}

} // namespace MonInfoGatherer
