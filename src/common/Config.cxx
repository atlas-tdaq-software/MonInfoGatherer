//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/Config.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"

using namespace MIG;

namespace {

/**
 *  If parameters contain "key=value" string then return "value" for a given key.
 *  Returns empty string if key is not found.
 */
std::string value(const std::vector<std::string>& parameters,
                  const std::string& key,
                  const std::string& defaultValue)
{
    for (auto&& param: parameters) {
        auto pos = param.find('=');
        if (pos != std::string::npos) {
            if (param.compare(0, pos, key) == 0) {
                return param.substr(pos+1);
            }
        }
    }
    return defaultValue;
}

void printList(std::ostream& out, const std::vector<std::string>& strings) {
    out << '[';
    bool first = true;
    for (auto&& item: strings) {
        if (first) {
            first = false;
        } else {
            out << ", ";
        }
        out << "\"" << item << "\"";
    }
    out << ']';
}

} // namespace

std::string ConfigParameters::value(const std::string& key, const std::string& defaultValue) const
{
    return ::value(parameters, key, defaultValue);
}

std::vector<std::string> ConfigParameters::values(const std::string& key) const
{
    std::vector<std::string> values;
    for (auto&& param: parameters) {
        auto pos = param.find('=');
        if (pos != std::string::npos) {
            if (param.compare(0, pos, key) == 0) {
                values.push_back(param.substr(pos+1));
            }
        }
    }
    return values;
}

// Return value of boolean parameter.
bool ConfigParameters::boolValue(const std::string& key, bool defaultValue) const
{
    auto val = value(key);
    if (not val.empty()) {
        if (val == "1") {
            return true;
        } else if (val == "0") {
            return false;
        } else {
            ers::warning(ConfigurationError(ERS_HERE,
                key + " is set incorrectly = " + val));
            return defaultValue;
        }
    }
    if (std::find(parameters.begin(), parameters.end(), key) != parameters.end()) {
        // "Parameter" is the same as "Parameter=1"
        return true;
    }
    return defaultValue;
}

// Return value of boolean parameter.
unsigned ConfigParameters::uintValue(const std::string& key, unsigned defaultValue) const
{
    auto val = value(key);
    if (not val.empty()) {
        try {
            unsigned value = boost::lexical_cast<unsigned>(val);
            return value;
        } catch (const std::exception& ex) {
            ers::warning(ConfigurationError(ERS_HERE,
                key + " is not integer = " + val));
        }
    }
    return defaultValue;
}

void ConfigParameters::print(std::ostream& out, int offset) const
{
    std::string pfx(offset*4, ' ');
    out << pfx << "Parameters: ";
    printList(out, parameters);
    out << '\n';
}

void HandlerConfig::print(std::ostream& out, int offset) const
{
    std::string pfx(offset*4, ' ');
    out << pfx << "Handler:\n"
            << pfx << "    ObjectRe: \"" << objectRe << "\"\n"
            << pfx << "    Name: \"" << name << "\"\n";
    parameters.print(out, offset+1);
}

void Config::print(std::ostream& out, int offset) const
{
    std::string pfx(offset*4, ' ');
    out << pfx << "Config:\n";
    out << pfx << "    Source Servers: ";
    printList(out, srcServers);
    out << '\n';
    out << pfx << "    ProviderRe: \"" << providerRe << "\"\n";
    out << pfx << "    ObjectRe: \"" << objectRe << "\"\n";
    out << pfx << "    Destination Servers: ";
    printList(out, dstServers);
    out << '\n';
    for (auto&& handlerConfig: handlerConfigs) {
        handlerConfig.print(out, offset+1);
    }
}
