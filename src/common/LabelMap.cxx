//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/LabelMap.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace MIG {

AxisLabelMap::AxisLabelMap(const oh::Axis& axis)
{
    const std::vector<std::string>& labels = axis.labels;
    size_t numLabels = labels.size();
    int axisBinCount = axis.nbins;
    // gatherer convention is that axis is labeled if number of
    // labels is positive and equal to the number of bins
    if (numLabels > 0 || (int) numLabels == axisBinCount) {
        m_hasLabels = true;
        const std::vector<int>& indices = axis.indices;
        for (unsigned t = 0; t < numLabels; ++t) {
            m_map[labels[t]] = indices[t];
        }
    }
}

void AxisLabelMap::clear()
{
    m_hasLabels = false;
    m_map.clear();
}

LabelMap::LabelMap(const std::vector<oh::Axis>& axes)
{
    // just in case number of axes is higher limit it to 3
    for (unsigned i = 0; i < axes.size() && i < 3; ++i) {
        m_maps[i] = AxisLabelMap(axes[i]);
    }
}

void LabelMap::clear()
{
    m_maps[0].clear();
    m_maps[1].clear();
    m_maps[2].clear();
}

} // namespace MonInfoGatherer
