//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/PeriodFinder.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cmath>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

using namespace MIG;


void PeriodFinder::add(std::chrono::microseconds timestamp)
{
    if (m_last.count() == 0) {
        // very first timestamp
        m_last = timestamp;
        return;
    }

    std::chrono::microseconds period = timestamp - m_last;
    m_last = timestamp;
    if (period < std::chrono::microseconds::zero()) {
        // totally out of order
        return;
    }
    if (period > m_cutoff) {
        // ginormous period
        return;
    }

    m_count += 1;
    m_period_sum += period;

    if (m_count > 1 and m_histo.empty()) {
        // means that we switched to average
        return;

    }
    unsigned period_sec = unsigned(std::round(period.count() / 1e6));
    m_histo[period_sec] += 1;
}

std::chrono::seconds PeriodFinder::period() const
{
    if (m_count == 0) {
        // no data
        return std::chrono::seconds::zero();
    }

    if (m_count < 10 or m_histo.empty()) {
        // use average for an estimate
        return std::chrono::seconds(unsigned(std::round(m_period_sum.count() / 1e6 / m_count)));
    }

    // Look at the histogram and do a best guess. Ideally, if there is a true
    // well defined period there will be a single bin or two adjacent bins
    // with majority of entries. In that case we want a mode of the
    // distribution. Alternatively it may be a multi-modal distribution, or it
    // could be a dense (e.g. gaussian) distribution, in that case we use
    // average but we also switch to "average mode" meaning that we do not
    // populate histogram.

    // find the bin with max. count
    size_t max_count = 0;
    unsigned max_bin = 0;
    for (auto&& bin: m_histo) {
        if (bin.second > max_count) {
            max_bin = bin.first;
            max_count = bin.second;
        }
    }

    // try to join neighbors on each side
    auto iter = m_histo.find(max_bin + 1);
    if (iter != m_histo.end()) {
        max_count += iter->second;
    }
    if (max_bin > 0) {
        auto iter = m_histo.find(max_bin - 1);
        if (iter != m_histo.end()) {
            max_count += iter->second;
        }
    }

    // if counts in these three bins is higher than 75% of all counts then
    // this is our mode.
    if (max_count >= ((m_count * 3) / 4)) {
        return std::chrono::seconds(max_bin);
    }

    // otherwise switch to average, but only when statistics is significant
    if (m_count > 100) {
        m_histo.clear();
    }

    return std::chrono::seconds(unsigned(std::round(m_period_sum.count() / 1e6 / m_count)));
}

void PeriodFinder::reset()
{
    m_last = std::chrono::microseconds::zero();
    m_count = 0;
    m_period_sum = std::chrono::microseconds::zero();
    m_histo.clear();
}

void PeriodFinder::merge(const PeriodFinder& other)
{
    // reset m_last
    m_last = std::chrono::microseconds::zero();

    m_count += other.m_count;
    m_period_sum += other.m_period_sum;
    for (auto&& bin: other.m_histo) {
        m_histo[bin.first] += bin.second;
    }
}
