//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/Clock.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

namespace {

/**
 * Default implementation of Clock interface for 1production case.
 *
 * This implementation uses `std::chrono::steady_clock` to obtain clock value.
 * This is the clock to be used in production running.
 */
class DefaultClock : public MIG::Clock {
public:

    ~DefaultClock() = default;

    // implementation of now() using steady clock
    time_point now() const override { return std::chrono::steady_clock::now(); }

};


} // namespace

std::unique_ptr<MIG::Clock> MIG::Clock::s_instance(new DefaultClock);
