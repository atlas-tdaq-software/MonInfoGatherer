//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/LibraryManager.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <dlfcn.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

namespace MIG {

LibraryManager::~LibraryManager()
{
    for (auto&& pair: m_libraries) {
        if (!dlclose(pair.second)) {
            ERS_LOG("Closing \"" << pair.first << "\" ... done");
        } else {
            ERS_LOG("Closing \"" << pair.first << "\" ... FAILED!!!!");
        }
    }
}

LibraryManager& LibraryManager::instance()
{
    static LibraryManager _instance;
    return _instance;
}

bool LibraryManager::loadLibrary(const std::string& libName)
{
    if (m_libraries.find(libName) != m_libraries.end()) {
        return true;
    }

    typedef void (*initFunc)(void);
    void* lib = dlopen(libName.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    const char* errVal = dlerror();
    if (!lib) {
        ERS_LOG("Opening library " << libName << " failed with " << errVal);
        return false;
    }

    union {
        void *from;
        initFunc to;
    } u;
    u.from = dlsym(lib, "register_objects");
    errVal = dlerror();
    if (errVal != 0) {
        ERS_LOG("Warning Library " << libName
                << " does not contain register_objects function. Error was " << errVal);
    } else {
        u.to();
    }

    m_libraries[libName] = lib;
    return true;
}

}
