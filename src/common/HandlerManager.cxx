//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/HandlerManager.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/HandlerBase.h"

using namespace MIG;

HandlerManager& HandlerManager::instance()
{
    static HandlerManager _instance;
    return _instance;
}

std::shared_ptr<HandlerBase>
HandlerManager::getHandler(const HandlerConfig& handlerConfig, InfoPublisher& publisher) const
{
    std::string handlerName = handlerConfig.name;
    if (handlerName.empty()) {
        ERS_LOG("Warning handler name can not be empty!");
        return nullptr;
    }
    auto it = m_handlerConstructors.find(handlerName);
    if (it  != m_handlerConstructors.end()) {
        auto func = it->second;
        auto handl = func(handlerConfig, publisher);
        return handl;
    } else {
        ERS_LOG("Couldn't find handler " << handlerName << std::endl);
    }
    return nullptr;
}

bool HandlerManager::registerHandler(const std::string& handlerName, HandlerFactory func)
{
    std::string name = handlerName;
    if (name.empty()) {
        ERS_LOG("Error! HandlerName is empty");
        return false;
    }
    ERS_LOG("Registering handler for " << handlerName);
    if (!(m_handlerConstructors.insert(std::make_pair(handlerName, func)).second)) {
        //    if(m_handlerConstructors.find(handlerName)!=m_handlerConstructors.end()){
        ERS_LOG("Warning Handler named " << handlerName
                << " is already registered. Not registering! Check Configuration!");
        return false;
    }
    return true;
}
