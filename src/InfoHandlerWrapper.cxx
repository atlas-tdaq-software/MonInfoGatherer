//
// Implementation of InfoHandlerWrapper class
//

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/InfoHandlerWrapper.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <sstream>
#include <ctime>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/inforeceiver.h"
#include "is/infoany.h"
#include "is/infodictionary.h"
#include "oh/interface/OHProvider.h"
#include "oh/OHUtil.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/HandlerBase.h"
#include "MonInfoGatherer/HandlerManager.h"


using namespace MIG;

/**
 *  Provider class which registers MIG as IS provider.
 *
 *  @note This class does not add any functionality on top of OHProvider,
 *  its only reason for existence is to make protected OHProvider methods public.
 */
class InfoHandlerWrapper::MyOHProvider : public OHProvider {
public:
    MyOHProvider(const IPCPartition& p, const std::string& server, const std::string& name)
        : OHProvider(p, server, name) {}

    using OHProvider::publish;
};

InfoHandlerWrapper::InfoHandlerWrapper(const Config& cfg,
                                       const std::string& providerName,
                                       const IPCPartition& partition)
        : m_cfg(cfg),
          m_myProviderName(providerName),
          m_handlers(),
          m_fullRe(cfg.providerRe + "\\." + cfg.objectRe)
{
    ERS_ASSERT_MSG(!m_cfg.handlerConfigs.empty(), "Configuration has empty handler list");
    for (auto hConfig: m_cfg.handlerConfigs) {
        // compile regex
        if (hConfig.name.empty()) {
            hConfig.name = "Default";
        }
        auto handler = HandlerManager::instance().getHandler(hConfig, *this);
        ERS_ASSERT_MSG(handler != nullptr,
                       "Can't get instance of infoHandler " << hConfig.name << " Check configuration");

        if (handler != nullptr) {
            try {
                std::regex objectRe(hConfig.objectRe);
                m_handlers.emplace_back(objectRe, handler);
            } catch (std::regex_error const& exc) {
                ConfigurationError issue(ERS_HERE,
                                         "Invalid regular expression \"" + hConfig.objectRe + "\"",
                                         exc);
                ers::error(issue);
                throw issue;
            }
        }
    }

    for (auto&& srv: m_cfg.dstServers) {
        m_providers.push_back(std::make_unique<MyOHProvider>(partition, srv, m_myProviderName));
    }
}

InfoHandlerWrapper::~InfoHandlerWrapper()
{
    ERS_LOG("InfoHandlerWrapper: callbackCount=" << m_callbackCount <<
            " publishCount=" << m_publishCount << " providerCount=" << m_callbacksPerProvider.size());
    for (auto&& pair: m_callbacksPerProvider) {
        ERS_LOG("InfoHandlerWrapper: provider=" << pair.first << " callbacks=" << pair.second);
    }
}

void InfoHandlerWrapper::_callbackHandler(ISCallbackInfo* cbinfo)
{
    std::string fullname = cbinfo->name();
    if (cbinfo->reason() == ISInfo::Deleted)
        return;
    std::string publisher = oh::util::get_provider_name(fullname);
     ++m_callbacksPerProvider[publisher];
    if (publisher == m_myProviderName)
        return;
    std::string name = oh::util::get_histogram_name(fullname);
    ISInfoAny info;
    try {
        cbinfo->value(info);
    } catch (const daq::is::RepositoryNotFound&) {
        return;
    } catch (const daq::is::InfoNotFound&) {
        return;
    } catch (const daq::is::InfoNotCompatible&) {
        return;
    }
    for (auto&& pair: m_handlers) {
        // call first matching handler
        if (std::regex_match(name, pair.first)) {
            pair.second->updateInfo(info, name, publisher);
            break;
        }
    }
    ++m_callbackCount;
}

void InfoHandlerWrapper::publish(ISInfo& info, const std::string& name, int nProv)
{
    // note that this method is called by the handlers and can be called
    // simultaneously from many threads. There is no synchronization now
    // because we do not modify any state (in any methods) while we are
    // subscribed to IS data.

    //  ERS_DEBUG(1, " received publish callback for "<<name);
    for (auto&& provider: m_providers) {
        try {
            provider->publish(info, name, info.tag());
        } catch (const daq::is::Exception& ex) {
            ERS_LOG("can't publish \"" << name << "\" to server \"" <<
                    provider->getServerName() << "\", got exception: " << ex);
        }
    }
    ++ m_publishCount;
}

void InfoHandlerWrapper::subscribe(ISInfoReceiver& isr)
{
    ERS_LOG("Subscribe called");
    try {
        ISCriteria crit(m_fullRe);
        for (auto& servername: m_cfg.srcServers) {
            isr.subscribe(servername, crit, &InfoHandlerWrapper::_callbackHandler, this);
            ERS_LOG("Subscribing to \"" << servername << "\" with RE \"" << m_fullRe << "\"");
        }
    } catch (const daq::is::Exception& ex) {
        ERS_LOG("IS exception: " << ex);
    } catch (const std::exception& ex) {
        ERS_LOG("Unknown exception " << ex.what());
    }
}

void InfoHandlerWrapper::unsubscribe(ISInfoReceiver& isr)
{
    ERS_LOG("unsubscribe called");
    try {
        ISCriteria crit(m_fullRe);
        for (auto& servername: m_cfg.srcServers) {
            ERS_LOG("unsubscribing from \"" << servername << "\" with RE \"" << m_fullRe << "\"");
            isr.unsubscribe(servername, crit, true);  //wait for completion
        }
    } catch (const daq::is::Exception& ex) {
        ERS_LOG("IS exception: " << ex);
    } catch (const std::exception& ex) {
        ERS_LOG("Unknown exception " << ex.what());
    }
}

void InfoHandlerWrapper::saveTiming(MIG::TimingSaver& saver)
{
    for (auto&& pair: m_handlers) {
        pair.second->saveTiming(saver);
    }
}

void InfoHandlerWrapper::saveStats(int fd)
{
    std::ostringstream buffer;
    // second precision is OK here
    std::time_t now = std::time(nullptr) * 1000000000ULL;
    buffer << "publications counter=" << m_publishCount << " " << now << "\n";
    buffer << "callbacks,provider=total counter=" << m_callbackCount << " " << now << "\n";
    for (auto&& pair: m_callbacksPerProvider) {
        buffer << "callbacks,provider=\"" << pair.first << "\" counter=" << pair.second << " " << now << "\n";
    }
    auto str = buffer.str();
    ::write(fd, str.data(), str.size());
}
