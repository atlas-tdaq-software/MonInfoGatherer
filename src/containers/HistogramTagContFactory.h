// -*- c++ -*-
#ifndef MONINFOGATHERER_HISTOGRAMTAGCONTFACTORY_H
#define MONINFOGATHERER_HISTOGRAMTAGCONTFACTORY_H
//
// Declaration of HistogramTagContFactory Class
//

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <memory>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "HistogramTagContBase.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/ProviderMap.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class InfoPublisher;
class HistoAlgorithm;
class TimingCollector;
}

namespace MIG {

/**
 * Class implementing factory for HistogramTagContBase instances.
 *
 * Template type can be one of oh::Histogram or oh::Efficiency.
 */
template <typename T>
class HistogramTagContFactory {
public:

    /**
     *  Constructor.
     *
     *  @param histoName Name of the histogram.
     *  @param parameters Configuration parameters.
     */
    HistogramTagContFactory(const std::string& histoName,
                            std::unique_ptr<HistoAlgorithm> alg,
                            InfoPublisher& publisher,
                            const ConfigParameters& parameters);

    HistogramTagContFactory(const HistogramTagContFactory&) = delete;
    HistogramTagContFactory& operator=(const HistogramTagContFactory&) = delete;

    ~HistogramTagContFactory();

    /**
     * Create new container for given tag.
     */
    std::shared_ptr<HistogramTagContBase<T>> makeTagCont(int tag);

private:

    std::string m_histoName;
    std::unique_ptr<HistoAlgorithm> m_alg;
    InfoPublisher& m_publisher;
    ConfigParameters m_parameters;
    ProviderMap m_providerMap; //all tagged histograms must have same providers
    bool m_forcePerLB;  // True if name matches PerLBHistogram parameter
    bool m_autoPerLB;   // True if PerLBHistogram=auto is specified
};
} // namespace MIG

#endif // MONINFOGATHERER_HISTOGRAMTAGCONTFACTORY_H
