#ifndef MIG_HISTOGRAMTAGCONTBASE_H
#define MIG_HISTOGRAMTAGCONTBASE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class TimingCollector;
}

namespace MIG {

/**
 * Interface for per-tag container classes.
 */
template <typename T>
class HistogramTagContBase {
public:

    virtual ~HistogramTagContBase() {}

    /**
     *  Receive new contribution from provider.
     *
     *  @param histo Histogram object, never nullptr.
     *  @param providerName Name of the provider for this contribution.
     *  @param timingColl Profiling data collector.
     *  @return true if this caused publication.
     */
    virtual bool tagOperate(std::unique_ptr<oh::ObjectBase<T>> histo,
                            const std::string& providerName,
                            TimingCollector& timingColl) = 0;

};

} // namespace MIG

#endif // MIG_HISTOGRAMTAGCONTBASE_H
