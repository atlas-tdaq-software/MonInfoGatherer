#ifndef MIG_HISTOGRAMHOLDER_H
#define MIG_HISTOGRAMHOLDER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <iosfwd>
#include <memory>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Special holder class for histogram which encapsulates some operations
 *  such as transparent compression/decompression of data.
 */
template <typename T>
class HistogramHolder {
public:

    using Histogram = oh::ObjectBase<T>;

    /**
     *  Make new HistogramHolder for given histogram.
     *
     *  @param histo IS histogram object
     *  @param minCompressSize Minimal size of bin data array to compress in bytes,
     *              if negative then do not compress anything
     *  @param fraction Compression goal, compression is not used if commpressed data
     *              is larger than this fraction of original data
     */
    static std::unique_ptr<HistogramHolder> makeHolder(std::unique_ptr<Histogram> histo,
                                                       int minCompressSize,
                                                       float fraction);

    HistogramHolder() = default;

    virtual ~HistogramHolder() = default;
    HistogramHolder(const HistogramHolder&) = delete;
    HistogramHolder& operator=(const HistogramHolder&) = delete;

    /**
     *  Retrieve histogram, making it usable.
     *
     *  Note that clients should not modify this object, as this may be
     *  a temporary copy and changes will be lost. If `clone` is true
     *  then returned object can be modified in-place without change to
     *  the held object.
     */
    virtual std::shared_ptr<Histogram> get(bool clone=false) const = 0;

};

// Class for representing compressed data (and way to build it).
// It is internal to the holder class but it is declared here in the header
// for testing purposes.
template <typename T>
class CompressedData {
public:

    // Type of compression for histogram data
    enum CompressionType {None, LZ4};

    /// This constructor transfers data from a vector.
    /// Vector is cleared if compression is OK, otherwise vector is not changed.
    CompressedData(std::vector<T>& data, int minCompressSize, float fraction);

    /// This constructor copies the data from array
    CompressedData(const T* data, unsigned numEmen, int minCompressSize, float fraction);

    // Returns true if compression was successful for data provided in constructor.
    bool compressed() const { return m_compression != CompressionType::None; }

    // Return size of original data
    unsigned size() const { return m_size; }

    // Return size of compressed data
    unsigned compressed_size() const { return m_data.size(); }

    // Uncompresses data into a vector
    void uncompress(std::vector<T>& data) const;

    // Uncompresses data and return a pointer
    std::unique_ptr<T[]> uncompress() const;

    void print(std::ostream& out) const;

private:

    // Compress buffer with lz4, check that compressed size is shorter
    // than a fraction of original size.
    // Returns true on success.
    bool _try_compress_lz4(const void* src, int srcSize, int minCompressSize, float fraction);

    // uncompress lz4 buffer
    void _uncompress_lz4(std::vector<T>& data) const;
    std::unique_ptr<T[]> _uncompress_lz4() const;

    std::vector<char> m_data;     // compressed data
    unsigned m_size;              // size of original data (count, not bytes)
    CompressionType m_compression = CompressionType::None;  // compression type
};

template <typename T>
inline
std::ostream&
operator<<(std::ostream& out, CompressedData<T> const& cdata) {
    cdata.print(out);
    return out;
}

} // namespace MIG

#endif // MIG_HISTOGRAMHOLDER_H
