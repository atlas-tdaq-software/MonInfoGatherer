//
// Implementation of HistogramCont
//

//-----------------------
// This Class's Header --
//-----------------------
#include "HistogramCont.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infoany.h"
#include "oh/core/DataTypes.h"
#include "oh/core/EfficiencyData.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"
#include "ers/ers.h"
#include "../algos/HistoAlgorithm.h"

using namespace MIG;

namespace {

// Helper class for producing brief axes information
struct AxesInfo {
    AxesInfo(const oh::ObjectBase<oh::Histogram>* hist_) : hist(hist_) {}
    const oh::ObjectBase<oh::Histogram>* hist;
};

std::ostream&
operator<<(std::ostream& out, const AxesInfo& ai)
{
    auto&& hist = *ai.hist;
    for (unsigned int i = 0; i < hist.dimension; ++i) {
        int axisBinCount = hist.get_bin_count((oh::Axis::ID) i);
        unsigned numLabels = hist.get_labels((oh::Axis::ID) i).size();
        out << " axis=" << i << ",#bins=" << axisBinCount << ",#labels=" << numLabels;
    }
    return out;
}

template <typename T>
struct HistTraits {
};

template <>
struct HistTraits<oh::Histogram> {
    static
    std::unique_ptr<oh::ObjectBase<oh::Histogram>> from_any(ISInfoAny& info)
    {
        auto&& currType = info.type();
        if (currType == oh::HistogramData<float>::type()) {
            auto histT = std::make_unique<oh::HistogramData<float>>();
            info >>= *histT;
            return histT;
        } else if (currType == oh::HistogramData<int>::type()) {
            auto histT = std::make_unique<oh::HistogramData<int>>();
            info >>= *histT;
            return histT;
        } else if (currType == oh::HistogramData<double>::type()) {
            auto histT = std::make_unique<oh::HistogramData<double>>();
            info >>= *histT;
            return histT;
        } else if (currType == oh::HistogramData<char>::type()) {
            auto histT = std::make_unique<oh::HistogramData<char>>();
            info >>= *histT;
            return histT;
        } else if (currType == oh::ProfileData::type()) {
            auto histT = std::make_unique<oh::ProfileData>();
            info >>= *histT;
            return histT;
        } else if (currType == oh::HistogramData<short>::type()) {
            auto histT = std::make_unique<oh::HistogramData<short>>();
            info >>= *histT;
            return histT;
        } else {
            ERS_LOG("Type " << currType << " is not supported by HistogramCont");
            throw UnsupportedObjectType(ERS_HERE, currType.name());
        }
    }

    static
    unsigned bin_count(const oh::ObjectBase<oh::Histogram>& hist)
    {
        unsigned bin_count = 1;
        for (auto axis: {oh::Axis::X,  oh::Axis::Y,  oh::Axis::Z}) {
            int bins = 	hist.get_bin_count(axis);
            if (bins > 0) {
                bin_count *= bins;
            }
        }
        return bin_count;
    }

    static
    AxesInfo axes_info(const oh::ObjectBase<oh::Histogram>& hist)
    {
        return AxesInfo(&hist);
    }

    // Returns true if any histogram axis is labeled.
    // Gatherer convention is that axis is labeled if number of
    // labels is positive and equal to the number of bins
    static bool isLabeled(const oh::ObjectBase<oh::Histogram>& hist)
    {
        for (unsigned int i = 0; i < hist.dimension; ++i) {
            int axisBinCount = hist.get_bin_count((oh::Axis::ID) i);
            unsigned numLabels = hist.get_labels((oh::Axis::ID) i).size();
            if (numLabels > 0 && (int)numLabels == axisBinCount) {
                return true;
            }
        }
        return false;
    }

    // Returns true if any histogram axis is incorrectly labeled,
    // meaning that number of labels is positive but not equal to
    // number of bins
    static bool isWrongLabels(const oh::ObjectBase<oh::Histogram>& hist)
    {
        for (unsigned int i = 0; i < hist.dimension; ++i) {
            int axisBinCount = hist.get_bin_count((oh::Axis::ID)i);
            if (axisBinCount < 0) continue;
            unsigned numLabels = hist.get_labels((oh::Axis::ID) i).size();
            if (numLabels > 0 and (int)numLabels != axisBinCount) {
                return true;
            }
        }
        return false;
    }
};

template <>
struct HistTraits<oh::Efficiency> {
    static
    std::unique_ptr<oh::EfficiencyData> from_any(ISInfoAny& info)
    {
        auto&& currType = info.type();
        if (currType == oh::EfficiencyData::type()) {
            auto histT = std::make_unique<oh::EfficiencyData>();
            info >>= *histT;
            return histT;
        } else {
            ERS_LOG("Type " << currType << " is not supported by HistogramCont");
            throw UnsupportedObjectType(ERS_HERE, currType.name());
        }
    }

    static
    unsigned bin_count(const oh::EfficiencyData& hist)
    {
        return HistTraits<oh::Histogram>::bin_count(hist.total);
    }

    static
    AxesInfo axes_info(const oh::EfficiencyData& hist)
    {
        return HistTraits<oh::Histogram>::axes_info(hist.total);
    }

    // Returns true if any histogram axis is labeled.
    // Gatherer convention is that axis is labeled if number of
    // labels is positive and equal to the number of bins
    static bool isLabeled(const oh::EfficiencyData& hist)
    {
        return HistTraits<oh::Histogram>::isLabeled(hist.total);
    }

    // Returns true if any histogram axis is incorrectly labeled,
    // meaning that number of labels is positive but not equal to
    // number of bins
    static bool isWrongLabels(const oh::EfficiencyData& hist)
    {
        return HistTraits<oh::Histogram>::isWrongLabels(hist.total);
    }
};


} // namespace

template <typename T>
HistogramCont<T>::HistogramCont(const std::string &name,
                                std::unique_ptr<HistoAlgorithm> alg,
                                InfoPublisher& publisher,
                                const ConfigParameters& parameters)
        : m_myName(name),
          m_checkLabelsOnce(true),
          m_labelMismatchFound(false),
          m_timing(),
          m_tagContFactory(name, std::move(alg), publisher, parameters)
{
    // set tag expiration time, default is ~5 minutes (a bit shorter to avoid
    // aliasing effect with 1-minute publication cycles)
    m_tagExpiration = std::chrono::seconds(parameters.uintValue("TagExpiration_sec", 5*60-3));

    auto timingHistorySize = parameters.uintValue("TimingHistorySize");
    m_timing.setHistorySize(timingHistorySize);
}

template <typename T>
HistogramCont<T>::~HistogramCont()
{
}

template <typename T>
void
HistogramCont<T>::_tagMapCleanup()
{
    // NOTE: logic that was implemented here before was broken for very long time.

    /*
      The idea is that we want to forget all tags that will not be published again.
      It is very hard to guess exactly when tag is safe to delete (HLT can do some
      crazy things). What we know:
        - number of tags that is published in single cycle is fixed in HLT
          configuration (usually 2), but that number is not known here
        - different providers can see events from different LBs, so the distance
          between min and max tag number in one cycle can be higher than above number
        - typically publication period and LB length should be close (but not always)
        - in some conditions processing of events from onw LB can extend well into
          one or two following LBs

      So the ideas for the algorith implemented here:
        - we track the most recent time when we receive contributions for given tag
          in m_tagTimes map
        - find all tags that were published withing a short period of the latest
          publication
        - drop everything that comes before that range
        - remember a set of tags that were dropped and ignore any new contributions
          for those tags

      With all of that one has to be careful with the reuse of the tags across
      multiple runs so we need to watch for tags wrapping around and starting from
      zero or small number again.
    */

    if (m_tagMap.empty()) {
        // Nothing to clear
        return;
    }
    if (m_tagMap.size() == 1 and m_tagMap.count(-1) > 0) {
        // don't clear non-LB containers.
        return;
    }

    // search for tag with max time
    auto max_iter = std::max_element(m_tagTimes.begin(), m_tagTimes.end(),
            [](TagTimes::value_type const& lhs, TagTimes::value_type const& rhs) {
                return lhs.second < rhs.second;
            });
    Clock::time_point max_tp = max_iter->second;

    ERS_DEBUG(1, "HistogramCont::_tagMapCleanup: " << m_myName
              << " m_tagMap.size=" << m_tagMap.size()
              << " m_tagTimes.size=" << m_tagTimes.size()
              << " m_droppedTags.size=" << m_droppedTags.size()
              << " latestTag=" << max_iter->first);

    // time before which tags are considered expired
    Clock::time_point min_tp = max_tp - m_tagExpiration;

    // drop everyhting before min_tp, but not -1 tag.
    for (auto it = m_tagTimes.begin(); it != m_tagTimes.end(); ) {
        if (it->first >= 0 and it->second < min_tp) {
            ERS_DEBUG(1, m_myName << " dropping old tag " << it->first);
            m_tagMap.erase(it->first);
            m_droppedTags.add(it->first);
            it = m_tagTimes.erase(it);
        } else {
            ++it;
        }
    }

    // Also if there was nothing published at all in last few minutes then drop everything.
    // This assumes that providers continue publishing histograms even if system is paused,
    // which is correct for monsvc-style publications, but can fail for other publishers.
    if (max_tp < Clock::instance().now() - m_tagExpiration) {
        ERS_DEBUG(1, m_myName << " puplication stopped, dropping all old tags");
        for (auto& item: m_tagTimes) {
            m_droppedTags.add(item.first);
        }
        m_tagTimes.clear();
        m_tagMap.clear();
    }
}

template <typename T>
void
HistogramCont<T>::_checkForNewRun(int tag, Clock::time_point const& now)
{
    /*
        If we still have state from a previous run when the new run starts then we need
        to forget all old tags. Trouble is how to decide that new run has started. One
        approach is to expect that run always starts with tag 0, but this can break if
        publication is lost for tag 0. So here we implement less strict heurisitcs:
            - if tag number is small (<3) and is in m_droppedTags
            - and highest tag in m_tagMap or in m_droppedTags is large (>10)
            - or if current time is sufficiently different from last observed publication
        then we rest all tags ads start afresh.
    */
    bool newRun = false;
    if (tag >= 0 and tag < 3 and m_droppedTags.contains(tag)) {
        int highestTag = m_droppedTags.max_element();
        for (auto& item: m_tagTimes) {
            highestTag = std::max(highestTag, item.first);
        }
        if (highestTag > 10) {
            // recent tags are much higher than current one, say we have new run
            ERS_DEBUG(1, m_myName << " new run detected, old highest tag=" << highestTag << ", new tag=" << tag);
            newRun = true;
        } else {
            // check time difference too
            auto timediff = now - m_latestContribution;
            if (timediff > m_tagExpiration*2) {
                ERS_DEBUG(1, m_myName << " new run detected, time difference is too large = "
                          << std::chrono::duration_cast<std::chrono::seconds>(timediff).count() << "sec");
                newRun = true;
            }
        }
    }
    if (newRun) {
        // reset all tags
        m_tagTimes.clear();
        m_tagMap.clear();
        m_droppedTags.clear();
    }

    // and remember time
    m_latestContribution = now;
}

template <typename T>
void
HistogramCont<T>::operate(ISInfoAny &info, const std::string& providerName)
{
    ERS_DEBUG(3, m_myName << " Got callback for " << providerName);

    OWLTime isnow(OWLTime::Microseconds);
    TimingCollector timingColl(m_timing, providerName, info.time().total_mksec_utc(), isnow.total_mksec_utc());
    timingColl.total_real_time.start();
    timingColl.total_cpu_time.start();

    int tag = info.tag();
    auto now = Clock::instance().now();

    // find or make HistogramTagCont for this tag
    std::shared_ptr<HistogramTagContBase<T>> currTag;
    {
        std::lock_guard<std::mutex> lock(m_tagMapMutex);

        _checkForNewRun(tag, now);

        if (tag >= 0 and m_droppedTags.contains(tag)) {
            ERS_LOG("Warning: received contribution from dropped tag=" << tag
                    << " histo=" << m_myName << " provider=" << providerName);
            return;
        }

        // remember time when we saw it
        m_tagTimes[tag] = now;

        typename TagMap::iterator tagMapIt = m_tagMap.find(tag);
        if (tagMapIt == m_tagMap.end()) {  //received a tag for the first time
            currTag = m_tagContFactory.makeTagCont(tag);
            m_tagMap[tag] = currTag;
        } else {
            currTag = tagMapIt->second;
        }
    }  //release tagmapmutex here

    // convert ISInfoAny to actual type
    auto hist = HistTraits<T>::from_any(info);

    timingColl.bin_count = HistTraits<T>::bin_count(*hist);

    // just print some info about histogram axes if debug is enabled
    ERS_DEBUG(2, m_myName << " Got new histo " << HistTraits<T>::axes_info(*hist)
              << " tag=" << tag << " provider=" << providerName);

    // do some basic checks, this will only print messages if it finds something
    _checkHistoLabels(*hist, providerName);

    // forward to tag-specific container
    bool published = currTag->tagOperate(std::move(hist), providerName, timingColl);
    if (published) {
        std::lock_guard<std::mutex> lock(m_tagMapMutex);
        _tagMapCleanup();
    }

    timingColl.save();
}

template <typename T>
void
HistogramCont<T>::execute()
{
    // periodically clean our mess
    ERS_DEBUG(2, "HistogramCont::execute: -- timer-based cleanup for " << m_myName);
    std::lock_guard<std::mutex> lock(m_tagMapMutex);
    _tagMapCleanup();
}

template <typename T>
void
HistogramCont<T>::_checkHistoLabels(Histogram& hist, const std::string& providerName)
{
    // check labeling consistency and print a message, only once per histogram
    if (m_checkLabelsOnce.exchange(false)) {

        // first update
        bool badLabels = HistTraits<T>::isWrongLabels(hist);
        if (badLabels) {
            ERS_LOG("Histogram " << m_myName << " from " << providerName << " tag=" << hist.tag()
                    << " has incompatible bin labeling:" << HistTraits<T>::axes_info(hist));
        }

        // remember if it was labeled
        m_labeledHisto = HistTraits<T>::isLabeled(hist);

    }

    // on subsequent calls check that histogram stays labeled, but stop checking after first mismatch
    // note that this is not protected so it can be triggered more than once
    if (not m_labelMismatchFound) {
        bool hasLabels = HistTraits<T>::isLabeled(hist);
        int wasLabeled = m_labeledHisto.exchange(hasLabels);
        if (hasLabels != wasLabeled) {
            m_labelMismatchFound = true;
            std::string mess = wasLabeled ? "had labels but doesn't have now!" : "didn't have labels but has now!";
            ERS_LOG("Histogram \"" << m_myName << "\" from provider \"" << providerName << "\" " << mess
                    << " current sum (bins,labels) " << HistTraits<T>::axes_info(hist));
        }
    }
}

template class MIG::HistogramCont<oh::Histogram>;
template class MIG::HistogramCont<oh::Efficiency>;
