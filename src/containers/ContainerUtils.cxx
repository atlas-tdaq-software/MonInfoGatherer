
//-----------------------
// This Class's Header --
//-----------------------
#include "ContainerUtils.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infodynany.h"
#include "is/type.h"

using namespace MIG;

namespace {

template<typename T>
void scaleVector(T& vec, double factor)
{
    for (size_t t = 0; t < vec.size(); t++) {
        vec[t] *= factor;
    }
}

}

// Scale contents of the info object.
void MIG::detail::scaleInfo(ISInfoDynAny& info, double factor)
{
    // scale every numeric (or numeric vector) attribute
    for (size_t k = 0; k < info.getAttributesNumber(); k++) {
        if (!info.isAttributeArray(k)) {
            switch (info.getAttributeType(k)) {
            case ISType::Float:
                info.getAttributeValue<float>(k) *= factor;
                break;
            case ISType::Double:
                info.getAttributeValue<double>(k) *= factor;
                break;
            case ISType::S32:
                info.getAttributeValue<int>(k) *= factor;
                break;
            case ISType::U32:
                info.getAttributeValue<unsigned int>(k) *= factor;
                break;
            case ISType::S64:
                info.getAttributeValue<int64_t>(k) *= factor;
                break;
            case ISType::U64:
                info.getAttributeValue<uint64_t>(k) *= factor;
                break;
            case ISType::Boolean:
                // TODO: scaling of booleans will never work correctly
                info.getAttributeValue<bool>(k) = info.getAttributeValue<bool>(k) && bool(factor);
                break;
            case ISType::S8:
                info.getAttributeValue<char>(k) *= factor;
                break;
            case ISType::U8:
                info.getAttributeValue<unsigned char>(k) *= factor;
                break;
            case ISType::S16:
                info.getAttributeValue<short>(k) *= factor;
                break;
            case ISType::U16:
                info.getAttributeValue<unsigned short>(k) *= factor;
                break;
            default:
                break;
            } //close switch
        } else { //vectors
            switch (info.getAttributeType(k)) {
            case ISType::Float:
                scaleVector(info.getAttributeValue<std::vector<float> >(k), factor);
                break;
            case ISType::Double:
                scaleVector(info.getAttributeValue<std::vector<double> >(k), factor);
                break;
            case ISType::S32:
                scaleVector(info.getAttributeValue<std::vector<int> >(k), factor);
                break;
            case ISType::U32:
                scaleVector(info.getAttributeValue<std::vector<unsigned int> >(k), factor);
                break;
            case ISType::S64:
                scaleVector(info.getAttributeValue<std::vector<int64_t> >(k), factor);
                break;
            case ISType::U64:
                scaleVector(info.getAttributeValue<std::vector<uint64_t> >(k), factor);
                break;
            // Boolean vectors can't be scaled
            // case ISType::Boolean:
            //   scaleVector(info.getAttributeValue<std::vector<bool> >(k),factor);
            //   break;
            case ISType::S8:
                scaleVector(info.getAttributeValue<std::vector<char> >(k), factor);
                break;
            case ISType::U8:
                scaleVector(info.getAttributeValue<std::vector<unsigned char> >(k), factor);
                break;
            case ISType::S16:
                scaleVector(info.getAttributeValue<std::vector<short> >(k), factor);
                break;
            case ISType::U16:
                scaleVector(info.getAttributeValue<std::vector<unsigned short> >(k), factor);
                break;
            default:
                break;
            }     //close switch
        }
    }
}
