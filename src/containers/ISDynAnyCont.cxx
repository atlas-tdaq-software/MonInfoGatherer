
//-----------------------
// This Class's Header --
//-----------------------
#include "ISDynAnyCont.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ContainerUtils.h"
#include "is/infoany.h"
#include "is/infodynany.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "ers/ers.h"
#include "../algos/ISSum.h"

using namespace MIG;

std::atomic<int> ISDynAnyCont::Accumulator::maxMessageCount(32);

ISDynAnyCont::ISDynAnyCont(const std::string &name,
                           const ISType& isType,
                           std::unique_ptr<ISSum> alg,
                           InfoPublisher& publisher,
                           const ConfigParameters& parameters)
        : m_myName(name),
          m_alg(std::move(alg)),
          m_publisher(publisher),
          m_updateCutoff(parameters.EOPRecoveryDelay(2500)),
          m_publishSum(parameters.boolValue("PublishSum", true)),
          m_publishAvg(parameters.boolValue("PublishAverage", false)),
          m_suffixSum(parameters.value("SuffixSum")),
          m_suffixAvg(parameters.value("SuffixAverage", "_avg")),
          m_acc(isType)
{
}

ISDynAnyCont::~ISDynAnyCont()
{
}

ISDynAnyCont::Accumulator::Accumulator(const ISType& isType_)
    : isType(isType_),
      sum(IPCPartition(), isType)
{
}

void ISDynAnyCont::Accumulator::reset()
{
    // bring every member to inital state, except timestamps that are used across cycles
    providers.clear();
    annotations.reset();
}

void ISDynAnyCont::Accumulator::reset(ISInfoAny& info, unsigned provider)
{
    // use histogram as inital accumulator value
    info >>= sum;

    providers.clear();
    providers.add(provider);

    if (provider >= timestamps.size()) {
        timestamps.resize(provider+1);
    }
    timestamps[provider] = (int64_t)sum.time().total_mksec_utc();;

    annotations.reset();
    annotations.merge(sum.annotations());
}

void ISDynAnyCont::Accumulator::add(ISInfoAny& info, unsigned provider, ISSum& alg)
{
    int64_t timestamp = (int64_t)info.time().total_mksec_utc();
    if (providers.empty()) {
        // use histogram as inital accumulator value
        info >>= sum;
    } else {
        // this can throw, will be handled by caller
        ISInfoDynAny dinfo(IPCPartition(), isType);
        info >>= dinfo;
        alg.operate(dinfo, sum);
    }
    providers.add(provider);
    // remember its timestamp
    if (provider >= timestamps.size()) {
        timestamps.resize(provider+1);
    }
    timestamps[provider] = timestamp;

    annotations.merge(info.annotations());
}

// check timestamp sanity against stored timestamps
bool ISDynAnyCont::Accumulator::checkTimestamp(int64_t time, unsigned provider,
                    std::string const& objectName, std::string const& providerName)
{
    if (timestamps.size() < provider+1) {
        // never seen this guy before, no previous timestamp
        return true;
    }

    // protect against double subscription (we have seen this in the past,
    // it may not ever happen again, but just make sure out data is sane)
    if (time == timestamps[provider]) {
        if (maxMessageCount > 0) {
            MIG::ISDuplicateTimestamp issue(ERS_HERE, objectName, providerName, timestamps[provider], time);
            ers::warning(issue);
            -- maxMessageCount;
        }
        // tell it to stop right here, duplicates will mess things badly
        return false;
    }

    // TODO: could check other conditions as well, e.g. time difference
    // between publications

    return true;
}

void ISDynAnyCont::publish()
{
    // update annotations
    m_acc.annotations.update(m_acc.sum.annotations());

    //publish info
    publish(m_acc.sum, m_myName, m_acc.providers.size());

    //copy published list to past updates list
    m_expectedProviders = m_acc.providers;

    //reset publishers
    m_acc.reset();
}

// Publish result of a summing
void ISDynAnyCont::publish(ISInfoDynAny& info, std::string const& name, int nUpdates)
{
    // publish sum first
    if (m_publishSum) {
        m_publisher.publish(info, name+m_suffixSum, nUpdates);
    }

    if (m_publishAvg) {
        // scale every numeric (or numeric vector) attribute
        if (nUpdates > 0) {
            double factor = 1.0 / nUpdates;
            detail::scaleInfo(info, factor);
        }
        // publish scaled info
        m_publisher.publish(info, name+m_suffixAvg, nUpdates);
    }
}

void ISDynAnyCont::operate(ISInfoAny &info, const std::string& providerName)
{
    //ERS_DEBUG(2,m_myName+std::string(" Got callback for ")+providerName);
    //ERS_LOG("Got operate ISDynAny "<<m_myName<<" from provider "<< providerName);

    // get provider index, this has internal synchronization
    unsigned provider, nProviders;
    bool newProvider;
    std::tie(provider, nProviders, newProvider) = m_providerMap.index(providerName);

    // lock all structures
    std::lock_guard<std::mutex> tl(m_updateMutex);

    // check timestamps and fiter duplicate publications
    if (not m_acc.checkTimestamp(int64_t(info.time().total_mksec_utc()), provider, m_myName, providerName)) {
        return;
    }

    if (m_expectedProviders.empty()) {
        // very first contribution, just use it as initial for accumulator,
        // do not publish.
        m_acc.reset(info, provider);

        m_expectedProviders.add(provider);
        return;
    }

    if (m_acc.providers.empty()) {
        // if this is the first info after the update
        // skip things that appear too soon (they belong to this same cycle)
        auto cutoff_mksec = std::chrono::duration_cast<std::chrono::microseconds>(m_updateCutoff);
        if (std::abs(int64_t(info.time().total_mksec_utc()) -
            int64_t(m_acc.sum.time().total_mksec_utc())) > cutoff_mksec.count()) {
            // if new info is produced at least 2.5 seconds
            m_acc.reset(info, provider);
            m_expectedProviders.add(provider);
        } else {
            // still remember it as if it was seen before
            m_expectedProviders.add(provider);
        }
        return;
    }

    if (m_acc.providers.contains(provider)) {
        //we see the same provider a second time? -> publish and start next cycle

        //publish anyway even if it is an incomplete sum.
        publish();

        // start next cycle with new object
        m_acc.reset(info, provider);

        return;
    }

    // do merging
    m_expectedProviders.add(provider);
    try {
        m_acc.add(info, provider, *m_alg);
    } catch (std::exception const& exc) {
        throw AlgorithmIssue(ERS_HERE, providerName, m_myName, 0, exc);
    }

    if (m_acc.providers == m_expectedProviders && not newProvider) {
        // if we reached expected number of providers then publish
        //don't publish if it is a new provider
        try {
            publish();
        } catch (const std::exception& ex) {
            ERS_LOG(" caught exception in publish(): " << ex.what());
        }
    }
}
