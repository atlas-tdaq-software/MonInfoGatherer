//
// Implementation of Annotations
//

//-----------------------
// This Class's Header --
//-----------------------
#include "Annotations.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <array>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "boost/lexical_cast.hpp"

using namespace MIG;

namespace {

const std::string level0 = "providers";    // annotations key for lowest level count
const std::string level_prefix = "mig.lvl";   // annotations key for other levels (followed by a number)

const int MAX_MIG_LEVEL = 16;  // Reasonable limit on number of merging levels

// extract level number from the annotation key
int annKeyLevel(const std::string& key)
{
    if (key == level0) {
        return 0;
    }
    if (key.compare(0, level_prefix.size(), level_prefix) == 0) {
        try {
            int lvl = boost::lexical_cast<unsigned>(key.substr(level_prefix.size()));
            // constrain the range
            if (lvl > MAX_MIG_LEVEL) {
                return -1;
            }
            return lvl;
        } catch (const boost::bad_lexical_cast& exc) {
            // just ignore it
            return -1;
        }
    }
    return -1;
}

// Parse counters in IS annotation and convert to numbers. Using arrays instead of vectors
unsigned parse_counters(const ISInfo::Annotations& ann, std::array<unsigned, MAX_MIG_LEVEL>& counters)
{
    std::fill(counters.begin(), counters.end(), 0U);
    unsigned nLevels = 0;
    for (auto& pair: ann) {
        int lvl = ::annKeyLevel(pair.first);
        if (lvl >= 0) {
            try {
                unsigned counter = pair.second.getValue<unsigned>();
                if (nLevels < unsigned(lvl + 1)) {
                    nLevels = lvl + 1;
                }
                counters[lvl] = counter;
            } catch (const daq::is::ValueConversionFailed& exc) {
                // malformed annotation, ignore
            }
        }
    }
    return nLevels;
}

} // namespace


void Annotations::reset()
{
    m_counters.clear();
    m_contrib_count = 0;
}

void Annotations::merge(const ISAnnotations& ann)
{
    std::array<unsigned, MAX_MIG_LEVEL> counters;
    auto nLevels = ::parse_counters(ann, counters);
    _merge(nLevels, counters.data());
    ++ m_contrib_count;
}

Annotations& Annotations::operator+=(const Annotations& other)
{
    _merge(other.m_counters.size(), other.m_counters.data());
    m_contrib_count += other.m_contrib_count;
    return *this;
}

void Annotations::update(ISAnnotations& ann)
{
    // Assumption here is that levels that already exist in annotations
    // are a subset of levels collected in this object, so we do not need
    // to clear anyhting from a map.
    // NOTE: iteration is done one-past-end of the vector, on purpose
    for (unsigned lvl = 0; lvl <= m_counters.size(); ++lvl) {
        unsigned counter = lvl < m_counters.size() ? m_counters[lvl] : m_contrib_count;
        ISAnnotation value(std::to_string(counter));
        if (lvl == 0) {
            ann.insert_or_assign(::level0, std::move(value));
        } else {
            ann.insert_or_assign(::level_prefix+std::to_string(lvl), std::move(value));
        }
    }
}

void Annotations::_merge(unsigned nLevels, unsigned const* counters)
{
    // need to handle complicated cases of mis-aligned annotations.

    if (nLevels == 0 and not m_counters.empty()) {
        // Looks like this new contribution skipped one or more levels of
        // gatherers, this should be treated as {providers=1}.
        ++ m_counters[0];
        return;
    }

    if (m_contrib_count > 0 and m_counters.empty() and nLevels > 0) {
        // means that contributions that came before skipped gatherers, each
        // of them should be treated as {providers=1}.
        m_counters.push_back(m_contrib_count);
    }

    if (m_counters.size() < nLevels) {
        // extend it to fit new data
        m_counters.resize(nLevels, 0);
    }
    for (unsigned lvl = 0; lvl != nLevels; ++ lvl) {
        m_counters[lvl] += counters[lvl];
    }
}
