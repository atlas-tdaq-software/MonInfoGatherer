// -*- c++ -*-
#ifndef MONINFOGATHERER_ISDYNANYCONT_H
#define MONINFOGATHERER_ISDYNANYCONT_H

//-----------------
// C/C++ Headers --
//-----------------
#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "MonInfoGatherer/ContainerBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Annotations.h"
#include "MonInfoGatherer/ProviderMap.h"
#include "MonInfoGatherer/SmallIntSet.h"
#include "is/infodynany.h"
#include "is/type.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class ConfigParameters;
class ISSum;
class InfoPublisher;
}

namespace MIG {

/**
 *  Container class for IS objects.
 *
 *  This container class calls the algorithm (usually ISSum) to update
 *  object data and publishes resulting summed object.
 */
class ISDynAnyCont: public ContainerBase {
public:

    /**
     *  Constructor.
     *  @param name name of the IS object
     *  @param objType IS type of the object
     *  @param Alg pointer to algorithm that is going to operate on the updates of this histogram.
     *  @param publisher Object which publishes summed data.
     *  @param parameters Configuration parameters
     */
    ISDynAnyCont(const std::string &name,
                 const ISType& objType,
                 std::unique_ptr<ISSum> alg,
                 InfoPublisher& publisher,
                 const ConfigParameters& parameters);
    ~ISDynAnyCont();

    void operate(ISInfoAny& info, const std::string& providerName) override;

    // these methods are for testing purposes only
    int numUpdatesSincePublish(int t=-1) const { return m_acc.providers.size(); }
    int numUpdatingProviders(int t=-1) const { return m_expectedProviders.size(); }

protected:

    // Publish result of a summing. Note that implementation is allowed
    // to change the content of `info` object, there should be no assumptions
    // in this class about content of `info` after this call.
    virtual void publish(ISInfoDynAny& info, std::string const& name, int nUpdates);

private:

    // publish summed info and reset internal state for next cycle
    void publish();

    // Allow access to internals from unit tests
    template <typename T> friend class MIGUnitTestFriendHack;

    struct Accumulator {

        Accumulator(const ISType& isType_);

        // reset to initial state
        void reset();

        // reset to initial state, and add one contribution
        void reset(ISInfoAny& sum, unsigned provider);

        // add one contribution
        void add(ISInfoAny& sum, unsigned provider, ISSum& alg);

        // check timestamp sanity against stored timestamps
        // returns true if it is OK, false if publication needs to be skipped
        bool checkTimestamp(int64_t time, unsigned provider,
                            std::string const& objectName, std::string const& providerName);

        const ISType isType;
        ISInfoDynAny sum;  // accumulator for contributions
        Annotations annotations;
        SmallIntSet providers;              // providers with accumulated contributions
        std::vector<int64_t> timestamps;    // microseconds since epoch for publication
        static std::atomic<int> maxMessageCount;  // max. number of messages to generate in checkTimestamp()
    };

    std::string m_myName;
    std::unique_ptr<ISSum> m_alg;
    InfoPublisher& m_publisher;
    std::chrono::milliseconds m_updateCutoff;
    const bool m_publishSum;
    const bool m_publishAvg;
    const std::string m_suffixSum;
    const std::string m_suffixAvg;
    std::mutex m_updateMutex;
    ProviderMap m_providerMap;
    SmallIntSet m_expectedProviders;        // providers expected to be seen in current cycle
    Accumulator m_acc;
};
} // namespace MIG

#endif // MONINFOGATHERER_ISDYNANYCONT_H
