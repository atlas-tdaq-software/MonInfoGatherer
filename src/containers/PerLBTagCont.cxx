//
// Implementation of PerLBTagCont
//

//-----------------------
// This Class's Header --
//-----------------------
#include "PerLBTagCont.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Annotations.h"
#include "is/infoany.h"
#include "oh/core/DataTypes.h"
#include "oh/core/EfficiencyData.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/LabelMap.h"
#include "MonInfoGatherer/ProviderMap.h"
#include "MonInfoGatherer/Timers.h"
#include "ers/ers.h"
#include "../algos/HistoAlgorithm.h"

using namespace MIG;

namespace {
template <typename T>
struct HistTraits {
};

template <>
struct HistTraits<oh::Histogram> {
    static const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Histogram>& hist) {
        return hist.axes;
    }
};

template <>
struct HistTraits<oh::Efficiency> {
    static const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Efficiency>& hist) {
        return hist.total.axes;
    }
};

}


template <typename T>
PerLBTagCont<T>::PerLBTagCont(const std::string &name,
                              HistoAlgorithm& alg,
                              InfoPublisher& publisher,
                              ProviderMap& providerMap,
                              const ConfigParameters& parameters)
  : m_myName(name),
    m_alg(alg),
    m_publisher(publisher),
    m_providerMap(providerMap),
    m_publishDelay(parameters.PerLBPublishDelay()),
    m_minCompressSizeBytes(parameters.uintValue("MinCompressSizeBytes", 16384)),
    m_compressTargetPercent(parameters.uintValue("CompressTargetPercent", 75))
{
}

template <typename T>
bool PerLBTagCont<T>::tagOperate(std::unique_ptr<Histogram> histo,
                                 const std::string& providerName,
                                 TimingCollector& timingColl)
{
    // get provider index
    unsigned provider, nProviders;
    bool newProvider;
    std::tie(provider, nProviders, newProvider) = m_providerMap.index(providerName);

    // lock thyself
    timingColl.lock_wait_real_time.start();
    std::lock_guard<std::mutex> lock(m_mutex);
    timingColl.lock_wait_real_time.stop();

    // make sure enough space in array
    if (provider >= m_contributions.size()) {
        m_contributions.resize(provider+1);
    }

    // just remember it
    float compressTarget = m_compressTargetPercent / 100.;
    m_contributions[provider] = HistogramHolder<T>::makeHolder(std::move(histo),
                                                               m_minCompressSizeBytes,
                                                               compressTarget);
    ++m_numUpdatesSincePublish;
    m_lastUpdate = Clock::instance().now();

    // Because publishing happens asynchronously but HistogramCont still
    // needs to know (approximately) when something was published we fake it
    // by returning true on first call _after_ the publishing.
    return m_numUpdatesSincePublish == 1;
}

// method called periodically to do timeout-based stuff
template <typename T>
void PerLBTagCont<T>::execute()
{
    // lock thyself
    std::lock_guard<std::mutex> lock(m_mutex);

    // if time since last publication is above threshold then publish
    if (m_numUpdatesSincePublish > 0) {
        auto now = Clock::instance().now();
        if (now - m_lastUpdate >= m_publishDelay) {
            _publish();
        } else {
            // check also degenrate case when publishers go crazy and
            // start publishing more frequently than m_publishDelay
            int np = numUpdatingProviders();
            if (m_numUpdatesSincePublish > np*2) {
                ERS_LOG("Warning: detected frequent publishing: #updates=" << m_numUpdatesSincePublish
                        << " #providers=" << np);
                _publish();
            }
        }
    }
}

template <typename T>
void PerLBTagCont<T>::_publish()
{
    // Sum all contributions, we cannot reuse input data
    // for a sum so the first contribution should be cloned.
    // TODO: small optimization possible if there is only one contribution.
    std::shared_ptr<Histogram> sum;
    int count = 0;
    LabelMap labelMap;
    Annotations annotations;
    for (auto&& histoHolder: m_contributions) {
        if (histoHolder == nullptr) continue;
        try {
            // try to sum all of them but skip failing ones
            if (sum == nullptr) {
                // clone histogram as initial accumulator value
                sum = histoHolder->get(true);
                labelMap = LabelMap(HistTraits<T>::axes(*sum));
                annotations.merge(sum->annotations());
            } else {
                // this can throw, will be handled by caller
                auto histo = histoHolder->get();
                m_alg.operate(*histo, *sum, labelMap);
                annotations.merge(histo->annotations());
            }
            ++count;
        } catch (const std::exception& ex) {
            ERS_LOG("Warning: Caught exception while merging per-lb histogram " << m_myName
                    << " exception was " << ex.what());
        }
    }

    // reset it even if publisher raises an exception
    m_numUpdatesSincePublish = 0;

    if (count > 0) {
        annotations.update(sum->annotations());
        try{
            m_publisher.publish(*sum, m_myName, count);
        } catch (const std::exception& ex) {
            ERS_LOG("Warning: Caught exception while publishing per-lb histogram " << m_myName
                    << " exception was " << ex.what());
        }
    }
}

template class MIG::PerLBTagCont<oh::Histogram>;
template class MIG::PerLBTagCont<oh::Efficiency>;
