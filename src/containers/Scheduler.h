#ifndef MIG_SCHEDULER_H
#define MIG_SCHEDULER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Interface for entities that need to be scheduled periodicaly.
 */
class Task {
public:
    virtual ~Task() {}

    /**
     *  Method called on this task to execute it.
     */
    virtual void execute() = 0;
};

/**
 *  Class representing a thread which wakes up all registered
 *  Dreamers periodically.
 *
 *  This class holds weak pointers to the Task instances so
 *  they can be destroyed by ourside code.
 */
class Scheduler {
public:

    /// Access to singleton instance
    static Scheduler& instance();

    /// If thread is still running then destructor will stop it first.
    ~Scheduler();

    /**
     *  Schedule task for periodic execution.
     * 
     *  Execution period is specified in milliseconds, exception is raised
     *  if period value is zero.
     */
    void add(std::shared_ptr<Task> const& task, std::chrono::milliseconds period);

    /// Forget all Dreamers
    void clear();

    /// Start running in a separate thread
    void start();

    /// Stop running
    void stop();

private:

    // make new instance and start running it
    Scheduler(bool start=true);

    // thread body function
    void run();

    // execute all tasks, this is only useful for unit tests (via MIGUnitTestFriendHack)
    void executeAllTasks();

    using Clock = std::chrono::steady_clock;

    struct ScheduledTask {
        std::weak_ptr<Task> task;
        Clock::duration period;
        Clock::time_point lastExecution;
    };

    std::vector<ScheduledTask> m_tasks;
    bool m_stop = false;
    std::mutex m_mutex;  // protects both m_tasks and m_stop
    std::condition_variable m_cond;
    std::thread m_thread;

    // Allow access to internals from unit tests
    template <typename T> friend class MIGUnitTestFriendHack;
};

} // namespace MIG

#endif // MIG_SCHEDULER_H
