// -*- c++ -*-
#ifndef MIG_HISTOGRAMTAGCONT_H
#define MIG_HISTOGRAMTAGCONT_H

//-----------------
// C/C++ Headers --
//-----------------
#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "HistogramTagContBase.h"
#include "Annotations.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/LabelMap.h"
#include "MonInfoGatherer/SmallIntSet.h"
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class ConfigParameters;
class HistoAlgorithm;
class InfoPublisher;
class ProviderMap;
class TimingCollector;
}

namespace MIG {

/**
 * This is a kind of container for individual histogram tags.
 * It is used internally by HistogramCont class, not suitable
 * for use outside that class.
 *
 * This class does not inherit from ContainerBase because it needs
 * more info passed from HistogramCont to operate() method
 */
template <typename T>
class HistogramTagCont : public HistogramTagContBase<T> {
public:

    using Histogram = oh::ObjectBase<T>;

    /**
     *  Make per-tag histogram container.
     *
     *  @param name name of the histogram
     *  @param alg pointer to algorithm that is going to operate on the updates of this histogram.
     *  @param publisher Object which publishes summed data.
     *  @param providerMap Map for provider names, reference is remembered.
     *  @param updateCutoff Time in mksec for in-cycle cutoff for new contributions
     */
    HistogramTagCont(const std::string &name,
                     HistoAlgorithm& alg,
                     InfoPublisher& publisher,
                     ProviderMap& providerMap,
                     const ConfigParameters& parameters);

    HistogramTagCont(const HistogramTagCont&) = delete;
    HistogramTagCont& operator=(const HistogramTagCont&) = delete;

    /**
     *  Receive new contribution from provider.
     *
     *  @see base class for documentation.
     */
    bool tagOperate(std::unique_ptr<Histogram> histo,
                    const std::string& providerName,
                    TimingCollector& timingColl) override;

    // these methods are for testing purposes only
    int numUpdatesSincePublish() const { return m_acc.providers.size(); }
    int numFinalProviders() const { return m_finalAcc.providers.size(); }
    int numUpdatingProviders() const { return m_expectedProviders.size(); }

private:

    // processing of final contributions
    bool _doFinal(std::unique_ptr<Histogram> info, unsigned provider, const std::string& providerName);

    struct Accumulator {

        // reset to initial state
        void reset();

        // reset to initial state, and add one contribution
        void reset(std::unique_ptr<Histogram> sum, unsigned provider);

        // add one contribution
        void add(std::unique_ptr<Histogram> sum, unsigned provider, HistoAlgorithm& alg);

        // check timestamp sanity against stored timestamps
        // returns true if it is OK, false if publication needs to be skipped
        bool checkTimestamp(int64_t time, unsigned provider,
                            std::string const& objectName, std::string const& providerName);

        std::unique_ptr<Histogram> sum;     // accumulator for contributions
        SmallIntSet providers;              // providers with accumulated contributions
        LabelMap labelMap;                  // label map for accumulated sum
        Annotations annotations;            // number of per-level histos from annotations
        std::vector<int64_t> timestamps;    // microseconds since epoch for publication
    };

    // publish current sum, must be called from protected section
    void _publish(Accumulator& acc) const;

    const std::string m_myName;
    HistoAlgorithm& m_alg;
    InfoPublisher& m_publisher;
    ProviderMap& m_providerMap; // shared with all other contaiers, thread-safe
    const std::chrono::milliseconds m_updateCutoff;
    const std::chrono::seconds m_finalPublishDeadband;  // Do not publish finals more frequently than this
    std::mutex m_tagMutex;    //individual mutexes per tag
    Accumulator m_acc;
    Accumulator m_finalAcc;
    SmallIntSet m_expectedProviders;        // providers expected to be seen in current cycle
    int64_t m_lastPublish = 0;    // microseconds since epoch for the time of last publish
    Clock::time_point m_finalPublishTime;    // time of final publish
};

} // namespace MIG

#endif // MIG_HISTOGRAMTAGCONT_H
