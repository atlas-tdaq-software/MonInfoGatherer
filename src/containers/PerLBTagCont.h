#ifndef MIG_PERLBTAGCONT_H
#define MIG_PERLBTAGCONT_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "HistogramHolder.h"
#include "HistogramTagContBase.h"
#include "Scheduler.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/LabelMap.h"
#include "MonInfoGatherer/SmallIntSet.h"
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class ConfigParameters;
class HistoAlgorithm;
class InfoPublisher;
class ProviderMap;
class TimingCollector;
}

namespace MIG {

/**
 * Tag container class for per-LB histograms.
 *
 * Per-LB histograms are different from regular histograms in that their
 * publishing is not guaranteed to be synchronize in the same way as for
 * regular histograms. As a result we need to keep their contributions
 * in memory and sum them at publishing time (publishing is timeout-based).
 */
template <typename T>
class PerLBTagCont : public HistogramTagContBase<T>, public Task {
public:

    using Histogram = oh::ObjectBase<T>;

    /**
     *  Make per-tag histogram container.
     *
     *  @param name name of the histogram
     *  @param alg pointer to algorithm that is going to operate on the updates of this histogram.
     *  @param publisher Object which publishes summed data.
     *  @param providerMap Map for provider names, reference is remembered.
     *  @param parameters Configuration parameters
     *  @param clock Clock instance providing current time
     */
    PerLBTagCont(const std::string &name,
                 HistoAlgorithm& alg,
                 InfoPublisher& publisher,
                 ProviderMap& providerMap,
                 const ConfigParameters& parameters);

    PerLBTagCont(const PerLBTagCont&) = delete;
    PerLBTagCont& operator=(const PerLBTagCont&) = delete;

    /**
     *  Receive new contribution from provider.
     *
     *  @see base class for documentation.
     */
    bool tagOperate(std::unique_ptr<Histogram> histo,
                    const std::string& providerName,
                    TimingCollector& timingColl) override;

    /**
     * Implementation of Task::execute().
     *
     * This method is called periodically to handle timeout-based
     * summing and publishing.
     */
    void execute() override;

    // these methods are for testing purposes only
    int numUpdatesSincePublish(int t=-1) const { return m_numUpdatesSincePublish; }
    int numUpdatingProviders(int t=-1) const {
        return m_contributions.size() -
            std::count(m_contributions.begin(), m_contributions.end(), nullptr);
    }

private:

    // publish current sum, must be called from protected section
    void _publish();

    const std::string m_myName;
    HistoAlgorithm& m_alg;
    InfoPublisher& m_publisher;
    ProviderMap& m_providerMap; // shared with all other containers, thread-safe
    const Clock::duration m_publishDelay;
    unsigned m_minCompressSizeBytes;
    unsigned m_compressTargetPercent;
    std::mutex m_mutex;    //individual mutexes per tag
    std::vector<std::unique_ptr<HistogramHolder<T>>> m_contributions;  // last contribution from each provider
    int m_numUpdatesSincePublish = 0;  // how many times tagOperate() was called since _publish()
    Clock::time_point m_lastUpdate;    // time of last tagOperate() call
};

} // namespace MIG

#endif // MIG_PERLBTAGCONT_H
