
//-----------------------
// This Class's Header --
//-----------------------
#include "ISDynAnyNGCont.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Annotations.h"
#include "ContainerUtils.h"
#include "is/infoany.h"
#include "is/infodynany.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "ers/ers.h"
#include "../algos/ISSum.h"

using namespace MIG;

ISDynAnyNGCont::ISDynAnyNGCont(const std::string &name,
                               const ISType& isType,
                               std::unique_ptr<ISSum> alg,
                               InfoPublisher& publisher,
                               const ConfigParameters& parameters)
  : m_myName(name),
    m_isType(isType),
    m_alg(std::move(alg)),
    m_publisher(publisher),
    m_publishDelay(parameters.ISDynAnyPublishDelay()),
    m_missingCycles(parameters.uintValue("ISDynAnyCyclesMissing", 3)),
    m_publishSum(parameters.boolValue("PublishSum", true)),
    m_publishAvg(parameters.boolValue("PublishAverage", false)),
    m_suffixSum(parameters.value("SuffixSum")),
    m_suffixAvg(parameters.value("SuffixAverage", "_avg")),
    m_periodCutoff(parameters.uintValue("PeriodCutoff_sec", 300))
{
}

ISDynAnyNGCont::~ISDynAnyNGCont()
{
}

void ISDynAnyNGCont::operate(ISInfoAny &info, const std::string& providerName)
{
    // get provider index
    unsigned provider, nProviders;
    bool newProvider;
    std::tie(provider, nProviders, newProvider) = m_providerMap.index(providerName);

    // lock thyself
    std::lock_guard<std::mutex> lock(m_updateMutex);

    // make sure enough space in array
    if (provider >= m_contributions.size()) {
        m_contributions.resize(provider+1);
    }
    if (m_contributions[provider].info == nullptr) {
        m_contributions[provider].info.reset(new ISInfoDynAny(IPCPartition(), m_isType));
        m_contributions[provider].periodFinder = PeriodFinder(m_periodCutoff);
    }

    // feed some data to period finder
    std::chrono::microseconds ts(info.time().total_mksec_utc());
    m_contributions[provider].periodFinder.add(ts);

    // just remember it
    info >>= *m_contributions[provider].info;

    // count updates
    for (auto&& contribution: m_contributions) {
        ++ contribution.nUpdatesSinceLastUpdate;
    }
    m_contributions[provider].nUpdatesSinceLastUpdate = 0;
    ++ m_contributions[provider].nUpdatesSincePublish;

    ++m_numUpdatesSincePublish;
    m_lastUpdate = Clock::instance().now();
    m_contributions[provider].lastUpdate = m_lastUpdate;

    // See if we have full set of providers updated since last _publish,
    // and publish if yes, but bootstrap needs some care.
    if (not newProvider) {
        bool all_providers = true;
        for (auto&& contribution: m_contributions) {
            if (contribution.info != nullptr and contribution.nUpdatesSincePublish == 0) {
                // expected a contribution but have not received it
                all_providers = false;
                break;
            }
        }
        if (all_providers) {
            // looks like all contributions were updated
            _publish(lock);
        }
    }
}

// method called periodically to do timeout-based stuff
void ISDynAnyNGCont::execute()
{
    // lock thyself
    std::lock_guard<std::mutex> lock(m_updateMutex);

    // in this method there should not be full set of contributions, that case
    // is handled by operae() method above. Ideally we want to publish with
    // the same rate as the initial providers, but there is no guarantee that
    // all contributions arrive at the same rate (dynamics can be complex
    // especially if synchronization is broken). Here we guess an estimate of
    // the provider publishing rate and wait for max of half period before
    // publishing the merged result.

    if (m_numUpdatesSincePublish == 0) {
        // no updates
        return;
    }

    auto const period = _guessPeriod();
    auto const zero = std::chrono::seconds::zero();
    auto const now = Clock::instance().now();

    // if we cannot determine period (too few publications arrived, should
    // only matter during ramp-up) then wait for max m_publishDelay since last
    // update before publishing.
    if (period == zero) {
        ERS_DEBUG(2, "execute: zero period");
        if (now - m_lastUpdate >= m_publishDelay) {
            ERS_DEBUG(2, "execute: zero period, exceeds delay");
            _publish(lock);
        }
        return;
    }

    // if too litle time passed since last update then wait longer
    if (now - m_lastUpdate < m_publishDelay) {
        ERS_DEBUG(2, "execute: skip, too short delay since last update");
        return;
    }

    // Otherwise wait for period/2 since previous publication
    if (now - m_lastPublish >= period / 2) {
        ERS_DEBUG(2, "execute: exceeds half period");
        _publish(lock);
    }
}

void ISDynAnyNGCont::_publish(std::lock_guard<std::mutex> const& /*ensure_lock*/)
{
    unsigned numProviders = std::count_if(
        m_contributions.cbegin(), m_contributions.cend(),
        [](auto& c) {return c.info != nullptr; });

    if (numProviders > 1) {
        // if some provider is missing for few cycles presume it's dead
        for (auto&& contribution: m_contributions) {
            if (contribution.info == nullptr) continue;

            auto const now = Clock::instance().now();
            auto const zero = std::chrono::seconds::zero();
            auto period = _guessPeriod();
            ERS_DEBUG(2, "_publish: combined period = " << period.count() << " sec");
            bool dead = false;
            if (period != zero) {
                dead = now - contribution.lastUpdate > m_missingCycles * period;
            } else {
                // Try to guess the same by counting updates from all
                // providers since the last update
                dead = contribution.nUpdatesSinceLastUpdate > m_missingCycles * (numProviders - 1);
            }
            ERS_DEBUG(2, "_publish: provider dead  = " << dead);
            if (dead) {
                contribution.info.reset(nullptr);
                contribution.periodFinder.reset();
            }
        }
    }

    // Sum all contributions.
    ISInfoDynAny sum;
    Annotations annotations;
    int count = 0;
    for (auto&& contribution: m_contributions) {
        if (contribution.info == nullptr) continue;
        try {
            // try to sum all of them but skip failing ones
            if (count == 0) {
                // make a copy
                sum = *contribution.info;
            } else {
                // this can throw, will be handled by caller
                m_alg->operate(*contribution.info, sum);
            }
            annotations.merge(contribution.info->annotations());
            ++count;
        } catch (const std::exception& ex) {
            ERS_LOG("Warning: Caught exception while merging ISInfoDynAny " << m_myName
                    << " exception was " << ex.what());
        }
    }

    // reset it even if publisher raises an exception
    m_numUpdatesSincePublish = 0;
    for (auto&& contribution: m_contributions) {
        if (contribution.nUpdatesSincePublish > 0) {
            // to avoid some aliasing behavior we reset this counter for all
            // contributions that were updated in this cycle.
            contribution.nUpdatesSinceLastUpdate = 0;
        }
        contribution.nUpdatesSincePublish = 0;
    }

    if (count > 0) {

        // update annotations
        annotations.update(sum.annotations());
        try {
            //publish info
            _publish(sum, m_myName, count);
        } catch (const std::exception& ex) {
            ERS_LOG("Warning: Caught exception while merging ISInfoDynAny " << m_myName
                    << " exception was " << ex.what());
        }
    }
}

// Publish result of a summing
void ISDynAnyNGCont::_publish(ISInfoDynAny& info, std::string const& name, int nUpdates)
{
    // publish sum first
    if (m_publishSum) {
        m_publisher.publish(info, name + m_suffixSum, nUpdates);
    }

    if (m_publishAvg) {
        // scale every numeric (or numeric vector) attribute
        if (nUpdates > 0) {
            double factor = 1.0 / nUpdates;
            detail::scaleInfo(info, factor);
        }
        // publish scaled info
        m_publisher.publish(info, name + m_suffixAvg, nUpdates);
    }

    m_lastPublish = Clock::instance().now();
}

std::chrono::seconds ISDynAnyNGCont::_guessPeriod() const
{
    // We need to determine a reasonable period for publishing merged data.
    // Ideally all input providers have the same period but there may be cases
    // when some providers publish more frequently (could happen when
    // publications are not synchronized and there is an intermediate Gatherer
    // which does not have enough statistics to guess correct value).

    // determine min/max period across providers (but only those providers
    // that know the answer)
    auto const zero = std::chrono::seconds::zero();
    std::chrono::seconds min_period = zero;
    std::chrono::seconds max_period = zero;
    for (auto&& contribution: m_contributions) {
        auto period = contribution.periodFinder.period();
        ERS_DEBUG(2, "_guessPeriod: provider period = " << period.count() << " sec");
        if (period == zero) {
            continue;
        }
        if (period > max_period) {
            max_period = period;
        }
        if (min_period == zero or period < min_period) {
            min_period = period;
        }
    }
    // check if all providers have the same period (which could be
    // zero/unknown)
    ERS_DEBUG(2, "_guessPeriod: provider min_period = " << min_period.count() << " sec");
    ERS_DEBUG(2, "_guessPeriod: provider max_period = " << max_period.count() << " sec");
    if (max_period == min_period) {
        return min_period;
    }

    // If there is difference there is no clear answer which one to chose
    // because publication may not be even periodic. What we do here is to
    // combine statistics from all providers and hope that higher statistics
    // will get us better result.
    PeriodFinder pf;
    for (auto&& contribution: m_contributions) {
        pf.merge(contribution.periodFinder);
    }
    auto period = pf.period();
    ERS_DEBUG(2, "_guessPeriod: combined period = " << period.count() << " sec");
    return period;
}
