//
// Implementation of MonInfoGathererHistoramHandler class
//

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/DefaultHandler.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <regex>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ISDynAnyCont.h"
#include "ISDynAnyNGCont.h"
#include "ers/ers.h"
#include "is/infoany.h"
#include "oh/core/Histogram.h"
#include "oh/core/Efficiency.h"
#include "HistogramCont.h"
#include "../algos/HistoAlgorithm.h"
#include "../algos/ISSum.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/Clock.h"


using namespace MIG;

namespace {

// check whether we want to use ISDynAnyNG container
bool _useNG(const std::string& name, const ConfigParameters& parameters)
{
    // get options from configuration
    auto options = parameters.values("ISDynAnyNG");

    if (options.empty()) {
        // use NG by default
        return true;
    }

    // if option matches object name then use NG
    bool useNG = false;
    for (auto&& option: options) {
        // empty option is OK, means to disable default if it the only option
        // but other matches can still be used.
        if (not option.empty()) {
            // should be a regular expression
            try {
                std::regex re(option);
                if (std::regex_match(name, re)) {
                    useNG = true;
                    break;
                }
            } catch (const std::regex_error& exc) {
                ERS_LOG("Failed to compile ISDynAnyNG regexp \"" << option << "\": " << exc.what());
            }
        }
    }

    return useNG;
}

}


DefaultHandler::DefaultHandler(const HandlerConfig& handlerConfig, InfoPublisher& publisher)
    : HandlerCommonBase(handlerConfig, publisher)
{
}

DefaultHandler::~DefaultHandler()
{
    if (m_failedObjectNames.size()) {
        std::stringstream oss;
        for (auto pair: m_failedObjectNames) {
            oss << "\"" << pair.first << "\" issue=\"" << pair.second << "\"\n";
        }
        ers::error(FailedObjects(ERS_HERE, oss.str()));
    }
}

void DefaultHandler::updateInfo(ISInfoAny& info, const std::string &name,
                                const std::string& providerName)
{
    if (m_failedObjectNames.find(name) != m_failedObjectNames.end())
        return;

    try {
        // forward to implementation
        _updateInfo_exc(info, name, providerName);
    } catch (std::exception const& exc) {
        // remember failed object name
        m_failedObjectNames.insert(std::make_pair(name, exc.what()));
        ers::error(HandlerError(ERS_HERE, providerName, name, exc));
    } catch (...) {
        // remember failed object name
        m_failedObjectNames.insert(std::make_pair(name, "Unknown exception"));
        ERS_LOG("Caught Unknown exception when updating histo name= " << name << " provider= "
                << providerName);
    }

}

std::shared_ptr<ContainerBase>
DefaultHandler::makeContainer(const std::string& name, const ISType& currType,
                              InfoPublisher& publisher, const ConfigParameters& parameters)
{
    std::shared_ptr<ContainerBase> newCont;
    if (currType.subTypeOf(oh::Histogram::type())) {
        auto alg = std::make_unique<HistoAlgorithm>(parameters);
        auto histCont = std::make_shared<HistogramCont<oh::Histogram>>(name, std::move(alg), publisher, parameters);
        // Schedule for wakeups every second
        Scheduler::instance().add(histCont, std::chrono::seconds(1));
        newCont = histCont;
    } else if (currType.subTypeOf(oh::Efficiency::type())) {
        auto alg = std::make_unique<HistoAlgorithm>(parameters);
        auto histCont = std::make_shared<HistogramCont<oh::Efficiency>>(name, std::move(alg), publisher, parameters);
        // Schedule for wakeups every second
        Scheduler::instance().add(histCont, std::chrono::seconds(1));
        newCont = histCont;
    } else {
        // not a histogram type, use ISSum algo
        auto alg = std::make_unique<ISSum>(parameters);
        if (_useNG(name, parameters)) {
            auto isCont = std::make_shared<ISDynAnyNGCont>(name, currType, std::move(alg), publisher,
                                                           parameters);
            // Schedule for wakeups every half second
            Scheduler::instance().add(isCont, std::chrono::milliseconds(500));
            newCont = isCont;
        } else {
            newCont = std::make_shared<ISDynAnyCont>(name, currType, std::move(alg), publisher, parameters);
        }
    }

    return newCont;
}
