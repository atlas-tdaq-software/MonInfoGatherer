// -*- c++ -*-
#ifndef MONINFOGATHERER_CONTAINERUTILS_H
#define MONINFOGATHERER_CONTAINERUTILS_H

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoDynAny;

/**
 *  Bunch of common utility methods used by container classes.
 */
namespace MIG {
namespace detail {

/**
 *  Scale contents of the info object.
 *
 *  Every numeric attribute, including numeric arrays, is scaled by the
 *  same factor, operation is performed in-place.
 */
void scaleInfo(ISInfoDynAny& info, double factor);

}} // namespace MIG::detail

#endif // MONINFOGATHERER_CONTAINERUTILS_H
