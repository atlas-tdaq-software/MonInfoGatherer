//
// Implementation of HistogramTagCont
//

//-----------------------
// This Class's Header --
//-----------------------
#include "HistogramTagCont.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infoany.h"
#include "oh/core/DataTypes.h"
#include "oh/core/EfficiencyData.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/LabelMap.h"
#include "MonInfoGatherer/ProviderMap.h"
#include "MonInfoGatherer/Timers.h"
#include "ers/ers.h"
#include "../algos/HistoAlgorithm.h"

using namespace MIG;

namespace {

bool isFinal(const oh::Object& hist)
{
    auto&& annotations = hist.annotations();
    return annotations.find("FINAL") != annotations.end();
}

// max. number of messages to generate in checkTimestamp()
std::atomic<int> maxMessageCount(32);

template <typename T>
struct HistTraits {
};

template <>
struct HistTraits<oh::Histogram> {

    // check labeling consistency
    static void check_labels(const oh::ObjectBase<oh::Histogram>& hist, const std::string& name) {
        for (unsigned int i = 0; i < hist.dimension; i++) {
            const std::vector<std::string> &labels = hist.get_labels((oh::Axis::ID) i);
            if (labels.size() > (size_t) hist.get_bin_count((oh::Axis::ID) i)) {
                ERS_LOG("Error in labels. Labels > bins @ axis" << i << " hist " << name);
            }
        }
    }

    static const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Histogram>& hist) {
        return hist.axes;
    }
};

template <>
struct HistTraits<oh::Efficiency> {

    // check labeling consistency
    static void check_labels(const oh::ObjectBase<oh::Efficiency>& hist, const std::string& name) {
        HistTraits<oh::Histogram>::check_labels(hist.total, name);
    }

    static const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Efficiency>& hist) {
        return hist.total.axes;
    }
};


} // namespace



template <typename T>
HistogramTagCont<T>::HistogramTagCont(const std::string &name,
                                      HistoAlgorithm& alg,
                                      InfoPublisher& publisher,
                                      ProviderMap& providerMap,
                                      const ConfigParameters& parameters)
        : m_myName(name),
          m_alg(alg),
          m_publisher(publisher),
          m_providerMap(providerMap),
          m_updateCutoff(parameters.EOPRecoveryDelay()),
          m_finalPublishDeadband(parameters.uintValue("FinalPublishDeadband_sec", 30))
{
}

template <typename T>
void HistogramTagCont<T>::Accumulator::reset()
{
    // bring every member to inital state, except timestamps that are used across cycles
    sum = nullptr;
    providers.clear();
    labelMap.clear();
    annotations.reset();
}

template <typename T>
void HistogramTagCont<T>::Accumulator::reset(std::unique_ptr<Histogram> histo, unsigned provider)
{
    // use histogram as inital accumulator value
    sum = std::move(histo);

    labelMap = LabelMap(HistTraits<T>::axes(*sum));

    annotations.reset();
    annotations.merge(sum->annotations());

    providers.clear();
    providers.add(provider);

    if (provider >= timestamps.size()) {
        timestamps.resize(provider+1);
    }
    timestamps[provider] = (int64_t)sum->time().total_mksec_utc();;
}

template <typename T>
void HistogramTagCont<T>::Accumulator::add(std::unique_ptr<Histogram> histo, unsigned provider, HistoAlgorithm& alg)
{
    int64_t timestamp = (int64_t)histo->time().total_mksec_utc();
    if (sum == nullptr) {
        // use histogram as inital accumulator value
        sum = std::move(histo);
        labelMap = LabelMap(HistTraits<T>::axes(*sum));
        annotations.merge(sum->annotations());
    } else {
        // this can throw, will be handled by caller
        alg.operate(*histo, *sum, labelMap);
        annotations.merge(histo->annotations());
    }
    providers.add(provider);
    // remember its timestamp
    if (provider >= timestamps.size()) {
        timestamps.resize(provider+1);
    }
    timestamps[provider] = timestamp;
}

// check timestamp sanity against stored timestamps
template <typename T>
bool HistogramTagCont<T>::Accumulator::checkTimestamp(int64_t time, unsigned provider,
                    std::string const& objectName, std::string const& providerName)
{
    if (timestamps.size() < provider+1) {
        // never seen this guy before, no previous timestamp
        return true;
    }

    // protect against double subscription (we have seen this in the past,
    // it may not ever happen again, but just make sure out data is sane)
    if (time == timestamps[provider]) {
        if (maxMessageCount > 0) {
            MIG::ISDuplicateTimestamp issue(ERS_HERE, objectName, providerName, timestamps[provider], time);
            ers::warning(issue);
            -- maxMessageCount;
        }
        // tell it to stop right here, duplicates will mess things badly
        return false;
    }

    // TODO: could check other conditions as well, e.g. time difference
    // between publications

    return true;
}

template <typename T>
void HistogramTagCont<T>::_publish(Accumulator& acc) const
{
    if (acc.sum == nullptr) {
        ERS_LOG("Warning: accumulator is empty in publish()");
        return;
    }

    // If publishing a non-final accumulator but final is non-empty we want to
    // merge the statistics, but only if they don't have overlapping providers
    if (&acc == &m_acc and m_finalAcc.sum != nullptr) {
        if (acc.providers.isdisjoint(m_finalAcc.providers)) {
            try {
                m_alg.operate(*m_finalAcc.sum, *acc.sum, acc.labelMap);
                acc.annotations += m_finalAcc.annotations;
                acc.providers |= m_finalAcc.providers;
            } catch (std::exception const& exc) {
                // cannot sum, try to publish what we have but without final contributions
                ERS_LOG("Warning: failed to merge final and non-final accumulators,"
                        " will publish non-final only: " << exc.what());
            }
        }
    }

    unsigned const nUpdates = acc.providers.size();
    acc.annotations.update(acc.sum->annotations());

    try {
        m_publisher.publish(*acc.sum, m_myName, nUpdates);      //publish info
    } catch (const std::exception& ex) {
        ERS_LOG("Caught exception while publishing histogram " << m_myName
                << " exception was " << ex.what());
    } catch (...) {
        ERS_LOG("Caught a non std exception while publishing histogram " << m_myName
                << " exception was caught by (...) clause.");
    }

    // reset the accumulator
    acc.reset();
}

template <typename T>
bool HistogramTagCont<T>::_doFinal(std::unique_ptr<Histogram> info, unsigned provider, const std::string& providerName)
{
    if (m_finalAcc.providers.contains(provider)) {
        // Provider publishes final contribution twice? There is no way
        // to handle this inconsistent behavior correctly, just ignore it.
        ERS_LOG("Warning: Seen FINAL tag from same provider twice prov="
                << providerName << " hist= " << m_myName << " tag=" << info->tag()
                << " previous time=" << m_finalAcc.timestamps[provider]
                << " current time=" << (int64_t)info->time().total_mksec_utc());
        return false;
    }

    // add it to a final accumulator
    m_finalAcc.add(std::move(info), provider, m_alg);

    if (not m_expectedProviders.contains(provider)) {
        // Final publication for a provider which is totally new, this should
        // only happen for very short runs, we do not want publish it to avoid
        // "pub storm".
        return false;
    }

    // Remove provider from expected list
    m_expectedProviders.remove(provider);

    // If expected list is empty now then it means that all providers
    // published their final contributions, publish now
    if (m_expectedProviders.empty()) {

        auto now = Clock::instance().now();
        if (m_finalPublishTime + m_finalPublishDeadband > now) {
            // means that we are getting final pubs from new providers, have to
            // abort publications to avoid "pub storm".
            return false;
        }
        m_finalPublishTime = now;

        // We don't expect non-final contributions at this point, though it
        // may still happen in some cases for short runs.
        if (not m_acc.providers.empty()) {
            ERS_LOG("Warning: Unexpected non-final providers found: non-final: "
                    << m_acc.providers << " final: " << m_finalAcc.providers
                    << " hist=" << m_myName);
            m_acc.reset();
        }

        _publish(m_finalAcc);
        return true;
    }

    return false;
}

template <typename T>
bool HistogramTagCont<T>::tagOperate(std::unique_ptr<Histogram> histo,
                                     const std::string& providerName,
                                     TimingCollector& timingColl)
{
    // get provider index
    unsigned provider, nProviders;
    bool newProvider;
    std::tie(provider, nProviders, newProvider) = m_providerMap.index(providerName);

    // lock thyself
    timingColl.lock_wait_real_time.start();
    std::lock_guard<std::mutex> lock(m_tagMutex);
    timingColl.lock_wait_real_time.stop();

    const bool finalHisto = ::isFinal(*histo);
    int64_t timestamp = (int64_t)histo->time().total_mksec_utc();
    if (finalHisto) {

        // final publications are treated separately
        return _doFinal(std::move(histo), provider, providerName);

    } else {

        // If provider is in the final set it can mean one of two things:
        // - provider "recovered" after going to final
        // - order of publications is reversed
        if (m_finalAcc.providers.contains(provider)) {

            if (timestamp <= m_finalAcc.timestamps[provider]) {
                // non-final publication was earlier but arrived here later - ignore it
                ERS_LOG("Warning: Timestap order mixed up for provider " << provider
                        << " final: " << m_finalAcc.timestamps[provider]
                        << " non-final: " << timestamp);
                return false;
            }
            if (m_finalAcc.providers.size() == 1) {
                // if final accumulator has only that one provider it is safe to reset.
                m_finalAcc.reset();
            } else {
                // no reasonable recovery in this case but reset anyways and hope
                // not much is lost.
                ERS_LOG("Warning: Recovered final provider " << provider
                        << " but final set has more providers:  final: "
                        << m_finalAcc.providers << ". Will reset final sum.");
                m_finalAcc.reset();
            }
        }

        // check timestamps and filter duplicate publications
        if (not m_acc.checkTimestamp(timestamp, provider, m_myName, providerName)) {
            return false;
        }
    }

    if (m_expectedProviders.empty()) {
        // very first contribution
        m_acc.reset(std::move(histo), provider);

        // assume all providers will be updating
        for (unsigned i = 0; i != nProviders; ++ i) {
            m_expectedProviders.add(i);
        }

        return false;
    }

    if (m_acc.providers.empty()) {

        // if this is the first histo after publish() (or first ever)
        if (!m_expectedProviders.contains(provider)) {
            // new provider, skip it if it falls into cutoff window, but count new provider
            auto cutoff_mksec = std::chrono::duration_cast<std::chrono::microseconds>(m_updateCutoff);
            if (std::abs(timestamp - m_lastPublish) < cutoff_mksec.count()) {
                // assume it belongs to the same cycle as recently published histogram
                ERS_DEBUG(1, (newProvider ? "New" : "Returning") << " provider " << providerName
                          << " " << m_myName << " probably from last cycle. Ignoring");
                m_expectedProviders.add(provider);
                return false;
            }
            if (!newProvider) {
                // known provider but was not seen last time - means re-appearing
                ERS_DEBUG(1, "Returning provider " << providerName << " " << m_myName);
            }
        }

        // if we are here it means that new cycle has started (for given tag)
        //ERS_LOG("Starting "<<m_myName<<" with tag "<<histo.tag()<<" from "<<providerName);

        // reset summary histo to current one
        m_acc.reset(std::move(histo), provider);
        m_expectedProviders.add(provider);

        // check labeling consistency
        HistTraits<T>::check_labels(*m_acc.sum, m_myName);

        return false;
    }

    if (m_acc.providers.contains(provider)) {

        //we see the same provider a second time? -> publish and start next cycle
        timingColl.publish_real_time.start();
        // remember timestamp
        m_lastPublish = (int64_t)m_acc.sum->time().total_mksec_utc();
        // copy non-final provider set to expected set
        m_expectedProviders = m_acc.providers;
        _publish(m_acc);
        timingColl.publish_real_time.stop();

        m_acc.reset(std::move(histo), provider);

        return true;

    }

    // at this point we can add new histogram to current sum
    m_expectedProviders.add(provider);
    int tag = histo->tag();
    try {
        timingColl.algo_real_time.start();
        timingColl.algo_cpu_time.start();
        m_acc.add(std::move(histo), provider, m_alg);
        timingColl.algo_real_time.stop();
        timingColl.algo_cpu_time.stop();
        ERS_DEBUG(2, m_myName << " operation done " << providerName);
    } catch (std::exception const& exc) {
        throw AlgorithmIssue(ERS_HERE, providerName, m_myName, tag, exc);
    }

    // don't publish if it is a new provider
    if (m_acc.providers == m_expectedProviders && !newProvider) {
        try {
            timingColl.publish_real_time.start();
            // remember timestamp
            m_lastPublish = (int64_t)m_acc.sum->time().total_mksec_utc();
            // copy non-final provider set to expected set
            m_expectedProviders = m_acc.providers;
            _publish(m_acc);
            timingColl.publish_real_time.stop();
            return true;
        } catch (const std::exception& ex) {
            ERS_LOG(" caught exception in publish(): " << ex.what());
        }
    }

    return false;
}

template class MIG::HistogramTagCont<oh::Histogram>;
template class MIG::HistogramTagCont<oh::Efficiency>;
