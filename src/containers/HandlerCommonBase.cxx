//
// Implementation of HandlerCommonBase class
//

//-----------------------
// This Class's Header --
//-----------------------
#include "MonInfoGatherer/HandlerCommonBase.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ISDynAnyCont.h"
#include "ers/ers.h"
#include "is/infoany.h"
#include "MonInfoGatherer/Exceptions.h"


using namespace MIG;

HandlerCommonBase::HandlerCommonBase(const HandlerConfig& handlerConfig,
                                     InfoPublisher& publisher)
    : m_publisher(publisher),
      m_parameters(handlerConfig.parameters)
{
}

HandlerCommonBase::~HandlerCommonBase()
{
    ERS_LOG("HandlerCommonBase: nHisto=" << m_containers.size() <<
            " updateCount=" << m_updateCount);
}

void HandlerCommonBase::updateInfo(ISInfoAny& info, const std::string &name,
                                   const std::string& providerName)
{
    try {
        // forward to implementation
        _updateInfo_exc(info, name, providerName);
    } catch (std::exception const& exc) {
        ers::error(HandlerError(ERS_HERE, providerName, name, exc));
    } catch (...) {
        ERS_LOG("Caught Unknown exception when updating histo name= " << name << " provider= "
                << providerName);
    }
}

void HandlerCommonBase::_updateInfo_exc(ISInfoAny& info, const std::string &name,
                                        const std::string& providerName)
{
    ++m_updateCount;
    std::shared_ptr<ContainerBase> cont;
    auto it = m_containers.find(name);
    if (it != m_containers.end()) {
        // got existing info
        cont = it->second;
    } else {
        // need to make new container for this info
        std::shared_ptr<ContainerBase> newCont(makeContainer(name, info.type(),
                                                             m_publisher, m_parameters));
        if (newCont) {
            auto succ = m_containers.insert(std::make_pair(name, newCont));
            // in case someone else managed to do it ahead of us
            cont = succ.first->second;
        }
    }

    if (cont != nullptr) {
        cont->operate(info, providerName);
    }
}

void HandlerCommonBase::saveTiming(MIG::TimingSaver& saver)
{
    for (auto&& pair: m_containers) {
        auto timing = pair.second->timing();
        if (timing != nullptr) {
            saver.saveEntries(pair.first, *timing);
        }
    }
}
