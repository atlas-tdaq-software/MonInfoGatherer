// -*- c++ -*-
#ifndef MIG_ANNOTATIONS_H
#define MIG_ANNOTATIONS_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/info.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 * Class which knows how to extract Gatherer counters from annotations
 * and merge them.
 */
class Annotations {
public:

    using ISAnnotations = ISInfo::Annotations;

    /// Reset all internal counters.
    void reset();

    /// Merge counters from a histogram annotations with current counters.
    void merge(const ISAnnotations& ann);

    /// Merge two accumulators together.
    Annotations& operator+=(const Annotations& other);

    /// Rewrite annotations in histogram based on collected stats.
    void update(ISAnnotations& ann);

    // Return counters, only useful for testing.
    std::vector<unsigned> const& counters() const { return m_counters; }

    // Return contribution count, only useful for testing.
    unsigned contrib_count() const { return m_contrib_count; }

private:

    // merge other counters
    void _merge(unsigned nLevels, unsigned const* counters);

    std::vector<unsigned> m_counters;  // number of per-level histos from annotations
    unsigned m_contrib_count = 0;      // number of contributions
};

} // namespace MIG

#endif // MIG_ANNOTATIONS_H
