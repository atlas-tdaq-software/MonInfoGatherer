//
// Implementation of HistogramTagContFactory
//

//-----------------------
// This Class's Header --
//-----------------------
#include "HistogramTagContFactory.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <regex>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/Config.h"
#include "HistogramTagCont.h"
#include "PerLBTagCont.h"
#include "Scheduler.h"
#include "../algos/HistoAlgorithm.h"
#include "oh/core/EfficiencyData.h"

using namespace MIG;

template <typename T>
HistogramTagContFactory<T>::HistogramTagContFactory(const std::string& histoName,
                                                    std::unique_ptr<HistoAlgorithm> alg,
                                                    InfoPublisher& publisher,
                                                    const ConfigParameters& parameters)
        : m_histoName(histoName),
          m_alg(std::move(alg)),
          m_publisher(publisher),
          m_parameters(parameters),
          m_forcePerLB(false),
          m_autoPerLB(true)
{
    // get some options from configuration
    auto options = parameters.values("PerLBHistogram");

    // "auto" is true if options are empty of "auto" is explicitly set
    m_autoPerLB = options.empty() or
            std::count(options.begin(), options.end(), "auto") > 0;

    // if option matches histogram name then force per-lb tag container
    for (auto&& option: options) {
        if (option != "auto" and option != "noauto") {
            // should be a regular expression
            try {
                std::regex re(option);
                if (std::regex_match(m_histoName, re)) {
                    m_forcePerLB = true;
                    break;
                }
            } catch (const std::regex_error& exc) {
                ERS_LOG("Failed to compile PerLBHistogram regexp \"" << option << "\": " << exc.what());
            }
        }
    }
}

template <typename T>
HistogramTagContFactory<T>::~HistogramTagContFactory()
{
}

template <typename T>
std::shared_ptr<HistogramTagContBase<T>>
HistogramTagContFactory<T>::makeTagCont(int tag)
{
    bool doPerLB = m_forcePerLB or (m_autoPerLB and tag >= 0);
    if (doPerLB) {
        ERS_DEBUG(2, "Making PerLBTagCont for histo " << m_histoName << " tag=" << tag);
        auto cont = std::make_shared<PerLBTagCont<T>>(m_histoName, *m_alg, m_publisher,
                                                      m_providerMap, m_parameters);
        // Schedule for wakeups every second
        Scheduler::instance().add(cont, std::chrono::seconds(1));
        return cont;
    } else {
        ERS_DEBUG(2, "Making HistogramTagCont for histo " << m_histoName << " tag=" << tag);
        return std::make_shared<HistogramTagCont<T>>(m_histoName, *m_alg, m_publisher,
                                                     m_providerMap, m_parameters);
    }
}

template class MIG::HistogramTagContFactory<oh::Histogram>;
template class MIG::HistogramTagContFactory<oh::Efficiency>;
