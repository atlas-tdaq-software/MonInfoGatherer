
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "MonInfoGatherer/Timers.h"
#include "ers/ers.h"


#if MIG_TIMING_ENABLE

namespace MIG {

void
WCTimer::start() {
    if (not m_enabled) return;
    if (m_ts.tv_sec == 0) {
        clock_gettime(CLOCK_MONOTONIC, &m_ts);
    }
}

void
WCTimer::stop() {
    if (not m_enabled) return;
    if (m_ts.tv_sec != 0) {
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        m_count += (ts.tv_sec - m_ts.tv_sec)*1000000000 + (ts.tv_nsec - m_ts.tv_nsec);
        m_ts.tv_sec = 0;
    }
}

void CPUTimer::start() {
    if (not m_enabled) return;
    if (!m_ticking) {
        struct rusage ru;
        getrusage(m_kind, &ru);
        m_utime = ru.ru_utime;
        m_stime = ru.ru_stime;
        m_ticking = true;
    }
}

void CPUTimer::stop() {
    if (not m_enabled) return;
    if (m_ticking) {
        struct rusage ru;
        getrusage(m_kind, &ru);
        m_count += (ru.ru_utime.tv_sec - m_utime.tv_sec)*1000000 + (ru.ru_utime.tv_usec - m_utime.tv_usec);
        m_count += (ru.ru_stime.tv_sec - m_stime.tv_sec)*1000000 + (ru.ru_stime.tv_usec - m_stime.tv_usec);
        m_ticking = false;
    }
}

void TimingEntries::append(TimingEntry const& entry) {
    if (m_historySize == 0) return;
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_entries1.size() < m_historySize / 2) {
        m_entries1.push_back(entry);
        return;
    }
    m_entries2.push_back(entry);
    // usually it's just one entry to remove, OK to use loop here.
    while (m_entries2.size() > m_historySize / 2) {
        m_entries2.pop_front();
    }
}

void TimingCollector::save() {
    if (not m_enabled) return;
    if (m_saved) return;

    // stop all timers
    total_real_time.stop();
    total_cpu_time.stop();
    lock_wait_real_time.stop();
    algo_real_time.stop();
    algo_cpu_time.stop();
    publish_real_time.stop();

    TimingEntry entry;
    entry.provider = m_provider;
    entry.publish_time_us = m_publish_time_us;
    entry.receive_time_us = m_receive_time_us;
    entry.total_real_ns = total_real_time.nsec();
    entry.total_cpu_us = total_cpu_time.usec();
    entry.lock_wait_real_ns = lock_wait_real_time.nsec();
    entry.algo_real_ns = algo_real_time.nsec();
    entry.algo_cpu_us = algo_cpu_time.usec();
    entry.publish_real_ns = publish_real_time.nsec();
    entry.bin_count = bin_count;

    m_entries.append(entry);

    m_saved = true;
}

void
TimingSaver::saveEntries(std::string const& objName, TimingEntries const& entries) {
    if (entries.empty()) return;

    bool firstEntry = false;
    if (m_entriesStream == nullptr) {
        firstEntry = true;
        makeStreams();
    }

    // get object Id
    unsigned oid = 0;
    auto oit = m_objNameMap.find(objName);
    if (oit == m_objNameMap.end()) {
        oid = m_objNameMap.size();
        m_objNameMap.insert(std::make_pair(objName, oid));
        if (oid == 0) {
            (*m_objNameStream) << "object_id\tobject_name\n";
        }
        (*m_objNameStream) << oid << '\t' << objName << '\n';
    } else {
        oid = oit->second;
    }

    for (auto&& entry: entries) {

        // get provider Id
        unsigned pid = 0;
        auto pit = m_provNameMap.find(entry.provider);
        if (pit == m_provNameMap.end()) {
            pid = m_provNameMap.size();
            m_provNameMap.insert(std::make_pair(entry.provider, pid));
            if (pid == 0) {
                (*m_provNameStream) << "provider_id\tprovider_name\n";
            }
            (*m_provNameStream) << pid << '\t' << entry.provider << '\n';
        } else {
            pid = pit->second;
        }

        if (firstEntry) {
            (*m_entriesStream) << "object_id\tprovider_id\tpublish_time_us\treceive_time_us\tbin_count\ttotal_real_ns\t"
                               << "total_cpu_us\tlock_wait_real_ns\talgo_real_ns\talgo_cpu_us\tpublish_real_ns\n";
            firstEntry = false;
        }
        (*m_entriesStream) << oid << '\t' << pid << '\t'
                           << entry.publish_time_us << '\t'
                           << entry.receive_time_us << '\t'
                           << entry.bin_count << '\t'
                           << entry.total_real_ns << '\t'
                           << entry.total_cpu_us << '\t'
                           << entry.lock_wait_real_ns << '\t'
                           << entry.algo_real_ns << '\t'
                           << entry.algo_cpu_us << '\t'
                           << entry.publish_real_ns << '\n';
    }
}

void TimingSaver::close() {
    m_objNameStream.reset();
    m_provNameStream.reset();
    m_entriesStream.reset();
}

void TimingSaver::makeStreams() {
    // replace {time} with current timestamp
    auto now = std::to_string(time(nullptr));
    std::string const tag("{time}");
    std::string prefix = m_fnamePfx;
    while (true) {
        std::string::size_type pos = prefix.find(tag);
        if (pos == std::string::npos) break;
        prefix.replace(pos, pos + tag.size(), now);
    }

    ERS_LOG("Saving timing info to CSV files with prefix " << prefix);
    m_objNameStream = std::make_unique<std::ofstream>(prefix + "_objects.tsv");
    m_provNameStream = std::make_unique<std::ofstream>(prefix + "_providers.tsv");
    m_entriesStream = std::make_unique<std::ofstream>(prefix + "_entries.tsv");
}

} // namespace MIG

#endif // MIG_TIMING_ENABLE
