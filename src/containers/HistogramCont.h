// -*- c++ -*-
#ifndef MONINFOGATHERER_HISTOGRAMCONT_H
#define MONINFOGATHERER_HISTOGRAMCONT_H
//
// Declaration of HistogramCont Class
//

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <memory>
#include <mutex>

//----------------------
// Base Class Headers --
//----------------------
#include "MonInfoGatherer/ContainerBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/SmallIntSet.h"
#include "MonInfoGatherer/Timers.h"
#include "HistogramTagCont.h"
#include "HistogramTagContFactory.h"
#include "Scheduler.h"
#include "oh/core/Histogram.h"
#include "oh/core/ObjectBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class ConfigParameters;
class HistoAlgorithm;
class InfoPublisher;
}

namespace MIG {

/**
 * Container for Histogram classes.
 *
 * Template type can be one of oh::Histogram or oh::Efficiency.
 */
template <typename T>
class HistogramCont: public ContainerBase, public Task {
public:

    using Histogram = oh::ObjectBase<T>;

    /**
     *  Constructor.
     *  @param name name of the histogram
     *  @param alg algorithm that is going to operate on the updates of this histogram.
     *  @param publisher Object which publishes summed data.
     *  @param parameters Configuration parameters
     */
    HistogramCont(const std::string &name,
                  std::unique_ptr<HistoAlgorithm> alg,
                  InfoPublisher& publisher,
                  const ConfigParameters& parameters);
    ~HistogramCont();

    void operate(ISInfoAny& info, const std::string& providerName) override;

    /**
     * Implementation of Task::execute().
     *
     * This method is called periodically to handle cleanup operations.
     */
    void execute() override;

    MIG::TimingEntries const* timing() const override { return &m_timing; }

    // these methods are for testing purposes only
    // beware: there is no proper synchronization here
    HistogramTagCont<T>* tag_cont(int tag = -1) const
    {
        auto iter = m_tagMap.find(tag);
        if (iter != m_tagMap.end()) {
            return dynamic_cast<HistogramTagCont<T>*>(iter->second.get());
        }
        return nullptr;
    }
    int numUpdatesSincePublish(int tag = -1) const
    {
        auto info = tag_cont(tag);
        return info != nullptr ? info->numUpdatesSincePublish() : -1;
    }
    int numUpdatingProviders(int tag = -1) const
    {
        auto info = tag_cont(tag);
        return info != nullptr ? info->numUpdatingProviders() : -1;
    }

private:

    using Tag = int;

    /*
     * Checks that labels in new histogram are reasonable. This method
     * only prints message to the log if it finds something wrong, this
     * is just for our internal diagnostics.
     *
     * Should only be called from protected code.
     */
    void _checkHistoLabels(Histogram& hist, const std::string &providerName);

    /*
     * Remove obsolete tags from tag map.
     */
    void _tagMapCleanup();

    /*
     * Detect when new run starts and reset tag data.
     */
    void _checkForNewRun(int tag, Clock::time_point const& now);

    using TagMap = std::map<Tag, std::shared_ptr<HistogramTagContBase<T>>>;
    using TagTimes = std::map<Tag, Clock::time_point>;

    std::string const m_myName;
    std::chrono::seconds m_tagExpiration; // Tag containers are removed if no contributions for that period
    std::mutex m_tagMapMutex;  // protects m_tagMap, m_tagTimes, and m_droppedTags
    TagMap m_tagMap;
    TagTimes m_tagTimes; // maps tag to its generation (number of attempts to cleanup)
    SmallIntSet m_droppedTags;  // tags that were dropped, new contributions from these tags are ignored
    Clock::time_point m_latestContribution; // time of the latest contribution
    std::atomic<bool> m_checkLabelsOnce;
    std::atomic<bool> m_labeledHisto;
    std::atomic<bool> m_labelMismatchFound;
    MIG::TimingEntries m_timing;
    HistogramTagContFactory<T> m_tagContFactory;
};

} // namespace MIG

#endif // MONINFOGATHERER_HISTOGRAMCONT_H
