//
// Implementation of HistogramHolder
//

//-----------------------
// This Class's Header --
//-----------------------
#include "HistogramHolder.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/Exceptions.h"
#include "oh/core/DataTypes.h"
#include "oh/core/EfficiencyData.h"
#include "lz4.h"

namespace {

// type traits for histogram classes
template <typename Histo>
struct HistoTypes {};

template <>
struct HistoTypes<oh::HistogramData<char>> {
    using bin_type = char;
};

template <>
struct HistoTypes<oh::HistogramData<short>> {
    using bin_type = short;
};

template <>
struct HistoTypes<oh::HistogramData<int>> {
    using bin_type = int;
};

template <>
struct HistoTypes<oh::HistogramData<float>> {
    using bin_type = float;
};

template <>
struct HistoTypes<oh::HistogramData<double>> {
    using bin_type = double;
};

template <>
struct HistoTypes<oh::ProfileData> {
    using bin_type = double;
};

} // namespace

namespace MIG {

/// This constructor transfers data from a vector
template <typename T>
CompressedData<T>::CompressedData(std::vector<T>& data, int minCompressSize, float fraction)
{
    m_size = data.size();
    int sizeBytes = m_size * sizeof(T);
    if (_try_compress_lz4(data.data(), sizeBytes, minCompressSize, fraction)) {
        // we need to reduce both size and capacity
        data.clear();
        data.shrink_to_fit();
    }
}

/// This constructor copies the data from array
template <typename T>
CompressedData<T>::CompressedData(const T* data, unsigned numEmen, int minCompressSize, float fraction)
{
    m_size = numEmen;
    int sizeBytes = m_size * sizeof(T);
    _try_compress_lz4(data, sizeBytes, minCompressSize, fraction);
}


// Uncompresses data into a vector
template <typename T>
void CompressedData<T>::uncompress(std::vector<T>& data) const
{
    if (m_compression == CompressionType::LZ4) {
        _uncompress_lz4(data);
    }
}

// Uncompresses data and return a pointer
template <typename T>
std::unique_ptr<T[]> CompressedData<T>::uncompress() const
{
    if (m_compression == CompressionType::LZ4) {
        return _uncompress_lz4();
    }
    return nullptr;
}


// Compress buffer with lz4, check that compressed size is shorter
// than a fraction of original size.
// returns true on success
template <typename T>
bool CompressedData<T>::_try_compress_lz4(const void* src, int srcSize, int minCompressSize, float fraction)
{
    ERS_DEBUG(2, "_try_compress_lz4: srcSize=" << srcSize << " minCompressSize=" << minCompressSize << " fraction=" << fraction);
    if (minCompressSize < 0 or srcSize < minCompressSize) return false;

    int destSize = LZ4_compressBound(srcSize);
    std::unique_ptr<char[]> compressedData(new char[destSize]);
    char* dstBuffer = compressedData.get();
    int compressedSize =  LZ4_compress_default(static_cast<const char*>(src), dstBuffer, srcSize, destSize);
    ERS_DEBUG(2, "_try_compress_lz4: compressedSize=" << compressedSize << " destSize=" << destSize);
    if (compressedSize == 0) {
        // compression failed
        ERS_LOG("Warinig: LZ4 compression failed: srcSize=" << srcSize << " destSize=" << destSize);
        return false;
    }
    if (compressedSize > srcSize*fraction) {
        // compression is not good enough
        ERS_DEBUG(2, "_try_compress_lz4: compressedSize=" << compressedSize << " above target " << srcSize*fraction);
        return false;
    }

    // copy to destination
    m_data.assign(dstBuffer, dstBuffer+compressedSize);
    ERS_DEBUG(2, "_try_compress_lz4: compressedSize=" << m_data.size());
    m_compression = LZ4;
    return true;
}

template <typename T>
void CompressedData<T>::_uncompress_lz4(std::vector<T>& data) const {
    // TODO: could we avoid zero-initialization?
    data.resize(m_size);
    int res [[gnu::unused]] =
            LZ4_decompress_safe(m_data.data(), reinterpret_cast<char*>(data.data()), m_data.size(), m_size * sizeof(T));
    ERS_DEBUG(2, "_uncompress_lz4: compressedSize=" << m_data.size() << " originalSize=" << m_size * sizeof(T) << " res=" << res);
}

template <typename T>
std::unique_ptr<T[]> CompressedData<T>::_uncompress_lz4() const {
    std::unique_ptr<T[]> decompressedData(new T[m_size]);
    int res [[gnu::unused]] =
            LZ4_decompress_safe(m_data.data(), reinterpret_cast<char*>(decompressedData.get()), m_data.size(), m_size * sizeof(T));
    ERS_DEBUG(2, "_uncompress_lz4: compressedSize=" << m_data.size() << " originalSize=" << m_size * sizeof(T) << " res=" << res);
    return decompressedData;
}

template <typename T>
void CompressedData<T>::print(std::ostream& out) const
{
    char const* ctypestr = "?";
    switch (m_compression) {
    case CompressedData<T>::None:
        ctypestr = "None";
        break;
    case CompressedData<T>::LZ4:
        ctypestr = "LZ4";
        break;
    }
    out << "ZData(" << ctypestr << ", " << m_data.size() << '/' << m_size*sizeof(T) << ")";
}

/**
 *  Holder for specific histogram type.
 *
 *  Template parameter Histo is one of the sub-classes of
 *  oh::ObjectBase<oh::Histogram> such as oh::HistogramData<X>,
 *  specialization for oh::ProfileData is below.
 */
template <typename Histo>
class HistogramHolderT: public HistogramHolder<oh::Histogram> {
public:

    HistogramHolderT(std::shared_ptr<Histo> histo, int minCompressSize, float fraction)
        : m_histo(histo),
          m_bins(m_histo->get_bins_array(), m_histo->get_bins_size(), minCompressSize, fraction),
          m_errors(m_histo->errors, minCompressSize, fraction)
    {
        if (m_bins.compressed()) {
            // free bins array in histogram
            m_histo->set_bins_array(nullptr, 0, false);
        }
        ERS_DEBUG(2, "HistogramHolderT: bins=" << m_bins << " errors" << m_errors);
    }

    virtual ~HistogramHolderT() = default;
    HistogramHolderT(const HistogramHolderT&) = delete;
    HistogramHolderT& operator=(const HistogramHolderT&) = delete;

    /// retrieve histogram, making it usable
    std::shared_ptr<Histogram> get(bool clone) const override {
        ERS_DEBUG(2, "HistogramHolderT::get -- bins=" << m_bins << " errors" << m_errors);
        if (not clone and not m_bins.compressed() and not m_errors.compressed()) {
            // original is OK
            ERS_DEBUG(2, "HistogramHolderT::get -- return original");
            return m_histo;
        }

        // have to make a copy
        ERS_DEBUG(2, "HistogramHolderT::get -- make copy");
        auto copy = std::make_shared<Histo>(*m_histo);

        // uncompress data
        if (m_bins.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress bins");
            auto bins = m_bins.uncompress();  // unique_ptr
            copy->set_bins_array(bins.release(), m_bins.size(), true);
        }
        if (m_errors.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress errors");
            m_errors.uncompress(copy->errors);
        }
        return copy;
    }

private:

    using bin_type = typename HistoTypes<Histo>::bin_type;

    std::shared_ptr<Histo> m_histo;
    const CompressedData<bin_type> m_bins;  // compressed bins_array
    const CompressedData<double> m_errors;  // compressed errors
};

// specialization for ProfileData
template <>
class HistogramHolderT<oh::ProfileData>: public HistogramHolder<oh::Histogram> {
public:

    using Histo = oh::ProfileData;

    HistogramHolderT(std::shared_ptr<Histo> histo, int minCompressSize, float fraction)
        : m_histo(histo),
          m_bins(m_histo->get_bins_array(), m_histo->get_bins_size(), minCompressSize, fraction),
          m_errors(m_histo->errors, minCompressSize, fraction),
          m_entries(m_histo->bin_entries, minCompressSize, fraction)
    {
        if (m_bins.compressed()) {
            // free bins array in histogram
            m_histo->set_bins_array(nullptr, 0, false);
        }
        ERS_DEBUG(2, "HistogramHolderT: bins=" << m_bins << " errors" << m_errors << " bin_entries=" << m_entries);
    }

    virtual ~HistogramHolderT() = default;
    HistogramHolderT(const HistogramHolderT&) = delete;
    HistogramHolderT& operator=(const HistogramHolderT&) = delete;

    /// retrieve histogram, making it usable
    std::shared_ptr<Histogram> get(bool clone) const override {
        ERS_DEBUG(2, "HistogramHolderT::get -- bins=" << m_bins << " errors" << m_errors << " bin_entries=" << m_entries);
        if (not clone and not m_bins.compressed() and not m_errors.compressed() and not m_entries.compressed()) {
            // original is OK
            ERS_DEBUG(2, "HistogramHolderT::get -- return original");
            return m_histo;
        }

        // have to make a copy
        ERS_DEBUG(2, "HistogramHolderT::get -- make copy");
        auto copy = std::make_shared<Histo>(*m_histo);

        // uncompress data
        if (m_bins.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress bins");
            auto bins = m_bins.uncompress();  // unique_ptr
            copy->set_bins_array(bins.release(), m_bins.size(), true);
        }
        if (m_errors.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress errors");
            m_errors.uncompress(copy->errors);
        }
        if (m_entries.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress bin_entries");
            m_entries.uncompress(copy->bin_entries);
        }
        return copy;
    }

private:

    using bin_type = typename HistoTypes<Histo>::bin_type;

    std::shared_ptr<Histo> m_histo;
    const CompressedData<bin_type> m_bins;  // compressed bins_array
    const CompressedData<double> m_errors;  // compressed errors
    const CompressedData<double> m_entries; // compressed bin_entries
};

// Specialization for Efficiency
template <>
class HistogramHolderT<oh::EfficiencyData>: public HistogramHolder<oh::Efficiency> {
public:

    HistogramHolderT(std::shared_ptr<oh::EfficiencyData> histo, int minCompressSize, float fraction)
        : m_histo(histo),
          m_bins_total(m_histo->total.get_bins_array(), m_histo->total.get_bins_size(), minCompressSize, fraction),
          m_errors_total(m_histo->total.errors, minCompressSize, fraction),
          m_bins_passed(m_histo->passed.get_bins_array(), m_histo->passed.get_bins_size(), minCompressSize, fraction),
          m_errors_passed(m_histo->passed.errors, minCompressSize, fraction)
    {
        if (m_bins_total.compressed()) {
            // free bins array in histogram
            m_histo->total.set_bins_array(nullptr, 0, false);
        }
        if (m_bins_passed.compressed()) {
            // free bins array in histogram
            m_histo->passed.set_bins_array(nullptr, 0, false);
        }
        ERS_DEBUG(2, "HistogramHolderT: total: bins=" << m_bins_total << " errors" << m_errors_total);
        ERS_DEBUG(2, "HistogramHolderT: passed: bins=" << m_bins_passed << " errors" << m_errors_passed);
    }

    virtual ~HistogramHolderT() = default;
    HistogramHolderT(const HistogramHolderT&) = delete;
    HistogramHolderT& operator=(const HistogramHolderT&) = delete;

    /// retrieve histogram, making it usable
    std::shared_ptr<Histogram> get(bool clone) const override {
        ERS_DEBUG(2, "HistogramHolderT::get total: bins=" << m_bins_total << " errors" << m_errors_total);
        ERS_DEBUG(2, "HistogramHolderT::get passed: bins=" << m_bins_passed << " errors" << m_errors_passed);
        if (not clone and not m_bins_total.compressed() and not m_errors_total.compressed()
                and not m_bins_passed.compressed() and not m_errors_passed.compressed()) {
            // original is OK
            ERS_DEBUG(2, "HistogramHolderT::get -- return original");
            return m_histo;
        }

        // have to make a copy
        ERS_DEBUG(2, "HistogramHolderT::get -- make copy");
        auto copy = std::make_shared<oh::EfficiencyData>(*m_histo);

        // uncompress data
        if (m_bins_total.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress total bins");
            auto bins = m_bins_total.uncompress();  // unique_ptr
            copy->total.set_bins_array(bins.release(), m_bins_total.size(), true);
        }
        if (m_errors_total.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress total errors");
            m_errors_total.uncompress(copy->total.errors);
        }
        if (m_bins_passed.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress passed bins");
            auto bins = m_bins_passed.uncompress();  // unique_ptr
            copy->passed.set_bins_array(bins.release(), m_bins_passed.size(), true);
        }
        if (m_errors_passed.compressed()) {
            ERS_DEBUG(2, "HistogramHolderT::get -- uncompress passed errors");
            m_errors_passed.uncompress(copy->passed.errors);
        }
        return copy;
    }

private:

    std::shared_ptr<oh::EfficiencyData> m_histo;
    const CompressedData<double> m_bins_total;  // compressed bins_array
    const CompressedData<double> m_errors_total;  // compressed errors
    const CompressedData<double> m_bins_passed;  // compressed bins_array
    const CompressedData<double> m_errors_passed;  // compressed errors
};


// Make new HistogramHolder for given histogram.
template <>
std::unique_ptr<HistogramHolder<oh::Histogram>>
HistogramHolder<oh::Histogram>::makeHolder(std::unique_ptr<Histogram> histo, int minCompressSize, float fraction)
{
    // have to be super-careful with type()
    auto&& currType = static_cast<ISInfo&>(*histo).type();
    if (currType == oh::HistogramData<float>::type()) {
        using Histo = oh::HistogramData<float>;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else if (currType == oh::HistogramData<int>::type()) {
        using Histo = oh::HistogramData<int>;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else if (currType == oh::HistogramData<double>::type()) {
        using Histo = oh::HistogramData<double>;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else if (currType == oh::HistogramData<char>::type()) {
        using Histo = oh::HistogramData<char>;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else if (currType == oh::ProfileData::type()) {
        using Histo = oh::ProfileData;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else if (currType == oh::HistogramData<short>::type()) {
        using Histo = oh::HistogramData<short>;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else {
        ERS_LOG("Type " << currType << " is not supported by HistogramHolder");
        throw UnsupportedObjectType(ERS_HERE, currType.name());
    }
}

template <>
std::unique_ptr<HistogramHolder<oh::Efficiency>>
HistogramHolder<oh::Efficiency>::makeHolder(std::unique_ptr<Histogram> histo, int minCompressSize, float fraction)
{
    // have to be super-careful with type()
    auto&& currType = static_cast<ISInfo&>(*histo).type();
    if (currType == oh::EfficiencyData::type()) {
        using Histo = oh::EfficiencyData;
        using HolderType = HistogramHolderT<Histo>;
        std::shared_ptr<Histogram> shared_histo(std::move(histo));
        return std::make_unique<HolderType>(std::dynamic_pointer_cast<Histo>(shared_histo), minCompressSize, fraction);
    } else {
        ERS_LOG("Type " << currType << " is not supported by HistogramHolder");
        throw UnsupportedObjectType(ERS_HERE, currType.name());
    }
}


// explicit specialization of all compressed types
template class CompressedData<char>;
template class CompressedData<short>;
template class CompressedData<int>;
template class CompressedData<float>;
template class CompressedData<double>;

// explicit specialization of all holder types
#define SPECIALIZE_HOLDER_TYPE(T) template class HistogramHolderT<T>;
FOR_OH_HISTOGRAM_TYPES(SPECIALIZE_HOLDER_TYPE)
template class HistogramHolderT<oh::EfficiencyData>;

} // namespace MIG
