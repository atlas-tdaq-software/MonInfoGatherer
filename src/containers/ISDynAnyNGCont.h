// -*- c++ -*-
#ifndef MONINFOGATHERER_ISDYNANYNGCONT_H
#define MONINFOGATHERER_ISDYNANYNGCONT_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <memory>
#include <mutex>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "MonInfoGatherer/ContainerBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/PeriodFinder.h"
#include "Scheduler.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/ProviderMap.h"
#include "is/infodynany.h"
#include "is/type.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class ConfigParameters;
class ISSum;
class InfoPublisher;
}

namespace MIG {

/**
 * Container class for IS objects.
 *
 * This container class calls the algorithm (usually ISSum) to update object
 * data and publishes resulting summed object. This is a new attempt to
 * implement a better summing option that is much less dependent on timing.
 * This is done similarly to per-LB histogram case where all contributions
 * are remembered and only summed when result is published.
 */
class ISDynAnyNGCont: public ContainerBase, public Task {
public:

    /**
     *  Constructor.
     *  @param name name of the IS object
     *  @param objType IS type of the object
     *  @param Alg pointer to algorithm that is going to operate on the updates of this histogram.
     *  @param publisher Object which publishes summed data.
     *  @param parameters Configuration parameters
     */
    ISDynAnyNGCont(const std::string &name,
                   const ISType& objType,
                   std::unique_ptr<ISSum> alg,
                   InfoPublisher& publisher,
                   const ConfigParameters& parameters);
    ~ISDynAnyNGCont();

    // Implementation of HistogramTagContBase::operate().
    void operate(ISInfoAny& info, const std::string& providerName) override;

    // Implementation of Task::execute().
    void execute() override;

    // these methods are for testing purposes only
    int numUpdatesSincePublish(int t=-1) const { return m_numUpdatesSincePublish; }
    int numUpdatingProviders(int t=-1) const {
        return std::count_if(
            m_contributions.begin(), m_contributions.end(),
            [](auto& c) { return c.info != nullptr; }
        );
    }

protected:

    // Publish result of a summing. Note that implementation is allowed
    // to change the content of `info` object, there should be no assumptions
    // in this class about content of `info` after this call.
    virtual void _publish(ISInfoDynAny& info, std::string const& name, int nUpdates);

private:

    // Publish summed info and reset internal state for next cycle.
    // Must be called from protected section, lock is used as a parameter
    // to ensure that, but lock is not used.
    void _publish(std::lock_guard<std::mutex> const& ensure_lock);

    // Guess publication period of all known providers, return 0 if guessing
    // fails.
    std::chrono::seconds _guessPeriod() const;

    // All data relatted to a contribution from one provider.
    struct ContributionData {
        // Contribution from provider, can be null if provider becomes dead.
        std::unique_ptr<ISInfoDynAny> info;
        // Count of updates from all other contributions since this
        // contribution was last updated and published.
        unsigned nUpdatesSinceLastUpdate = 0;
        // Count of updates for this contribution since last _publish().
        unsigned nUpdatesSincePublish = 0;
        // time of last tagOperate() call for this contribution
        Clock::time_point lastUpdate;
        // Period finder for this contribution
        PeriodFinder periodFinder;
    };


    std::string m_myName;
    const ISType m_isType;
    std::unique_ptr<ISSum> m_alg;
    InfoPublisher& m_publisher;
    const Clock::duration m_publishDelay;
    const unsigned m_missingCycles;
    const bool m_publishSum;
    const bool m_publishAvg;
    const std::string m_suffixSum;
    const std::string m_suffixAvg;
    const std::chrono::seconds m_periodCutoff;
    std::mutex m_updateMutex;
    ProviderMap m_providerMap;
    std::vector<ContributionData> m_contributions;
    int m_numUpdatesSincePublish = 0;  // how many times tagOperate() was called since _publish()
    Clock::time_point m_lastUpdate;    // time of last tagOperate() call
    Clock::time_point m_lastPublish;   // time of last publishing of merged result

    // Allow access to internals from unit tests
    template <typename T> friend class MIGUnitTestFriendHack;
};
} // namespace MIG

#endif // MONINFOGATHERER_ISDYNANYNGCONT_H
