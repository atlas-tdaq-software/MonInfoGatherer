//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/HandlerManager.h"
#include "MonInfoGatherer/DefaultHandler.h"

using namespace MIG;

namespace {

template <typename T>
std::shared_ptr<HandlerBase> factory(const HandlerConfig& handlerConfig, InfoPublisher& publisher) {
    return std::make_shared<T>(handlerConfig, publisher);
}

}

extern "C" void register_objects(){
    ERS_LOG("registering handlers from libMIGContainers.so");
    ERS_LOG("Library was built on " << __DATE__ << " " << __TIME__);
    ERS_LOG("Parameters from commandline were " << '\n'
          << " Package Version=" << MonInfVersion << '\n'
          << " Build Host     =" << MIGBuildHost << '\n'
          << " Build Date     =" << __DATE__ << " " <<__TIME__);

    auto& manager = HandlerManager::instance();

    manager.registerHandler("Default", ::factory<DefaultHandler>);
    manager.registerHandler("DefaultHandler", ::factory<DefaultHandler>);
}
