//
// Implementation of Scheduler
//

//-----------------------
// This Class's Header --
//-----------------------
#include "Scheduler.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

using namespace MIG;

Scheduler& Scheduler::instance()
{
    static Scheduler scheduler;
    return scheduler;
}

// make new instance and start running it
Scheduler::Scheduler(bool start)
{
    if (start) {
        this->start();
    }
}

Scheduler::~Scheduler()
{
    // if thread is still running then stop it,
    // do not allow exceptions to leak from destructor
    try {
        stop();
    } catch (std::exception const& exc) {
        ERS_LOG("Warning: exception happened while stopping thread: " << exc.what());
    } catch (...) {
        ERS_LOG("Warning: unknown exception happened while stopping thread");
    }
}

void Scheduler::add(std::shared_ptr<Task> const& task, std::chrono::milliseconds period)
{
    if (period.count() == 0) {
        throw std::invalid_argument("Scheduler::add -- zero period for scheduled tasks");
    }
    std::lock_guard<std::mutex> lock(m_mutex);
    m_tasks.push_back(ScheduledTask{task, period, Clock::now()});
    m_cond.notify_all();
}

// Forget all Dreamers
void Scheduler::clear()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_tasks.clear();
}

/// Start running in a separate thread
void Scheduler::start()
{
    if (m_thread.joinable()) {
        // looks like we are running already
        return;
    }

    // start it
    m_stop = false;
    m_thread = std::thread(&Scheduler::run, this);
}

// Stop running
void Scheduler::stop()
{
    if (not m_thread.joinable()) {
        // looks like we are dead already
        return;
    }

    ERS_LOG("Scheduler: stopping thread");
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_stop = true;
    }
    m_cond.notify_all();
    m_thread.join();
}

// thread body function
void Scheduler::run()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    ERS_LOG("Scheduler: start thread, #tasks=" << m_tasks.size());

    auto logTime = Clock::now();
    while (true) {

        auto now = Clock::now();
        if (now - logTime >= std::chrono::minutes(1)) {
            logTime = now;
            ERS_DEBUG(1, "Scheduler: #tasks=" << m_tasks.size());
        }

        // if task list is empty sleep for a while and retry
        if (m_tasks.empty()) {
            m_cond.wait_for(lock, std::chrono::seconds(1));
            if (m_stop) {
                // we are done
                break;
            }
            continue;
        }

        now = Clock::now();

        // find shortest period of time we can sleep
        Clock::duration sleep(0);
        for (auto&& task: m_tasks) {
            auto taskNextExecTime = task.lastExecution + task.period;
            auto taskSleep = taskNextExecTime - now;
            if (taskSleep.count() <= 0) {
                // overslept a little
                sleep = Clock::duration(0);
                break;
            } else if (sleep.count() == 0) {
                sleep = taskSleep;
            } else if (taskSleep < sleep) {
                sleep = taskSleep;
            }
        }

        // doze a little
        if (sleep.count() > 0) {
            m_cond.wait_for(lock, sleep);
            if (m_stop) {
                // we are done
                break;
            }
        }

        // find tasks ready for execution, copy them so that they can be
        // executed with unlocked mutex
        now = Clock::now();
        std::vector<ScheduledTask> executeTasks;
        executeTasks.reserve(m_tasks.size());
        for (auto&& task: m_tasks) {
            if (task.lastExecution + task.period <= now) {
                executeTasks.push_back(task);
                task.lastExecution = now;
            }
        }

        // tasks have to be executed with mutex unlocked to avoid possible deadlocks
        lock.unlock();
        for (auto&& task: executeTasks) {
            auto&& ptr = task.task.lock();
            if (ptr) {
                try {
                    ptr->execute();
                } catch (std::exception const& exc) {
                    // we will forget about it, just print a message here
                    ERS_LOG("Warning: exception happened while executing task: " << exc.what());
                }
            }
        }
        lock.lock();

        // cleanup dead tasks
        auto new_end = std::remove_if(m_tasks.begin(), m_tasks.end(),
                                      [](const ScheduledTask& task) { return task.task.use_count() == 0; });
        m_tasks.erase(new_end, m_tasks.end());
    }
    ERS_LOG("Scheduler: stop thread, #tasks=" << m_tasks.size());
}

void Scheduler::executeAllTasks()
{
    for (auto&& task: m_tasks) {
        auto&& ptr = task.task.lock();
        if (ptr) {
            try {
                ptr->execute();
            } catch (std::exception const& exc) {
                // we will forget about it, just print a message here
                ERS_LOG("Warning: exception happened while executing task: " << exc.what());
            }
        }
    }
}
