// -*- c++ -*-
#ifndef MONINFOGATHERER_CONFIG_H
#define MONINFOGATHERER_CONFIG_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <iosfwd>
#include <set>
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Class for configuration parameters.
 *
 *  This class mirrors Parameters attribute in MIGMatchHandler class from OKS schema.
 */
class ConfigParameters {
public:
    ConfigParameters() = default;
    explicit ConfigParameters(const std::vector<std::string>& parameters_)
        : parameters(parameters_)
    {}

    /**
     *  If algorithm parameters contain "key=value" string then return "value" for a given key.
     *  Returns defaultValue if key is not found.
     */
    std::string value(const std::string& key, const std::string& defaultValue=std::string()) const;

    /**
     *  If algorithm parameters contain "key=value" strings then return all values for a given key.
     *  Returns empty vector if key is not found.
     */
    std::vector<std::string> values(const std::string& key) const;

    /**
     *  Return value of boolean parameter.
     *
     *  Parameter can be specified as:
     *    - "Parameter=1"
     *    - "Parameter=0"
     *    - "Parameter"    -- same as "Parameter=1"
     */
    bool boolValue(const std::string& key, bool defaultValue=false) const;

    /**
     *  Return value of unsigned integer parameter.
     *
     *  Parameter can be specified as:
     *    - "Parameter=value"
     */
    unsigned uintValue(const std::string& key, unsigned defaultValue=0) const;

    /**
     *  Return value of Normalized parameter.
     */
    bool Normalized() const { return boolValue("Normalized"); }

    /**
     *  Return value of Concatenate parameter.
     */
    bool Concatenate() const  { return boolValue("Concatenate"); }

    /**
     *  Return value of EOPRecoveryDelay parameter, default is 5 seconds
     */
    std::chrono::milliseconds EOPRecoveryDelay(unsigned defaultDelay=5000) const {
        auto value = uintValue("EOPRecoveryDelay_ms", defaultDelay);
        return std::chrono::milliseconds(value == 0 ? defaultDelay : value);
    }

    /**
     *  Return value of PerLBPublishDelay_ms parameter, default is 1.5 seconds
     */
    std::chrono::milliseconds PerLBPublishDelay() const {
        auto msec = uintValue("PerLBPublishDelay_ms", 1500);
        return std::chrono::milliseconds(msec);
    }

    /**
     *  Return value of ISDynAnyPublishDelay_ms parameter, default is 2.5 seconds
     */
    std::chrono::milliseconds ISDynAnyPublishDelay() const {
        auto msec = uintValue("ISDynAnyPublishDelay_ms", 2500);
        return std::chrono::milliseconds(msec);
    }

    // Print object, adding `offset` space characters on each line
    void print(std::ostream& out, int offset=0) const;

    std::vector<std::string> parameters;
};

inline
std::ostream& operator<<(std::ostream& out, const ConfigParameters& cfg) {
    cfg.print(out);
    return out;
}

/**
 *  Class for handler configuration.
 *
 *  This class mirrors MIGMatchHandler class from OKS schema.
 */
class HandlerConfig {
public:
    HandlerConfig() = default;
    HandlerConfig(const std::string& objectRe_,
                  const std::string& name_,
                  const std::vector<std::string>& parameters_)
        : objectRe(objectRe_),
          name(name_),
          parameters(parameters_)
    {}

    // Print object, adding `offset` space characters on each line
    void print(std::ostream& out, int offset=0) const;

    std::string objectRe;
    std::string name;
    ConfigParameters parameters;
};

inline
std::ostream& operator<<(std::ostream& out, const HandlerConfig& cfg) {
    cfg.print(out);
    return out;
}

/**
 *  Class for single subscription configuration.
 *
 *  This class mirrors MIGConfiguration class from OKS schema.
 */
class Config {
public:
    Config() = default;
    Config(const std::string& providerRe_,
           const std::string& objectRe_,
           const std::vector<std::string>& srcServers_,
           const std::vector<std::string>& dstServers_,
           const std::vector<HandlerConfig>& handlerConfigs_)
        : providerRe(providerRe_),
          objectRe(objectRe_),
          srcServers(srcServers_),
          dstServers(dstServers_),
          handlerConfigs(handlerConfigs_)
    {}

    // Print object, adding `offset` space characters on each line
    void print(std::ostream& out, int offset=0) const;

    std::string providerRe;
    std::string objectRe;
    std::vector<std::string> srcServers;
    std::vector<std::string> dstServers;
    std::vector<HandlerConfig> handlerConfigs;
};

inline
std::ostream& operator<<(std::ostream& out, const Config& cfg) {
    cfg.print(out);
    return out;
}

} // namespace MIG

#endif // MONINFOGATHERER_CONFIG_H
