#ifndef MONINFOGATHERER_SMALLINTSET_H
#define MONINFOGATHERER_SMALLINTSET_H

//-----------------
// C/C++ Headers --
//-----------------
#include <climits>
#include <iosfwd>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Class representing set of unsigned integer numbers.
 *
 *  This works efficiently for small non-negative integers only, do not try
 *  to use it for numbers larger than few hundreds. Implementation uses
 *  bitset (array of bit buckets). Most useful set operations are defined.
 */
class SmallIntSet {
public:

    SmallIntSet() = default;
    SmallIntSet(const SmallIntSet&) = default;
    SmallIntSet(SmallIntSet&&) = default;

    SmallIntSet& operator=(const SmallIntSet&) = default;
    SmallIntSet& operator=(SmallIntSet&&) = default;

    /// Return number of elements in a set.
    unsigned size() const {
        unsigned size = 0;
        for (auto word: m_buckets) {
            // use gcc builtin method
            size += __builtin_popcountl(word);
        }
        return size;
    }

    /// Return true if set is empty.
    bool empty() const {
        for (auto word: m_buckets) {
            if (word != 0) return false;
        }
        return true;
    }

    /// Add new number to the set.
    void add(unsigned num) {
        auto p = bucket_and_mask(num);
        unsigned word = p.first;
        if (word >= m_buckets.size()) {
            m_buckets.resize(word + 1);
        }
        m_buckets[word] |= p.second;
    }

    /// Remove number from the set. Nothing happens if number is not in the set.
    void remove(unsigned num) {
        auto p = bucket_and_mask(num);
        if (p.first < m_buckets.size()) {
            m_buckets[p.first] &= ~p.second;
        }
    }

    /// Remove all elements from a set
    void clear() {
        m_buckets.clear();
    }

    /// Check if number is in the set.
    bool contains(unsigned num) const {
        auto p = bucket_and_mask(num);
        return p.first < m_buckets.size() && m_buckets[p.first] & p.second;
    }

    /// Return minimum number in the set or -1 if set is empty
    int min_element() const;

    /// Return maximum number in the set or -1 if set is empty
    int max_element() const;

    /// Return true if two sets are disjoint (have no common elements)
    bool isdisjoint(const SmallIntSet& other) const {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        for (unsigned i = 0; i != len; ++ i) {
            if (m_buckets[i] & other.m_buckets[i]) return false;
        }
        return true;
    }

    /// Return true if two sets are equal
    bool operator==(const SmallIntSet& other) const {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        for (unsigned i = 0; i != len; ++ i) {
            if (m_buckets[i] != other.m_buckets[i]) return false;
        }
        for (unsigned i = len; i != m_buckets.size(); ++ i) {
            if (m_buckets[i] != 0) return false;
        }
        for (unsigned i = len; i != other.m_buckets.size(); ++ i) {
            if (other.m_buckets[i] != 0) return false;
        }
        return true;
    }

    /// Return true if two sets are not equal
    bool operator!=(const SmallIntSet& other) const {
        return ! this->operator==(other);
    }

    /// Return true if this set is a subset of other
    bool issubset(const SmallIntSet& other) const {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        for (unsigned i = 0; i != len; ++ i) {
            if ((m_buckets[i] & other.m_buckets[i]) != m_buckets[i]) return false;
        }
        for (unsigned i = len; i != m_buckets.size(); ++ i) {
            if (m_buckets[i] != 0) return false;
        }
        return true;
    }

    /// Return true if this set is a superset of other
    bool issuperset(const SmallIntSet& other) const {
        return other.issubset(*this);
    }

    /// In-place union operator
    SmallIntSet& operator|=(const SmallIntSet& other) {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        for (unsigned i = 0; i != len; ++ i) {
            m_buckets[i] |= other.m_buckets[i];
        }
        if (other.m_buckets.size() > m_buckets.size()) {
            m_buckets.insert(m_buckets.end(), other.m_buckets.begin() + len, other.m_buckets.end());
        }
        return *this;
    }

    /// In-place intersection operator
    SmallIntSet& operator&=(const SmallIntSet& other) {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        m_buckets.resize(len);
        for (unsigned i = 0; i != len; ++ i) {
            m_buckets[i] &= other.m_buckets[i];
        }
        return *this;
    }

    /// In-place difference operator
    SmallIntSet& operator-=(const SmallIntSet& other) {
        unsigned len = std::min(m_buckets.size(), other.m_buckets.size());
        for (unsigned i = 0; i != len; ++ i) {
            m_buckets[i] &= ~other.m_buckets[i];
        }
        return *this;
    }

    void print(std::ostream& out) const;

private:

    // "long" because of __builtin_popcountl
    using word_t = unsigned long;
    enum { WORD_BITS = sizeof(word_t)*CHAR_BIT };

    // return word index and mask for a given set member
    static std::pair<unsigned, word_t> bucket_and_mask(unsigned num) {
        unsigned word = num / WORD_BITS;
        word_t mask = word_t(1) << (num % WORD_BITS);
        return std::make_pair(word, mask);
    }

    std::vector<word_t> m_buckets;
};

inline
std::ostream& operator<<(std::ostream& out, const SmallIntSet& set) {
    set.print(out);
    return out;
}

inline
SmallIntSet operator|(const SmallIntSet& lhs, const SmallIntSet& rhs) {
    SmallIntSet result(lhs);
    result |= rhs;
    return result;
}

inline
SmallIntSet operator&(const SmallIntSet& lhs, const SmallIntSet& rhs) {
    SmallIntSet result(lhs);
    result &= rhs;
    return result;
}

inline
SmallIntSet operator-(const SmallIntSet& lhs, const SmallIntSet& rhs) {
    SmallIntSet result(lhs);
    result -= rhs;
    return result;
}

} // namespace MIG

#endif // MONINFOGATHERER_SMALLINTSET_H
