// -*- c++ -*-
#ifndef MONINFOGATHERER_HANDLERMANAGER_H
#define MONINFOGATHERER_HANDLERMANAGER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <unordered_map>
#include <memory>
#include <functional>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace MIG {
class HandlerBase;
class InfoPublisher;
}

namespace MIG {

/**
 *  Factory for handler instances.
 *
 *  Clients register factory functions for each handler name and later
 *  call `getHandler()` method to obtain new instance of a handler.
 *
 *  Note that this class is not thread safe, all factory registrations
 *  must happen at initialization time in one thread, `getHandler()`
 *  can be called after that from any thread.
 */
class HandlerManager {
public:

    using HandlerFactory = std::function<std::shared_ptr<HandlerBase>(const HandlerConfig& handlerConfig,
                                                                      InfoPublisher& publisher)>;

    /**
     *  Returns HandlerManager instance.
     */
    static HandlerManager& instance();

    /**
     *  Returns new handler instance with the given name.
     *
     *  Returns 0 if algorithm is not found.
     *
     *  @param[in] handlerProp Configuration object for new handler
     *  @param[in] publisher Reference to a publisher object
     */
    std::shared_ptr<HandlerBase> getHandler(const HandlerConfig& handlerConfig,
                                            InfoPublisher& publisher) const;

    /**
     *  Registers handler factory to the library.
     *
     *  @param[in] name name of the handler.
     *  @param[in] factoryFunction Function which creates instances of handlers.
     *  @return true for successful registration, false if name was already registered.
     */
    bool registerHandler(const std::string& name, HandlerFactory factoryFunction);

private:

    HandlerManager() = default;

    HandlerManager(const HandlerManager&) = delete;
    HandlerManager& operator=(const HandlerManager&) = delete;

    std::unordered_map<std::string, HandlerFactory> m_handlerConstructors;
};

} // namespace MIG

#endif // MONINFOGATHERER_HANDLERMANAGER_H
