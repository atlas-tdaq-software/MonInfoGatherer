// -*- c++ -*-
#ifndef MONINFOGATHERER_INFOHANDLERWRAPPER_H
#define MONINFOGATHERER_INFOHANDLERWRAPPER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <atomic>
#include <string>
#include <vector>
#include <memory>
#include <regex>
#include <tbb/concurrent_unordered_map.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/Config.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class IPCPartition;
class ISInfo;
class ISCallbackInfo;
class ISInfoReceiver;
namespace MIG {
class HandlerBase;
class TimingSaver;
}

namespace MIG {
/**
 *  Class which handles subscription to IS data and publication
 *  of summed data from a single handler.
 *
 *  This class wraps single handler instance and knows how to subscribe
 *  for input IS data and how to publish output IS data.
 */
class InfoHandlerWrapper : public InfoPublisher {
public:

    /**
     *  Make new instance.
     *
     *  @param cfg            Configuration with subscription parameters
     *  @param providerName   Provider name uses to publish the results
     *  @param partition      Partition where output IS server reside
     */
    InfoHandlerWrapper(const Config& cfg,
                       const std::string& providerName,
                       const IPCPartition& partition);

    ~InfoHandlerWrapper();

    InfoHandlerWrapper(const InfoHandlerWrapper&) = delete;
    InfoHandlerWrapper& operator=(const InfoHandlerWrapper&) = delete;

    /**
     *  Subscribe and start receiving IS data.
     *
     *  @param isr   Receiver instance used for subscription
     */
    void subscribe(ISInfoReceiver& isr);

    /**
     *  Unsubscribe and stop receiving IS data.
     *
     *  No new data will be receives by a callback once this method returns.
     *
     *  @param isr   Receiver instance used for subscription
     */
    void unsubscribe(ISInfoReceiver& isr);

    // implements InfoPublisher interface
    void publish(ISInfo& info, const std::string& name, int nProv) override;

    /**
     *  Save all collected timing information.
     */
    void saveTiming(MIG::TimingSaver& saver);

    /**
     * Write stats to an open file in InfluxDB format.
     */
    void saveStats(int fd);

private:

    // IS callback method
    void _callbackHandler(ISCallbackInfo* cbinfo);

    class MyOHProvider;

    typedef std::pair<std::regex, std::shared_ptr<HandlerBase>> Handler;

    Config const m_cfg;
    std::string const m_myProviderName;
    std::vector<Handler> m_handlers;
    std::string const m_fullRe;
    std::vector<std::unique_ptr<MyOHProvider>> m_providers;
    std::atomic<unsigned> m_callbackCount{0};  // counts callbacks
    std::atomic<unsigned> m_publishCount{0};  // counts publish callbacks
    tbb::concurrent_unordered_map<std::string, unsigned> m_callbacksPerProvider;
};
} // namespace MIG

#endif // MONINFOGATHERER_INFOHANDLERWRAPPER_H
