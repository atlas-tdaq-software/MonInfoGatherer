#ifndef MIG_CLOCK_H
#define MIG_CLOCK_H

#include <chrono>
#include <memory>

namespace MIG {
namespace test {
template <typename T> class ClockFriendHack;
}}

namespace MIG {

/**
 * Abstract clock class providing time stamps.
 *
 * Some MIG classes will rely on this interface for obtaining current time
 * instead of using std::chrono directly. Default implementation used in
 * production code (see DefaultClock class below) uses
 * `std::chrono::steady_clock` class for its functionality. Alternative
 * implementations are used for testing, test code needs more control over
 * time advancement.
 */
class Clock {
public:

    using clock_type = std::chrono::steady_clock;
    using duration = clock_type::duration;
    using time_point = clock_type::time_point;

    /**
     * Return clock instance.
     *
     * Normally in production code this instance will use system clock
     * (std::chrono::steady_clock type). Unit tests can replace this instance
     * with something different.
     */
    static Clock const& instance() { return *s_instance; }

    virtual ~Clock() = default;

    /**
     * Return current time.
     *
     * The method is guaranteed to be thread-safe.
     */
    virtual time_point now() const = 0;

private:

    // Allow access to internals from unit tests
    template <typename T> friend class ::MIG::test::ClockFriendHack;

    static std::unique_ptr<Clock> s_instance;
};

} // namespace MIG

#endif // MIG_CLOCK_H
