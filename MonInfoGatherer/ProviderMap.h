// -*- c++ -*-
#ifndef MONINFOGATHERER_PROVIDERMAP_H
#define MONINFOGATHERER_PROVIDERMAP_H

//-----------------
// C/C++ Headers --
//-----------------
#include <mutex>
#include <string>
#include <unordered_map>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  Class which implements thread-safe mapping of provider name
 *  to its numeric index.
 */
class ProviderMap {
public:

    /**
     *  Return provider index for given name.
     *
     *  @param provider  Provider name
     *  @returns tuple<unsigned, unsigned, bool>, first element is provider index,
     *    second element is total number of providers, third element is true if
     *    new provider was added to mapping.
     */
    std::tuple<unsigned, unsigned, bool> index(const std::string& provider) {
        std::lock_guard<std::mutex> lock(m_mutex);
        auto it = m_providerMap.find(provider);
        if (it != m_providerMap.end()) {
            return std::make_tuple(it->second, m_providerMap.size(), false);
        }
        // add new mapping
        unsigned index = m_providerMap.size();
        m_providerMap.insert(std::make_pair(provider, index));
        return std::make_tuple(index, m_providerMap.size(), true);
    }

private:

    std::mutex m_mutex;
    std::unordered_map<std::string, unsigned> m_providerMap;
};
} // namespace MIG

#endif // MONINFOGATHERER_PROVIDERMAP_H
