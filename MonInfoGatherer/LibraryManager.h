// -*- c++ -*-
#ifndef __MIG_LIBRARY_MANAGER_H
#define __MIG_LIBRARY_MANAGER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <unordered_map>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace MIG {

/**
 *  A utility class to manage shared libraries of containers and
 *  algorithms.
 */
class LibraryManager {
public:

    /// Returns an instance of LibraryManager.
    static LibraryManager& instance();

    /**
     *  Loads specified library.
     *
     *  Loads dynamic library, tries to find and execute register_objects() method
     *  from it.
     *
     *  @param libName Dynamic library name
     *  @returns True if library is loaded successfully.
     */
    bool loadLibrary(const std::string &libName);

private:

    LibraryManager() = default;
    ~LibraryManager();

    LibraryManager(const LibraryManager&) = delete;
    LibraryManager& operator=(const LibraryManager&) = delete;

    std::unordered_map<std::string, void*> m_libraries;
};

}

#endif
