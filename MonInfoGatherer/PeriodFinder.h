#ifndef MIG_PERIODFINDER_H
#define MIG_PERIODFINDER_H

#include <chrono>
#include <map>

namespace MIG {
namespace test {
template <typename T> class PeriodFinderFriendHack;
}}

namespace MIG {

/**
 * Class that tries to find publication period from a sequence of timestamps.
 *
 * Most or all IS publication that MIG receives are made periodically with a
 * fixed period (though few pubs may be published randomly). This class
 * receives publication timestamps and tries to deduce actual publication
 * period from that.
 */
class PeriodFinder {
public:

    /**
     * Constructor
     *
     * @param cutoff_sec Maximum publication interval that can exist, seconds.
     */
    explicit PeriodFinder(std::chrono::seconds cutoff = std::chrono::seconds(300))
        : m_cutoff(cutoff) {}

    PeriodFinder(const PeriodFinder&) = default;
    PeriodFinder& operator=(const PeriodFinder&) = default;
    PeriodFinder(PeriodFinder&&) = default;
    PeriodFinder& operator=(PeriodFinder&&) = default;

    /**
     * Add one more timestamp.
     *
     * @param timestamp Timestamp in microseconds since epoch.
     */
    void add(std::chrono::microseconds timestamp);

    /**
     * Return an estimate of the period in seconds, zero is returned if no
     * estimate can be produced.
     *
     * If period cannot be determined (less than two timestamps were added)
     * then zero is returned.
     */
    std::chrono::seconds period() const;

    /**
     * Reset all statistics, same as constructing from  scratch.
     */
    void reset();

    /**
     * Merge data from other finder, which is supposed to be consistent.
     *
     * `merge()` resets last added timestamp to 0.
     *
     */
    void merge(const PeriodFinder& other);

private:

    // Allow access to internals from unit tests
    template <typename T> friend class ::MIG::test::PeriodFinderFriendHack;

    std::chrono::seconds m_cutoff;
    std::chrono::microseconds m_last{0};  // Last added timestamp
    size_t m_count = 0;   // Total count of periods added
    std::chrono::microseconds m_period_sum{0};   // Total sum of periods added
    mutable std::map<unsigned, size_t> m_histo; // histogram of the per-second periods
};

} // namespace MIG

#endif // MIG_PERIODFINDER_H
