// -*- c++ -*-
#ifndef MONINFOGATHERER_INFOPUBLISHER_H
#define MONINFOGATHERER_INFOPUBLISHER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfo;

namespace MIG {

/**
 *  Interface for publisher classes.
 *
 *  Publisher class is used by MIG to publish summed results. Standard
 *  implementation would publish data in IS server, other implementations
 *  are possible too (e.g. for testing purposes).
 *
 */
class InfoPublisher {
public:

    virtual ~InfoPublisher() = default;

    /**
     *  Method called to publish info object.
     *
     *  This method can throw exceptions for error conditions.
     *
     *  @param info Object to publish
     *  @param name Object name, does not include provider name (publisher can
     *              add provider to the name before publishing).
     *  @param nContrib Number of contributions summed in this object.
     */
    virtual void publish(ISInfo& info, const std::string& name, int nContrib) = 0;
};
} // namespace MIG

#endif // MONINFOGATHERER_INFOPUBLISHER_H
