#ifndef MIG_TIMERS_H
#define MIG_TIMERS_H

#include <deque>
#include <iosfwd>
#include <iterator>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <vector>


namespace MIG {

#if MIG_TIMING_ENABLE

/**
 *  Timer which measures elapsed wall clock time.
 */
class WCTimer {
public:

    explicit WCTimer(bool start_=false) {
        if (start_) start();
    }

    // start timer, can be called repeatedly
    void start();

    // stop timer, can be called repeatedly
    void stop();

    // return accumulated value as total number of nanoseconds,
    // should only be called when timer is stopped
    uint64_t nsec() const { return m_count; }

    // disable this timer, start/stop will do nothing
    void disable() { m_enabled = false; }

private:
    bool m_enabled = true;  // if false then start/stop do nothing
    struct timespec m_ts{0, 0};
    uint64_t m_count = 0;
};


/**
 *  Timer which measures system and user CPU time.
 *
 *  By default it measures per-thread time, can change it via template param.
 *  Typical time resolution for CPU time is 1 millisec.
 */
class CPUTimer {
public:

    explicit CPUTimer(int kind=RUSAGE_THREAD, bool start_=false) :m_kind(kind) {
        if (start_) start();
    }

    // start timer, can be called repeatedly
    void start();

    // stop timer, can be called repeatedly
    void stop();

    // return accumulated value as total number of microseconds,
    // should only be called when timer is stopped
    uint32_t usec() const { return m_count; }

    // disable this timer, start/stop will do nothing
    void disable() { m_enabled = false; }

private:
    int m_kind;
    bool m_enabled = true;  // if false then start/stop do nothing
    struct timeval m_utime{0, 0};
    struct timeval m_stime{0, 0};
    uint32_t m_count = 0;
    bool m_ticking = false;
};


/**
 *  Various time measurementrs from the same callback
 */
struct TimingEntry {
    std::string provider;         // provider name
    uint64_t publish_time_us;     // IS info object publish time (from object itself)
    uint64_t receive_time_us;     // Callback time (local time)
    uint64_t total_real_ns;       // total real time spent in a callback
    uint32_t total_cpu_us;        // total CPU (user+sys) time spent in a callback
    uint64_t lock_wait_real_ns;   // real time spent on lock wait
    uint64_t algo_real_ns;        // real time spent in summing algo
    uint32_t algo_cpu_us;         // CPU time spent in summing algo
    uint64_t publish_real_ns;     // real time spent on publish call
    uint32_t bin_count;           // number of bins in a histogram
};

/**
 *  Collection for TimingEntry instances
 */
class TimingEntries {
public:

    template <typename Iter1, typename Iter2>
    struct concat_iterator {

        typedef std::forward_iterator_tag iterator_category;
        typedef typename std::iterator_traits<Iter1>::value_type value_type;
        typedef value_type* pointer;
        typedef const value_type& const_reference;

        concat_iterator( Iter1 b1, Iter1 e1, Iter2 b2, Iter2 e2 )
            : seq1( b1 ), seq1end( e1 ), seq2( b2 ), seq2end( e2 ) {}

        concat_iterator& operator++() {
            if (seq1 != seq1end) {
                ++seq1;
            } else {
                ++seq2;
            }
            return *this;
        }
        const_reference operator*() const { return seq1 != seq1end ? *seq1 : *seq2; }
        pointer operator->() const { return seq1 != seq1end ? &(*seq1) : &(*seq2); }
        bool operator==(concat_iterator const& rhs) const {
            return seq1 == rhs.seq1 && seq1end == rhs.seq1end
                && seq2 == rhs.seq2 && seq2end == rhs.seq2end;
        }
        bool operator!=(concat_iterator const& rhs) const { return !(*this == rhs); }

    private:
        Iter1 seq1;
        Iter1 seq1end;
        Iter2 seq2;
        Iter2 seq2end;
    };

    typedef std::vector<TimingEntry>::const_iterator _iter1;
    typedef std::deque<TimingEntry>::const_iterator _iter2;
    typedef concat_iterator<_iter1, _iter2> const_iterator;

    TimingEntries() {}

    // do not want to copy this
    TimingEntries(TimingEntries const&) = delete;
    TimingEntries& operator=(TimingEntries const&) = delete;

    // add one more entry, thread-safe
    void append(TimingEntry const& entry);

    const_iterator begin() const {
        return const_iterator(m_entries1.begin(), m_entries1.end(), m_entries2.begin(), m_entries2.end());
    }
    const_iterator end() const {
        return const_iterator(m_entries1.end(), m_entries1.end(), m_entries2.end(), m_entries2.end());
    }

    bool empty() const { return m_entries1.empty(); }

    /**
     * Set per-container history size.
     *
     * Default history size is 0 which disables history for this container and
     * ignores append() call. Use this method to change history size before
     * calling append() if you want to save history.
     */
    void setHistorySize(unsigned size) {
        m_historySize = size;
        m_entries1.reserve(size / 2);
    }

    unsigned historySize() const { return m_historySize; }

private:
    unsigned m_historySize = 0;
    std::vector<TimingEntry> m_entries1;
    std::deque<TimingEntry> m_entries2;
    std::mutex m_mutex;
};

/**
 *  Class that has a bunch of timers than can be started/stopped by client.
 *  It has a method to trnsfer all values of all timers into TimingEntries collection.
 */
class TimingCollector {
public:
    TimingCollector(TimingEntries& entries, std::string const& provider,
                    uint64_t publish_time_us, const uint64_t receive_time_us)
        : m_entries(entries), m_provider(provider),
          m_publish_time_us(publish_time_us), m_receive_time_us(receive_time_us),
          m_enabled(entries.historySize() > 0)
    {
        if (not m_enabled) {
            total_real_time.disable();
            total_cpu_time.disable();
            lock_wait_real_time.disable();
            algo_real_time.disable();
            algo_cpu_time.disable();
            publish_real_time.disable();
        }
    }

    // do not want to copy this
    TimingCollector(TimingCollector const&) = delete;
    TimingCollector& operator=(TimingCollector const&) = delete;

    ~TimingCollector() { save(); }

    // save all accumulated timing to TimingEntries
    void save();

    // all timers are public
    WCTimer total_real_time;
    CPUTimer total_cpu_time;
    WCTimer lock_wait_real_time;
    WCTimer algo_real_time;
    CPUTimer algo_cpu_time;
    WCTimer publish_real_time;
    uint32_t bin_count = 0;

private:
    TimingEntries& m_entries;
    const std::string m_provider;
    const uint64_t m_publish_time_us;     // IS info object publish time (from object itself)
    const uint64_t m_receive_time_us;     // Callback time (local time)
    bool m_saved = false;
    bool m_enabled = true;  // if false then do nothing (as an optimization)
};

/**
 *  Class which dumps complete accumulated info to a bunch of CSV files.
 */
class TimingSaver {
public:

    explicit TimingSaver(std::string const& fnamePfx) : m_fnamePfx(fnamePfx) {}

    TimingSaver(TimingSaver const&) = delete;
    TimingSaver& operator=(TimingSaver const&) = delete;

    void saveEntries(std::string const& objName, TimingEntries const& entries);

    void close();

private:

    void makeStreams();

    std::string const m_fnamePfx;
    std::unique_ptr<std::ostream> m_objNameStream;
    std::unique_ptr<std::ostream> m_provNameStream;
    std::unique_ptr<std::ostream> m_entriesStream;
    std::map<std::string, unsigned> m_objNameMap;
    std::map<std::string, unsigned> m_provNameMap;
};

#else // MIG_TIMING_ENABLE

// ===================================================================================
// "Optimized" code which compiles to nothing when MIG_TIMING_ENABLE is 0 or undefined
// ===================================================================================

class WCTimer {
public:
    explicit WCTimer(bool start_=false) {}
    void start() {}
    void stop() {}
    uint64_t nsec() const { return 0; }
};

class CPUTimer {
public:
    explicit CPUTimer(int kind=RUSAGE_THREAD, bool start_=false) {}
    void start() {}
    void stop() {}
    uint32_t usec() const { return 0; }
};

struct TimingEntry {
};

class TimingEntries {
public:
    TimingEntries() {}
    TimingEntries(TimingEntries const&) = delete;
    TimingEntries& operator=(TimingEntries const&) = delete;
    void append(TimingEntry const& entry) {}
    static void setHistorySize(unsigned size) {}
private:
};

class TimingCollector {
public:
    TimingCollector(TimingEntries& entries, std::string const& provider,
                    uint64_t publish_time_us, const uint64_t receive_time_us) {}
    TimingCollector(TimingCollector const&) = delete;
    TimingCollector& operator=(TimingCollector const&) = delete;
    ~TimingCollector() {}
    void save() {}
    // all timers are public
    WCTimer total_real_time;
    CPUTimer total_cpu_time;
    WCTimer lock_wait_real_time;
    WCTimer algo_real_time;
    CPUTimer algo_cpu_time;
    WCTimer publish_real_time;
    uint32_t bin_count = 0;
};

class TimingSaver {
public:
    explicit TimingSaver(std::string const& fnamePfx) {}
    TimingSaver(TimingSaver const&) = delete;
    TimingSaver& operator=(TimingSaver const&) = delete;
    void saveEntries(std::string const& objName, TimingEntries const& entries) {}
    void close() {}
};

#endif // MIG_TIMING_ENABLE

} // namespace MIG

#endif // MIG_TIMERS_H
