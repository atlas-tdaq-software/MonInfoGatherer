// -*- c++ -*-
#ifndef MONINFOGATHERER_HANDLERCOMMONBASE_H
#define MONINFOGATHERER_HANDLERCOMMONBASE_H
//
// Declaration of HandlerCommonBase Class
//

//-----------------
// C/C++ Headers --
//-----------------
#include <atomic>
#include <memory>
#include <string>
#include <tbb/concurrent_unordered_map.h>

//----------------------
// Base Class Headers --
//----------------------
#include "MonInfoGatherer/HandlerBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/Timers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoAny;
class ISType;
namespace MIG {
class ContainerBase;
class InfoPublisher;
}

namespace MIG {

/**
 *  Partial implementation of HandlerBase common to all handler classes.
 *
 *  This implementation makes new container instance (ContainerBase class) for
 *  each separate object name and forwards object data to those containers.
 *  Subclasses will need to implement `makeContainer()` method to instantiate
 *  particular container type.
 */
class HandlerCommonBase : public HandlerBase {
public:
    HandlerCommonBase(const HandlerConfig& handlerConfig,
                      InfoPublisher& publisher);
    ~HandlerCommonBase();

    /**
     *  Method called by IS callback on every new update.
     *
     *  Implementation in this class just calls `_updateInfo_exc()` method
     *  catches any exceptions raised by it and produces error message for
     *  an exception. Sub-class that needs to do something else with exceptions
     *  has to override this method.
     */
    void updateInfo(ISInfoAny& info, const std::string& name, const std::string& provider) override;

    void saveTiming(MIG::TimingSaver& saver) override;

protected:

    /**
     *  This method contains default implementation of updateInfo which can throw exceptions.
     *  Implementation of updateInfo() in this class calls this method and handles possible
     *  exceptions by printing error messages.
     */
    void _updateInfo_exc(ISInfoAny& info, const std::string& name, const std::string& provider);

private:

    /**
     *  This method is called to create new container instances.
     */
    virtual std::shared_ptr<ContainerBase>
    makeContainer(const std::string& name, const ISType& currType,
                  InfoPublisher& publisher, const ConfigParameters& parameters) = 0;

    InfoPublisher& m_publisher;
    tbb::concurrent_unordered_map<std::string, std::shared_ptr<ContainerBase>> m_containers;
    ConfigParameters const m_parameters;
    std::atomic<unsigned> m_updateCount{0};  // total number of calls to updateInfo()
};
} // namespace MIG

#endif // MONINFOGATHERER_HANDLERCOMMONBASE_H
