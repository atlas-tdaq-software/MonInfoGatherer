#ifndef  MONINFOGATHERER_EXCEPTIONS_H
#define  MONINFOGATHERER_EXCEPTIONS_H

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <ers/ers.h>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

ERS_DECLARE_ISSUE( MIG, Exception, ERS_EMPTY, ERS_EMPTY)
//Generic incompatibility class
ERS_DECLARE_ISSUE_BASE(MIG,
        FailedObjects,
        MIG::Exception,
        "Following objects had issues during running:\n" << reason,
        ERS_EMPTY,
        ((std::string)reason)
)
ERS_DECLARE_ISSUE_BASE(MIG,
        IncompatibleTypes,
        MIG::Exception,
        "Histogram \"" << name << "\" instances have incompatible types: "
                << typeName1 << " and " << typeName2,
        ERS_EMPTY,
        ((std::string)name)
        ((std::string)typeName1)
        ((std::string)typeName2)
)
ERS_DECLARE_ISSUE_BASE(MIG,
        UnsupportedType,
        MIG::Exception,
        "Histogram \"" << name << "\" has unsupported type: " << typeName,
        ERS_EMPTY,
        ((std::string)name)
        ((std::string)typeName)
)
ERS_DECLARE_ISSUE_BASE(MIG,
        UnsupportedObjectType,
        MIG::Exception,
        "IS object has unsupported type: " << typeName,
        ERS_EMPTY,
        ((std::string)typeName)
)
ERS_DECLARE_ISSUE_BASE(MIG,
        IncompatibleAxisBinning,
        MIG::Exception,
        "Axis \"" << name << "\" has incompatible binning: " << reason,
        ERS_EMPTY,
        ((std::string)name)
        ((std::string)reason)
)
ERS_DECLARE_ISSUE_BASE(MIG,
        IncompatibleBinning,
        MIG::Exception,
        "Histogram \"" << name << "\" has incompatible binning: " << reason,
        ERS_EMPTY,
        ((std::string)name)
        ((std::string)reason)
)
ERS_DECLARE_ISSUE_BASE( MIG,
        IncompatibleDimensions,
        MIG::Exception,
        "Histogram \"" << name << "\" instances have different dimensions: " << dim1 << " vs " << dim2,
        ERS_EMPTY,
        ((std::string)name)
        ((int)dim1)
        ((int)dim2)
)
ERS_DECLARE_ISSUE_BASE( MIG,
        UnexpectedBinCount,
        MIG::Exception,
        "Unexpected size of " << name << " array: expected " << expected << " bins, actual count is " << actual,
        ERS_EMPTY,
        ((std::string)name)
        ((int)expected)
        ((int)actual)
)
ERS_DECLARE_ISSUE_BASE( MIG,
        MissingLabelMap,
        MIG::Exception,
        "Label map is not provided",
        ERS_EMPTY,
)
ERS_DECLARE_ISSUE_BASE( MIG,
        AlgorithmIssue,
        MIG::Exception,
        "Algorithm failed for " << provider << "." << name << " (tag=" << tag <<")",
        ERS_EMPTY,
        ((std::string)provider)
        ((std::string)name)
        ((int)tag)
)
ERS_DECLARE_ISSUE_BASE( MIG,
        HandlerError,
        MIG::Exception,
        "Update failed for " << provider << "." << name,
        ERS_EMPTY,
        ((std::string)provider)
        ((std::string)name)
)
ERS_DECLARE_ISSUE(MIG,
        ConfigurationError,
        "MIG configuration failure: " << type,
        ((std::string)type )
)
ERS_DECLARE_ISSUE(MIG,
        ISDuplicateTimestamp,
        "Received duplicate IS Timestamp (potential is_server issue): "
        << " object=\"" << objectName
        << "\" provider=\"" << providerName << "\""
        << " previous timestamp=" << previousTime
        << " current timestamp=" << timeNow,
        ((std::string)objectName)((std::string)providerName)
        ((uint64_t)previousTime)((uint64_t)timeNow)
)
ERS_DECLARE_ISSUE(MIG,
        NonEmptyDynLabelled,
        "Non-empty bins in dynamically labelled axis: "
        << " histogram title=\"" << title << "\"",
        ((std::string)title)
)

#endif // MONINFOGATHERER_EXCEPTIONS_H
