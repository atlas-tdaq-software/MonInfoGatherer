#ifndef MONINFOGATHERER_LABELMAP_H
#define MONINFOGATHERER_LABELMAP_H
//--------------------------------------------------------------------------
// File and Version Information:
//      Helper class to facilitate label re-mapping
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>
#include <unordered_map>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "oh/core/Axis.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace MIG {

/// @addtogroup MonInfoGatherer

/**
 *  @ingroup MonInfoGatherer
 *
 *  Encapsulation of the label map structure for single axis.
 *
 */
class AxisLabelMap  {
public:

    /**
     *  Default constructor.
     *
     *  Creates an instance with disabled label maps for all dimensions.
     */
    AxisLabelMap() = default;

    /**
     *  Constructor from pre-existing axis.
     *
     *  Fills maps from labels defined in axis.
     */
    AxisLabelMap(const oh::Axis& axis);

    // want all copy/move semantics enabled
    AxisLabelMap(const AxisLabelMap& axis) = default;
    AxisLabelMap(AxisLabelMap&& axis) = default;
    AxisLabelMap& operator=(const AxisLabelMap& axis) = default;
    AxisLabelMap& operator=(AxisLabelMap&& axis) = default;

    /**
     *  Check whether label maps are enabled for given axis
     */
    bool hasLabelMap() const { return m_hasLabels; }

    /**
     * Find bin index for given label name.
     *
     * Returns -1 if label is not known.
     */
    int index(const std::string& label) const {
        auto iter = m_map.find(label);
        if (iter != m_map.end()) {
            return iter->second;
        }
        return -1;
    }

    /**
     *  Add new label.
     *
     *  The index for new label will be equal to current number of labels
     *  plus one (bin numbering starts with 1). New label index is returned
     *  from the method. Returns -1 if label is already present.
     */
    int addLabel(const std::string& label) {
        int index = m_map.size() + 1;
        auto res = m_map.insert(std::make_pair(label, index));
        return res.second ? index : -1;
    }

    /// re-initialize maps
    void clear();

private:

    bool m_hasLabels = false;
    std::unordered_map<std::string, int> m_map;

};

/**
 *  @ingroup MonInfoGatherer
 *
 *  Encapsulation of the label map structures.
 *
 */
class LabelMap  {
public:

    /**
     *  Default constructor.
     *
     *  Creates an instance with disabled label maps for all dimensions.
     */
    LabelMap() = default;

    /**
     *  Constructor from pre-existing axes.
     *
     *  Fills maps from labels defined in axes.
     */
    LabelMap(const std::vector<oh::Axis>& axes);

    // want all copy/move semantics enabled
    LabelMap(const LabelMap& axis) = default;
    LabelMap(LabelMap&& axis) = default;
    LabelMap& operator=(const LabelMap& axis) = default;
    LabelMap& operator=(LabelMap&& axis) = default;

    /**
     *  Check whether label maps are enabled for given axis
     */
    bool hasLabelMap(int axis) const { return m_maps[axis].hasLabelMap(); }

    /**
     *  Get axis map
     */
    AxisLabelMap& axisMap(int axis) { return m_maps[axis]; }

    /**
     *  Check whether label maps are enabled for any axis
     */
    bool hasLabelMaps() const {
        return m_maps[0].hasLabelMap() || m_maps[1].hasLabelMap() || m_maps[2].hasLabelMap();
    }

    /**
     * Find bin index for given label name on a given axis.
     *
     * Returns -1 if label is not known.
     */
    int index(int axis, const std::string& label) const {
        return m_maps[axis].index(label);
    }

    /**
     *  Add new label to a given axis.
     *
     *  The index for new label will be equal to current number of labels
     *  plus one (bin numbering starts with 1). New label index is returned
     *  from the method. Returns -1 if label is already present.
     */
    int addLabel(int axis, const std::string& label) {
        return m_maps[axis].addLabel(label);
    }

    /// re-initialize maps
    void clear();

private:

    AxisLabelMap m_maps[3];

};

} // namespace MonInfoGatherer

#endif // MONINFOGATHERER_LABELMAP_H
