// -*- c++ -*-
#ifndef MONINFOGATHERER_HANDLERBASE_H
#define MONINFOGATHERER_HANDLERBASE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoAny;
class ISInfo;
namespace MIG {
class TimingSaver;
}

namespace MIG {

/**
 *  Base class for Handler implementations.
 *
 *  Handlers are responsible of receiving IS callbacks and forwarding them
 *  to appropriate container object (which should be created by
 *  implementations of this interface). Most handlers would likely have
 *  very similar implementation which differs only in the type of the
 *  container being created for summing objects.
 */
class HandlerBase {
public:

    virtual ~HandlerBase() = default;

    /**
     *  Called by IS callback on every new update.
     *
     *  @param info IS object data.
     *  @param name Name of the IS object.
     *  @param provider Name of the provider which published this object.
     */
    virtual void updateInfo(ISInfoAny& info, const std::string& name, const std::string& provider) = 0;

    /**
     *  Save all collected timing information.
     */
    virtual void saveTiming(MIG::TimingSaver& saver) {}

};
} // namespace MIG

#endif // MONINFOGATHERER_HANDLERBASE_H
