// -*- c++ -*-
#ifndef MONINFOGATHERER_MIGCONTROLLABLE_H
#define MONINFOGATHERER_MIGCONTROLLABLE_H
//

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <mutex>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "RunControl/Common/Controllable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "RunControl/Common/RunControlCommands.h"
#include "ers/ers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoReceiver;
namespace MIG {
class InfoHandlerWrapper;
}

namespace MIG {
/**
 *  Implementation of TDAQ Controllable interface for MIG.
 */
class MIGControllable: public daq::rc::Controllable {
public:
    /**
     *  Make an instance
     *
     *  @param name      Name of the provider for publishing summed data
     *  @param sleepFor  Number of seconds to sleep in each transition
     */
    MIGControllable(const std::string& providerName, unsigned int sleepFor);

    MIGControllable(MIGControllable const&) = delete;
    MIGControllable& operator=(MIGControllable const&) = delete;

    virtual ~MIGControllable() noexcept(true);

    void configure(const daq::rc::TransitionCmd& args) override;
    void connect(const daq::rc::TransitionCmd& args) override;
    void prepareForRun(const daq::rc::TransitionCmd& args) override;
    void stopGathering(const daq::rc::TransitionCmd& args) override;
    void disconnect(const daq::rc::TransitionCmd& args) override;
    void unconfigure(const daq::rc::TransitionCmd& args) override;
    void publish() override;

private:

    std::vector<std::unique_ptr<InfoHandlerWrapper>> m_handlers;
    unsigned int const m_sleep;
    std::string const m_providerName;
    std::unique_ptr<ISInfoReceiver> m_isr;
    int m_mon_fd = -1;
    std::mutex m_publishMutex;
};
} // namespace MIG

#endif // MONINFOGATHERER_MIGCONTROLLABLE_H
