// -*- c++ -*-
#ifndef MONINFOGATHERER_DEFAULTHANDLER_H
#define MONINFOGATHERER_DEFAULTHANDLER_H
//
// Declaration of DefaultHandler Class
// NOTE: Unlike other handler classes this class is used directly by other
// packages (GathererProxy) and it needs to in a public include folder.
//

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_unordered_map.h>

//----------------------
// Base Class Headers --
//----------------------
#include "MonInfoGatherer/HandlerCommonBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoAny;
class ISType;
namespace MIG {
class ContainerBase;
class HandlerConfig;
class InfoPublisher;
}

namespace MIG {

/**
 *  Implementation of handler interface with support for both
 *  histogram and non-histogram types.
 */
class DefaultHandler : public HandlerCommonBase {
public:

    /**
     * Regular constructor used by HandlerManager.
     */
    DefaultHandler(const HandlerConfig& handlerConfig, InfoPublisher& publisher);

    ~DefaultHandler();

    // Overrides base class method
    void updateInfo(ISInfoAny& info, const std::string& name, const std::string& provider) override;

private:

    // Overrides base class method
    std::shared_ptr<ContainerBase>
    makeContainer(const std::string& name, const ISType& currType,
                  InfoPublisher& publisher, const ConfigParameters& parameters) override;

    tbb::concurrent_unordered_map<std::string, std::string> m_failedObjectNames;
};
} // namespace MIG

#endif // MONINFOGATHERER_DEFAULTHANDLER_H
