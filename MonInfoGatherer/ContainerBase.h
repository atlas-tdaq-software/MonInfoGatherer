// -*- c++ -*-
#ifndef MONINFOGATHERER_CONTAINERBASE_H
#define MONINFOGATHERER_CONTAINERBASE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class ISInfoAny;
namespace MIG {
class TimingEntries;
}

namespace MIG {

/**
 *  Base class for MonInfoGatherer containers.
 *
 *  Container is an object which collects individual contributions for
 *  the same object (via operate() method) and merges them together (probably
 *  using external algorithm(s)). Merged result would be published by sub-class
 *  using their specific methods.
 */
class ContainerBase {
public:
    virtual ~ContainerBase() = default;

    /**
     *  Called by the handler with new contribution for an object.
     *
     *  @param[in] update updated object data.
     *  @param[in] providerName name of the provider publishing this update.
     */
    virtual void operate(ISInfoAny& update, const std::string& providerName) = 0;

    /**
     *  Returns pointer to timing information collected by container.
     */
    virtual MIG::TimingEntries const* timing() const { return nullptr; }
};
} // namespace MIG

#endif // MONINFOGATHERER_CONTAINERBASE_H
