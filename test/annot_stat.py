#!/usr/bin/env python

from __future__ import print_function, division

import argparse
import ispy
import os
import time

def main():

    parser = argparse.ArgumentParser(description='CPU monitoring.')
    parser.add_argument("-i", dest="interval", default=10, type=int,
                        help="Number of seconds between quering OH server")
    parser.add_argument("-p", dest="partition", default=None,
                        help="Partition name")
    parser.add_argument("server", help="OH server name")
    parser.add_argument("count", type=int, help="expected annotation count")
    args = parser.parse_args()

    part = args.partition or os.environ.get('TDAQ_PARTITION')
    part = ispy.IPCPartition(part)
    crit = ispy.ISCriteria(".*")

    # header
    print(",".join(["time", "full_fraction", "avg_frac"]))

    while True:
        
        now = time.time()
        next = (int(now + 0.1) // args.interval + 1) * args.interval
        wait = next - now
        # print ("sleep %f sec" % wait)
        time.sleep(wait)
        
        now = int(time.time() + 0.1)

        hist_count = 0
        full_count = 0
        sum_count = 0

        it = ispy.ISInfoIterator(part, args.server, crit)
        while it.next():
            any = ispy.ISInfoDynAny()            
            it.value(any)
            ann = getattr(any, "annotations", [])
            if ann and ann[0] == 'Gatherer':
                count = int(ann[1])
                sum_count += count
                hist_count += 1
                if count >= args.count:
                    full_count += 1

        full_fraction = 0.
        avg_frac = 0.
        if hist_count > 0:
            full_fraction = full_count / float(hist_count)
            avg_frac = sum_count / float(hist_count*args.count) 

        data = [str(now)] + ["%.3f" % x for x in (full_fraction, avg_frac)]
        print(",".join(data))

if __name__ == "__main__":
    main()
