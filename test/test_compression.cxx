#include <sys/time.h>
#include <sys/resource.h>
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <zlib.h>
#include <zstd.h>
#include <lz4.h>
#include <snappy.h>


namespace bpo = boost::program_options;

namespace {



struct Timer {

    Timer(long& to_store, unsigned iterations=1) : _to_store(to_store), _iterations(iterations) {
        getrusage(RUSAGE_SELF, &_start);
    }
    ~Timer() {
        struct rusage end;
        getrusage(RUSAGE_SELF, &end);
        long mksec = timeval_diff_usec(_start.ru_utime, end.ru_utime) + timeval_diff_usec(_start.ru_stime, end.ru_stime);
        _to_store = mksec / _iterations;
    }

    long timeval_diff_usec(struct timeval const& tv1, struct timeval const& tv2)
    {
        return (tv2.tv_sec - tv1.tv_sec) * 1000000 + tv2.tv_usec - tv1.tv_usec;
    }

    long& _to_store;
    unsigned _iterations;
    struct rusage _start;
};


void test_zlib(std::string const& data, int level, unsigned comp_iterations, unsigned decomp_iterations) {
    unsigned initialSize = data.size();
    uLongf destSize = compressBound(initialSize);
    std::unique_ptr<Bytef[]> compressedData(new Bytef[destSize]);
    long comp_time = 0, uncomp_time = 0;

    comp_iterations = std::max(1U, comp_iterations);
    uLongf compressedSize = 0;
    for (int loop = 0; loop != 2; ++loop) {  // run it twice so that CPU freq control has time to adjust
        Timer timer(comp_time, comp_iterations);
        // run in a loop
        for (unsigned i = 0; i != comp_iterations; ++i) {
            compressedSize = destSize;
            compress2(compressedData.get(), &compressedSize, reinterpret_cast<const Bytef*>(data.data()), data.size(), level);
        }
    }

    // now decompress it
    if (decomp_iterations > 0) {
        Timer timer(uncomp_time, decomp_iterations);
        std::unique_ptr<Bytef[]> decompressedData(new Bytef[initialSize]);
        for (unsigned i = 0; i != decomp_iterations; ++i) {
            uLongf decompressedSize = initialSize;
            uncompress(decompressedData.get(), &decompressedSize, compressedData.get(), compressedSize);
        }
    }

    std::cout << "zlib" << level << "    "
              << initialSize << "    " << compressedSize << "    "
              << (compressedSize*100 / initialSize) << "%    "
              << comp_time << " mksec    " << uncomp_time << " mksec    "
              << (comp_time + uncomp_time) << " mksec\n";
}

void test_lz4(std::string const& data, int accel, unsigned comp_iterations, unsigned decomp_iterations) {
    unsigned initialSize = data.size();
    int destSize = LZ4_compressBound(initialSize);
    std::unique_ptr<char[]> compressedData(new char[destSize]);
    long comp_time = 0, uncomp_time = 0;

    comp_iterations = std::max(1U, comp_iterations);
    int compressedSize = 0;
    for (int loop = 0; loop != 2; ++loop) {  // run it twice so that CPU freq control has time to adjust
        Timer timer(comp_time, comp_iterations);
        // run in a loop
        for (unsigned i = 0; i != comp_iterations; ++i) {
            compressedSize =  LZ4_compress_fast(data.data(), compressedData.get(), data.size(), destSize, accel);
        }
    }

    // now decompress it
    if (decomp_iterations > 0) {
        Timer timer(uncomp_time, decomp_iterations);
        std::unique_ptr<char[]> decompressedData(new char[data.size() + 10]);
        for (unsigned i = 0; i != decomp_iterations; ++i) {
            LZ4_decompress_fast(compressedData.get(), decompressedData.get(), initialSize);
        }
    }

    std::cout << "lz4-" << accel << "    "
              << initialSize << "    " << compressedSize << "    "
              << (compressedSize*100 / initialSize) << "%    "
              << comp_time << " mksec    " << uncomp_time << " mksec    "
              << (comp_time + uncomp_time) << " mksec\n";
}

void test_zstd(std::string const& data, int level, unsigned comp_iterations, unsigned decomp_iterations) {
    unsigned initialSize = data.size();
    size_t destSize = ZSTD_compressBound(initialSize);
    std::unique_ptr<char[]> compressedData(new char[destSize]);
    long comp_time = 0, uncomp_time = 0;

    comp_iterations = std::max(1U, comp_iterations);
    size_t compressedSize = 0;
    for (int loop = 0; loop != 2; ++loop) {  // run it twice so that CPU freq control has time to adjust
        Timer timer(comp_time, comp_iterations);
        // run in a loop
        for (unsigned i = 0; i != comp_iterations; ++i) {
            compressedSize = ZSTD_compress(compressedData.get(), destSize, data.data(), initialSize, level);
        }
    }

    // now decompress it
    if (decomp_iterations > 0) {
        Timer timer(uncomp_time, decomp_iterations);
        std::unique_ptr<char[]> decompressedData(new char[initialSize]);
        for (unsigned i = 0; i != decomp_iterations; ++i) {
            ZSTD_decompress(decompressedData.get(), initialSize, compressedData.get(), compressedSize);
        }
    }

    std::cout << "zstd" << level << "    "
              << initialSize << "    " << compressedSize << "    "
              << (compressedSize*100 / initialSize) << "%    "
              << comp_time << " mksec    " << uncomp_time << " mksec    "
              << (comp_time + uncomp_time) << " mksec\n";
}

void test_snappy(std::string const& data, unsigned comp_iterations, unsigned decomp_iterations) {
    unsigned initialSize = data.size();
    size_t destSize = snappy::MaxCompressedLength(initialSize);
    std::unique_ptr<char[]> compressedData(new char[destSize]);
    long comp_time = 0, uncomp_time = 0;

    comp_iterations = std::max(1U, comp_iterations);
    size_t compressedSize = 0;
    for (int loop = 0; loop != 2; ++loop) {  // run it twice so that CPU freq control has time to adjust
        Timer timer(comp_time, comp_iterations);
        // run in a loop
        for (unsigned i = 0; i != comp_iterations; ++i) {
            snappy::RawCompress(data.data(), initialSize, compressedData.get(), &compressedSize);
        }
    }

    // now decompress it
    if (decomp_iterations > 0) {
        Timer timer(uncomp_time, decomp_iterations);
        std::unique_ptr<char[]> decompressedData(new char[initialSize]);
        for (unsigned i = 0; i != decomp_iterations; ++i) {
            snappy::RawUncompress(compressedData.get(), compressedSize, decompressedData.get());
        }
    }

    std::cout << "snappy" << "    "
              << initialSize << "    " << compressedSize << "    "
              << (compressedSize*100 / initialSize) << "%    "
              << comp_time << " mksec    " << uncomp_time << " mksec    "
              << (comp_time + uncomp_time) << " mksec\n";
}

} // namespace

int main(int argc, char** argv)
try {
    std::string filePath;
    int zlib_level = -1;
    int zstd_level = -1;
    int lz4_accel = -1;
    unsigned comp_iterations = 1000;
    unsigned decomp_iterations = 1000;

    bpo::options_description desc("Test application for compression algos");
    desc.add_options()
        ("data-file", bpo::value<std::string>(&filePath), "Path to file with raw data.")
        ("zlib-level,z", bpo::value<int>(&zlib_level)->default_value(-1), "zlib compression level.")
        ("zstd-level", bpo::value<int>(&zstd_level)->default_value(-1), "zstd compression level.")
        ("lz4-accel", bpo::value<int>(&lz4_accel)->default_value(-1), "lz4 acceleration level.")
        ("snappy", "test snappy compression.")
        ("iter,n", bpo::value<unsigned>(&comp_iterations)->default_value(1000), "number of compression iterations.")
        ("de-iter,m", bpo::value<unsigned>(&decomp_iterations)->default_value(1000), "number of de-compression iterations.")
        ("help,h", "Print help message");
    bpo::positional_options_description positionals;
    positionals.add("data-file", -1);

    bpo::variables_map vm;
    bpo::store(bpo::command_line_parser(argc, argv).options(desc).positional(positionals).run(), vm);
    bpo::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    // read the file
    std::ifstream ifs(filePath);
    std::string str = std::string(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());

    if (zlib_level >= 0) {
        test_zlib(str, zlib_level, comp_iterations, decomp_iterations);
    }
    if (zstd_level >= 0) {
        test_zstd(str, zstd_level, comp_iterations, decomp_iterations);
    }
    if (lz4_accel >= 0) {
        test_lz4(str, lz4_accel, comp_iterations, decomp_iterations);
    }
    if (vm.count("snappy")) {
        test_snappy(str, comp_iterations, decomp_iterations);
    }


} catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
}

