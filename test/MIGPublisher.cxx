#include <sys/types.h>
#include <sys/wait.h>
#include <dlfcn.h>
#include <unistd.h>
#include <iostream>
#include <exception>
#include <thread>
#include <memory>
#include <chrono>
#include <functional>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <future>
#include <utility>
#include <limits>
#include <sstream> 
#include <random>
#include <regex>
#include <boost/program_options.hpp>

#include "ers/ers.h"
#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/GraphData.h>
#include <oh/exceptions.h>

#include "TFile.h"
#include "TKey.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TClass.h"
#include "THashList.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TObjString.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TGraphBentErrors.h"
#include "TGraph2DErrors.h"
#include "TObjArray.h"

//try creating ISInfoAny
#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "is/infoany.h"
#include "oh/OHRootProvider.h"
#include "tbb/concurrent_queue.h"

namespace boostPOpt = boost::program_options;

bool parsePath(const std::string& path, std::string& run, std::string& lb, std::string& server,
               std::string& provider, std::string& rest, int depth)
{
    const char* ps = path.c_str(), *pend = ps + path.size();
    const char* psold = ps;
    //std::vector<std::string> s;
    //std::cout<<"splitting "<<path<<" -> ";
    std::vector<std::string*> s { &run, &lb, &server, &provider, &rest };
    uint count = depth;
    while (ps != pend) {
        if (*ps == '/') {
            if ((ps - psold) > 1) {
                *(s.at(count)) = std::string(psold, ps - psold);
                count++;
            }
            psold = ps + 1;
        }
        ps++;
        if (count == s.size() - 1)
            break;
    }
    if (ps != pend) {
        *(s.back()) = std::string(psold - 1);
        count++;
    }
    // if(count==s.size()){
    //   for(auto& i:s){
    //     std::cout<<*i<<", ";
    //   }
    // }
    // std::cout<<std::endl;

    return count == s.size();
}

long parseRootFile(TDirectory* d, const std::string& basename, std::regex* provRx, std::regex* histRx,
                   int rootDirDepth,
                   std::unordered_map<std::string, std::vector<std::pair<std::string, TH1*>>*> &providerHists)
{
    unsigned long count = 0;
    TIter it(d->GetListOfKeys());
    TKey* key;
    TObject* obj;
    std::string run, lb, server, provName, rest;
    bool atDepth = parsePath(basename, run, lb, server, provName, rest, rootDirDepth);
    if (!atDepth) {
        while ((key = (TKey*) it())) {
            obj = d->Get(key->GetName());
            if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
                std::string name = basename + "/" + obj->GetName();
                count += parseRootFile((TDirectory*) obj, name, provRx, histRx, rootDirDepth, providerHists);
            }
            delete obj;
        }
        return count;
    }
    if (!std::regex_match(provName, *provRx)) {  //simulate subscription
        return count;
    }
    auto prit = providerHists.find(provName);
    if (prit == providerHists.end()) {
        prit = (providerHists.insert(std::make_pair(provName, new std::vector<std::pair<std::string, TH1*> >))).first;
    }

    while ((key = (TKey*) it())) {
        obj = d->Get(key->GetName());
        std::string name = basename + "/" + obj->GetName();
        if (obj->IsA()->InheritsFrom(TH1::Class())) {
            std::string objName(rest + "/" + obj->GetName());
            if (!std::regex_match(objName, *histRx)) {  //simulate subscription
                delete obj;
                continue;
            }
            auto hh = static_cast<TH1*>(obj);
            hh->SetDirectory(0);
            prit->second->emplace_back(std::make_pair(objName, hh));
            ++count;
        } else if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
            count += parseRootFile((TDirectory*) obj, name, provRx, histRx, rootDirDepth, providerHists);
            delete obj;
        }
    }
    return count;
}

void runPublishers(IPCPartition &p, const std::string serverName, const std::string providerName,
                   std::chrono::time_point<std::chrono::steady_clock> tstart, unsigned int interval,
                   std::vector<std::pair<std::string, TH1*>> *hists, int numPublishes)
{
    auto prov = new OHRootProvider(p, serverName, providerName);
    auto sd = std::chrono::milliseconds(interval);
    auto tPublish = tstart;
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(hists->begin(), hists->end(), g);
    std::vector < std::pair<std::string, std::string> > ann;
    auto tnow = std::chrono::steady_clock::now();
    //while((tPublish-tnow)<sd*0.1)tPublish+=sd;
    for (int i = 0; i < numPublishes; i++) {
        std::this_thread::sleep_until(tPublish);
        ERS_LOG(providerName << " Starting publication cycle " << i);
        for (auto& p : *hists) {
            prov->publish(*(p.second), p.first, 0, ann);  // no tag support yet
        }
        std::shuffle(hists->begin(), hists->end(), g);
        tnow = std::chrono::steady_clock::now();
        while ((tPublish - tnow) < sd * 0.05) {
            tPublish += sd;
        }
        ERS_LOG(providerName << " publication cycle " << i << " finished. Published " << hists->size()
                << " histograms");
    }
    std::shuffle(hists->begin(), hists->end(), g);
    ERS_LOG("Starting Finalization ");
    ann.resize(1);
    ann[0].first = "FINAL";
    ann[0].second = "FINAL";
    for (auto& p : *hists) {
        prov->publish(*(p.second), p.first, 0, ann);  // no tag support yet
    }
    ERS_LOG("Final publication done, returning; ");
    delete prov;
}

int main(int argc, char* argv[])
{

    std::string partName;
    std::string serverName;
    std::string rootFile;
    unsigned int sleepFor, numPublishes, maxProv;
    unsigned int hfdepth;
    std::string pregexString, hregexString;
    bool useForking = false;
    boost::program_options::options_description desc("This is a publisher for root files saved by oh_cp.");
    try {
        desc.add_options()
            ("sleepFor,s", boostPOpt::value<unsigned int>(&sleepFor)->default_value(10000), "Sleep between publishes in ms (default 10000,min 8000)")
            ("maxProviders,m", boostPOpt::value<unsigned int>(&maxProv)->default_value(100), "Maximum number of providers)")
            ("numPublishes,p", boostPOpt::value<unsigned int>(&numPublishes)->default_value(50), "Number of publish cycles (default 50)")
            //("numHistograms,H"       ,boostPOpt::value<decltype(numHistograms)>(&numHistograms)->default_value(std::numeric_limits<decltype(numHistograms)>::max())   , "Number of publish cycles (default ulint::max)")
            ("version,v", "Print MonInfoGatherer version")
            //("duplication-factor,d" ,boostPOpt::value<unsigned int>(&dupFac)->default_value(2), "Histogram Duplication factor")
            ("histFileDepth,D", boostPOpt::value<unsigned int>(&hfdepth)->default_value(2), "Histogram File Directory depth to define whether offline structure is there or not")
            ("root-file,f", boostPOpt::value < std::string > (&rootFile)->default_value(""), "Name of the root file for reading histograms")
            ("partition,P", boostPOpt::value < std::string > (&partName)->default_value("initial"), "Name of the partition (default 'initial')")
            ("server,S", boostPOpt::value < std::string > (&serverName)->default_value("Dummy"), "Name of the server (default 'Dummy'")
            ("provider=regex,r", boostPOpt::value < std::string > (&pregexString)->default_value(".*"), "provider regex")
            ("hist-regex,R", boostPOpt::value < std::string > (&hregexString)->default_value(".*"), "Histogram regex")
            ("forking-mode,F", boostPOpt::bool_switch()->default_value(false), "Fork instead of threading")
            ("help,h", "Print help message");

        std::cout << std::endl << "Input parameters are " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"" << std::endl;
        }
        std::cout << std::endl << "Command to run " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;
        IPCCore::init(argc, argv);
        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        boostPOpt::notify(vm);
        std::cout << "Got input parameters\n";
        boostPOpt::basic_parsed_options<char> bop =
                boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                          << bop.options.at(t).value.at(k) << "\n";
            }
        }

        if (vm.count("help")) {
            std::cout << "MIGPublisher\n";
            std::cout << desc << "\n";
            return EXIT_SUCCESS;
        }

        if (vm.count("version")) {
            std::cout << MonInfVersion << "\n";
            return EXIT_SUCCESS;
        }

        if (!vm.count("root-file")) {
            std::cerr << "Need Root File\n";
            std::cout << desc << "\n";
            return EXIT_FAILURE;
        }
        useForking = vm["forking-mode"].as<bool>();
        std::regex * prx = 0, *hrx = 0;

        try {
            prx = new std::regex(pregexString);
        } catch (const std::exception& ex) {
            std::cerr << "Caught exception on provider regex creation " << ex.what() << std::endl;
            return EXIT_FAILURE;
        }
        try {
            hrx = new std::regex(hregexString);
        } catch (const std::exception& ex) {
            std::cerr << "Caught exception on histogram regex creation " << ex.what() << std::endl;
            return EXIT_FAILURE;
        }

        if (sleepFor < 8000)
            sleepFor = 10000;
        TFile f(rootFile.c_str(), "READ");
        if (!f.IsOpen()) {
            std::cerr << "Failed to open Root file " << rootFile << std::endl;
            return EXIT_FAILURE;
        }

        std::cout << "Reading Histograms\n";
        auto tstart = std::chrono::steady_clock::now();
        std::string bd;
        std::unordered_map<std::string, std::vector<std::pair<std::string, TH1*>>*> provHists;
        size_t nhists = parseRootFile(&f, bd, prx, hrx, hfdepth, provHists);
        std::cout << "Read " << nhists << " histograms for " << provHists.size() << "providers in "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tstart).count() / 1000.
                  << "(s)\n";
        f.Close();
        std::vector<std::thread*> threads;
        unsigned int nprov = (provHists.size() < maxProv) ? provHists.size() : maxProv;
        threads.resize(nprov);
        auto prit = provHists.begin();
        std::vector<pid_t> children;
        tstart = std::chrono::steady_clock::now();
        bool mother = true;
        if (useForking) {
            IPCCore::shutdown();
            for (size_t t = 0; t < threads.size(); t++) {
                errno = 0;
                auto cpid = fork();
                if (cpid == 0) {
                    mother = false;
                    IPCCore::init(argc, argv);
                    IPCPartition p(partName);
                    ERS_LOG("Started provider " << prit->first << " pid=" << getpid());
                    runPublishers(p, serverName, prit->first, tstart, sleepFor, prit->second, numPublishes);
                    break;
                } else {
                    if (cpid == -1) {
                        std::cerr << "Fork failed with " << errno << " " << strerror(errno) << std::endl;
                    } else {
                        std::cout << "FORK RETURNED " << cpid << "\n";
                    }
                    prit++;
                }
            }
            if (mother) {
                IPCCore::init(argc, argv);
                std::this_thread::sleep_for(std::chrono::seconds(5));
                while (true) {
                    int retVal = 0;
                    errno = 0;
                    auto baby = waitpid(0, &retVal, WNOHANG);
                    if (baby == -1) {
                        ERS_LOG("All children exited!");
                        if (errno == ECHILD)
                            break;
                    } else if (baby == 0) {  //children working
                        std::this_thread::sleep_for(std::chrono::seconds(1));
                    } else {
                        if (WIFEXITED(retVal)) {
                            int exitStat = WEXITSTATUS(retVal);
                            ERS_LOG("Child with PID=" << baby << " exited normally with status=" << exitStat);
                            if (exitStat != 0) {
                                char errbuff[200];
                                snprintf(errbuff, 200,
                                         "Child pid= %d exited with unexpected return value %d ", baby, exitStat);
                                ERS_LOG(errbuff);
                            }
                        }
                        if (WIFSIGNALED(retVal)) {
                            int exitStat = WTERMSIG(retVal);
                            ERS_LOG("Child with PID=" << baby << " exited with a signal=" << WTERMSIG(retVal));
                            if (exitStat != 0) {
                                char errbuff[200];
                                snprintf(errbuff, 200, "Child pid= %d exited with signal  %d ", baby, exitStat);
                                ERS_LOG(errbuff);
                            }
                        }
                        if (WIFSTOPPED(retVal)) {
                            ERS_LOG("Child with PID=" << baby << " stopped by a signal=" << WSTOPSIG(retVal));
                            char errbuff[200];
                            snprintf(errbuff, 200, "Child pid= %d stopped", baby);
                            ERS_LOG(errbuff);
                        }
                    }
                }
            }
        } else {
            IPCPartition p(partName);
            for (size_t t = 0; t < threads.size(); t++) {
                ERS_LOG("Starting provider " << prit->first);
                threads[t] = new std::thread(std::bind(runPublishers, p, serverName, prit->first,
                                                       tstart, sleepFor, prit->second, numPublishes));
                prit++;
            }
            for (auto&t : threads) {
                t->join();
            }
        }
    } catch (const ers::Issue& ex) {
        // if(!IPCFailed)ers::fatal(ex);
        std::cout << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (const boost::program_options::error& ex) {
        std::cerr << "Exception from program options " << ex.what() << std::endl;
        // if(!IPCFailed) ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    } catch (const std::exception& ex) {
        //ers::error(daq::rc::CmdlParsing(ERS_HERE, "", ex));
        std::cerr << "Caught exception " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
