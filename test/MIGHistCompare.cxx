#include <memory>
#include <unordered_map>
#include <condition_variable>
#include <mutex>
#include <regex>
#include <signal.h>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TSystem.h>
#include <oh/OHRootReceiver.h>
#include "ipc/core.h"
#include "ers/ers.h"
#include "ipc/exceptions.h"
#include "ipc/partition.h"
#include "oh/OHSubscriber.h"
#include "owl/regexp.h"
#include "is/inforeceiver.h"
#include "is/exceptions.h"
#include <boost/program_options.hpp>
#include "is/serveriterator.h"

namespace boostPOpt = boost::program_options;
bool keepWaiting = true;
std::condition_variable waitCond;
std::mutex condMutex;

class MIGHistCompare: public OHRootReceiver {
public:
    MIGHistCompare(const std::string & file_name,
                   const std::string & binLabel,
                   bool remove_policy = false,
                   int compression_level = 0,
                   int SaveFrequency = 1,
                   int memLimit = 200)
            : file_(TFile::Open(file_name.c_str(), "RECREATE")),
              error_(false),
              remove_(remove_policy)
    {
        m_filename = file_name;
        if (SaveFrequency < 1) {
            saveFrequency = 1;
        } else {
            saveFrequency = SaveFrequency;
        }
        if (file_ == nullptr) {
            error_ = true;
        } else {
            file_->SetCompressionLevel(compression_level);
        }
        updateCount = new std::unordered_map<std::string, int>;
        if (memLimit < 50 || memLimit > 1000) {
            memoryLimit = 200 * 1024;
        } else {
            memoryLimit = memLimit * 1024;
        }
        currProc = new ProcInfo_t();
        size_t posold = 0;
        size_t pos = 0;
        while ((pos = binLabel.find(':', posold)) != std::string::npos) {
            binLabels.push_back(binLabel.substr(posold, pos));
            std::cout << "Bin Label " << binLabels.size() << " " << binLabels.back() << std::endl;
            posold = pos + 1;
        }
        binLabels.push_back(binLabel.substr(posold, pos));
        std::cout << "Bin Label " << binLabels.size() << " " << binLabels.back() << std::endl;
    }

    ~MIGHistCompare() {
        if (!error_) {
            file_->Close();
        }
        delete currProc;
        delete updateCount;
    }

    bool error() const
    {
        return error_;
    }

    void setProviderRegexp(const std::string &re)
    {
        providerRE = re;
    }

    void pushd(const std::string & directory) const
    {
        std::string name = validate(directory);
        TDirectory * dir = (TDirectory*) gDirectory->FindObject(name.c_str());
        if (!dir) {
            dir = gDirectory->mkdir(name.c_str());
        }
        dir->cd();
    }

    void popd() const
    {
        gDirectory->cd("..");
    }

private:

    std::string validate(const std::string & name) const
    {
        std::string result = name;
        replace(result.begin(), result.end(), ':', '_');
        return result;
    }

    void write(const char * name, TObject * object) const
    {
        int begin = 0;
        while (name[begin] == '/')
            begin++;

        for (int i = begin; name[i] != 0; ++i) {
            if (name[i] == '/') {
                std::string dir(name, begin, i - begin);
                pushd(dir);
                write(name + i + 1, object);
                popd();
                return;
            }
        }
        object->Write(name);
    }

    void save_histogram(TNamed * object) const
    {
        if (!error_) {
            std::string objName(object->GetName());
            auto it = updateCount->find(objName);
            if (it == updateCount->end()) {
                (*updateCount)[objName] = 0;
                it = updateCount->find(objName);
            }
            if ((it->second) % saveFrequency == 0) {
                write(object->GetName(), object);
            }
            it->second++;
            gSystem->GetProcInfo(currProc);
            if (currProc->fMemResident > memoryLimit) {
                std::cout << "Memory is " << currProc->fMemResident << ". Flushing file" << std::endl;
                file_->Write();
                file_->Flush();
            }
        }
    }

    void receive(OHRootHistogram & hh)
    {
        TNamed* gg = hh.histogram.get();
        double val = 0.0;
        if (binLabels.size() > 0) {
            if (gg->InheritsFrom("TH3")) {
                TH3* h = dynamic_cast<TH3*>(gg);
                int binx = h->GetXaxis()->FindBin(binLabels.at(0).c_str());
                int biny = h->GetYaxis()->FindBin(binLabels.at(1).c_str());
                int binz = h->GetZaxis()->FindBin(binLabels.at(2).c_str());
                val = h->GetBinContent(binx, biny, binz);
            } else if (gg->InheritsFrom("TH2")) {
                TH2* h = dynamic_cast<TH2*>(gg);
                int binx = h->GetXaxis()->FindBin(binLabels.at(0).c_str());
                int biny = h->GetYaxis()->FindBin(binLabels.at(1).c_str());
                val = h->GetBinContent(binx, biny);
            } else {
                TH1* h = dynamic_cast<TH1*>(gg);
                int binx = h->GetXaxis()->FindBin(binLabels.at(0).c_str());
                val = h->GetBinContent(binx);
            }
            std::cout << "Received " << gg->GetName() << " from  " << providerRE << " at " << hh.time.str()
                      << ", bincontent=" << val << std::endl;
        } else {
            std::cout << "Received " << gg->GetName() << " from  " << providerRE << " at " << hh.time.str()
            << std::endl;
        }
        //save_histogram( gg  );
    }

    void receive(std::vector<OHRootHistogram*> & set)
    {
        for (auto&& hist: set) {
            save_histogram(hist->histogram.get());
        }
    }

    void receive(OHRootGraph & hh)
    {
        save_histogram(hh.graph.get());
    }

    void receive(std::vector<OHRootGraph*> & set)
    {
        for (auto&& graph: set) {
            save_histogram(graph->graph.get());
        }
    }

    void receive(OHRootGraph2D & hh)
    {
        save_histogram(hh.graph.get());
    }

    void receive(std::vector<OHRootGraph2D*> & set)
    {
        for (auto&& graph: set) {
            save_histogram(graph->graph.get());
        }
    }

    bool removePolicy()
    {
        return remove_;
    }

private:
    std::unique_ptr<TFile> file_;
    std::unordered_map<std::string, int> *updateCount;
    int saveFrequency;
    bool error_;
    bool remove_;
    std::vector<std::string> binLabels;
    std::string m_filename, providerRE;
    long int memoryLimit;
    ProcInfo_t* currProc;
};

extern "C" void signalhandler(int)
{
    signal(SIGINT, SIG_DFL);
    signal(SIGTERM, SIG_DFL);
    keepWaiting = false;
    // TODO: do not use pthread primitives in signal handlers
    waitCond.notify_all();
    //ERS_INFO("Terminating rc_is2cool_archive...");
}

void endCallBack(ISCallbackInfo *isc)
{
    //  RunParams eor;
    if (!isc)
        return;
    ERS_DEBUG(1, "Got EOR callback." << std::endl);
    keepWaiting = false;
    waitCond.notify_all();
}

int main(int argc, char ** argv)
{
    std::string ar0(argv[0]);
    try {
        IPCCore::init(argc, argv);
    } catch (const daq::ipc::Exception& ex) {
        std::cerr << __PRETTY_FUNCTION__ << " IPCCore::init() error " << std::endl << ex.what() << std::endl;
        return -1;
    }
    int filter = 1;
    int compression = 2;
    int memlimit = 200;
    std::string histo_regexp = ".*";
    std::string partition_name;
    std::string server_name;
    std::string file_name;
    std::string provider_regexp;
    std::string binLabel;
    std::regex serverRegexp;
    std::regex histogramRegexp;
    std::regex providerRegexp;
    try {
        std::cout << "Input parameters\n";
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"\n";
        }
        boost::program_options::options_description desc(
                "This program publishes histograms with different properties to given OH servers.");
        desc.add_options()
            ("partition,p", boostPOpt::value < std::string > (&partition_name), "Partition Name")
            ("server,s", boostPOpt::value < std::string > (&server_name), "server regular expresion")
            ("filename,f", boostPOpt::value < std::string > (&file_name), "Output file name")
            ("histRegexp,H", boostPOpt::value < std::string > (&histo_regexp), "histo regular expression")
            ("provider,P", boostPOpt::value < std::string > (&provider_regexp), "Provider regexp")
            ("binlabel,B", boostPOpt::value < std::string > (&binLabel), "print binContent")
            ("filter,F", boostPOpt::value<int>(&filter)->default_value(1),"Histogram filter period (every Fth histogram will be saved)")
            ("compression,c", boostPOpt::value<int>(&compression)->default_value(2), "ROOT file compression level")
            ("memlimit,m", boostPOpt::value<int>(&memlimit)->default_value(200), "ROOT file flush memory limit")
            ("help", "Print help message");

        boostPOpt::basic_parsed_options<char> bop = boostPOpt::parse_command_line(argc, argv, desc);
        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                          << bop.options.at(t).value.at(k) << "\n";
            }
        }
        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::parse_command_line(argc, argv, desc), vm);
        boostPOpt::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return 0;
        }
        if (vm.count("partition") == 0) {
            std::cout << "you must give partition name" << std::endl;
            return 5;
        }
        if (vm.count("server") == 0) {
            std::cout << "you must give server name" << std::endl;
            return 5;
        }
        if (vm.count("filename") == 0) {
            std::cout << "you must give file name" << std::endl;
            return 5;
        }
        if (vm.count("provider") == 0) {
            std::cout << "you must give a provider regexp" << std::endl;
            return 5;
        }

    } catch (const std::exception& ex) {
        std::cerr << "Caught Exception " << ex.what() << std::endl;
    }
    ERS_DEBUG(1, "Creating regular expressions." << std::endl);
    try {
        providerRegexp.assign(provider_regexp);
    } catch (const std::regex_error& e) {
        std::cerr << "Invalid provider regular expression " << provider_regexp << std::endl;
        return 127;
    }
    try {
        histogramRegexp.assign(histo_regexp);
    } catch (const std::regex_error& e) {
        std::cerr << "Invalid histogram regular expression " << histo_regexp << std::endl;
        return 127;
    }

    try {
        serverRegexp.assign(server_name);
    } catch (const std::regex_error& e) {
        std::cerr << "Invalid server regular expression " << server_name << std::endl;
        return 127;
    }

    ERS_DEBUG(1, "Creating file saver" << std::endl);
    MIGHistCompare saver(file_name, binLabel, false, compression, filter, memlimit);
    if (saver.error()) {
        std::cerr << "Error in root file, check the path and permissions" << std::endl << "File Name \""
                  << file_name << "\"\n";
        return -3;
    } else {
        ERS_DEBUG(1, "File saver created succesfully" << std::endl);
    }

    std::unique_ptr<OHSubscriber> ohhs;
    std::unique_ptr<ISInfoReceiver> inforec;

    std::vector < std::string > subscribedServers;
    OWLRegexp provider(provider_regexp);
    OWLRegexp histoRegexp(histo_regexp);

    try {
        IPCPartition p(partition_name);
        ISServerIterator dd(p);
        ohhs = std::make_unique<OHSubscriber>(p, saver, true);
        while (dd()) {
            if (std::regex_match(dd.name(), serverRegexp)) {
                ohhs->subscribe(dd.name(), provider, histoRegexp, true);
                subscribedServers.push_back(dd.name());
                std::cout << "Subscribing to " << subscribedServers.back() << std::endl;
            }
        }
    } catch (const daq::oh::Exception& ex) {
        std::cerr << "Error in OHSubscriber" << std::endl << ex.what() << std::endl;
        return -4;
    }
    ERS_DEBUG(1, "Subscribing for EOR." << std::endl);
    try {
        IPCPartition p(partition_name);
        inforec = std::make_unique<ISInfoReceiver>(p);
        inforec->subscribe("RunParams.EOR_RunParams", endCallBack);
    } catch (const daq::is::Exception& ex) {
        std::cerr << "Error in Runparams subscription" << std::endl;
        return -5;
    }

    // Install signal handler
    signal(SIGINT, signalhandler);
    signal(SIGTERM, signalhandler);
    keepWaiting = true;
    while (keepWaiting) {
        std::unique_lock < std::mutex > ml(condMutex);
        waitCond.wait(ml);
    }
    try {
        ERS_DEBUG(1, "Trying to unsubscribe." << std::endl);
        inforec->unsubscribe("RunParams.EOR_RunParams");
    } catch (const daq::is::Exception& ex) {
        std::cerr << "Error in unsubscribing." << std::endl << ex.what() << std::endl;
    }

    return 0;
}
