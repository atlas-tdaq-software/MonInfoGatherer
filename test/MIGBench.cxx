//
// MIGBench
//

#include <dlfcn.h>
#include <unistd.h>
#include <iostream>
#include <exception>
#include <thread>
#include <mutex>
#include <memory>
#include <chrono>
#include <functional>
#include <future>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <sstream>
#include <random>
#include <regex>
#include <boost/program_options.hpp>

#include "ers/ers.h"
#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/GraphData.h>
#include <oh/exceptions.h>
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/DefaultHandler.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/LibraryManager.h"

#include "TFile.h"
#include "TKey.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TClass.h"
#include "THashList.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TObjString.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TGraphBentErrors.h"
#include "TGraph2DErrors.h"
#include "TObjArray.h"

#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "is/infoany.h"
#include "oh/OHRootProvider.h"
#include "tbb/concurrent_queue.h"

using namespace MIG;

bool parsePath(const std::string & path, std::string & run, std::string & lb, std::string & server,
               std::string & provider, std::string & rest, int depth)
{
    const char* ps = path.c_str(), *pend = ps + path.size();
    const char* psold = ps;
    //std::vector<std::string> s;
    //std::cout<<"splitting "<<path<<" -> ";
    std::vector<std::string*> s { &run, &lb, &server, &provider, &rest };
    uint count = depth;
    while (ps != pend) {
        if (*ps == '/') {
            if ((ps - psold) > 1) {
                *(s.at(count)) = std::string(psold, ps - psold);
                count++;
            }
            psold = ps + 1;
        }
        ps++;
        if (count == s.size() - 1)
            break;
    }
    if (ps != pend) {
        *(s.back()) = std::string(psold - 1);
        count++;
    }
    // if(count==s.size()){
    //   for(auto& i:s){
    //     std::cout<<*i<<", ";
    //   }
    // }
    // std::cout<<std::endl;

    return count == s.size();
}

class DummProv: public OHProvider {
public:
    DummProv(const IPCPartition &p, const std::string & server, const std::string & name,
             OHCommandListener * lst = 0)
            : OHProvider(p, server, name, lst)
    {}

    void publish(ISInfo& info, const std::string &name) {
        OHProvider::publish(info, name);
    }
};

int HistFileDirDepth = 1;
namespace boostPOpt = boost::program_options;

#define TESTEQ(X,Y,MSG) \
    if ((X != Y) && ((::fabs(X-Y)/((X!=0)?X:Y)) > 0.001)){ \
        ERS_LOG(MSG << " published=" << X << " reference " << Y); \
        std::stringstream oss; \
        oss << "Annotations "; \
        std::vector<std::pair<std::string,std::string>> anns; \
        src->get_annotations(anns); \
        for(auto& ann: anns) { \
            oss << ann.first << " = " << ann.second << " "; \
        } \
        ERS_LOG(oss.str()); \
        if (prov) { \
            prov->publish(*src,name); \
        } else { \
            oss.str(""); \
            oss << "published= "; \
            src->print(oss); \
            oss << " ref= "; \
            dst->print(oss); \
            ERS_LOG(oss.str()); \
        } \
        if (true) return; \
    }

#define ROOT_HISTOGRAM_TYPES(decl)	\
    decl(TH1C)	decl ## _SEPARATOR	\
    decl(TH2C)	decl ## _SEPARATOR	\
    decl(TH3C)	decl ## _SEPARATOR	\
    decl(TH1S)	decl ## _SEPARATOR	\
    decl(TH2S)	decl ## _SEPARATOR	\
    decl(TH3S)	decl ## _SEPARATOR	\
    decl(TH1I)	decl ## _SEPARATOR	\
    decl(TH2I)	decl ## _SEPARATOR	\
    decl(TH3I)	decl ## _SEPARATOR	\
    decl(TH1F)	decl ## _SEPARATOR	\
    decl(TH2F)	decl ## _SEPARATOR	\
    decl(TH3F)	decl ## _SEPARATOR	\
    decl(TProfile)	decl ## _SEPARATOR	/* must precede the TH1D since TProfile inherits from TH1D */ \
    decl(TH1D)	decl ## _SEPARATOR	\
    decl(TProfile2D)decl ## _SEPARATOR	/* must precede the TH2D since TProfile2D inherits from TH2D */ \
    decl(TH2D)	decl ## _SEPARATOR	\
    decl(TProfile3D)decl ## _SEPARATOR	/* must precede the TH3D since TProfile3D inherits from TH3D */ \
    decl(TH3D)

#define EXACT_CONVERT(x)	if ( histogram.IsA() == x::Class() ) \
    prov->publish(static_cast<const x &>( histogram ), objName,0 ) ;
#define EXACT_CONVERT_SEPARATOR	else

#define BASE_CONVERT(x)		if ( histogram.InheritsFrom( x::Class() ) ) \
    prov->publish(static_cast<const x &>( histogram ), objName,0 ) ;
#define BASE_CONVERT_SEPARATOR	else

#define TYPECASTBLOCK(x) if( currType==oh::HistogramData<x>::type())\
    validateBins<x>(src,dst,vind,name,prov);
#define TYPECASTBLOCK_SEPARATOR else

#define IS_HISTOGRAM_TYPES(decl) \
    decl(float)  decl ## _SEPARATOR \
    decl(int)  decl ## _SEPARATOR \
    decl(double)  decl ## _SEPARATOR \
    decl(char)  decl ## _SEPARATOR \
    decl(short)

class InfoCont {
public:
    InfoCont(std::shared_ptr<ISInfoAny> &hp, const std::string& n, const std::string &pN, size_t nb, size_t nl)
            : h(hp), name(n), provName(pN), nbins(nb), nlabels(nl)
    {}

    std::shared_ptr<ISInfoAny> h;
    std::string name;
    std::string provName;
    size_t nbins;
    size_t nlabels;
};

struct CBDummy: public InfoPublisher {
    void publish(ISInfo&, const std::string&, int) override {}
};

int queueFiller(std::vector<InfoCont>& hists, tbb::concurrent_queue<InfoCont*> &queue,
                std::chrono::time_point<std::chrono::steady_clock> tstart, int numPublishes,
                unsigned int sleepDuration)
{
    auto sd = std::chrono::milliseconds(sleepDuration);
    time_t tsec = OWLTime(OWLTime::Microseconds).c_time();
    std::random_device rd;
    std::mt19937 g(rd());
    std::vector<InfoCont*> hps;
    hps.resize(hists.size());
    for (size_t t = 0; t < hists.size(); t++) {
        hps[t] = &(hists[t]);
    }
    std::shuffle(hps.begin(), hps.end(), g);
    auto tnow = std::chrono::steady_clock::now();
    auto tPublish = tstart + sd;
    while ((tPublish - tnow) < sd * 0.1)
        tPublish += sd;
    for (int i = 0; i < numPublishes; i++) {
        std::this_thread::sleep_until(tPublish);
        std::shuffle(hps.begin(), hps.end(), g);
        ERS_LOG("Starting queueFill " << i);
        tsec += 20;
        tnow = std::chrono::steady_clock::now();
        for (auto& ic : hps) {
            const_cast<OWLTime&>(ic->h->time()) = OWLTime(tsec);
            queue.push(ic);
        }
        tnow = std::chrono::steady_clock::now();
        while ((tPublish - tnow) < sd * 0.05) {
            tPublish += sd;
        }
    }
    return 0;
}

void runPublishers(std::shared_ptr<DefaultHandler> handler,
                   tbb::concurrent_queue<InfoCont*> *queue,
                   bool *keepRunning)
{
    InfoCont* ic;
    bool publishing = false;
    while (true) {
        ic = 0;
        queue->try_pop(ic);
        if (!ic) {
            if (publishing) {
                char buff[500];
                auto thread_id = std::this_thread::get_id();
                snprintf(buff, 500, "Thread %lu Publishing finished at %15.3f",
                         *reinterpret_cast<unsigned long*>(&thread_id),
                         std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()/1000.);
                // ERS_LOG(
                // 	"Thread "<<std::this_thread::get_id()
                // 	<<" Publishing finished at "
                // 	<<std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()/1000.);
                ERS_LOG(buff);
                publishing = false;
            }
            if (!(*keepRunning))
                break;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        } else {
            if (!publishing) {
                // ERS_LOG("Thread "<<std::this_thread::get_id()
                // 	<<" Publishing started at "
                // 	<<std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()/1000.);
                char buff[500];
                auto thread_id = std::this_thread::get_id();
                snprintf(buff, 500, "Thread %lu Publishing Started at %15.3f",
                        *reinterpret_cast<unsigned long*>(&thread_id),
                        std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()/1000.);
                ERS_LOG(buff);
                publishing = true;
            }
            try {
                handler->updateInfo(*(ic->h), ic->name, ic->provName);
            } catch (const std::exception& ex) {
                std::cerr << "Caught exception " << ex.what() << std::endl;
            }
        }
    }
}

void publishValidation(std::unordered_map<std::string, TH1*>* h, OHRootProvider* prov)
{
    for (auto& i : *h) {
        std::string objName(i.first);
        const auto& histogram(*(i.second));
        ROOT_HISTOGRAM_TYPES(EXACT_CONVERT) else {
            ROOT_HISTOGRAM_TYPES(BASE_CONVERT)
        }
        delete i.second;
    }
}

std::unordered_map<std::string, TH1*> ValidHists;
TObjArray List;

long parseRootFile(TDirectory* d, const std::string& basename, OHRootProvider* prov, std::regex* pRx,
                   std::regex* hRx, unsigned long maxHists, bool doValid, int rootDirDepth)
{
    unsigned long count = 0;
    TIter it(d->GetListOfKeys());
    TKey* key;
    TObject* obj;
    const std::vector<std::pair<std::string, std::string> > ann;
    // const char * bp=basename.c_str();
    // const char * bpend=bp+basename.size();
    std::string run, lb, server, provName, rest;
    bool atDepth = parsePath(basename, run, lb, server, provName, rest, rootDirDepth);
    if (!atDepth) {
        while ((key = (TKey*) it())) {
            obj = d->Get(key->GetName());
            if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
                std::string name = basename + "/" + obj->GetName();
                count += parseRootFile((TDirectory*) obj, name, prov, pRx, hRx, maxHists, doValid,
                                       rootDirDepth);
            }
            delete obj;
        }
        return count;
    }
    if (!std::regex_match(provName, *pRx)) {  //simulate subscription
        return count;
    }
    while ((key = (TKey*) it())) {
        obj = d->Get(key->GetName());
        std::string name = basename + "/" + obj->GetName();
        if (obj->IsA()->InheritsFrom(TH1::Class())) {
            std::string objName(rest + "/" + obj->GetName());
            if (!std::regex_match(objName, *hRx)) {  //simulate subscription
                delete obj;
                continue;
            }
            //std::cout<<"Matched histogram"<<objName<<std::endl;
            if (doValid) {
                auto h = static_cast<TH1*>(obj);
                auto it = ValidHists.find(objName);
                if (it != ValidHists.end()) {
                    List.Clear();
                    List.Add(h);
                    if (it->second->Merge(&List) == -1) {
                        ERS_LOG("Merge failed " << objName << " provider=" << provName);
                    }

                } else {
                    auto tm = (TH1*) h->Clone();
                    tm->SetDirectory(0);
                    ValidHists[objName] = tm;
                }
            }
            const auto & histogram(*(static_cast<TH1*>(obj)));
            ROOT_HISTOGRAM_TYPES(EXACT_CONVERT) else
            ROOT_HISTOGRAM_TYPES(BASE_CONVERT)
            ++count;
        } else if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
            count += parseRootFile((TDirectory*) obj, name, prov, pRx, hRx, maxHists - count, doValid,
                                   rootDirDepth);
            delete obj;
        }
        if (count > maxHists)
            break;
    }
    return count;
}

template<typename T>
void validateBins(oh::ObjectBase<oh::Histogram>* srcO, oh::ObjectBase<oh::Histogram>* dstO,
                  const std::vector<size_t> &binIndices, const std::string& name, DummProv *prov)
{
    auto src = dynamic_cast<oh::HistogramData<T>*>(srcO);
    auto dst = dynamic_cast<oh::HistogramData<T>*>(dstO);
    const auto sbins = src->get_bins_array();
    const auto dbins = dst->get_bins_array();
    auto serrs = src->get_errors();
    auto derrs = dst->get_errors();
    TESTEQ(src->get_bins_size(), binIndices.size(),
           "Bin indices size and src bins size do not match " << name);
    TESTEQ(dst->get_bins_size(), binIndices.size(),
           "Bin indices size and dst bins size do not match " << name);
    //std::cout<<name<<" size src= "<<src->get_bins_size()<<" dst= "<<dst->get_bins_size()<<" binI="<<binIndices.size();
    if (serrs && derrs) {
        for (size_t t = 0; t < binIndices.size(); t++) {
            TESTEQ(sbins[t], dbins[binIndices[t]], " BinContents for bin " << t << " are different " << name);
            TESTEQ(serrs[t], derrs[binIndices[t]], " BinErrors for bin " << t << " are different " << name);
        }
    } else {
        for (size_t t = 0; t < binIndices.size(); t++) {
            TESTEQ(sbins[t], dbins[binIndices[t]],
                   " BinContents for sbin " << t << " dbin=" << binIndices[t] << " are different " << name);
            // std::cout<<" "<<t<<" b[t]="<<binIndices[t]<<",";
        }
    }
    // std::cout<<std::endl;
}

template<>
void validateBins<oh::ProfileData>(oh::ObjectBase<oh::Histogram>* srcO, oh::ObjectBase<oh::Histogram>*dstO,
                                   const std::vector<size_t> &binIndices, const std::string &name,
                                   DummProv *prov)
{
    auto src = dynamic_cast<oh::ProfileData*>(srcO);
    auto dst = dynamic_cast<oh::ProfileData*>(dstO);
    const auto sbins = src->get_bins_array();
    const auto dbins = dst->get_bins_array();
    auto serrs = src->get_errors();
    auto derrs = dst->get_errors();
    const auto &sent = src->bin_entries;
    const auto &dent = src->bin_entries;
    if (serrs && derrs) {
        for (size_t t = 0; t < binIndices.size(); t++) {
            TESTEQ(sbins[t], dbins[binIndices[t]], " BinContents for bin " << t << " are different " << name);
            TESTEQ(serrs[t], derrs[binIndices[t]], " BinErrors for bin " << t << " are different " << name);
            TESTEQ(sent[t], dent[binIndices[t]], " BinEntries for bin " << t << " are different " << name);
        }
    } else {
        for (size_t t = 0; t < binIndices.size(); t++) {
            TESTEQ(sbins[t], dbins[binIndices[t]], " BinContents for bin " << t << " are different " << name);
            TESTEQ(sent[t], dent[binIndices[t]], " BinEntries for bin " << t << " are different " << name);
        }
    }
}

struct CBRec: public InfoPublisher {
    void cbReceive(ISCallbackInfo *cbinfo)
    {
        std::string fullname = cbinfo->name();
        if (cbinfo->reason() == ISInfo::Deleted)
            return;
        std::string publisher = oh::util::get_provider_name(fullname);
        std::string name = oh::util::get_histogram_name(fullname);
        int nbins = 0;
        int nlabels = 0;
        for (size_t t = 0; t < dupFac; t++) {
            auto info = std::make_shared<ISInfoAny>();
            try {
                cbinfo->value(*info);
                if (!info->type().subTypeOf(oh::Histogram::type()))
                    return;
            } catch (const daq::is::RepositoryNotFound&) {
                return;
            } catch (const daq::is::InfoNotFound&) {
                return;
            } catch (const daq::is::InfoNotCompatible&) {
                return;
            }
            {
                std::unique_lock < std::mutex > l(m_vecMtx);
                auto it = hmap.insert(std::make_pair(name, 0));
                char pname[20];
                snprintf(pname, 20, "Provider-%05d", it.first->second++);  //assign a unique name
                hists->emplace_back(info, name, std::string(pname), nbins, nlabels);
            }
        }
    }

    void cbReceiveValid(ISCallbackInfo *cbinfo)
    {
        std::string fullname = cbinfo->name();
        if (cbinfo->reason() == ISInfo::Deleted)
            return;
        std::string publisher = oh::util::get_provider_name(fullname);
        std::string name = oh::util::get_histogram_name(fullname);
        auto info = std::make_shared<ISInfoAny>();
        try {
            cbinfo->value(*info);
            if (!info->type().subTypeOf(oh::Histogram::type()))
                return;
        } catch (const daq::is::RepositoryNotFound&) {
            return;
        } catch (const daq::is::InfoNotFound&) {
            return;
        } catch (const daq::is::InfoNotCompatible&) {
            return;
        }
        auto currType = info->type();
        oh::Histogram* h = nullptr;
        if (currType == oh::HistogramData<float>::type()) {
            auto H = new oh::HistogramData<float>();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;
        } else if (currType == oh::HistogramData<int>::type()) {
            auto H = new oh::HistogramData<int>();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;
        } else if (currType == oh::HistogramData<double>::type()) {
            auto H = new oh::HistogramData<double>();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;

        } else if (currType == oh::HistogramData<char>::type()) {
            auto H = new oh::HistogramData<char>();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;

        } else if (currType == oh::ProfileData::type()) {
            auto H = new oh::ProfileData();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;

        } else if (currType == oh::HistogramData<short>::type()) {
            auto H = new oh::HistogramData<short>();
            (*info) >> *H;
            nTotBins = H->get_bins_size();
            nTotLabels += H->get_labels(oh::Axis::X).size();
            nTotLabels += H->get_labels(oh::Axis::Y).size();
            nTotLabels += H->get_labels(oh::Axis::Z).size();
            h = H;
        }
        if (!validationHists.insert(std::make_pair(name, h)).second) {
            std::cerr << "Warning same named histogram published twice! " << name << std::endl;
        }
    }

    // implementation of InfoPublisher interface
    void publish(ISInfo &inf, const std::string &name, int tag) override
    {
        auto it = validationHists.find(name);
        if (it == validationHists.end()) {
            ERS_LOG("Can't find histogram " << name << " in validation list");
            return;
        }
        oh::ObjectBase<oh::Histogram>* src = dynamic_cast<oh::ObjectBase<oh::Histogram>*>(&inf);
        auto dst = dynamic_cast<oh::ObjectBase<oh::Histogram>*>(it->second);
        std::vector<size_t> ind[3];
        std::unordered_map<std::string, int> labelMaps[3];
        int srcDim = src->dimension;
        int dstDim = dst->dimension;
        ind[0].push_back(0);
        ind[1].push_back(0);
        ind[2].push_back(0);
        static const oh::Axis::ID axID[3] = { oh::Axis::X, oh::Axis::Y, oh::Axis::Z };
        TESTEQ(srcDim, dstDim, "Dimensions are different " << name);
        TESTEQ(src->entries, dst->entries, "Entry counts are different " << name);
        for (int i = 0; i < srcDim; i++) {
            auto& srcAxis = src->axes.at(i);
            auto& dstAxis = dst->axes.at(i);
            if (srcAxis.kind != dstAxis.kind) {
                ERS_LOG("AXIS Types are different for " << name << " at axis " << i);
            }
            size_t srcNbins = src->get_bin_count(axID[i]) + 2;
            size_t dstNbins = dst->get_bin_count(axID[i]) + 2;
            TESTEQ(srcNbins, dstNbins, "AXIS Bin counts are different " << name << " for axis " << i);
            if (srcAxis.kind == oh::Axis::Fixed) {
                double srcLowEdge, srcBinWidth, dstLowEdge, dstBinWidth;
                src->get_axis_range(axID[i], srcLowEdge, srcBinWidth);
                dst->get_axis_range(axID[i], dstLowEdge, dstBinWidth);
                TESTEQ(srcLowEdge, dstLowEdge, "AXIS lower bounds are different " << name << " for axis " << i);
                TESTEQ(srcBinWidth, dstBinWidth, "AXIS BinWidths are different " << name << " for axis " << i);
            } else {
                const auto& srcAxRange = src->get_axis_range(axID[i]);
                const auto& dstAxRange = dst->get_axis_range(axID[i]);
                for (size_t k = 0; k < srcAxRange.size(); k++) {
                    TESTEQ(srcAxRange[k], dstAxRange[k],
                           "AXIS Bin Bounds are different " << name << " for axis " << i);
                }
            }
            // size_t srcNbins=src->get_bin_count(axID[i])+2;
            // size_t dstNbins=dst->get_bin_count(axID[i])+2;
            // TESTEQ(srcNbins,dstNbins,"AXIS Bin counts are different "<<name<<" for axis "<<i);
            ind[i].resize(srcNbins, 0);
            for (size_t t = 0; t < srcNbins; t++)
                ind[i][t] = t;
            auto &slabs = srcAxis.labels;
            auto &sinds = srcAxis.indices;
            auto &dlabs = dstAxis.labels;
            auto &dinds = dstAxis.indices;
            TESTEQ(slabs.size(), dlabs.size(),
                   "AXIS label counts are different " << name << " for axis " << i);
            TESTEQ(sinds.size(), dinds.size(),
                   "AXIS indice counts are different " << name << " for axis " << i);
            auto& lm = labelMaps[i];
            bool missingLabels = false, duplicateLabels = false;

            if (slabs.size() || dlabs.size()) {
                for (size_t t = 0; t < slabs.size(); t++) {
                    auto it = lm.insert(std::make_pair(slabs[t], sinds[t]));
                    if (!it.second) {
                        ERS_LOG("Duplicate label in axis " << i << " published histo '" << slabs[t] << "' "
                                << name);
                        duplicateLabels = true;
                    }
                    lm[slabs[t]] = sinds[t];
                    //std::cout<<"Pub pos="<<sinds[t]<<" label="<<slabs[t]<<" axis="<<i<<std::endl;
                }
                for (size_t t = 0; t < dlabs.size(); t++) {
                    auto lit = lm.find(dlabs[t]);
                    if (lit == lm.end()) {
                        ERS_LOG("Missing label in axis " << i << " in published histo '" << dlabs[t] << "' "
                                << name);
                        missingLabels = true;
                        continue;
                    }
                    //std::cout<<"Ref pos="<<dinds[t]<<" spos="<<lit->second<<" label="<<dlabs[t]<<" axis="<<i<<std::endl;
                    ind[i][lit->second] = dinds[t];
                }

                if (duplicateLabels) {      //if duplicate labels exist disregard labels
                    for (size_t t = 0; t < ind[i].size(); t++) {
                        ind[i][t] = t;
                    }
                } else if (missingLabels) {
                    return;
                }
            } else {      //no labels for the incides
                for (size_t t = 0; t < ind[i].size(); t++) {
                    ind[i][t] = t;
                }
            }
        }
        size_t planeSize = ind[1].size() * ind[0].size();
        std::vector < size_t > vind(planeSize * ind[2].size());
        for (size_t t = 0; t < vind.size(); t++)
            vind[t] = t;
        for (uint zi = 0; zi < ind[2].size(); zi++) {
            size_t z = ind[2][zi];
            size_t zsoff = zi * planeSize;
            size_t zdoff = z * planeSize;
            for (uint yi = 0; yi < ind[1].size(); yi++) {
                size_t y = ind[1][yi];
                size_t ysoff = yi * ind[0].size() + zsoff;
                size_t ydoff = y * ind[0].size() + zdoff;
                for (uint xi = 0; xi < ind[0].size(); xi++) {
                    size_t x = ind[0][xi];
                    vind[xi + ysoff] = x + ydoff;
                }
            }
        }
        auto currType = inf.type();
        IS_HISTOGRAM_TYPES(TYPECASTBLOCK) else if (currType == oh::ProfileData::type()) {
            validateBins<oh::ProfileData>(src, dst, vind, name, prov);
        } else {
            ERS_LOG("Histogram Type unknown " << name << " type=" << currType);
        }
    }

    std::vector<InfoCont> *hists;
    std::unordered_map<std::string, int> hmap;
    size_t dupFac, nTotLabels, nTotBins;
    std::mutex m_vecMtx;
    tbb::concurrent_unordered_map<std::string, oh::Histogram*> validationHists;
    DummProv* prov;
};

int main(int argc, char* argv[])
{

    std::string parentName;
    std::string segmentName;
    std::string myName;
    std::string rootFile;
    bool doValidation = false;
    unsigned int sleepFor, numThreads, numPublishes, dupFac;
    unsigned long numHistograms;
    unsigned int hfdepth;
    std::string problemServer;
    std::string pregexString, hrxstring;
    boost::program_options::options_description desc(
            "This is a benchmark for MonInfoGatherer framework. Need \"Dummy\" IS server in initial partition to run.");
    try {
        desc.add_options()
            ("sleepFor,s", boostPOpt::value<unsigned int>(&sleepFor)->default_value(10000), "Sleep between publishes in ms (default 10000)")
            ("numThreads,t", boostPOpt::value<unsigned int>(&numThreads)->default_value(7), "Number of threads (default 7)")
            ("numPublishes,p", boostPOpt::value<unsigned int>(&numPublishes)->default_value(50), "Number of publish cycles (default 50)")
            ("numHistograms,H", boostPOpt::value<decltype(numHistograms)>(&numHistograms)->default_value(std::numeric_limits<decltype(numHistograms)>::max()),
                "Number of publish cycles (default ulint::max)")
            ("version,v", "Print MonInfoGatherer version")
            ("duplication-factor,d", boostPOpt::value<unsigned int>(&dupFac)->default_value(1), "Histogram Duplication factor")
            ("histFileDepth,D", boostPOpt::value<unsigned int>(&hfdepth)->default_value(2), "Histogram File Directory depth")
            ("root-file,r", boostPOpt::value < std::string > (&rootFile)->default_value(""), "Name of the root file for reading histograms")
            ("validation,V", boostPOpt::bool_switch()->default_value(false), "Whether to compare results with Root results")
            ("problem-destination,P", boostPOpt::value < std::string > (&problemServer)->default_value(""),
                "Name of the ISServer to publish histograms with validation problems")
            ("provider-regex,X", boostPOpt::value < std::string > (&pregexString)->default_value(".*"), "regular expression for provider")
            ("histogram-regex,x", boostPOpt::value < std::string > (&hrxstring)->default_value(".*"), "regular expression for histograms")
            ("help,h", "Print help message");

        std::cout << std::endl << "Input parameters are\n";
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"\n";
        }
        std::cout << std::endl << "Command to run\n";
        for (int i = 0; i < argc; i++) {
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;
        IPCCore::init(argc, argv);
        IPCPartition p("initial");
        auto isr = std::make_shared<ISInfoReceiver>(p, false);      //Parallel callbacks
        auto isdict = std::make_shared<ISInfoDictionary>(p);
        ISCriteria crit(".*");
        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        boostPOpt::notify(vm);
        std::cout << "Got input parameters\n";
        boostPOpt::basic_parsed_options<char> bop =
                boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                          << bop.options.at(t).value.at(k) << "\n";
            }
        }
        std::vector<InfoCont> hists;
        hists.reserve(5000);

        if (vm.count("help")) {
            std::cout << "MIGBench\n" << desc << std::endl;
            return EXIT_SUCCESS;
        }
        doValidation = vm["validation"].as<bool>();
        if (vm.count("version")) {
            std::cout << MonInfVersion << std::endl;
            return EXIT_SUCCESS;
        }
        if (!vm.count("root-file")) {
            std::cerr << "Need Root File" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
        std::regex * prx = 0, *hrx = 0;
        try {
            prx = new std::regex(pregexString);
            hrx = new std::regex(hrxstring);
        } catch (const std::exception& ex) {
            std::cerr << "Caught exception on regex creation " << ex.what() << std::endl;
            return EXIT_FAILURE;
        }

        //auto fn=std::bind(cbReceive,hists,std::placeholders::_1);
        //  auto fn=[&hists](ISCallbackInfo *cb)->void{cbReceive(hists,cb);};
        if (sleepFor < 8000)
            sleepFor = 10000;
        HistFileDirDepth = hfdepth;
        std::cout << "Program parameters:\n"
                  << "  Input Root File     = " << rootFile << "\n"
                  << "  Number of Threads   = " << numThreads << "\n"
                  << "  Number of Publishes = " << numPublishes << "\n"
                  << "  Provider Rx         = " << pregexString << "\n"
                  << "  Histogram Rx        = " << hrxstring << "\n"
                  << "  Duplication Factor  = " << dupFac << "\n"
                  << "  Publish period      = " << sleepFor << " (ms)" << "\n"
                  << "  Validation          = " << (doValidation ? "Enabled" : "Disabled")
                  << "\n";
        if (!problemServer.empty())
            std::cout << "  Failed Dest Server  = " << problemServer << "\n";
        std::cout << "  Histogram Limit     = " << numHistograms << "\n"
                  << "  Histogram File depth= " << HistFileDirDepth << "\n"
                  << "\n";

        auto prov = new OHRootProvider(p, "Dummy", "MIGBench");
        CBRec cbr;
        cbr.hists = &hists;
        cbr.dupFac = dupFac;
        cbr.nTotLabels = 0;
        cbr.nTotBins = 0;
        cbr.prov = 0;
        isr->subscribe("Dummy", crit, &CBRec::cbReceive, &cbr);
        TFile f(rootFile.c_str(), "READ");
        if (!f.IsOpen()) {
            std::cerr << "Failed to open Root file " << rootFile << std::endl;
            return EXIT_FAILURE;
        }
        std::cout << "Reading Histograms" << std::endl;
        auto tstart = std::chrono::steady_clock::now();
        std::string bd;

        size_t nhists = parseRootFile(&f, bd, prov, prx, hrx, numHistograms, doValidation, HistFileDirDepth);

        std::cout << "Read " << nhists << " histograms in "
                 << std::chrono::duration_cast < std::chrono::milliseconds > (std::chrono::steady_clock::now() - tstart).count() / 1000.
                 << "(s)" << std::endl;
        isr->unsubscribe("Dummy", crit, true);
        if (doValidation) {
            isr->subscribe("Dummy", crit, &CBRec::cbReceiveValid, &cbr);
            publishValidation(&ValidHists, prov);
            isr->unsubscribe("Dummy", crit, true);
        }
        if (nhists == 0) {
            std::cerr << "No histograms found or match regexp check input file and regexp" << std::endl;
            return EXIT_FAILURE;
        }

        ValidHists.clear();
        f.Close();
        {
            int cwait = 0;
            int csame = 0;
            std::cout << "Waiting for callbacks to finish" << std::endl;
            size_t sold = 0;
            while (cwait < 100 && csame < 5) {
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                if (sold == hists.size()) {
                    csame++;
                    if (csame > 2 && csame < 5) {
                        ERS_LOG("No updates on histogram count " << csame);
                    }
                    if (csame == 5) {
                        std::cout << "Hist size was " << hists.size() << " for " << csame
                        << " updates. Continuing" << std::endl;
                        break;
                    }
                } else {
                    sold = hists.size();
                    csame = 0;
                }
                cwait++;
            }
        }
        if ((nhists * dupFac) != hists.size()) {
            std::cout << "Warning was expecting " << nhists << " histograms got " << hists.size()
            << std::endl;
        } else {
            std::cout << "Will loop over " << hists.size() << " histograms" << std::endl;
        }
        delete prov;
        prov = 0;
        isr.reset();
        isdict.reset();
        if (!problemServer.empty()) {
            try {
                auto pprov = new DummProv(p, problemServer, "MIGBench");
                cbr.prov = pprov;
            } catch (const std::exception& ex) {
                std::cerr << "Caught exception " << ex.what() << std::endl;
                return EXIT_FAILURE;
            }
        } else {
            IPCCore::shutdown();
        }
        HandlerConfig hConfig(".*", "", {});

        CBDummy cbDummy;
        std::shared_ptr<DefaultHandler> handler;
        if (doValidation) {
            handler = std::make_shared < DefaultHandler > (hConfig, cbr);
        } else {
            handler = std::make_shared < DefaultHandler > (hConfig, cbDummy);
        }
        std::vector<std::thread*> threads;
        threads.resize(numThreads);
        std::vector < std::vector < uint64_t >> tstats;
        tstats.resize(numThreads);
        auto tsync = std::chrono::steady_clock::now();
        tbb::concurrent_queue<InfoCont*> pqueue;
        auto filler = std::async(std::launch::async, queueFiller, std::ref(hists), std::ref(pqueue), tsync,
                                 numPublishes, sleepFor);
        bool keepRunning = true;
        for (size_t t = 0; t < numThreads; t++) {
            //threads.push_back(new std::thread(std::bind(runPublishers,hists,tstats.at(t),tsync,handler,numPublishes,sleepFor,t)));
            threads[t] = new std::thread(std::bind(runPublishers, handler, &pqueue, &keepRunning));
        }
        filler.get();
        keepRunning = false;
        for (auto& t : threads) {
            t->join();
        }
    } catch (const ers::Issue& ex) {
        // if(!IPCFailed)ers::fatal(ex);
        std::cout << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (const boost::program_options::error& ex) {
        std::cerr << "Exception from program options " << ex.what() << std::endl;
        // if(!IPCFailed) ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    } catch (const std::exception& ex) {
        //ers::error(daq::rc::CmdlParsing(ERS_HERE, "", ex));
        std::cerr << "Caught exception " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
