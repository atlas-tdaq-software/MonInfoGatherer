////////////////////////////////////////////////////////////////////////
//
//  Copied from p_receiver.cc in IS package and modified
//  Sami Kama
//
////////////////////////////////////////////////////////////////////////

#include <mutex>
#include <cstring>
#include <ctime>
#include <chrono>
#include <thread>
#include <random>
#include <unordered_map>
#include <signal.h>
#include <iostream>
#include <atomic>

#include <cmdl/cmdargs.h>
#include <ers/ers.h>
#include <ipc/core.h>
#include <owl/semaphore.h>
#include <is/infoany.h>
#include <is/infodynany.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <owl/timer.h>


std::unordered_map<std::string, std::mutex*> histList;

OWLSemaphore * semaphore = 0;
int signalReceived = 0;
int Updates = 1;
std::atomic_int Count(0);
int Verbose = 0;
int Delay = 0;
int ncallbacks = 0;
int stagger_duration = 0;
std::mutex coutMtx;
ISInfoDictionary *dict = 0;
int Period = -1;
std::string destName;

void callback(const ISCallbackInfo * isc)
{
    // static OWLTimer t;
    ISInfoDynAny isa;
    isc->value(isa);
    std::vector<std::string> v;
    try {
        if (isa.getAttributeType(0) == ISType::String) {
            v = isa.getAttributeValue<std::vector<std::string>>(0);
        } else {
            return;
        }
    } catch (...) {
        return;
    }
    std::string anns;
    for (auto &r : v)
        anns += r + " ";
    char buff[100];
    auto tnow = std::chrono::system_clock::now();
    std::time_t t = std::chrono::system_clock::to_time_t(tnow);
    //  char buff[100];
    auto countMS = std::chrono::duration_cast<std::chrono::milliseconds>(tnow.time_since_epoch()).count();
    auto countS = std::chrono::duration_cast<std::chrono::seconds>(tnow.time_since_epoch()).count();
    if (std::strftime(buff, sizeof(buff), "%H:%M:%S", std::localtime(&t))) {
        snprintf(buff + strlen(buff), 100 - strlen(buff), ",%03ld ", countMS - countS * 1000);
    }
    // TODO: should use is/tutils for this
    std::string name = isc->name();
    std::size_t serverPos = name.find('.');
    std::string serverName = name.substr(0, serverPos);
    std::size_t providerPos = name.find('.', serverPos + 1);
    std::string providerName = name.substr(serverPos + 1, providerPos - serverPos - 1);
    std::string objName = name.substr(providerPos + 1, std::string::npos);
    std::mutex* hmutx = 0;
    {
        std::unique_lock<std::mutex> lock(coutMtx);
        // TODO: this is leaking a lot of mutexes?
        auto res = histList.insert(std::make_pair(providerName, new std::mutex()));
        hmutx = res.first->second;
    }
    //printf("%s %s %s\n",buff,anns.c_str());
    if (stagger_duration > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(stagger_duration));
    }
    if (Period > 0) {
        if (hmutx) {
            std::unique_lock<std::mutex> lock(*hmutx);
            ncallbacks++;
            if (ncallbacks >= Period) {
                dict->checkin(destName + objName, isc->tag(), isa);
                ncallbacks = 0;
            }
        }
    }
    //if(!res.second)dict->checkin(
    std::unique_lock<std::mutex> lock(coutMtx);
    std::cout << buff << isc->time() << " " << isc->name() << " " << anns << std::endl;
}

void signal_handler(int sig)
{
    if (semaphore != 0) {
        signalReceived = sig;
        semaphore->post();
    }
}

int main(int argc, char ** argv)
{
    try {
        IPCCore::init(argc, argv);
    } catch (const daq::ipc::Exception& ex) {
        is::fatal(ex);
    }

    // Declare arguments
    CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStrList server_names('n', "server", "[server-name ...]", "servers to subscribe to.", CmdArg::isREQ);
    CmdArgStr Obj_re('r', "regex", "regular-expression", "Object Regular expression");
    CmdArgInt Stagger('S', "stagger", "stagger_duration", "Duration to stagger callbacks(milliSeconds)");
    CmdArgInt publishPeriod('T', "period", "publish-period",
                            "How often publish to destination (#of callbacks)");
    CmdArgStr destination('d', "destination", "destination-server", "Destination Server");

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &partition_name, &server_names, &Obj_re, &Stagger, &publishPeriod, &destination, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    cmd.description(
            "This program implements performance tests for the Information Service.\n"
            "It prints for every callback invocation the following information:\n"
            "\"info name\" \"reason\" \"difference between current time and information time in seconds\".");

    // Parse arguments
    Stagger = 0;
    publishPeriod = -1;
    Obj_re = ".*\\./SHIFT/TrigSteer_HLT/Rate20s";
    destination = "DF_Histogramming";
    cmd.parse(arg_iter);

    stagger_duration = Stagger;
    Period = publishPeriod;
    IPCPartition partition(partition_name);
    ISInfoReceiver ir(partition);
    dict = new ISInfoDictionary(partition);
    destName = std::string(destination) + ".DummReceiver.";
    signal(SIGINT, signal_handler);
    signal(SIGKILL, signal_handler);
    std::cout << "Using re \"" << std::string(Obj_re) << "\"\n";
    ISCriteria criteria((std::string(Obj_re)));
    try {
        for (size_t i = 0; i < server_names.count(); i++) {
            ir.subscribe(std::string(server_names[i]), criteria, callback);
            std::cout << "Subscribe to all successfull\n";
        }

        OWLSemaphore s;
        semaphore = &s;
        s.wait();

        if (signalReceived) {
            std::cout << " Signal " << signalReceived << " received.\n";
        }

        std::cout << "Receiver " << getpid() << " has been notified " << Count << " times.\n";

        std::cout << " Exiting ...";
        std::cout << " ( Unsubscribing ...";
        for (size_t i = 0; i < server_names.count(); i++) {
            ir.unsubscribe((const char*) server_names[i], criteria);
            std::cout << "unsubscribe to all successfull ";
        }
        std::cout << " Done )";
        std::cout << " done.\n";
    } catch (const daq::is::Exception& ex) {
        is::fatal(ex);
    }

    return 0;
}
