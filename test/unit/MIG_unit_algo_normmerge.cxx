//--------------------------------------------------------------------------
// Description:
//	Test suite case for normalized many-to-one bin merge.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_normmergealgo
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

namespace {

// Fixture that uses HistoAlgorithm
struct HistoAlgoFixture {
    void operate(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst) {
        ConfigParameters cfg({"Normalized=1"});
        HistoAlgorithm algo(cfg);
        LabelMap lblMap;
        algo.operate(src, dst, lblMap);
    }
};

}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_same_bins, HistoAlgoFixture)
{
    // same axes, that should just use AddAlgorithm.inc
    // which sums underflow/overflow bins
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 30, axes, {1, 2, 3, 4, 5});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 20, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 30);
    BOOST_CHECK_EQUAL(dst->entries, 50);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    float expect[] = {0.6, 1.2, 3.0, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which resets underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 60);
    BOOST_CHECK_EQUAL(dst->entries, 100);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    float expect[] = {1000, 3, 6.6, 9, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_with_err, HistoAlgoFixture)
{
    // same as previous but with error bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 60);
    BOOST_TEST(dst->entries == 100);
    BOOST_TEST(src->get_bins_size() == 8);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.size() == 8);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {1000, 3, 6.6, 9, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expect[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_mix_err_1, HistoAlgoFixture)
{
    // same as previous but with only one histo having errors
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 60);
    BOOST_TEST(dst->entries == 100);
    BOOST_TEST(src->get_bins_size() == 8);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.size() == 0);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {1000, 3, 6.6, 9, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    float expecterr[] = {1000, 0, 1.2, 1.2, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_mix_err_2, HistoAlgoFixture)
{
    // same as previous but with only one histo having errors
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 60);
    BOOST_TEST(dst->entries == 100);
    BOOST_TEST(src->get_bins_size() == 8);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.size() == 8);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {1000, 3, 6.6, 9, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    float expecterr[] = {0, 3, 5.4, 7.8, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 60);
    BOOST_CHECK_EQUAL(dst->entries, 100);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    float expect[] = {0, 2, 5.4, 7, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_with_err, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 60);
    BOOST_CHECK_EQUAL(dst->entries, 100);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    BOOST_TEST(src->errors.size() == 5);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {0, 2, 5.4, 7, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expect[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_mix_err_1, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 60);
    BOOST_CHECK_EQUAL(dst->entries, 100);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    BOOST_TEST(src->errors.size() == 0);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {0, 2, 5.4, 7, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    float expecterr[] = {0, 2, 3.6, 5.2, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_mix_err_2, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<float>(oh::Histogram::D1, "h1d", 60, axes_src, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<float>(oh::Histogram::D1, "h1d", 40, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 60);
    BOOST_CHECK_EQUAL(dst->entries, 100);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    BOOST_TEST(src->errors.size() == 5);
    BOOST_TEST(dst->errors.size() == 5);
    float expect[] = {0, 2, 5.4, 7, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i], tt::tolerance(0.000001));
    }
    float expecterr[] = {0, 0, 1.8, 1.8, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}
