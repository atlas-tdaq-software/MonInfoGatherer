//--------------------------------------------------------------------------
// Description:
//	Test suite for the Scheduler class.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <atomic>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "../../src/containers/Scheduler.h"

using namespace MIG;

#define BOOST_TEST_MODULE mig_unit_scheduler
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

using namespace MIG;

namespace {

class TestTask : public Task {
public:
    void execute() override {
        ++count;
    }

    std::atomic<int> count;
};
}


// ==============================================================

BOOST_AUTO_TEST_CASE(test_basic)
{
    // schedule few tasks and check how many times each of them was executed

    auto task1 = std::make_shared<TestTask>();
    auto task2 = std::make_shared<TestTask>();
    auto task3 = std::make_shared<TestTask>();

    Scheduler& scheduler = Scheduler::instance();

    // start tree trasks with different periods
    scheduler.add(task1, std::chrono::milliseconds(50));
    scheduler.add(task2, std::chrono::milliseconds(20));
    scheduler.add(task3, std::chrono::milliseconds(10));

    while (task1->count < 10) {
        // just burn some CPU
    }

    // don't update anyhting
    scheduler.stop();

    BOOST_TEST(task1->count >= 10);
    BOOST_TEST(task1->count < 12);
    BOOST_TEST(task2->count >= 23);
    BOOST_TEST(task2->count < 30);
    BOOST_TEST(task3->count >= 45);
    BOOST_TEST(task2->count < 55);
}
