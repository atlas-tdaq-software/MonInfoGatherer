//--------------------------------------------------------------------------
// Description:
//	Test suite case for the LibraryManager class.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/LibraryManager.h"

using namespace MIG;

#define BOOST_TEST_MODULE MIG_unit_libmanager
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

using namespace MIG;

// ==============================================================

BOOST_AUTO_TEST_CASE(test_instance)
{
    BOOST_CHECK_NO_THROW(LibraryManager::instance());
}

BOOST_AUTO_TEST_CASE(test_known_lib)
{
    // this does not have register_objects() but we don't care
    bool ok = LibraryManager::instance().loadLibrary("libMIGCommon.so");
    BOOST_CHECK(ok);
}

BOOST_AUTO_TEST_CASE(test_unknown_lib)
{
    bool ok = LibraryManager::instance().loadLibrary("libMIGCommon_not.so");
    BOOST_CHECK(not ok);
}
