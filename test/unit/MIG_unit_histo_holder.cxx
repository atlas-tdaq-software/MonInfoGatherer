//--------------------------------------------------------------------------
// Description:
//      Test suite for HistoHolder class
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

#define BOOST_TEST_MODULE MIG_unit_histo_holder
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MIG_test_utils.h"
#include "../../src/containers/HistogramHolder.h"

namespace tt = boost::test_tools;
using namespace MIG;
using namespace MIG::test;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

template <typename T>
std::vector<T>
bins2vector(oh::HistogramData<T> const& histo) {
    T const* data = histo.get_bins_array();
    unsigned data_size = histo.get_bins_size();
    return std::vector<T>(data, data+data_size);
}
}


// ==============================================================

BOOST_AUTO_TEST_CASE(test_compresseddata_vector)
{
    {
        // with minSize == -1 no compression at all
        std::vector<int> v({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        CompressedData<int> cdata(v, -1, 1.);
        BOOST_TEST(not cdata.compressed());
        BOOST_TEST(cdata.size() == 10);
        BOOST_TEST(cdata.compressed_size() == 0);
        BOOST_TEST(v.size() == 10);
    }
    {
        // size is below min. size
        std::vector<int> v({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        CompressedData<int> cdata(v, 512, 1.);
        BOOST_TEST(not cdata.compressed());
        BOOST_TEST(v.size() == 10);
    }
    {
        // this should compress
        std::vector<int> v(100, 1.);
        CompressedData<int> cdata(v, 0, .5);
        BOOST_TEST(cdata.compressed());
        BOOST_TEST(cdata.size() == 100);
        BOOST_TEST(cdata.compressed_size() < 100);
        BOOST_TEST(v.empty());
        std::vector<int> v_out;
        cdata.uncompress(v_out);
        BOOST_TEST(v_out == std::vector<int>(100, 1.), tt::per_element());
    }
}

BOOST_AUTO_TEST_CASE(test_compresseddata_array)
{
    {
        // with minSize == -1 no compression at all
        int v[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CompressedData<int> cdata(v, 10, -1, 1.);
        BOOST_TEST(not cdata.compressed());
        BOOST_TEST(cdata.size() == 10);
        BOOST_TEST(cdata.compressed_size() == 0);
    }
    {
        // size is below min. size
        int v[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CompressedData<int> cdata(v, 10, 512, 1.);
        BOOST_TEST(not cdata.compressed());
    }
    {
        // this should compress
        int v[100];
        std::fill_n(v, 100, 1);
        CompressedData<int> cdata(v, 100, 0, .5);
        BOOST_TEST(cdata.compressed());
        BOOST_TEST(cdata.size() == 100);
        BOOST_TEST(cdata.compressed_size() < 100);
        std::unique_ptr<int[]> v_out = cdata.uncompress();
        BOOST_TEST(v_out.get());
        for (int i = 0; i != 100; ++i) {
            BOOST_TEST(v_out.get()[i] == 1);
        }
    }
}

BOOST_AUTO_TEST_CASE(test_histo_holder_small)
{
    // test for small size histogram that should not compress
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<int> bins({1, 2, 3, 4, 5});
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, bins);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 1024, 0.);

    // this should return the same histogram
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 == h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors.empty());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors.empty());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_small_with_err)
{
    // test for small size histogram that should not compress
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<int> bins({1, 2, 3, 4, 5});
    std::vector<double> errs({1., 2., 3., 4., 5.});
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, bins, errs);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 1024, 0.);

    // this should return the same histogram
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 == h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_small_prof)
{
    // test for small size histogram that should not compress
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<double> bins({1., 2., 3., 4., 5.});
    std::vector<double> errs({1., 2., 3., 4., 5.});
    auto histo1 = makeProf(oh::Histogram::D1, "h1d", 15, axes, bins, errs, errs);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 1024, 0.);

    // this should return the same histogram
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::ProfileData>(h1);
    BOOST_TEST(ptr1 == h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
    BOOST_TEST(h1t->bin_entries == errs, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::ProfileData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
    BOOST_TEST(h1t->bin_entries == errs, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_small_effi)
{
    // test for small size histogram that should not compress
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<double> bins({1., 2., 3., 4., 5.});
    auto histo1 = makeEffi(oh::Histogram::D1, "h1d", 15, axes, bins, bins);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Efficiency>::makeHolder(std::move(histo1), 1024, 0.);

    // this should return the same histogram
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::EfficiencyData>(h1);
    BOOST_TEST(ptr1 == h1t.get());
    BOOST_TEST(bins2vector(h1t->total) == bins, tt::per_element());
    BOOST_TEST(bins2vector(h1t->passed) == bins, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::EfficiencyData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(h1t->total) == bins, tt::per_element());
    BOOST_TEST(bins2vector(h1t->passed) == bins, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_compressed)
{
    // test for large histogram that should compress
    std::vector<oh::Axis> axes({
        makeAxis(1000, "X", {0., 10.})
    });
    std::vector<int> bins(1002, 1);
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, bins);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 512, 1.);

    // this should return new copy
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors.empty());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors.empty());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_compressed_with_err)
{
    // test for large histogram that should compress
    std::vector<oh::Axis> axes({
        makeAxis(1000, "X", {0., 10.})
    });
    std::vector<int> bins(1002, 1);
    std::vector<double> errs(1002, 3.);
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, bins, errs);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 512, 1.);

    // this should return new copy
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::HistogramData<int>>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_compressed_prof)
{
    // test for large histogram that should compress
    std::vector<oh::Axis> axes({
        makeAxis(1000, "X", {0., 10.})
    });
    std::vector<double> bins(1002, 1.);
    std::vector<double> errs(1002, 3.);
    auto histo1 = makeProf(oh::Histogram::D1, "h1d", 15, axes, bins, errs, errs);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Histogram>::makeHolder(std::move(histo1), 512, 1.);

    // this should return new copy
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::ProfileData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
    BOOST_TEST(h1t->bin_entries == errs, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::ProfileData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(*h1t) == bins, tt::per_element());
    BOOST_TEST(h1t->errors == errs, tt::per_element());
    BOOST_TEST(h1t->bin_entries == errs, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_histo_holder_compressed_effi)
{
    // test for large histogram that should compress
    std::vector<oh::Axis> axes({
        makeAxis(1000, "X", {0., 10.})
    });
    std::vector<double> totals(1002, 3.);
    std::vector<double> passed(1002, 1.);
    auto histo1 = makeEffi(oh::Histogram::D1, "h1d", 15, axes, totals, passed);
    auto ptr1 = histo1.get();

    auto holder = HistogramHolder<oh::Efficiency>::makeHolder(std::move(histo1), 512, 1.);

    // this should return new copy
    auto h1 = holder->get();
    auto h1t = std::dynamic_pointer_cast<oh::EfficiencyData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(h1t->total) == totals, tt::per_element());
    BOOST_TEST(bins2vector(h1t->passed) == passed, tt::per_element());

    // this should return new copy
    h1 = holder->get(true);
    h1t = std::dynamic_pointer_cast<oh::EfficiencyData>(h1);
    BOOST_TEST(ptr1 != h1t.get());
    BOOST_TEST(bins2vector(h1t->total) == totals, tt::per_element());
    BOOST_TEST(bins2vector(h1t->passed) == passed, tt::per_element());
}
