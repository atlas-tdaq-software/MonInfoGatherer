//--------------------------------------------------------------------------
// Description:
//	Test case for simple histogram bin-by-bin adding (old AddAlgorithm).
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_algo_add
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

// Fixture that uses HistoAlgorithm for everything
struct HistoAlgoFixture {
    template <typename T>
    void operate(oh::ObjectBase<T>& src, oh::ObjectBase<T>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap;
        algo.operate(src, dst, lblMap);
    }
};


BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmNoErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5});
    auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 15);
    BOOST_CHECK_EQUAL(histo2->entries, 22);
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK(histo1->errors.empty());
    BOOST_CHECK(histo2->errors.empty());
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1., 2., 3., 4., 5.});
    auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 15);
    BOOST_CHECK_EQUAL(histo2->entries, 22);
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo1->errors.size(), 5);
    BOOST_CHECK_EQUAL(histo2->errors.size(), 5);
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
    }
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->errors[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrorsMixed1, HistoAlgoFixture)
{
    // one of the two histograms has errors array
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5});
    auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_TEST(histo1->entries == 15);
    BOOST_TEST(histo2->entries == 22);
    BOOST_TEST(histo1->get_bins_size() == 5);
    BOOST_TEST(histo2->get_bins_size() == 5);
    BOOST_TEST(histo1->errors.empty());
    BOOST_TEST(histo2->errors.size() == 5);
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->get_bins_array()[i] == expect[i]);
    }
    std::vector<double> expect_errors({0., 0., 3., 3., 1.});
    BOOST_TEST(histo2->errors == expect_errors, tt::per_element());
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrorsMixed2, HistoAlgoFixture)
{
    // one of the two histograms has errors array
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1., 2., 3., 4., 5.});
    auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_TEST(histo1->entries == 15);
    BOOST_TEST(histo2->entries == 22);
    BOOST_TEST(histo1->get_bins_size() == 5);
    BOOST_TEST(histo2->get_bins_size() == 5);
    BOOST_TEST(histo1->errors.size() == 5);
    BOOST_TEST(histo2->errors.size() == 5);
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->get_bins_array()[i] == expect[i]);
    }
    std::vector<double> expect_errors({1., 2., 3., 4., 5.});
    BOOST_TEST(histo2->errors == expect_errors, tt::per_element());
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmProfNoErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeProf(oh::Histogram::D1, "h1d", 15, axes, {1., 2., 3., 4., 5.}, {1., 2., 3., 4., 5.});
    auto histo2 = makeProf(oh::Histogram::D1, "h1d", 7, axes, {0., 0., 3., 3., 1.}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 15);
    BOOST_CHECK_EQUAL(histo2->entries, 22);
    BOOST_CHECK(histo1->errors.empty());
    BOOST_CHECK(histo2->errors.empty());
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo1->bin_entries.size(), 5);
    BOOST_CHECK_EQUAL(histo2->bin_entries.size(), 5);

    double expect[] = {1., 2., 6., 7., 6.};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(histo2->bin_entries[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmEffNoErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeEffi(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {0, 1, 1, 2, 0});
    auto histo2 = makeEffi(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1}, {0, 0, 1, 2, 0});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_TEST(histo1->total.entries == 15);
    BOOST_TEST(histo1->passed.entries == 15);
    BOOST_TEST(histo2->total.entries == 22);
    BOOST_TEST(histo2->passed.entries == 22);
    BOOST_TEST(histo1->total.get_bins_size() == 5);
    BOOST_TEST(histo1->passed.get_bins_size() == 5);
    BOOST_TEST(histo2->total.get_bins_size() == 5);
    BOOST_TEST(histo2->passed.get_bins_size() == 5);
    BOOST_CHECK(histo1->total.errors.empty());
    BOOST_CHECK(histo1->passed.errors.empty());
    BOOST_CHECK(histo2->total.errors.empty());
    BOOST_CHECK(histo2->passed.errors.empty());
    BOOST_TEST(histo2->weight == 0.5);
    BOOST_TEST(histo2->betaAlpha == 1.);
    BOOST_TEST(histo2->betaBeta == 1.);
    int expect_total[] = {1, 2, 6, 7, 6};
    int expect_passed[] = {0, 1, 2, 4, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->total.get_bins_array()[i], expect_total[i]);
        BOOST_CHECK_EQUAL(histo2->passed.get_bins_array()[i], expect_passed[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmFailures, HistoAlgoFixture)
{
    // test for some failure modes in AddAlorithm

    {
        // testing different dimensions
        std::vector<oh::Axis> axes1({
            makeAxis(3, "X", {0., 10.})
        });
        std::vector<oh::Axis> axes2({
            makeAxis(3, "X", {0., 10.}),
            makeAxis(3, "Y", {0., 100.})
        });

        auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes1, {1, 2, 3, 4, 5});
        auto histo2 = makeHist<int>(oh::Histogram::D2, "h2d", 7, axes2, {0, 0, 3, 3, 1});

        BOOST_CHECK_THROW(this->operate(*histo1, *histo2), IncompatibleDimensions);
    }

    {
        // testing different axis types
        std::vector<oh::Axis> axes1({
            makeAxis(3, "X", {0., 10.})
        });
        std::vector<oh::Axis> axes2({
            makeVarAxis(3, "X", {0., 10.}, std::vector<std::string>())
        });

        auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes1, {1, 2, 3, 4, 5});
        auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes2, {1, 2, 3, 4, 5});

        BOOST_CHECK_THROW(this->operate(*histo1, *histo2), IncompatibleBinning);
    }

    {
        // testing different binning ranges
        std::vector<oh::Axis> axes1({
            makeAxis(3, "X", {0., 10.})
        });
        std::vector<oh::Axis> axes2({
            makeAxis(3, "X", {0., 10.01})
        });

        auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes1, {1, 2, 3, 4, 5});
        auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes2, {1, 2, 3, 4, 5});

        BOOST_CHECK_THROW(this->operate(*histo1, *histo2), IncompatibleBinning);
    }

    {
        // testing incorrect sizes of error arrays
        std::vector<oh::Axis> axes({
            makeAxis(3, "X", {0., 10.})
        });
        auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6});
        auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5});

        BOOST_CHECK_THROW(this->operate(*histo1, *histo2), UnexpectedBinCount);
    }

    {
        // testing incorrect sizes of error arrays
        std::vector<oh::Axis> axes({
            makeAxis(3, "X", {0., 10.})
        });
        auto histo1 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5});
        auto histo2 = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6});

        BOOST_CHECK_THROW(this->operate(*histo1, *histo2), UnexpectedBinCount);
    }

}
