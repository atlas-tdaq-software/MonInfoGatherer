//--------------------------------------------------------------------------
// Description:
//	Test suite case for the HandlerManager class.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/HandlerManager.h"
#include "MonInfoGatherer/HandlerBase.h"
#include "MonInfoGatherer/InfoPublisher.h"

using namespace MIG;

#define BOOST_TEST_MODULE MIG_unit_handlermanager
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

using namespace MIG;

namespace {

class TestHandler: public MIG::HandlerBase {
public:
    void updateInfo(ISInfoAny& info, const std::string& name, const std::string& provider) override {}
};

std::shared_ptr<MIG::HandlerBase> testHandlerFactory(const MIG::HandlerConfig&, InfoPublisher&) {
    return std::make_shared<TestHandler>();
}

class DummyPublisher : public InfoPublisher {
public:
    void publish(ISInfo& info, const std::string& name, int nContrib) override {
    }
};

}

// ==============================================================

BOOST_AUTO_TEST_CASE(test_instance)
{
    BOOST_CHECK_NO_THROW(HandlerManager::instance());
}

BOOST_AUTO_TEST_CASE(test_register)
{
    HandlerManager::HandlerFactory factory(testHandlerFactory);

    // this does not have register_objects() but we don't care
    HandlerManager& mgr = HandlerManager::instance();

    bool ok = mgr.registerHandler("test", factory);
    BOOST_CHECK(ok);
    ok = mgr.registerHandler("test2", factory);
    BOOST_CHECK(ok);

    // second registration fails
    ok = mgr.registerHandler("test", factory);
    BOOST_CHECK(not ok);

    // empty naem fails
    ok = mgr.registerHandler("", factory);
    BOOST_CHECK(not ok);
}

BOOST_AUTO_TEST_CASE(test_get)
{
    HandlerManager::HandlerFactory factory(testHandlerFactory);

    // this does not have register_objects() but we don't care
    HandlerManager& mgr = HandlerManager::instance();

    mgr.registerHandler("test", factory);

    // no name defined
    DummyPublisher dummyPub;
    HandlerConfig hConfig;
    auto handler = mgr.getHandler(hConfig, dummyPub);
    BOOST_CHECK(handler == nullptr);

    // use existing name
    hConfig.name = "test";
    handler = mgr.getHandler(hConfig, dummyPub);
    BOOST_CHECK(handler != nullptr);

    // unknown name
    hConfig.name = "test_x";
    handler = mgr.getHandler(hConfig, dummyPub);
    BOOST_CHECK(handler == nullptr);
}
