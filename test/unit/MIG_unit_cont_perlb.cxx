//--------------------------------------------------------------------------
// Description:
//      Test suite for PerLBTagCont class
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <memory>
#include <string>

#define BOOST_TEST_MODULE MIG_unit_cont_perlb
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/ProviderMap.h"
#include "MonInfoGatherer/Timers.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "../../src/containers/PerLBTagCont.h"
#include "oh/core/HistogramData.h"
#include "MIG_test_utils.h"
#include "MIG_test_clock.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

// needed for Boost to print in tt:per_element
std::ostream&
operator<<(std::ostream& out, std::pair<std::string, ISAnnotation> const& pair) {
    return out << '(' << pair.first << ", " << pair.second.data() << ')';
}

namespace {

using Histogram = oh::HistogramData<int>;
const unsigned entries_per_hist = 15;
std::chrono::milliseconds publishDelay(10);


// Factory for Histogram objects
struct DataFactory {
    std::unique_ptr<Histogram> operator()(int providerId, int tag=-1) {
        std::vector<oh::Axis> axes({
            makeAxis(3, "X", {0., 10.})
        });
        auto histo = makeHist<int>(oh::Histogram::D1, "h1d", entries_per_hist, axes, {1, 2, 3, 4, 5});
        ISProxy<>::set_tag(*histo, tag);
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(*histo).type();
        if (do_final) {
            histo->setAnnotation("FINAL", "FINAL");
        }
        for (unsigned lvl = 0; lvl != do_annotations.size(); ++lvl) {
            auto lvlCount = do_annotations[lvl];
            if (lvl == 0) {
                histo->setAnnotation("providers", std::to_string(lvlCount));
            } else {
                histo->setAnnotation("mig.lvl" + std::to_string(lvl), std::to_string(lvlCount));
            }
        }
        ISProxy<>::set_time(*histo, Clock::instance().now());
        return histo;
    }

    std::vector<int> do_annotations;
    bool do_final = false;
};

struct ContFactory {
    std::unique_ptr<PerLBTagCont<oh::Histogram>>
    operator()(InfoPublisher& publisher) {
        ConfigParameters param({"PerLBPublishDelay_ms=" + std::to_string(publishDelay.count())});
        return std::make_unique<PerLBTagCont<oh::Histogram>>("Histo", alg, publisher, providerMap, param);
    }
    HistoAlgorithm alg;
    ProviderMap providerMap;
};

template <typename Cont, typename ContFactory, typename DataFactory>
struct PerLBContFixture : public SimplePublisher, public PublishBase {

    // publish one object for a given provider
    void publish(int providerId, int tag=-1) override {
        std::string provider = "provider" + std::to_string(providerId);
        ERS_LOG("Publishing " << provider);
        if (cont == nullptr) {
            cont = contFactory(*this);
        }
        TimingEntries timing;
        TimingCollector timingCollector(timing, "", 0, 0);
        cont->tagOperate(dataFactory(providerId, tag), provider, timingCollector);
    }

    std::unique_ptr<Cont> cont;
    ContFactory contFactory;
    DataFactory dataFactory;
};


using Fixture = TClockFixture<
    AnnotationsCheck<
        PublishCheck<
            PerLBContFixture<PerLBTagCont<oh::Histogram>, ContFactory, DataFactory>
>>>;

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE(inputs_test, Fixture)
{
    publish(0);
    this->check_pubs()
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);

    publishList({1, 3, 5});
    this->check_pubs()
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(4);

    publishList({1, 3, 5});
    this->check_pubs()
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(7);
}

BOOST_FIXTURE_TEST_CASE(publish_test, Fixture)
{
    publishList({1, 3, 5});
    this->check_pubs()
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(3);

    // this should not cause publication
    cont->execute();
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(3);

    // delay for longer than publishDelay
    tclock += publishDelay * 2;

    // should publish now
    cont->execute();
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(3)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(0);

    tclock += publishDelay * 2;

    // no updates, should not publish
    cont->execute();
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(3)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(0);

    publishList({0, 1, 2, 3, 4, 5});

    tclock += publishDelay * 2;

    // should publish now
    cont->execute();
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(6)
        .nUpdatingProviders(6)
        .nUpdatesSincePublish(0);
}

BOOST_FIXTURE_TEST_CASE(annotations1, Fixture)
{
    // test for annotations, case when histograms come without annotations,
    // in that case gatherer should add single level of annotations

    dataFactory.do_annotations = {};

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    tclock += publishDelay * 2;
    cont->execute();
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    check_annotations({7});
}

BOOST_FIXTURE_TEST_CASE(annotations2, Fixture)
{
    // test for annotations, case when histograms come with annotations,
    // in that case gatherer should add extra level of annotations

    dataFactory.do_annotations = {100, 10};

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    tclock += publishDelay * 2;
    cont->execute();
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    check_annotations({700, 70, 7});

    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    tclock += publishDelay * 2;
    cont->execute();
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    check_annotations({700, 70, 7});
}
