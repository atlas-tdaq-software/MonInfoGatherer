//--------------------------------------------------------------------------
// Description:
//	Test suite case for labelled histograms.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_algo_lblmerge
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "../../src/algos/HistoAlgorithm.h"
#include "MonInfoGatherer/LabelMap.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

/*
 * There are three classes of tests that we do here with fully-labeled
 * histograms:
 *  - source and destination labels are the same and come in the same order
 *  - source and destination labels are the same but order is different
 *  - source and destination labels are different, destination will be expanded
 *  - (there is also case with partial labels, and case with no labels)
 *
 * And we want to test all three cases for 1D, 2D, 3D histograms and profiles
 */

namespace {

// values for overflow/underflow bins
const int underflow = 100;
const int overflow = 333;

// fill histogram data for 1D case
template <typename T=int>
std::vector<T> fill1d(unsigned len) {
    len += 2;
    std::vector<T> data(len, 0);
    data[0] = underflow;
    for (unsigned i = 1; i != len-1; ++i) {
        data[i] = i;
    }
    data[len-1] = overflow;
    return data;
}

// index into 1-d array for 2-d data
unsigned idx2(unsigned x, unsigned y, unsigned lenx) {
    return y*lenx + x;
}

// fill histogram data for 2D case
template <typename T=int>
std::vector<T> fill2d(unsigned lenx, unsigned leny) {
    lenx += 2;
    leny += 2;
    std::vector<T> data(lenx*leny, 0);
    for (unsigned x = 0; x != lenx; ++ x) {
        data[idx2(x, 0, lenx)] += underflow;
        data[idx2(x, leny-1, lenx)] += overflow;
    }
    for (unsigned y = 0; y != leny; ++ y) {
        data[idx2(0, y, lenx)] += underflow;
        data[idx2(lenx-1, y, lenx)] += overflow;
    }
    for (unsigned y = 1; y != leny-1; ++ y) {
        for (unsigned x = 1; x != lenx-1; ++ x) {
            data[idx2(x, y, lenx)] += y*10 + x;
        }
    }
    return data;
}

// index into 1-d array for 3-d data
unsigned idx3(unsigned x, unsigned y, unsigned z, unsigned lenx, unsigned leny) {
    return z*lenx*leny + y*lenx + x;
}

// fill histogram data for 3D case
template <typename T=int>
std::vector<T> fill3d(unsigned lenx, unsigned leny, unsigned lenz) {
    lenx += 2;
    leny += 2;
    lenz += 2;
    std::vector<T> data(lenx*leny*lenz, 0);
    for (unsigned x = 0; x != lenx; ++ x) {
        for (unsigned y = 0; y != leny; ++ y) {
            data[idx3(x, y, 0, lenx, leny)] += underflow;
            data[idx3(x, y, lenz-1, lenx, leny)] += overflow;
        }
    }
    for (unsigned z = 0; z != lenz; ++ z) {
        for (unsigned y = 0; y != leny; ++ y) {
            data[idx3(0, y, z, lenx, leny)] += underflow;
            data[idx3(lenx-1, y, z, lenx, leny)] += overflow;
        }
    }
    for (unsigned z = 0; z != lenz; ++ z) {
        for (unsigned x = 0; x != lenx; ++ x) {
            data[idx3(x, 0, z, lenx, leny)] += underflow;
            data[idx3(x, leny-1, z, lenx, leny)] += overflow;
        }
    }
    for (unsigned z = 1; z != lenz-1; ++ z) {
        for (unsigned y = 1; y != leny-1; ++ y) {
            for (unsigned x = 1; x != lenx-1; ++ x) {
                data[idx3(x, y, z, lenx, leny)] += z*100 + y*10 + x;
            }
        }
    }
    return data;
}


// Histogram type-specific code will be in specializations of this
// class, template parameter is the same as in LabelledMergeAlgorithm
// (i.e. it is a oh::HistogramData<T> or oh::ProfileData)
template <typename T>
struct HistogramTraits {
};

// specialization for oh::HistogramData<T>
template <typename T>
struct HistogramTraits<oh::HistogramData<T>> {

    typedef T value_type;

    // factory method for histogram
    static std::unique_ptr<oh::HistogramData<T>>
    makeHistFromAxes(std::vector<oh::Axis> const& axes) {
        std::vector<value_type> data;
        auto dim = oh::Histogram::D1;
        if (axes.size() == 3) {
            dim = oh::Histogram::D3;
            data = fill3d<value_type>(axes[0].nbins, axes[1].nbins, axes[2].nbins);
        } else  if (axes.size() == 2) {
            dim = oh::Histogram::D2;
            data = fill2d<value_type>(axes[0].nbins, axes[1].nbins);
        } else {
            data = fill1d<value_type>(axes[0].nbins);
        }

        return MIG::test::makeHist<T>(dim, "h3d", 100, axes, data);
    }

    // factory method for histogram
    static std::unique_ptr<oh::HistogramData<T>>
    makeHist(const std::vector<std::string>& labelsx,
             const std::vector<std::string>& labelsy=std::vector<std::string>(),
             const std::vector<std::string>& labelsz=std::vector<std::string>()) {

        std::vector<oh::Axis> axes;
        axes.push_back(makeAxis(labelsx.size(), "X", {0., 10.}, labelsx));
        if (not labelsz.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
            axes.push_back(makeAxis(labelsz.size(), "Z", {0., 10.}, labelsz));
        } else  if (not labelsy.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
        }

        return makeHistFromAxes(axes);
    }

    static void check_entries(const oh::HistogramData<T>& histo, int entries) {
        BOOST_TEST(histo.entries == entries);
    }

    static void check_bins_size(const oh::HistogramData<T>& histo, unsigned size) {
        BOOST_TEST(histo.get_bins_size() == size);
    }

    static void check_axis_labels(const oh::HistogramData<T>& histo, unsigned axis,
                                  const std::vector<std::string>& labels) {
        BOOST_CHECK(histo.axes[axis].labels == labels);
    }

    // check histogram bin contents against expected data
    static void check(const std::vector<value_type>& expect, const oh::HistogramData<T>& histo) {
        for (unsigned i = 0; i != expect.size(); ++i) {
            BOOST_CHECK_MESSAGE(histo.get_bins_array()[i] == expect[i],
                    "index " << i << ": [" << histo.get_bins_array()[i] <<
                    " == " << expect[i] << "]");
        }
    }

    void operate(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap(dst.axes);
        algo.operate(src, dst, lblMap);
    }

    const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Histogram>& hist) {
        return hist.axes;
    }
};


// specialization for Profile
template <>
struct HistogramTraits<oh::ProfileData> {

    typedef double value_type;

    // factory method for histogram
    static std::unique_ptr<oh::ProfileData>
    makeHistFromAxes(std::vector<oh::Axis> const& axes) {
        std::vector<value_type> data;
        auto dim = oh::Histogram::D1;
        if (axes.size() == 3) {
            dim = oh::Histogram::D3;
            data = fill3d<value_type>(axes[0].nbins, axes[1].nbins, axes[2].nbins);
        } else  if (axes.size() == 2) {
            dim = oh::Histogram::D2;
            data = fill2d<value_type>(axes[0].nbins, axes[1].nbins);
        } else {
            data = fill1d<value_type>(axes[0].nbins);
        }

        return makeProf(dim, "prof3d", 100, axes, data, data);
    }

    // factory method for 3-d histogram
    static std::unique_ptr<oh::ProfileData>
    makeHist(const std::vector<std::string>& labelsx,
            const std::vector<std::string>& labelsy=std::vector<std::string>(),
            const std::vector<std::string>& labelsz=std::vector<std::string>()) {

        std::vector<oh::Axis> axes;
        axes.push_back(makeAxis(labelsx.size(), "X", {0., 10.}, labelsx));
        if (not labelsz.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
            axes.push_back(makeAxis(labelsz.size(), "Z", {0., 10.}, labelsz));
        } else  if (not labelsy.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
        }

        return makeHistFromAxes(axes);
    }

    static void check_entries(const oh::ProfileData& histo, int entries) {
        BOOST_TEST(histo.entries == entries);
    }

    static void check_bins_size(const oh::ProfileData& histo, unsigned size) {
        BOOST_TEST(histo.get_bins_size() == size);
    }

    static void check_axis_labels(const oh::ProfileData& histo, unsigned axis,
                                  const std::vector<std::string>& labels) {
        BOOST_CHECK(histo.axes[axis].labels == labels);
    }

    // check histogram bin contents against expected data
    static void check(const std::vector<value_type>& expect, const oh::ProfileData& histo) {
        for (unsigned i = 0; i != expect.size(); ++i) {
            BOOST_CHECK_MESSAGE(histo.get_bins_array()[i] == expect[i],
                    "values index " << i << ": [" << histo.get_bins_array()[i] <<
                    " == " << expect[i] << "]");
            BOOST_CHECK_MESSAGE(histo.bin_entries[i] == expect[i],
                    "entries index " << i << ": [" << histo.bin_entries[i] <<
                    " == " << expect[i] << "]");
        }
    }

    void operate(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap(dst.axes);
        algo.operate(src, dst, lblMap);
    }

    const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Histogram>& hist) {
        return hist.axes;
    }
};

// specialization for Efficiency
template <>
struct HistogramTraits<oh::EfficiencyData> {

    typedef double value_type;

    // factory method for histogram
    static std::unique_ptr<oh::EfficiencyData>
    makeHistFromAxes(std::vector<oh::Axis> const& axes) {
        std::vector<value_type> data;
        auto dim = oh::Histogram::D1;
        if (axes.size() == 3) {
            dim = oh::Histogram::D3;
            data = fill3d<value_type>(axes[0].nbins, axes[1].nbins, axes[2].nbins);
        } else  if (axes.size() == 2) {
            dim = oh::Histogram::D2;
            data = fill2d<value_type>(axes[0].nbins, axes[1].nbins);
        } else {
            data = fill1d<value_type>(axes[0].nbins);
        }

        return makeEffi(dim, "prof3d", 100, axes, data, data);
    }

    // factory method for 3-d histogram
    static std::unique_ptr<oh::EfficiencyData>
    makeHist(const std::vector<std::string>& labelsx,
            const std::vector<std::string>& labelsy=std::vector<std::string>(),
            const std::vector<std::string>& labelsz=std::vector<std::string>()) {

        std::vector<oh::Axis> axes;
        axes.push_back(makeAxis(labelsx.size(), "X", {0., 10.}, labelsx));
        if (not labelsz.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
            axes.push_back(makeAxis(labelsz.size(), "Z", {0., 10.}, labelsz));
        } else  if (not labelsy.empty()) {
            axes.push_back(makeAxis(labelsy.size(), "Y", {0., 10.}, labelsy));
        }

        return makeHistFromAxes(axes);
    }

    static void check_entries(const oh::EfficiencyData& histo, int entries) {
        BOOST_TEST(histo.total.entries == entries);
        BOOST_TEST(histo.passed.entries == entries);
    }

    static void check_bins_size(const oh::EfficiencyData& histo, unsigned size) {
        BOOST_TEST(histo.total.get_bins_size() == size);
        BOOST_TEST(histo.passed.get_bins_size() == size);
    }

    static void check_axis_labels(const oh::EfficiencyData& histo, unsigned axis,
                                  const std::vector<std::string>& labels) {
        BOOST_CHECK(histo.total.axes[axis].labels == labels);
        BOOST_CHECK(histo.passed.axes[axis].labels == labels);
    }

    // check histogram bin contents against expected data
    static void check(const std::vector<value_type>& expect, const oh::EfficiencyData& histo) {
        for (unsigned i = 0; i != expect.size(); ++i) {
            BOOST_CHECK_MESSAGE(histo.total.get_bins_array()[i] == expect[i],
                    "values index " << i << ": [" << histo.total.get_bins_array()[i] <<
                    " == " << expect[i] << "]");
            BOOST_CHECK_MESSAGE(histo.passed.get_bins_array()[i] == expect[i],
                    "values index " << i << ": [" << histo.passed.get_bins_array()[i] <<
                    " == " << expect[i] << "]");
        }
    }

    void operate(oh::ObjectBase<oh::Efficiency>& src, oh::ObjectBase<oh::Efficiency>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap(dst.total.axes);
        algo.operate(src, dst, lblMap);
    }

    const std::vector<oh::Axis>& axes(const oh::ObjectBase<oh::Efficiency>& hist) {
        return hist.total.axes;
    }
};


struct HistoMergeHistFixture : HistogramTraits<oh::HistogramData<int>> {
};

struct HistoMergeProfFixture : HistogramTraits<oh::ProfileData> {
};

struct HistoMergeEffiFixture : HistogramTraits<oh::EfficiencyData> {
};

typedef boost::mpl::vector<HistoMergeHistFixture, HistoMergeProfFixture, HistoMergeEffiFixture> FixturesAll;

} // namespace


BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_1D_same_labels, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels, this should just sum everything
    auto src = F::makeHist({"A", "B", "C"});
    auto dst = F::makeHist({"A", "B", "C"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 5);
    F::check_bins_size(*dst, 5);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B", "C"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {underflow+underflow, 1+1, 2+2, 3+3, overflow+overflow};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_1D_same_labels_order, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same labels but different bin order
    auto src = F::makeHist({"C", "A", "B"});
    auto dst = F::makeHist({"A", "B", "C"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 5);
    F::check_bins_size(*dst, 5);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B", "C"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {underflow, 1+2, 2+3, 3+1, overflow};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_1D_resize, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // case with non-matching labels
    auto src = F::makeHist({"B", "C", "A"});
    auto dst = F::makeHist({"A", "B"});

    LabelMap lblMap(this->axes(*dst));
    HistoAlgorithm algo;
    BOOST_REQUIRE_NO_THROW(algo.operate(*src, *dst, lblMap));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 5);
    F::check_bins_size(*dst, 5);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B", "C"}));
    // overflow and underflow should not be summed, and in this case they should
    // be set to 0
    std::vector<value_type> expect = {0, 1+3, 2+1, 2, 0};
    F::check(expect, *dst);

    // one more time with new labels
    auto src2 = F::makeHist({"D", "E"});

    BOOST_REQUIRE_NO_THROW(algo.operate(*src2, *dst, lblMap));

    F::check_entries(*dst, 300);
    F::check_bins_size(*dst, 7);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B", "C", "D", "E"}));
    // overflow and underflow should not be summed, and in this case they should
    // be set to 0
    std::vector<value_type> expect2 = {0, 1+3, 2+1, 2, 1, 2, 0};
    F::check(expect2, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_same_labels, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels, this should just sum everything
    auto src = F::makeHist({"A", "B"}, {"T", "U"});
    auto dst = F::makeHist({"A", "B"}, {"T", "U"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 16);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {
            (underflow+underflow)*2, underflow*2, underflow*2, (underflow+overflow)*2,
            underflow*2, 11+11, 12+12, overflow*2,
            underflow*2, 21+21, 22+22, overflow*2,
            (underflow+overflow)*2, overflow*2, overflow*2, (overflow+overflow)*2};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_same_labels_order, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels, this should just sum everything
    auto src = F::makeHist({"B", "A"}, {"U", "T"});
    auto dst = F::makeHist({"A", "B"}, {"T", "U"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 16);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {
            underflow+underflow, underflow, underflow, underflow+overflow,
            underflow, 11+22, 12+21, overflow,
            underflow, 21+12, 22+11, overflow,
            underflow+overflow, overflow, overflow, overflow+overflow};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_resize, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels, this should just sum everything
    auto src = F::makeHist({"B", "A"}, {"U", "T"});
    auto dst = F::makeHist({"A"}, {"T"});

    LabelMap lblMap(this->axes(*dst));
    HistoAlgorithm algo;
    BOOST_REQUIRE_NO_THROW(algo.operate(*src, *dst, lblMap));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 16);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be set to 0
    std::vector<value_type> expect = {
            0, 0, 0, 0,
            0, 11+22, 21, 0,
            0, 12, 11, 0,
            0, 0, 0, 0};
    F::check(expect, *dst);

    // one more time with new labels
    auto src2 = F::makeHist({"C"}, {"V"});

    BOOST_REQUIRE_NO_THROW(algo.operate(*src2, *dst, lblMap));

    F::check_entries(*dst, 300);
    F::check_bins_size(*dst, 25);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B", "C"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"T", "U", "V"}));
    // overflow and underflow should not be summed, and in this case they should
    // be set to 0
    std::vector<value_type> expect2 = {
            0,     0,  0,  0, 0,
            0, 11+22, 21,  0, 0,
            0,    12, 11,  0, 0,
            0,     0,  0, 11, 0,
            0,     0,  0,  0, 0};
    F::check(expect2, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_mixed_same, F, FixturesAll, F)
{
    // mixed axes, one labelled, one not

    typedef typename F::value_type value_type;

    // same binning and labels, this should just sum everything
    std::vector<oh::Axis> axes{
        makeAxis(2, "X", {0., 10.}, {"A", "B"}),
        makeAxis(2, "Y", {0., 10.})
    };
    auto src = F::makeHistFromAxes(axes);
    auto dst = F::makeHistFromAxes(axes);

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 16);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>());
    std::vector<value_type> expect = {
            2*(underflow+underflow), 2*underflow, 2*underflow, 2*(underflow+overflow),
            2*underflow, 11+11, 12+12, 2*overflow,
            2*underflow, 21+21, 22+22, 2*overflow,
            2*(underflow+overflow), 2*overflow, 2*overflow, 2*(overflow+overflow)};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_mixed_merge1, F, FixturesAll, F)
{
    // mixed axes, one labelled, one not

    typedef typename F::value_type value_type;

    // non-labelled axis has twice the bins in src, so that dst
    // should not be resized
    std::vector<oh::Axis> axes1{
        makeAxis(2, "X", {0., 10.}, {"A", "B"}),
        makeAxis(4, "Y", {0., 5.})
    };
    auto src = F::makeHistFromAxes(axes1);
    std::vector<oh::Axis> axes2{
        makeAxis(2, "X", {0., 10.}, {"A", "B"}),
        makeAxis(2, "Y", {0., 10.})
    };
    auto dst = F::makeHistFromAxes(axes2);

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 24);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>());
    // X axis is direct-summed, some over/underflow bins are summed and some are not
    std::vector<value_type> expect = {
            underflow+underflow, underflow, underflow, underflow+overflow,
            3*underflow, 11+11+21, 12+12+22, 3*overflow,
            3*underflow, 21+31+41, 22+32+42, 3*overflow,
            underflow+overflow, overflow, overflow, overflow+overflow};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_2D_mixed_merge2, F, FixturesAll, F)
{
    // mixed axes, one labelled, one not

    typedef typename F::value_type value_type;

    // non-labelled axis has twice the bins in dst, so that dst
    // should be resized
    std::vector<oh::Axis> axes1{
        makeAxis(2, "X", {0., 10.}, {"A", "B"}),
        makeAxis(2, "Y", {0., 10.})
    };
    auto src = F::makeHistFromAxes(axes1);
    std::vector<oh::Axis> axes2{
        makeAxis(2, "X", {0., 10.}, {"A", "B"}),
        makeAxis(4, "Y", {0., 5.})
    };
    auto dst = F::makeHistFromAxes(axes2);

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 16);
    F::check_bins_size(*dst, 16);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>());
    // overflow and underflow should not be summed, and in this case they should
    // be set to 0, in new implementation some under/overflow bins are summed
    std::vector<value_type> expect = {
            0, 0, 0, 0,
            3*underflow, 11+11+21, 12+12+22, 3*overflow,
            3*underflow, 21+31+41, 22+32+42, 3*overflow,
            0, 0, 0, 0};
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_3D_same_labels, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels, this should just sum everything
    auto src = F::makeHist({"A", "B"}, {"Q", "R"}, {"T", "U"});
    auto dst = F::makeHist({"A", "B"}, {"Q", "R"}, {"T", "U"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 64);
    F::check_bins_size(*dst, 64);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"Q", "R"}));
    F::check_axis_labels(*dst, 2, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {
            // z-underflow plane
            6*underflow, 4*underflow, 4*underflow, 4*underflow+2*overflow,
            4*underflow, 2*underflow, 2*underflow, 2*underflow+2*overflow,
            4*underflow, 2*underflow, 2*underflow, 2*underflow+2*overflow,
            4*underflow+2*overflow, 2*underflow+2*overflow, 2*underflow+2*overflow, 2*underflow+4*overflow,
            // z bin 0 plane
            4*underflow, 2*underflow, 2*underflow, 2*underflow+2*overflow,
            2*underflow,     111+111,     112+112, 2*overflow,
            2*underflow,     121+121,     122+122, 2*overflow,
            2*underflow+2*overflow, 2*overflow, 2*overflow, 4*overflow,
            // z bin 1 plane
            4*underflow, 2*underflow, 2*underflow, 2*underflow+2*overflow,
            2*underflow,     211+211,     212+212, 2*overflow,
            2*underflow,     221+221,     222+222, 2*overflow,
            2*underflow+2*overflow, 2*overflow, 2*overflow, 4*overflow,
            // z-overflow plane
            4*underflow+2*overflow, 2*underflow+2*overflow, 2*underflow+2*overflow, 2*underflow+4*overflow,
            2*underflow+2*overflow, 2*overflow, 2*overflow, 4*overflow,
            2*underflow+2*overflow, 2*overflow, 2*overflow, 4*overflow,
            2*underflow+4*overflow, 4*overflow, 4*overflow, 6*overflow,
    };
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_3D_same_labels_order, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels but order is mixed, this should just sum everything
    auto src = F::makeHist({"B", "A"}, {"R", "Q"}, {"U", "T"});
    auto dst = F::makeHist({"A", "B"}, {"Q", "R"}, {"T", "U"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 64);
    F::check_bins_size(*dst, 64);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"Q", "R"}));
    F::check_axis_labels(*dst, 2, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be taken from dst
    std::vector<value_type> expect = {
            // z-underflow plane
            3*underflow, 2*underflow, 2*underflow, 2*underflow+overflow,
            2*underflow,   underflow,   underflow, underflow+overflow,
            2*underflow,   underflow,   underflow, underflow+overflow,
            2*underflow+overflow, underflow+overflow, underflow+overflow, underflow+2*overflow,
            // z bin 0 plane
            2*underflow, underflow, underflow, underflow+overflow,
              underflow,   111+222,   112+221, overflow,
              underflow,   121+212,   122+211, overflow,
            underflow+overflow, overflow, overflow, 2*overflow,
            // z bin 1 plane
            2*underflow, underflow, underflow, underflow+overflow,
              underflow,   211+122,   212+121, overflow,
              underflow,   221+112,   222+111, overflow,
            underflow+overflow, overflow, overflow, 2*overflow,
            // z-overflow plane
            2*underflow+overflow, underflow+overflow, underflow+overflow, underflow+2*overflow,
            underflow+overflow,   overflow,   overflow, 2*overflow,
            underflow+overflow,   overflow,   overflow, 2*overflow,
            underflow+2*overflow, 2*overflow, 2*overflow, 3*overflow,
    };
    F::check(expect, *dst);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(LabelledMergeAlgorithm_3D_resize, F, FixturesAll, F)
{
    typedef typename F::value_type value_type;

    // same axes and labels but order is mixed, this should just sum everything
    auto src = F::makeHist({"B", "A"}, {"R", "Q"}, {"U", "T"});
    auto dst = F::makeHist({"A"}, {"Q"}, {"T"});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    F::check_entries(*src, 100);
    F::check_entries(*dst, 200);
    F::check_bins_size(*src, 64);
    F::check_bins_size(*dst, 64);
    F::check_axis_labels(*dst, 0, std::vector<std::string>({"A", "B"}));
    F::check_axis_labels(*dst, 1, std::vector<std::string>({"Q", "R"}));
    F::check_axis_labels(*dst, 2, std::vector<std::string>({"T", "U"}));
    // overflow and underflow should not be summed, and in this case they should
    // be reset to 0
    std::vector<value_type> expect = {
            // z-underflow plane
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            // z bin 0 plane
            0,        0, 0, 0,
            0,  111+222,   221, 0,
            0,      212,   211, 0,
            0,        0,     0, 0,
            // z bin 1 plane
            0,        0, 0, 0,
            0,      122,   121, 0,
            0,      112,   111, 0,
            0,        0,     0, 0,
            // z-overflow plane
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
    };
    F::check(expect, *dst);
}
