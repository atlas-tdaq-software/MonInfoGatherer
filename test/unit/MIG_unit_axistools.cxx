//--------------------------------------------------------------------------
// Description:
//	Test suite case for AddAlgorithm.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_axistools
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/AxisTools.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
using namespace MIG::AxisTools;
using namespace MIG::AxisTools::detail;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

struct MergeAxisFixture {
    MergeAxis ma;
    oh::Axis::Kind expectedKind = oh::Axis::NoAxis;

    void checkNewAxis(int numBins, double lowerBound, double upperBound, double binWidth) {
        BOOST_CHECK_EQUAL(ma.kind, expectedKind);
        BOOST_CHECK_EQUAL(ma.numBins, numBins);
        BOOST_CHECK_EQUAL(ma.lowerBound, lowerBound);
        BOOST_CHECK_EQUAL(ma.upperBound, upperBound);
        BOOST_CHECK_EQUAL(ma.binWidth, binWidth);
    }
    void checkNewAxisWithtolerance(int numBins, double lowerBound, double upperBound, double binWidth) {
        BOOST_CHECK_EQUAL(ma.kind, expectedKind);
        BOOST_CHECK_EQUAL(ma.numBins, numBins);
        // compare edges using tolerances, allow 10% larger tolerance just in case
        double prec = binWidth * binEdgeTolerance * 1.1;
        if (prec == 0) {
            // can happen for variable kind
            prec = (upperBound - lowerBound) / numBins * binEdgeTolerance * 1.1;
        }
        BOOST_CHECK(std::abs(ma.lowerBound - lowerBound) < prec);
        BOOST_CHECK(std::abs(ma.upperBound - upperBound) < prec);
        BOOST_CHECK(std::abs(ma.binWidth - binWidth) < prec);
    }
    void checkAxis1(int mergeFactor, int firstBin, int lastBin, bool sameAsMerged) {
        BOOST_CHECK_EQUAL(ma.mergeFactor1, mergeFactor);
        BOOST_CHECK_EQUAL(ma.firstBin1, firstBin);
        BOOST_CHECK_EQUAL(ma.lastBin1, lastBin);
        BOOST_CHECK_EQUAL(ma.sameAs1(), sameAsMerged);
    }
    void checkAxis2(int mergeFactor, int firstBin, int lastBin, bool sameAsMerged) {
        BOOST_CHECK_EQUAL(ma.mergeFactor2, mergeFactor);
        BOOST_CHECK_EQUAL(ma.firstBin2, firstBin);
        BOOST_CHECK_EQUAL(ma.lastBin2, lastBin);
        BOOST_CHECK_EQUAL(ma.sameAs2(), sameAsMerged);
    }
};

struct DynamicLabelledAxisFixture {
    DynamicLabelledAxis dla;
    AxisLabelMap lblMap;

    DynamicLabelledAxisFixture() : check(*this) {}

    DynamicLabelledAxisFixture& numBins(int numBins) {
        BOOST_CHECK_EQUAL(dla.axis.nbins, numBins);
        return *this;
    }
    DynamicLabelledAxisFixture& srcIndices(const std::vector<int>& srcIndices) {
        BOOST_TEST(dla.srcIndices == srcIndices, boost::test_tools::per_element());
        return *this;
    }
    DynamicLabelledAxisFixture& dstIndices(const std::vector<int>& dstIndices) {
        BOOST_TEST(dla.dstIndices == dstIndices, boost::test_tools::per_element());
        return *this;
    }
    DynamicLabelledAxisFixture& resize(bool resize) {
        BOOST_TEST(dla.resize == resize);
        return *this;
    }
    DynamicLabelledAxisFixture& labels(const std::vector<std::string>& labels) {
        BOOST_TEST(dla.axis.labels == labels, boost::test_tools::per_element());
        return *this;
    }
    DynamicLabelledAxisFixture& noLabels() {
        BOOST_TEST(dla.axis.labels.empty());
        return *this;
    }
    DynamicLabelledAxisFixture& labelMap(const std::vector<std::string>& labels) {
        int index = 1;
        for (auto& lbl: labels) {
            BOOST_TEST(lblMap.index(lbl) == index);
            ++ index;
        }
        return *this;
    }

    DynamicLabelledAxisFixture& check; // Just a convenience name for (*this)
};

struct MakeBinDataMergeFixture {
    BinMergeDescriptor descr;

    enum CheckType {SRC, DST, BOTH};

    void checkDescr(bool resize, unsigned newAxesSize, bool dstBinMergeNotNull=false) {
        BOOST_CHECK_EQUAL(descr.resize, resize);
        if (dstBinMergeNotNull) {
            BOOST_CHECK(descr.dstBinMerge != nullptr);
        } else {
            BOOST_CHECK(descr.dstBinMerge == nullptr);
        }
        BOOST_CHECK(descr.srcBinMerge != nullptr);
        BOOST_CHECK_EQUAL(descr.newDstAxes.size(), newAxesSize);
    }
    template <typename T>
    void checkMergerType(CheckType check=SRC) {
        if (check == SRC or check == BOTH) {
            auto merger = descr.srcBinMerge.get();
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
        }
        if (check == DST or check == BOTH) {
            auto merger = descr.dstBinMerge.get();
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
        }
    }
    template <typename T, typename TSub>
    void checkMergerType2(CheckType check=SRC) {
        if (check == SRC or check == BOTH) {
            auto merger = descr.srcBinMerge.get();
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
            BOOST_CHECK(dynamic_cast<TSub*>(merger->subMerge()) != nullptr);
        }
        if (check == DST or check == BOTH) {
            auto merger = descr.dstBinMerge.get();
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
            BOOST_CHECK(dynamic_cast<TSub*>(merger->subMerge()) != nullptr);
        }
    }
    template <typename T, typename TSub, typename TSubSub>
    void checkMergerType3(CheckType check=SRC) {
        if (check == SRC or check == BOTH) {
            auto merger = descr.srcBinMerge.get();
            BOOST_REQUIRE(merger != nullptr and merger->subMerge() != nullptr);
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
            BOOST_CHECK(dynamic_cast<TSub*>(merger->subMerge()) != nullptr);
            BOOST_CHECK(dynamic_cast<TSubSub*>(merger->subMerge()->subMerge()) != nullptr);
        }
        if (check == DST or check == BOTH) {
            auto merger = descr.dstBinMerge.get();
            BOOST_REQUIRE(merger != nullptr and merger->subMerge() != nullptr);
            BOOST_CHECK(dynamic_cast<T*>(merger) != nullptr);
            BOOST_CHECK(dynamic_cast<TSub*>(merger->subMerge()) != nullptr);
            BOOST_CHECK(dynamic_cast<TSubSub*>(merger->subMerge()->subMerge()) != nullptr);
        }
    }
    void checkNewBins(const std::vector<unsigned>& numBins) {
        BOOST_REQUIRE_EQUAL(descr.newDstAxes.size(), numBins.size());
        for (unsigned i = 0; i != numBins.size(); ++i) {
            BOOST_CHECK_EQUAL(descr.newDstAxes[i].nbins, numBins[i]);
        }
    }
    void checkLabels(const oh::Axis& axis, const std::vector<std::string>& labels, const std::vector<int>& indices) {

        BOOST_REQUIRE(labels.size() == indices.size());
        BOOST_REQUIRE(axis.labels.size() == axis.indices.size());

        std::vector<std::string> newAxisLabels, expected;
        for (unsigned i = 0; i != labels.size(); ++i) {
            if (expected.size() <= unsigned(indices[i])) {
                expected.resize(indices[i]+1);
            }
            expected[indices[i]] = labels[i];
        }
        for (unsigned i = 0; i != axis.labels.size(); ++i) {
            if (newAxisLabels.size() <= unsigned(axis.indices[i])) {
                newAxisLabels.resize(axis.indices[i]+1);
            }
            newAxisLabels[axis.indices[i]] = axis.labels[i];
        }
        BOOST_TEST(newAxisLabels == expected, boost::test_tools::per_element());
    }
};

}

BOOST_AUTO_TEST_CASE(test_sameBinning_kind)
{
    // test for sameBinning() method with different axes kinds
    auto axis1 = makeAxis(5, "X", {0., 1.});
    auto axis2 = makeVarAxis(5, "X", {0., 1., 2., 3., 4., 5.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);
}

BOOST_AUTO_TEST_CASE(test_sameBinning_fixed)
{
    // test for sameBinning() method with fixed binning case

    // compare to itself should always be OK
    auto axis1 = makeAxis(3, "X", {0., 10.});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis1));

    // Axis title does not matter, small difference in edges too
    axis1 = makeAxis(5, "X", {0., 10.});
    auto axis2 = makeAxis(5, "xxx", {0., 10.});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    // Small difference in edges is OK
    axis1 = makeAxis(5, "X", {0., 10.});
    axis2 = makeAxis(5, "X", {-binEdgeTolerance/2, 10.*(1+binEdgeTolerance/10)});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    // These should be different
    axis1 = makeAxis(5, "X", {0., 10.});
    axis2 = makeAxis(6, "X", {0., 10.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeAxis(5, "X", {0., 10.});
    axis2 = makeAxis(5, "X", {0., 1.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeAxis(5, "X", {0., 10.});
    axis2 = makeAxis(5, "X", {0.0001, 10.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    // precision is relative to bin width
    axis1 = makeAxis(5, "X", {0., .0001});
    axis2 = makeAxis(5, "X", {0., .0001*(1+binEdgeTolerance/10)});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    axis1 = makeAxis(5, "X", {0., .0001});
    axis2 = makeAxis(5, "X", {0., .0001*(1+binEdgeTolerance*2)});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);
}

BOOST_AUTO_TEST_CASE(test_sameBinning_variable)
{
    // test for sameBinning() method with variable binning case

    // compare to itself should always be OK
    auto axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis1));

    // Axis title does not matter
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    auto axis2 = makeVarAxis(3, "yyy", {0., 1., 2., 3.});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    // Small difference in edges is OK
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis2 = makeVarAxis(3, "X", {binEdgeTolerance/2, 1-binEdgeTolerance/3, 2., 3+binEdgeTolerance/2});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    // zero bins is OK, even with different bin edge
    axis1 = makeVarAxis(0, "X", {0.});
    axis2 = makeVarAxis(0, "X", {3.});
    BOOST_CHECK_NO_THROW(sameBinning(axis1, axis2));

    // These should be different
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis2 = makeVarAxis(4, "X", {0., 1., 2., 3., 4.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis2 = makeVarAxis(3, "X", {0., .1, .2, .3});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis1 = makeVarAxis(3, "X", {0., 1.+binEdgeTolerance*2, 2., 3.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis1 = makeVarAxis(3, "X", {binEdgeTolerance*2, 1., 2., 3.});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.+binEdgeTolerance*2});
    BOOST_CHECK_THROW(sameBinning(axis1, axis2), IncompatibleAxisBinning);
}

BOOST_FIXTURE_TEST_CASE(test_MergeAxis_default, MergeAxisFixture)
{
    // test for MergeAxis class default constructor
    checkNewAxis(-1, 0., 0., 0.);
    checkAxis1(1, 0, 1, false);
    checkAxis2(1, 0, 1, false);

    // can assign too
    MergeAxis ma1;
    ma = ma1;
    checkNewAxis(-1, 0., 0., 0.);
    checkAxis1(1, 0, 1, false);
    checkAxis2(1, 0, 1, false);
}

BOOST_AUTO_TEST_CASE(test_MergeAxis_kind)
{
    // test for MergeAxis class with different axes kinds
    auto axis1 = makeAxis(5, "X", {0., 1.});
    auto axis2 = makeVarAxis(5, "X", {0., 1., 2., 3., 4., 5.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);
}

BOOST_FIXTURE_TEST_CASE(test_MergeAxis_fixed_same_binning, MergeAxisFixture)
{
    // test for MergeAxis class with fixed binning case,
    // bining is supposed to be ~identical

    expectedKind = oh::Axis::Fixed;

    // compare to itself should always be OK
    auto axis1 = makeAxis(3, "X", {0., 10.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis1));
    checkNewAxis(3, 0., 30., 10.);
    checkAxis1(1, 1, 4, true);
    checkAxis2(1, 1, 4, true);

    // Axis title does not matter
    axis1 = makeAxis(5, "X", {0., 10.});
    auto axis2 = makeAxis(5, "xxx", {0., 10.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(5, 0., 50., 10.);

    // Small difference in edges is OK
    axis1 = makeAxis(5, "X", {0., 10.});
    axis2 = makeAxis(5, "X", {0. + 10*binEdgeTolerance/2, 10.*(1 + binEdgeTolerance/10)});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxisWithtolerance(5, 0., 50., 10.);

    // These should be different
    axis1 = makeAxis(5, "X", {0., 1.});
    axis2 = makeAxis(5, "X", {0., 1+binEdgeTolerance*2});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeAxis(5, "X", {0., 1.});
    axis2 = makeAxis(5, "X", {binEdgeTolerance*2, 1.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);

    // precision is relative to bin width
    axis1 = makeAxis(5, "X", {0., 100.});
    axis2 = makeAxis(5, "X", {0., 100.*(1 + binEdgeTolerance/10)});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxisWithtolerance(5, 0., 500., 100.);

    axis1 = makeAxis(5, "X", {0., 100.});
    axis2 = makeAxis(5, "X", {0., 100.*(1 + binEdgeTolerance*2)});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);
}

BOOST_FIXTURE_TEST_CASE(test_MergeAxis_fixed_same_binwidth, MergeAxisFixture)
{
    // test for MergeAxis class with fixed binning case,
    // but different binning with the same bin width

    expectedKind = oh::Axis::Fixed;

    // same lower edge
    auto axis1 = makeAxis(5, "X", {0., 1.});
    auto axis2 = makeAxis(10, "X", {0., 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis1(1, 1, 6, false);
    checkAxis2(1, 1, 11, true);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis2(1, 1, 6, false);
    checkAxis1(1, 1, 11, true);

    // same upper edge
    axis1 = makeAxis(5, "X", {5., 1.});
    axis2 = makeAxis(10, "X", {0., 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis1(1, 6, 11, false);
    checkAxis2(1, 1, 11, true);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis2(1, 6, 11, false);
    checkAxis1(1, 1, 11, true);

    // one contained in another
    axis1 = makeAxis(5, "X", {3., 1.});
    axis2 = makeAxis(10, "X", {0., 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis1(1, 4, 9, false);
    checkAxis2(1, 1, 11, true);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(10, 0., 10., 1.);
    checkAxis2(1, 4, 9, false);
    checkAxis1(1, 1, 11, true);

    // disjoint
    axis1 = makeAxis(5, "X", {0., 1.});
    axis2 = makeAxis(10, "X", {10., 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(20, 0., 20., 1.);
    checkAxis1(1, 1, 6, false);
    checkAxis2(1, 11, 21, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(20, 0., 20., 1.);
    checkAxis2(1, 1, 6, false);
    checkAxis1(1, 11, 21, false);

    // overlapping, tiny difference in widths/edges
    axis1 = makeAxis(10, "X", {0., 1.+binEdgeTolerance/20});
    axis2 = makeAxis(10, "X", {5.+binEdgeTolerance/2, 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxisWithtolerance(15, 0., 15., 1.);
    checkAxis1(1, 1, 11, false);
    checkAxis2(1, 6, 16, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxisWithtolerance(15, 0., 15., 1.);
    checkAxis2(1, 1, 11, false);
    checkAxis1(1, 6, 16, false);
}

BOOST_FIXTURE_TEST_CASE(test_MergeAxis_fixed_different_binwidth, MergeAxisFixture)
{
    // test for MergeAxis class with fixed binning case,
    // but different binning and different bin width

    expectedKind = oh::Axis::Fixed;

    // same lower edge and upper edge
    auto axis1 = makeAxis(5, "X", {0., 2.});
    auto axis2 = makeAxis(10, "X", {0., 1.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(5, 0., 10., 2.);
    checkAxis1(1, 1, 6, true);
    checkAxis2(2, 1, 6, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(5, 0., 10., 2.);
    checkAxis2(1, 1, 6, true);
    checkAxis1(2, 1, 6, false);

    // non-compatible bin widths
    axis1 = makeAxis(5, "X", {0., 2.});
    axis2 = makeAxis(3, "X", {0., 3.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);

    // non-aligned bounds for finer-grained bins
    axis1 = makeAxis(9, "X", {0., 1.});
    axis2 = makeAxis(3, "X", {3., 2.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);
    axis1 = makeAxis(9, "X", {0., 1.});
    axis2 = makeAxis(3, "X", {0., 2.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);
    axis1 = makeAxis(10, "X", {0., 1.});
    axis2 = makeAxis(3, "X", {3., 2.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);

    // partially overlapping but compatible
    axis1 = makeAxis(10, "X", {0., 1.});
    axis2 = makeAxis(10, "X", {4., 2.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(12, 0., 24., 2.);
    checkAxis1(2, 1, 6, false);
    checkAxis2(1, 3, 13, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(12, 0., 24., 2.);
    checkAxis2(2, 1, 6, false);
    checkAxis1(1, 3, 13, false);

    // disjoint
    axis1 = makeAxis(10, "X", {0., 1.});
    axis2 = makeAxis(4, "X", {12., 2.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxis(10, 0., 20., 2.);
    checkAxis1(2, 1, 6, false);
    checkAxis2(1, 7, 11, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxis(10, 0., 20., 2.);
    checkAxis2(2, 1, 6, false);
    checkAxis1(1, 7, 11, false);

    // partially overlapping with tiny differences
    axis1 = makeAxis(10, "X", {binEdgeTolerance/3, 1.});
    axis2 = makeAxis(10, "X", {4.-binEdgeTolerance/3, 2.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxisWithtolerance(12, 0., 24., 2.);
    checkAxis1(2, 1, 6, false);
    checkAxis2(1, 3, 13, false);
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis2, axis1));
    checkNewAxisWithtolerance(12, 0., 24., 2.);
    checkAxis2(2, 1, 6, false);
    checkAxis1(1, 3, 13, false);
}

BOOST_FIXTURE_TEST_CASE(test_MergeAxis_variable, MergeAxisFixture)
{
    // test for MergeAxis class with variable binning case,
    // only works for identical binning

    expectedKind = oh::Axis::Variable;

    // compare to itself should always be OK
    auto axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis1));
    checkNewAxis(3, 0., 3., 0.);
    checkAxis1(1, 1, 4, true);
    checkAxis2(1, 1, 4, true);

    // Tiny differences in edges are OK
    axis1 = makeVarAxis(3, "X", {0.+binEdgeTolerance/2, 1., 2.-binEdgeTolerance/2, 3.-binEdgeTolerance/2});
    auto axis2 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    BOOST_REQUIRE_NO_THROW(ma = MergeAxis(axis1, axis2));
    checkNewAxisWithtolerance(3, 0., 3., 0.);
    checkAxis1(1, 1, 4, true);
    checkAxis2(1, 1, 4, true);

    // different bin count
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis2 = makeVarAxis(4, "X", {0., 1., 2., 3., 4.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);

    // different edges
    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.});
    axis2 = makeVarAxis(3, "X", {1., 2., 3., 4.});
    BOOST_CHECK_THROW(MergeAxis(axis1, axis2), IncompatibleAxisBinning);
 }

BOOST_AUTO_TEST_CASE(test_labelType)
{
    // test for labelType() method
    LabelType lblType = LabelType::NoLabels;

    oh::Axis axis1, axis2;

    // NoLabel type

    axis1 = makeAxis(3, "X", {0., 10.});
    axis2 = makeAxis(3, "X", {0., 10.});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::NoLabels);

    axis1 = makeVarAxis(3, "X", {0., 10.});
    axis2 = makeVarAxis(3, "X", {0., 10.});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::NoLabels);

    // StaticLabel type

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::StaticLabels);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::StaticLabels);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    axis2 = makeAxis(3, "X", {0., 10.}, {"B", "A"}, {3, 1});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::StaticLabels);

    // DynamicLabel type

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"E", "F", "G"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    axis1 = makeVarAxis(3, "X", {0., 1., 2., 3.}, {"A", "B", "C"});
    axis2 = makeVarAxis(3, "X", {0., 1., 2., 3.}, {"E", "F", "G"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    axis1 = makeAxis(3, "X", {0., 10.});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B", "C"});
    axis2 = makeAxis(3, "X", {0., 10.});
    BOOST_REQUIRE_NO_THROW(lblType = labelType(axis1, axis2));
    BOOST_CHECK(lblType == LabelType::DynamicLabels);

    // Invalid mix

    axis1 = makeVarAxis(3, "X", {0., 10.});
    axis2 = makeAxis(3, "X", {0., 10.});
    BOOST_CHECK_THROW(labelType(axis1, axis2), IncompatibleAxisBinning);

    // non-labelled vs fixed
    axis1 = makeAxis(3, "X", {0., 10.});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    BOOST_CHECK_THROW(labelType(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    axis2 = makeAxis(3, "X", {0., 10.});
    BOOST_CHECK_THROW(labelType(axis1, axis2), IncompatibleAxisBinning);

    // внтфьшс vs fixed
    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    BOOST_CHECK_THROW(labelType(axis1, axis2), IncompatibleAxisBinning);

    axis1 = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    axis2 = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    BOOST_CHECK_THROW(labelType(axis1, axis2), IncompatibleAxisBinning);

}

// =========================== Tests for DynamicLabelledAxis =========================

BOOST_FIXTURE_TEST_CASE(test_DynamicLabelledAxis_default, DynamicLabelledAxisFixture)
{
    // test for DynamicLabelledAxis default constructor
    check.numBins(-1).srcIndices({}).dstIndices({}).resize(false);

    // and assignment too
    DynamicLabelledAxis remap;
    dla = remap;
    check.numBins(-1).srcIndices({}).dstIndices({}).resize(false);
}


BOOST_FIXTURE_TEST_CASE(test_DynamicLabelledAxis_errors, DynamicLabelledAxisFixture)
{
    // test for DynamicLabelledAxis with different error conditions

    // different kind of axes
    auto srcAxis = makeAxis(3, "X", {0., 1.});
    auto dstAxis = makeVarAxis(3, "X", {0., 1., 2., 3, 4,});
    AxisLabelMap lblMap(dstAxis);
    BOOST_CHECK_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap), IncompatibleAxisBinning);

    // non-labelled axes
    srcAxis = makeAxis(3, "X", {0., 1.});
    dstAxis = makeAxis(3, "X", {0., 1.});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_CHECK_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap), IncompatibleAxisBinning);

    // statically labelled axes
    srcAxis = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    dstAxis = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_CHECK_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap), IncompatibleAxisBinning);

    srcAxis = makeAxis(3, "X", {0., 10.});
    dstAxis = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_CHECK_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap), IncompatibleAxisBinning);

    srcAxis = makeAxis(3, "X", {0., 10.}, {"A", "B"}, {2, 3});
    dstAxis = makeAxis(3, "X", {0., 10.});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_CHECK_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap), IncompatibleAxisBinning);
}

BOOST_FIXTURE_TEST_CASE(test_DynamicLabelledAxis, DynamicLabelledAxisFixture)
{
    oh::Axis srcAxis, dstAxis;

    BOOST_TEST_MESSAGE("same labels");
    srcAxis = makeAxis(5, "X", {0., 10.}, {"A", "B"});
    dstAxis = makeAxis(5, "X", {0., 10.}, {"A", "B"});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_REQUIRE_NO_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap));
    check
        .numBins(5)
        .srcIndices({0, 1, 2})
        .dstIndices({0, 1, 2})
        .resize(false)
        .labels({"A", "B"})
        .labelMap({"A", "B"});

    BOOST_TEST_MESSAGE("different labels");
    srcAxis = makeAxis(5, "X", {0., 10.}, {"A", "B"});
    dstAxis = makeAxis(5, "X", {0., 10.}, {"C", "D"});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_REQUIRE_NO_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap));
    check
        .numBins(4)
        .srcIndices({0, 3, 4})
        .dstIndices({0, 1, 2})
        .resize(true)
        .labels({"C", "D", "A", "B"})
        .labelMap({"C", "D", "A", "B"});

    BOOST_TEST_MESSAGE("different labels, resizing");
    srcAxis = makeAxis(2, "X", {0., 10.}, {"A", "B"});
    dstAxis = makeAxis(2, "X", {0., 10.}, {"C", "A"});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_REQUIRE_NO_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap));
    check
        .numBins(3)
        .srcIndices({0, 2, 3})
        .dstIndices({0, 1, 2})
        .resize(true)
        .labels({"C", "A", "B"})
        .labelMap({"C", "A", "B"});

    BOOST_TEST_MESSAGE("non-labelled src");
    srcAxis = makeAxis(1, "X", {0., 10.});
    dstAxis = makeAxis(2, "X", {0., 10.}, {"A", "B"});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_REQUIRE_NO_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap));
    check
        .numBins(2)
        .srcIndices({})
        .dstIndices({0, 1, 2})
        .resize(false)
        .labels({"A", "B"})
        .labelMap({"A", "B"});

    BOOST_TEST_MESSAGE("non-labelled dst");
    srcAxis = makeAxis(3, "X", {0., 10.}, {"A", "B"});
    dstAxis = makeAxis(1, "X", {0., 10.});
    lblMap = AxisLabelMap(dstAxis);
    BOOST_REQUIRE_NO_THROW(dla = DynamicLabelledAxis(srcAxis, dstAxis, lblMap));
    check
        .numBins(2)
        .srcIndices({0, 1, 2})
        .dstIndices({})
        .resize(true)
        .labels({"A", "B"})
        .labelMap({"A", "B"});
}

// ========================= tests for makeBinDataMerge ======================

BOOST_FIXTURE_TEST_CASE(test_makeBinDataMerge_static_labels, MakeBinDataMergeFixture)
{
    // test for LabelType == StaticLabels
    std::vector<oh::Axis> srcAxes;
    std::vector<oh::Axis> dstAxes;

    BOOST_TEST_MESSAGE("check fully labelled 1-D axes (static)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("check patially labelled 1-D axes (static or dynamic)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("check patially labelled 1-D axes (static)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"}, {1, 3})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("check patially labelled 1-D axes (static)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"}, {2, 3})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"B", "A"}, {3, 2})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("2D case fully labelled");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("2D case non-fully labelled");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y"}, {1, 3})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y"}, {1, 3})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("3D case identical labelling");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"}),
               makeAxis(3, "Z", {0., 10.}, {"@", "#", "$"}, {3, 2, 1})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"}),
               makeAxis(3, "Z", {0., 10.}, {"@", "#", "$"}, {3, 2, 1})};
    descr = makeBinDataMerge(3, srcAxes, dstAxes);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();
}

BOOST_FIXTURE_TEST_CASE(test_makeBinDataMerge_dynamic_labels, MakeBinDataMergeFixture)
{
    // test for LabelType == DynamicLabels
    std::vector<oh::Axis> srcAxes;
    std::vector<oh::Axis> dstAxes;
    LabelMap lblMap;

    BOOST_TEST_MESSAGE("check dynamically labelled disjoint labels (fully labelled)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"E", "F", "G"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkNewBins({6});
    // dst labels go first
    checkLabels(descr.newDstAxes[0], {"A", "B", "C", "E", "F", "G"}, {4, 5, 6, 1, 2, 3});
    checkMergerType<BinDataMergeMapX>();

    BOOST_TEST_MESSAGE("check dynamically labelled disjoint labels (not fully labelled)");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"B"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkNewBins({2});
    checkLabels(descr.newDstAxes[0], {"A", "B"}, {2, 1});
    checkMergerType<BinDataMergeMapX>();

    BOOST_TEST_MESSAGE("src is a subset of dst labels");
    srcAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeMapX>();

    BOOST_TEST_MESSAGE("dst is a subset of src labels, resize needed");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    dstAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkNewBins({3});
    checkLabels(descr.newDstAxes[0], {"A", "B", "C"}, {1, 2, 3});
    checkMergerType<BinDataMergeMapX>(BOTH);

    BOOST_TEST_MESSAGE("2D case");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"})};
    dstAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}),
               makeAxis(4, "Y", {0., 10.}, {"x", "y"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkNewBins({3, 3});
    checkLabels(descr.newDstAxes[0], {"A", "B", "C"}, {1, 2, 3});
    checkLabels(descr.newDstAxes[1], {"x", "y", "z"}, {1, 2, 3});
    checkMergerType2<BinDataMergeMap, BinDataMergeMapX>(BOTH);

    BOOST_TEST_MESSAGE("different bin size, still should be dynamic labels, resulting binning probably comes from dst");
    srcAxes = {makeAxis(4, "X", {0., 5.}, {"A", "B"})};
    dstAxes = {makeAxis(4, "X", {0., 10.}, {"A", "B", "C", "D"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeMapX>();
}

BOOST_FIXTURE_TEST_CASE(test_makeBinDataMerge_direct, MakeBinDataMergeFixture)
{
    // exact same binning for all axes, no labels

    LabelMap lblMap;
    std::vector<oh::Axis> srcAxes;
    std::vector<oh::Axis> dstAxes;

    BOOST_TEST_MESSAGE("1D, same binning");
    srcAxes = {makeAxis(3, "X", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("2D, same bining");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("3D, same bining");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.})};
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();
}

BOOST_FIXTURE_TEST_CASE(test_makeBinDataMerge_many, MakeBinDataMergeFixture)
{
    // many-to-one mapping for bins

    LabelMap lblMap;
    std::vector<oh::Axis> srcAxes;
    std::vector<oh::Axis> dstAxes;

    BOOST_TEST_MESSAGE("1D, same bin width, dst is wider");
    srcAxes = {makeAxis(3, "X", {10., 10.})};
    dstAxes = {makeAxis(5, "X", {0., 10.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeManyX>();

    BOOST_TEST_MESSAGE("1D, same bin width, src is wider");
    srcAxes = {makeAxis(5, "X", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {10., 10.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkMergerType<BinDataMergeManyX>(BOTH);
    checkNewBins({5});

    BOOST_TEST_MESSAGE("1D, same bin width, disjoint");
    srcAxes = {makeAxis(3, "X", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {40., 10.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkMergerType<BinDataMergeManyX>(BOTH);
    checkNewBins({7});

    BOOST_TEST_MESSAGE("1D, different bin width");
    srcAxes = {makeAxis(6, "X", {0., 5.})};
    dstAxes = {makeAxis(3, "X", {0., 10.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeManyX>();

    BOOST_TEST_MESSAGE("1D, different bin width");
    srcAxes = {makeAxis(3, "X", {0., 10.})};
    dstAxes = {makeAxis(6, "X", {0., 5.})};
    descr = makeBinDataMerge(1, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 1, true);
    checkMergerType<BinDataMergeManyX>(BOTH);
    checkNewBins({3});

    BOOST_TEST_MESSAGE("1D, incomparable widths");
    srcAxes = {makeAxis(4, "X", {0., 3.})};
    dstAxes = {makeAxis(6, "X", {0., 2.})};
    BOOST_CHECK_THROW(makeBinDataMerge(1, srcAxes, dstAxes, &lblMap), IncompatibleAxisBinning);

    BOOST_TEST_MESSAGE("2D, same bin width, dst is wider");
    srcAxes = {makeAxis(3, "X", {10., 10.}), makeAxis(3, "Y", {10., 10.})};
    dstAxes = {makeAxis(5, "X", {0., 10.}), makeAxis(5, "Y", {0., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType2<BinDataMergeMany,BinDataMergeManyX>();

    BOOST_TEST_MESSAGE("2D, same bin width, src is wider");
    srcAxes = {makeAxis(5, "X", {0., 10.}), makeAxis(5, "Y", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {10., 10.}), makeAxis(3, "Y", {10., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkMergerType2<BinDataMergeMany, BinDataMergeManyX>(BOTH);
    checkNewBins({5, 5});

    BOOST_TEST_MESSAGE("3D, same bin width, disjoint, extra axis");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.}), makeAxis(3, "Z", {0., 10.}), makeAxis(3, "t", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {40., 10.}), makeAxis(3, "Y", {40., 10.}), makeAxis(3, "Z", {40., 10.}), makeAxis(3, "t", {40., 10.})};
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 4, true);
    checkMergerType3<BinDataMergeMany, BinDataMergeMany, BinDataMergeManyX>(BOTH);
    checkNewBins({7, 7, 7, 3});
}

BOOST_FIXTURE_TEST_CASE(test_makeBinDataMerge_mixed, MakeBinDataMergeFixture)
{
    // axes of different types

    LabelMap lblMap;
    std::vector<oh::Axis> srcAxes;
    std::vector<oh::Axis> dstAxes;

    BOOST_TEST_MESSAGE("2D case with dynamic");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"})};
    dstAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}),
               makeAxis(3, "Y", {0., 10.}, {"x", "y", "z"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkMergerType2<BinDataMergeDirect, BinDataMergeMapX>(BOTH);
    checkNewBins({3, 3});


    BOOST_TEST_MESSAGE("X is labelled, Y is not");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("Y is labelled, X is not");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "X", {0., 10.}, {"A", "B", "C"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType<BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("X is 1-to-1, Y is N-to-1");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(6, "Y", {0., 5.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType2<BinDataMergeMany, BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("X is 1-to-1, Y is N-to-1, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(6, "Y", {0., 5.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkMergerType2<BinDataMergeMany, BinDataMergeDirectX>(BOTH);
    checkNewBins({3, 3});

    BOOST_TEST_MESSAGE("X is N-to-1, Y is 1-to-1");
    srcAxes = {makeAxis(6, "X", {0., 5.}), makeAxis(3, "Y", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType2<BinDataMergeDirect, BinDataMergeManyX>();

    BOOST_TEST_MESSAGE("X is N-to-1, Y is 1-to-1, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.})};
    dstAxes = {makeAxis(4, "X", {0., 5.}), makeAxis(3, "Y", {0., 10.})};
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkMergerType2<BinDataMergeDirect, BinDataMergeManyX>(BOTH);
    checkNewBins({3, 3});

    BOOST_TEST_MESSAGE("X is labelled, Y is N-to-1");
    srcAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}), makeAxis(4, "Y", {0., 5.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType2<BinDataMergeMany, BinDataMergeMapX>();

    BOOST_TEST_MESSAGE("X is labelled, Y is N-to-1, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.})};
    dstAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}), makeAxis(4, "Y", {0., 5.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(2, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 2, true);
    checkMergerType2<BinDataMergeMany, BinDataMergeMapX>(BOTH);
    checkNewBins({3, 3});

    BOOST_TEST_MESSAGE("3D mix #1, no resize");
    srcAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}), makeAxis(4, "Y", {0., 5.}), makeAxis(3, "Z", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.}), makeAxis(3, "Z", {0., 10.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType3<BinDataMergeDirect, BinDataMergeMany, BinDataMergeMapX>();

    BOOST_TEST_MESSAGE("3D mix #2, no resize");
    srcAxes = {makeAxis(4, "X", {0., 5.}), makeAxis(3, "Y", {0., 10.}), makeAxis(2, "Z", {0., 10.}, {"A", "B"})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.}), makeAxis(3, "Z", {0., 10.}, {"A", "B", "C"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType3<BinDataMergeMap, BinDataMergeDirect, BinDataMergeManyX>();

    BOOST_TEST_MESSAGE("3D mix #3, no resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(2, "Y", {0., 10.}, {"A", "B"}), makeAxis(4, "Z", {0., 5.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Z", {0., 10.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(false, 0);
    checkMergerType3<BinDataMergeMany, BinDataMergeMap, BinDataMergeDirectX>();

    BOOST_TEST_MESSAGE("3D mix #1, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Y", {0., 10.}), makeAxis(3, "Z", {0., 10.})};
    dstAxes = {makeAxis(2, "X", {0., 10.}, {"A", "B"}), makeAxis(4, "Y", {0., 5.}), makeAxis(3, "Z", {0., 10.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 3, true);
    checkMergerType3<BinDataMergeDirect, BinDataMergeMany, BinDataMergeMapX>(BOTH);
    checkNewBins({3, 3, 3});

    BOOST_TEST_MESSAGE("3D mix #2, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.}), makeAxis(3, "Z", {0., 10.}, {"A", "B", "C"})};
    dstAxes = {makeAxis(4, "X", {0., 5.}), makeAxis(3, "Y", {0., 10.}), makeAxis(2, "Z", {0., 10.}, {"A", "B"})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 3, true);
    checkMergerType3<BinDataMergeMap, BinDataMergeDirect, BinDataMergeManyX>(BOTH);
    checkNewBins({3, 3, 3});

    BOOST_TEST_MESSAGE("3D mix #3, with resize");
    srcAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(3, "Y", {0., 10.}, {"A", "B", "C"}), makeAxis(3, "Z", {0., 10.})};
    dstAxes = {makeAxis(3, "X", {0., 10.}), makeAxis(2, "Y", {0., 10.}, {"A", "B"}), makeAxis(4, "Z", {0., 5.})};
    lblMap = LabelMap(dstAxes);
    descr = makeBinDataMerge(3, srcAxes, dstAxes, &lblMap);
    checkDescr(true, 3, true);
    checkMergerType3<BinDataMergeMany, BinDataMergeMap, BinDataMergeDirectX>(BOTH);
    checkNewBins({3, 3, 3});
}
