#ifndef MIG_TEST_CLOCK_H
#define MIG_TEST_CLOCK_H

// Implementation of Clock for use in unit tests

#include <iostream>

#include "MonInfoGatherer/Clock.h"


namespace MIG {
namespace test {

/**
 * Default implementation of Clock interface for 1production case.
 *
 * This implementation uses `std::chrono::steady_clock` to obtain clock value.
 * This is the clock to be used in production running.
 */
class TestClock : public Clock {
public:

    TestClock() {
        system_sync();
    }

    ~TestClock() = default;

    // implementation of Clock::now()
    time_point now() const override { return m_now; }

    void system_sync() {
        // set to current system time but drop fractional seconds
        // to make it more readable
        auto now = system_now();
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
        m_now = time_point(seconds);
    }

    void set(time_point t) { m_now = t; }

    TestClock& operator+=(duration d) { m_now += d; return *this; }
    TestClock& operator-=(duration d) { m_now -= d; return *this; }

private:

    // access to system clock current time
    static time_point system_now() { return clock_type::now(); }

    time_point m_now;

};

inline
std::ostream&
operator<<(std::ostream& out, const TestClock& clock) {
    return out << std::chrono::duration_cast<std::chrono::milliseconds>(
        clock.now().time_since_epoch()).count() << " msec";
}

template <typename T=TestClock>
class ClockFriendHack {
public:

    /**
     * Replace MIG Clock singleton with TestClock instance.
     *
     * Returns pointer to new object, object is valid until next call to
     * this method.
     */
    static TestClock* installTestClock() {
        TestClock* clock = new TestClock;
        Clock::s_instance.reset(clock);
        return clock;
    }
};

/**
 * Test fixture mixin class which installs test clock
 * and provides type access to that global instance.
 */
template <typename Fixture>
struct TClockFixture : public Fixture {

    TClockFixture() : tclock(*ClockFriendHack<>::installTestClock())
    {
        tclock.system_sync();
    }

    TestClock& tclock;
};


}} // namespace MIG::test

#endif // MIG_TEST_CLOCK_H
