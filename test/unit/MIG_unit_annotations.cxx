//--------------------------------------------------------------------------
// Description:
//      Test suite for annotation merging code
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <vector>
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "../../src/containers/Annotations.h"

#define BOOST_TEST_MODULE MIG_unit_annotations
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace MIG;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

ISInfo::Annotations make_ann(const std::vector<std::string>& strings)
{
    ISInfo::Annotations ann;
    for (unsigned i = 1; i < strings.size(); i += 2) {
        ann[strings[i-1]] = ISAnnotation(strings[i]);
    }
    return ann;
}

}

// needed for Boost to print in tt:per_element
std::ostream&
operator<<(std::ostream& out, std::pair<std::string, ISAnnotation> const& pair) {
    return out << '(' << pair.first << ", " << pair.second.data() << ')';
}


// ==============================================================

BOOST_AUTO_TEST_CASE(make_instance)
{
    Annotations ann;
    BOOST_TEST(ann.counters().empty());
}

BOOST_AUTO_TEST_CASE(merge_aligned)
{
    // test for adding aligned annotations with no non-gatherer stuff in them
    Annotations ann;

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({1}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 1);

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({2}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "10"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({12}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    ann.reset();
    BOOST_TEST(ann.counters().empty());
    BOOST_TEST(ann.contrib_count() == 0);

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "100", "mig.lvl1", "10", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({100, 10, 1}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 1);

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "300", "mig.lvl1", "20", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({400, 30, 2}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);
}

BOOST_AUTO_TEST_CASE(merge_aligned2)
{
    // test for adding aligned annotations with non-gatherer stuff in them
    Annotations ann;

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1", "FINAL", "FINAL"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"Finita", "", "providers", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({2}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"Anno", "tation", "providers", "10", "20", "mig.lvl1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({12}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"FINAL", "FINAL", "providers", "100", "mig.lvl1", "10", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({100, 10, 1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "300", "FINAL", "FINAL", "mig.lvl1", "20", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({400, 30, 2}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);
}

BOOST_AUTO_TEST_CASE(merge_misaligned)
{
    // test for adding misaligned annotations with no non-gatherer stuff in them
    Annotations ann;

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1", "mig.lvl1", "20"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({2, 20}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "10"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({12, 20}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "100", "mig.lvl1", "10", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({100, 10, 1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "300", "mig.lvl1", "20"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({400, 30, 1}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);
}

BOOST_AUTO_TEST_CASE(merge_misaligned2)
{
    // test for adding misaligned annotations with some non-gatherer stuff in them
    Annotations ann;

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"FINAL", "FINAL", "providers", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "1", "FINAL", "FINAL", "mig.lvl1", "20"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({2, 20}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "10", "FINAL", "FINAL"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({12, 20}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"FINAL", "FINAL", "providers", "100", "mig.lvl1", "10", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({100, 10, 1}), tt::per_element());

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "300", "mig.lvl1", "20", "FINAL", "FINAL"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({400, 30, 1}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);
}

BOOST_AUTO_TEST_CASE(merge_misaligned3)
{
    // test for adding misaligned annotations with some empty annotations
    Annotations ann;

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>(), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    {
        ISInfo::Annotations updated;
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "3",
        }), tt::per_element());
    }

    ann.reset();

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "24"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({24}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 1);

    {
        ISInfo::Annotations updated;
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "24",
            "mig.lvl1", "1",
        }), tt::per_element());
    }

    ann.reset();

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "23"})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({24}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 2);

    {
        ISInfo::Annotations updated;
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "24",
            "mig.lvl1", "2",
        }), tt::per_element());
    }

    ann.reset();

    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "22"})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({24}), tt::per_element());
    BOOST_TEST(ann.contrib_count() == 3);

    {
        ISInfo::Annotations updated;
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "24",
            "mig.lvl1", "3",
        }), tt::per_element());
    }
}

BOOST_AUTO_TEST_CASE(update)
{
    // test for update() method
    Annotations ann;
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "100", "mig.lvl1", "10", "mig.lvl2", "1"})));
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "300", "mig.lvl1", "20", "mig.lvl2", "1"})));
    BOOST_TEST(ann.counters() == std::vector<unsigned>({400, 30, 2}), tt::per_element());

    {
        ISInfo::Annotations updated;
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "400",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2"
        }), tt::per_element());
    }

    {
        ISInfo::Annotations updated(::make_ann({"providers", "x"}));
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "400",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2"
        }), tt::per_element());
    }
    {
        ISInfo::Annotations updated(::make_ann({"providers", "x", "mig.lvl1", "y", "mig.lvl2", "z"}));
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "400",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2"
        }), tt::per_element());
    }
    {
        ISInfo::Annotations updated(::make_ann({"providers", "x", "mig.lvl1", "y", "mig.lvl2", "z", "mig.lvl3", "1"}));
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "400",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2"
        }), tt::per_element());
    }
    {
        // extra stuff is replaced with empty strings
        ISInfo::Annotations updated(::make_ann({"providers", "x", "mig.lvl1", "y", "mig.lvl2", "z", "mig.lvl3", "1", "mig.lvl4", "foo"}));
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "providers", "400",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2",
            "mig.lvl4", "foo"
        }), tt::per_element());
    }
    {
        // non-gatherer stuff is left intact
        ISInfo::Annotations updated(::make_ann({"FINAL", "FINAL", "providers", "x", "extra", ""}));
        ann.update(updated);
        BOOST_TEST(updated == ::make_ann({
            "FINAL", "FINAL",
            "providers", "400",
            "extra", "",
            "mig.lvl1", "30",
            "mig.lvl2", "2",
            "mig.lvl3", "2",
        }), tt::per_element());
    }
}

BOOST_AUTO_TEST_CASE(merge_errors)
{
    // test for handling some erroneous cases
    Annotations ann;

    // incorrect input is replaced with 0
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "", "mig.lvl1", "xyz"})));
    BOOST_TEST(ann.counters().empty());

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    // leading digits are used as numbers
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "3.24", "mig.lvl1", "1e2"})));
    BOOST_TEST(ann.counters().empty());

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    // leading digits are used as numbers, leading space is ignored
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"providers", "3xyzzy", "mig.lvl1", "  100  "})));
    BOOST_TEST(ann.counters().empty());

    ann.reset();
    BOOST_TEST(ann.counters().empty());

    // keys in unexpected position
    BOOST_CHECK_NO_THROW(ann.merge(::make_ann({"x", "providers", "1", "mig.lvl1", "100", "y"})));
    BOOST_TEST(ann.counters().empty());
}
