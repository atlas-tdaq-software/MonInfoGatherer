//--------------------------------------------------------------------------
// Description:
//      Test suite for ISDynAny*Cont classes
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <memory>
#include <vector>
#include <unistd.h>

#define BOOST_TEST_MODULE MIG_unit_cont_isdynany
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "../../src/algos/ISSum.h"
#include "../../src/containers/ISDynAnyCont.h"
#include "MIG_test_clock.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace MIG {
// special class with full access to container internals
template <typename T=int>
class MIGUnitTestFriendHack {
public:
    template <typename Cont>
    static void mess_up_providerMap(Cont* cont) {
        if (cont != nullptr) {
            BOOST_TEST_MESSAGE("messing up provider map with extra entries");
            cont->m_providerMap.index(".");
            cont->m_providerMap.index("*");
        }
    }
};
}

// needed for Boost to print in tt:per_element
std::ostream&
operator<<(std::ostream& out, std::pair<std::string, ISAnnotation> const& pair) {
    return out << '(' << pair.first << ", " << pair.second.data() << ')';
}

namespace {

// recovery delay in old algorithm
std::chrono::milliseconds bunchInclusionTimeout(2500);

// delay between publication cycles by providers
std::chrono::milliseconds providerCyclePeriod(5000);


// Factory for ISInfoTest objects
struct DataFactory {
    ISInfoTest operator()(int providerId, int tag) {
        ISInfoTest data;
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        data.testValue = 1;
        // replace system current time with our clock time
        ISProxy<>::set_time(data, Clock::instance().now() + time_delta);
        if (do_final) {
            data.setAnnotation("FINAL", "FINAL");
        }
        for (unsigned lvl = 0; lvl != do_annotations.size(); ++lvl) {
            auto lvlCount = do_annotations[lvl];
            if (lvl == 0) {
                data.setAnnotation("providers", std::to_string(lvlCount));
            } else {
                data.setAnnotation("mig.lvl" + std::to_string(lvl), std::to_string(lvlCount));
            }
        }
        return data;
    }
    static std::chrono::microseconds time_delta;
    std::vector<int> do_annotations;
    bool do_final = false;
};

std::chrono::microseconds DataFactory::time_delta(0);

// factory for ISDynAnyCont objects
struct ContFactory {
    std::unique_ptr<ISDynAnyCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({"EOPRecoveryDelay_ms=" + std::to_string(bunchInclusionTimeout.count())});
        return std::make_unique<ISDynAnyCont>("ISInfoTest", ISInfoTest::type(),
                                              std::make_unique<ISSum>(),
                                              publisher, parameters);
    }
};

// factory for ISDynAnyCont which publishes averages
struct ContFactoryAvg {
    std::unique_ptr<ISDynAnyCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({"EOPRecoveryDelay_ms=" + std::to_string(bunchInclusionTimeout.count()),
                                     "PublishSum=0",
                                     "PublishAverage=1",
                                     "SuffixAverage="});
        return std::make_unique<ISDynAnyCont>("ISInfoTest", ISInfoTest::type(),
                                              std::make_unique<ISSum>(),
                                              publisher, parameters);
    }
};

// factory for ISDynAnyCont which publishes both sums and averages
struct ContFactorySumAvg {
    std::unique_ptr<ISDynAnyCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({"EOPRecoveryDelay_ms=" + std::to_string(bunchInclusionTimeout.count()),
                                     "PublishSum=1",
                                     "PublishAverage=1",
                                     "SuffixSum=_sum",
                                     "SuffixAverage=_avg"});
        return std::make_unique<ISDynAnyCont>("ISInfoTest", ISInfoTest::type(),
                                              std::make_unique<ISSum>(),
                                              publisher, parameters);
    }
};

// Checks value of the merged results in ISDynAnyCont container
struct ISDynAnyFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyCont, ContFactory, DataFactory>>>> {
    void check_result() {
        auto iter = results.find("ISInfoTest");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        double expected = cont->numUpdatingProviders();
        BOOST_TEST(value == expected);
    }
};

// Checks value of the merged results in ISDynAnyAvgCont container
struct ISDynAnyAvgFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyCont, ContFactoryAvg, DataFactory>>>> {
    void check_result() {
        auto iter = results.find("ISInfoTest");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        // for average we should always have 1
        BOOST_TEST(value == 1.);
    }
};

// Checks value of the merged results in ISDynAnySumAvgCont container
struct ISDynAnySumAvgFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyCont, ContFactorySumAvg, DataFactory>>>> {

    ISDynAnySumAvgFixture() { callbacks_per_update = 2; }

    void check_result() {

        auto iter = results.find("ISInfoTest_sum");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        double expected = cont->numUpdatingProviders();
        BOOST_TEST(value == expected);

        iter = results.find("ISInfoTest_avg");
        BOOST_REQUIRE(iter != results.end());
        value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        // for average we should always have 1
        BOOST_TEST(value == 1.);
    }
};

typedef boost::mpl::vector<ISDynAnyFixture, ISDynAnyAvgFixture, ISDynAnySumAvgFixture> Fixtures;

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test, F, Fixtures, F)
{
    BOOST_CHECK(this->cont == nullptr);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0);
    this->publish(1);
    BOOST_CHECK(this->cont != nullptr);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);

    this->publish(2);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(2)
        .nUpdatesSincePublish(2);

    this->publish(3);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(3);

    this->tclock += providerCyclePeriod;

    // repeating provider should cause result publishing
    this->publish(1);
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(3)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(1);
    this->check_result();

    this->publish(2);
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(3)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(2);

    // all known providers, should cause result publishing
    this->publish(3);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(3)
        .nUpdatingProviders(3)
        .nUpdatesSincePublish(0);
    this->check_result();

    // wait a bit (longer than timeout)
    this->tclock += providerCyclePeriod;

    // same provider twice, but slightly different timestamps
    this->publish(1);
    this->tclock += std::chrono::milliseconds(3);
    this->publish(1);
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(1)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);
    this->check_result();
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(new_provider_1, F, Fixtures, F)
{
    // Test for new provider being added

    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    this->tclock += providerCyclePeriod;

    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    this->check_result();

    this->tclock += providerCyclePeriod;

    // add new provider, make sure it's not last in the bunch
    this->publishRange(0, 2, true);
    this->publish(7);
    this->publishRange(3, 6, true);
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(8)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);
    this->check_result();

    this->tclock += providerCyclePeriod;

    this->publishRange(0, 7, true);
    this->check_pubs()
        .nCallbacks(4)
        .nUpdates(8)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);
    this->check_result();

    this->tclock += providerCyclePeriod;

    this->publishRange(0, 7, true);
    this->check_pubs()
        .nCallbacks(5)
        .nUpdates(8)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);
    this->check_result();
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(miss_provider_1, F, Fixtures, F)
{
    // Test for provider going missing

    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    this->tclock += providerCyclePeriod;

    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    this->check_result();

    this->tclock += providerCyclePeriod;

    // #6 is dead now, this cycle will not cause publication
    this->publishRange(0, 5, true);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(6);

    this->tclock += providerCyclePeriod;

    // here it should realize that one is missing
    this->publishRange(0, 5, true);
    this->check_pubs()
        .nCallbacks(4)
        .nUpdates(6)
        .nUpdatingProviders(6)
        .nUpdatesSincePublish(0);
    this->check_result();

    this->tclock += providerCyclePeriod;

    this->publishRange(0, 5, true);
    this->check_pubs()
        .nCallbacks(5)
        .nUpdates(6)
        .nUpdatingProviders(6)
        .nUpdatesSincePublish(0);
    this->check_result();
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(single_provider, F, Fixtures, F)
{
    // Test for the case with  a single provider (some ipc_proxies)

    BOOST_CHECK_NO_THROW(this->publish(0));
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0));
    // Should it be 2, 1, 1, 0 ?
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(1)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);
    this->check_result();

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0));
    // Should it be 3, 1, 1, 0 ?
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(1)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);
    this->check_result();

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0));
    // Should it be 4, 1, 1, 0 ?
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(1)
        .nUpdatingProviders(1)
        .nUpdatesSincePublish(1);
    this->check_result();
}

/*
 * test case for the problem reported in
 * https://its.cern.ch/jira/browse/ATDSUPPORT-164
 * Reproduces the sequence similar to what is defined in tools/isPublish.py
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_atdsupport_164, F, Fixtures, F)
{
    this->publishRange(0, 6, true);
    ERS_LOG("First set");
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);
    this->tclock += providerCyclePeriod;

    this->publishRange(0, 6, true);
    ERS_LOG("second set, First in sequence next");
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    this->check_result();
    this->tclock += providerCyclePeriod;

    // should add new provider
    this->publish(7);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(1);
    this->publishRange(0, 6, true);

    // should trigger publication
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(8)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);
    this->check_result();

    // in 2.5sec-cutoff implementation this will be ignored
    this->publish(8);
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(8)
        .nUpdatingProviders(9)
        .nUpdatesSincePublish(0);
    ERS_LOG("Last in sequence above and Delayed transaction next");
    this->tclock += bunchInclusionTimeout / 2;

    // still ignored
    this->publish(9);
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(8)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(0);
    ERS_LOG("third publish set");
    this->tclock += providerCyclePeriod;

    // full cycle with all providers, expect it to publish the result
    this->publishRange(0, 9, true);
    this->check_pubs()
        .nCallbacks(4)
        .nUpdates(10)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(0);
    this->check_result();
    ERS_LOG("fourth publish");
    this->tclock += providerCyclePeriod;

    // full publication
    this->publishRange(0, 9, true);
    this->check_pubs()
        .nCallbacks(5)
        .nUpdates(10)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(0);
    this->check_result();
    ERS_LOG("fifth publish,missing provider next");
    this->tclock += providerCyclePeriod;

    // missing provider, no publishing
    this->publishRange(0, 8, true);
    this->check_pubs()
        .nCallbacks(5)
        .nUpdates(10)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(9);
    ERS_LOG("sixth publish one provider is missing");
    this->tclock += providerCyclePeriod;

    // same missing provider, should publish twice
    this->publishRange(0, 8, true);
    this->check_pubs()
        .nCallbacks(7)
        .nUpdates(9)
        .nUpdatingProviders(9)
        .nUpdatesSincePublish(0);
    this->check_result();
    ERS_LOG("seventh publish same provider is missing, back on next");
    this->tclock += providerCyclePeriod;

    // re-introduce #9
    this->publishRange(0, 4, true);
    this->publish(9);
    this->check_pubs()
        .nCallbacks(7)
        .nUpdates(9)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(6);
    this->publishRange(5, 8, true);
    this->check_pubs()
        .nCallbacks(8)
        .nUpdates(10)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(0);
    this->check_result();
    ERS_LOG("full publish");
    this->tclock += providerCyclePeriod;

    this->publishRange(0, 9, true);
    this->check_pubs()
        .nCallbacks(9)
        .nUpdates(10)
        .nUpdatingProviders(10)
        .nUpdatesSincePublish(0);
    this->check_result();
    ERS_LOG("full publish again");
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing negative time difference
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_timeshift, F, Fixtures, F)
{
    std::chrono::microseconds mksec_off = - bunchInclusionTimeout / 2;

    BOOST_TEST_MESSAGE("First four publications");
    this->publishRange(0, 3, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(4);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Next cycle, known provider, this should publish");
    this->publish(0);
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(4)
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(1);
    this->publish(4);
    this->check_pubs()
        .nCallbacks(1)
        .nUpdates(4)
        .nUpdatingProviders(5)
        .nUpdatesSincePublish(2);
    BOOST_TEST_MESSAGE("all providers from first cycle are seen, should publish");
    this->publishRange(1, 3);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(5)
        .nUpdatingProviders(5)
        .nUpdatesSincePublish(0);
    BOOST_TEST_MESSAGE("new provider, should be skipped after publish (offset triggers a bug in old code)");
    DataFactory::time_delta = mksec_off;
    this->publish(5);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(5)
        .nUpdatingProviders(6)
        .nUpdatesSincePublish(0);
    DataFactory::time_delta = std::chrono::microseconds(0);
    this->publishList({6, 7});
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(5)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    this->publishRange(0, 6);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(5)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(7);
    BOOST_TEST_MESSAGE("triggers publish");
    this->publish(7);
    this->check_pubs()
        .nCallbacks(3)
        .nUpdates(8)
        .nUpdatingProviders(8)
        .nUpdatesSincePublish(0);
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing providers counting
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_providercount, F, Fixtures, F)
{
    BOOST_TEST_MESSAGE("First four publications");
    this->publishRange(0, 3, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(4);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    MIGUnitTestFriendHack<>::mess_up_providerMap(this->cont.get());
    BOOST_TEST_MESSAGE("Next cycle, new provider, should not publish, count increments");
    this->publish(4);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(5)
        .nUpdatesSincePublish(5);
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing duplicate subscription
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_dups, F, Fixtures, F)
{
    BOOST_TEST_MESSAGE("First cycle");
    this->publishRange(0, 3, true);
    // duplicate notification from provider 1
    this->publish(1);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(4);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Second cycle");
    this->publishRange(0, 3, true);
    // duplicate notification from provider 1
    this->publish(1);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(4)
        .nUpdatingProviders(4)
        .nUpdatesSincePublish(0);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(annotations1, F, Fixtures, F)
{
    // test for annotations, case when objects come without annotations,
    // in that case gatherer should add single level of annotations

    this->dataFactory.do_annotations = {};

    BOOST_TEST_MESSAGE("First cycle");
    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Second cycle");
    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    this->check_annotations({7});
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(annotations2, F, Fixtures, F)
{
    // test for annotations, case when objects come with annotations,
    // in that case gatherer should add extra level of annotations

    this->dataFactory.do_annotations = {100, 10};

    BOOST_TEST_MESSAGE("First cycle");
    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(0)
        .nUpdates(0)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(7);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Second cycle");
    this->publishRange(0, 6, true);
    this->check_pubs()
        .nCallbacks(2)
        .nUpdates(7)
        .nUpdatingProviders(7)
        .nUpdatesSincePublish(0);
    this->check_annotations({700, 70, 7});
}
