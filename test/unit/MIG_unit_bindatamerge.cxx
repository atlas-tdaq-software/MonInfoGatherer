//--------------------------------------------------------------------------
// Description:
//	Test suite case for BinDataMerge.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Exceptions.h"
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/BinDataMerge.h"

using namespace MIG;

#define BOOST_TEST_MODULE MIG_unit_bindatamerge
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tt = boost::test_tools;


BOOST_AUTO_TEST_CASE(test_BinDataMergeDirectX)
{
    {
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<int> expect({1, 2, 6, 7, 6});
        BinDataMergeDirectX merge(src.size());
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // use smaller size
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<int> expect({1, 2, 6, 7, 1});
        BinDataMergeDirectX merge(src.size()-1);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }

}

BOOST_AUTO_TEST_CASE(test_BinDataMergeDirect)
{
    {
        // 2-D case, both dimensions use Direct
        std::vector<int> src({
                1, 2, 3, 4, 5,
                6, 7, 8, 9, 10
        });
        std::vector<int> dst({
                0, 0, 3, 3, 1,
                4, 5, 2, 6, 7});
        std::vector<int> expect({
                1, 2, 6, 7, 6,
                10, 12, 10, 15, 17
        });
        unsigned strideY = 5;
        auto merge = std::make_unique<BinDataMergeDirect>(2, strideY, strideY,
                std::make_unique<BinDataMergeDirectX>(strideY));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // transpose of the above
        std::vector<int> src({
                1, 2, 3, 4, 5,
                6, 7, 8, 9, 10
        });
        std::vector<int> dst({
                0, 0, 3, 3, 1,
                4, 5, 2, 6, 7
        });
        std::vector<int> expect({
                1, 2, 6, 7, 6,
                10, 12, 10, 15, 17
        });
        unsigned strideY = 2;
        auto merge = std::make_unique<BinDataMergeDirect>(5, strideY, strideY,
                std::make_unique<BinDataMergeDirectX>(strideY));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, different strides
        std::vector<int> src({
                1, 2, 3,
                6, 7, 8,
        });
        std::vector<int> dst({
                0, 0, 3, 3, 1,
                4, 5, 2, 6, 7
        });
        std::vector<int> expect({
                1, 2, 6, 3, 1,
                10, 12, 10, 6, 7
        });
        unsigned strideYSrc = 3;
        unsigned strideYDst = 5;
        auto merge = std::make_unique<BinDataMergeDirect>(2, strideYSrc, strideYDst,
                std::make_unique<BinDataMergeDirectX>(strideYSrc));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 3-D case, all dimensions use Direct
        std::vector<int> src({
                1, 2, 3,    // z = 0
                4, 5, 6,

                7, 8, 9,    // z = 1
                0, 1, 2
        });
        std::vector<int> dst({
                0, 0, 3,    // z = 0
                3, 1, 4,

                5, 2, 6,    // z = 1
                0, 3, 2
        });
        std::vector<int> expect({
                1, 2, 6,
                7, 6, 10,

                12, 10, 15,
                0, 4, 4
        });
        unsigned strideZ = 6;
        unsigned strideY = 3;
        auto merge = std::make_unique<BinDataMergeDirect>(2, strideZ, strideZ,
                std::make_unique<BinDataMergeDirect>(2, strideY, strideY,
                std::make_unique<BinDataMergeDirectX>(strideY)));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeManyX)
{
    {
        // merge factor 1, overflow, undeflow are not summed
        std::vector<int> src({   1, 2, 3, 4, 5,  6,  7,  8, 9});
        std::vector<int> dst({   0, 0, 3, 3, 1,  4,  5,  2, 6});
        std::vector<int> expect({0, 2, 6, 7, 6, 10, 12, 10, 6});
        BinDataMergeManyX merge(src.size(), 1, 1);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // merge factor 1, with offset
        std::vector<int> src({      1, 2, 3, 4, 5});
        std::vector<int> dst({   0, 0, 3, 3, 1, 4, 5, 2, 6});
        std::vector<int> expect({0, 0, 5, 6, 5, 4, 5, 2, 6});
        BinDataMergeManyX merge(src.size(), 2, 1);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // merge factor 2, with offset
        std::vector<int> src({   1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        std::vector<int> dst({   0, 0, 3, 3, 1, 4, 5, 2});
        std::vector<int> expect({0, 2+3+0, 4+5+3, 6+7+3, 8+9+1, 4, 5, 2});
        BinDataMergeManyX merge(src.size(), 1, 2);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // merge factor 3, with offset
        std::vector<int> src({   1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
        std::vector<int> dst({   0, 0, 3, 3, 1, 4, 5, 2});
        std::vector<int> expect({0, 0, 3, 2+3+4+3, 5+6+7+1, 8+9+10+4, 5, 2});
        BinDataMergeManyX merge(src.size(), 3, 3);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // merge factor larger than number of bins in src
        std::vector<int> src({   0, 1, 1, 1, 1, 1, 1, 0});
        std::vector<int> dst({   0, 0, 3, 3, 1, 4, 5, 2});
        std::vector<int> expect({0, 6, 3, 3, 1, 4, 5, 2});
        BinDataMergeManyX merge(src.size(), 1, 1024);
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeMany)
{
    {
        // 2-D case, mf=1, offset=1
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  2,  3, 20,
                20,  2,  3,  4, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20,  8, 10,  4, 20,
                20,  7,  6,  6, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        auto merge = std::make_unique<BinDataMergeMany>(4, 1, 1, 5, 5,
                std::make_unique<BinDataMergeManyX>(5, 1, 1));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, mf=2 for Y, offset=1
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  2,  3, 20,
                20,  2,  3,  4, 20,
                20,  3,  4,  5, 20,
                20,  4,  5,  6, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20, 10, 13,  8, 20,
                20, 12, 12, 13, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        auto merge = std::make_unique<BinDataMergeMany>(6, 1, 2, 5, 5,
                std::make_unique<BinDataMergeManyX>(5, 1, 1));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, mf=3 for Y, offset=2 for X&Y
        std::vector<int> src({
                10, 10, 10,  10, // y-underflow
                20,  1,  2,  20,
                20,  2,  3,  20,
                20,  3,  4,  20,
                30, 10, 10,  30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  9, 11, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        auto merge = std::make_unique<BinDataMergeMany>(5, 2, 3, 4, 5,
                std::make_unique<BinDataMergeManyX>(4, 2, 1));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, large merge factor for both X and Y
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  1,  1, 20,
                20,  1,  1,  1, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20, 13,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        auto merge = std::make_unique<BinDataMergeMany>(4, 1, 1024, 5, 5,
                std::make_unique<BinDataMergeManyX>(5, 1, 1024));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeMapX)
{
    {
        // 1-to-1 mapping
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<unsigned> binMap({0, 1, 2, 3, 4});
        std::vector<int> expect({0, 2, 6, 7, 1});
        BinDataMergeMapX merge(src.size(), std::move(binMap));
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // remap bins, over/underflow are ignored
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<unsigned> binMap({2, 3, 1, 2, 2});
        std::vector<int> expect({0, 3, 7, 5, 1});
        BinDataMergeMapX merge(src.size(), std::move(binMap));
        merge.merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // Test for checkEmptyBins returning false
        std::vector<int> src({0, 1, 2, 3, 4, 5, 0});
        std::vector<unsigned> binMap({0, 1, 2, 3});
        BinDataMergeMapX merge(src.size(), std::move(binMap));
        BOOST_TEST(not merge.checkEmptyBins(src.data()));
    }
    {
        // Test for checkEmptyBins returning false
        std::vector<int> src({
                0, 0, 0, 0, 0, // y-underflow
                0, 1, 2, 3, 0,
                0, 2, 3, 0, 0,
                0, 3, 0, 0, 0,
                0, 0, 0, 0, 0, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 5;
        std::vector<unsigned> binMapX({0, 1, 2});
        auto merge = std::make_unique<BinDataMergeDirect>(5,
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeMapX>(5, std::move(binMapX)));
        BOOST_TEST(not merge->checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeMap)
{
    {
        // 2-D case, 1-to-1 mapping
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  2,  3, 20,
                20,  2,  3,  4, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<unsigned> binMapX({0, 1, 2, 3, 4});
        std::vector<unsigned> binMapY({0, 1, 2, 3});
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20,  8, 10,  4, 20,
                20,  7,  6,  6, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 5;
        auto merge = std::make_unique<BinDataMergeMap>(4, std::move(binMapY),
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeMapX>(5, std::move(binMapX)));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, re-mapping in Y
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  2,  3, 20,
                20,  2,  3,  4, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, 10, 10, // y-underflow
                20,  7,  8,  1, 20,
                20,  5,  3,  2, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<unsigned> binMapX({0, 1, 2, 3, 4});
        std::vector<unsigned> binMapY({2, 2, 1, 1});  // first/last do not matter
        std::vector<int> expect({
                10, 10, 10, 10, 10, // y-underflow
                20,  9, 11,  5, 20,
                20,  6,  5,  5, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 5;
        auto merge = std::make_unique<BinDataMergeMap>(4, std::move(binMapY),
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeMapX>(5, std::move(binMapX)));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // 2-D case, 2-to-1 mapping for Y, 3-to-1 map for X
        std::vector<int> src({
                10, 10, 10, 10, 10, // y-underflow
                20,  1,  2,  3, 20,
                20,  2,  3,  4, 20,
                30, 10, 10, 10, 30, // y-overflow
        });
        std::vector<int> dst({
                10, 10, 10, // y-underflow
                20,  8, 20,
                30, 10, 30, // y-overflow
        });
        std::vector<unsigned> binMapX({0, 1, 1, 1, 0});
        std::vector<unsigned> binMapY({0, 1, 1, 0});
        std::vector<int> expect({
                10, 10, 10, // y-underflow
                20, 23, 20,
                30, 10, 30, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 3;
        auto merge = std::make_unique<BinDataMergeMap>(4, std::move(binMapY),
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeMapX>(5, std::move(binMapX)));
        merge->merge(src.data(), dst.data());
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // Test for checkEmptyBins returning false
        std::vector<int> src({
                0, 0, 0, 0, 0, // y-underflow
                0, 1, 2, 0, 0,
                0, 2, 3, 0, 0,
                0, 1, 0, 0, 0,
                0, 0, 0, 0, 0, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 5;  // not really used in this test
        std::vector<unsigned> binMapY({0, 1, 2});
        auto merge = std::make_unique<BinDataMergeMap>(5, std::move(binMapY),
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeDirectX>(5));
        BOOST_TEST(not merge->checkEmptyBins(src.data()));
    }
    {
        // Test for checkEmptyBins returning false
        std::vector<int> src({
                0, 0, 0, 0, 0, // y-underflow
                0, 1, 2, 3, 0,
                0, 2, 3, 4, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, // y-overflow
        });
        unsigned strideYSrc = 5;
        unsigned strideYDst = 5;  // not really used in this test
        std::vector<unsigned> binMapX({0, 1, 2});
        std::vector<unsigned> binMapY({0, 1, 2});
        auto merge = std::make_unique<BinDataMergeMap>(5, std::move(binMapY),
                strideYSrc, strideYDst,
                std::make_unique<BinDataMergeMapX>(5, std::move(binMapX)));
        BOOST_TEST(not merge->checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeDirectX_norm)
{
    {
        // floating point
        std::vector<float> src({1, 2, 3, 4, 5});
        std::vector<float> dst({0, 0, 3, 3, 1});
        std::vector<float> expect({0.75, 1.5, 3., 3.75, 4.});
        BinDataMergeDirectX merge(src.size());
        merge.merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // integers are truncated floats
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<int> expect({0, 1, 3, 3, 4});
        BinDataMergeDirectX merge(src.size());
        merge.merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeDirect_norm)
{
    {
        // 2-D case, both dimensions use Direct
        std::vector<float> src({
                1, 2, 3, 4, 5,
                6, 7, 8, 9, 10
        });
        std::vector<float> dst({
                0, 0, 3, 3, 1,
                4, 5, 2, 6, 7});
        std::vector<float> expect({
                .75, 1.5, 3., 3.75, 4.,
                5.5, 6.5, 6.5, 8.25, 9.25
        });
        unsigned strideY = 5;
        auto merge = std::make_unique<BinDataMergeDirect>(2, strideY, strideY,
                std::make_unique<BinDataMergeDirectX>(strideY));
        merge->merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
    {
        // integers are truncated floats
        std::vector<int> src({
                1, 2, 3, 4, 5,
                6, 7, 8, 9, 10
        });
        std::vector<int> dst({
                0, 0, 3, 3, 1,
                4, 5, 2, 6, 7});
        std::vector<int> expect({
                0, 1, 3, 3, 4,
                5, 6, 6, 8, 9
        });
        unsigned strideY = 5;
        auto merge = std::make_unique<BinDataMergeDirect>(2, strideY, strideY,
                std::make_unique<BinDataMergeDirectX>(strideY));
        merge->merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge->checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeManyX_norm)
{
    {
        // merge factor 2, with offset
        std::vector<float> src({   1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        std::vector<float> dst({   0, 0, 3, 3, 1, 4, 5, 2});
        std::vector<float> expect({0, 3.75, 7.5, 10.5, 13., 4, 5, 2});
        BinDataMergeManyX merge(src.size(), 1, 2);
        merge.merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // integers are truncated floats
        std::vector<int> src({   1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        std::vector<int> dst({   0, 0, 3, 3, 1, 4, 5, 2});
        std::vector<int> expect({0, 3, 7, 10, 13, 4, 5, 2});
        BinDataMergeManyX merge(src.size(), 1, 2);
        merge.merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        // merge factor larger than bin count
        std::vector<float> src({   1, 1, 1, 1, 1, 1, 1, 1});
        std::vector<float> dst({   0, 1, 3, 3, 1, 4, 5, 2});
        // TODO: bins that are not merged have weight 1
        std::vector<float> expect({0, 4.75, 3, 3, 1, 4, 5, 2});
        BinDataMergeManyX merge(src.size(), 1, 100);
        merge.merge(src.data(), dst.data(), 0.75, 0.25);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
}

BOOST_AUTO_TEST_CASE(test_BinDataMergeMapX_norm)
{
    {
        std::vector<float> src({1, 2, 3, 4, 5});
        std::vector<float> dst({0, 0, 3, 3, 1});
        std::vector<unsigned> binMap({2, 3, 1, 2, 2});
        std::vector<float> expect({0, 0.75, 3.25, 2.75, 1});
        BinDataMergeMapX merge(src.size(), std::move(binMap));
        merge.merge(src.data(), dst.data(), 0.25, 0.75);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
    {
        std::vector<int> src({1, 2, 3, 4, 5});
        std::vector<int> dst({0, 0, 3, 3, 1});
        std::vector<unsigned> binMap({2, 3, 1, 2, 2});
        std::vector<int> expect({0, 0, 3, 2, 1});
        BinDataMergeMapX merge(src.size(), std::move(binMap));
        merge.merge(src.data(), dst.data(), 0.25, 0.75);
        BOOST_TEST(dst == expect, tt::per_element());
        BOOST_TEST(merge.checkEmptyBins(src.data()));
    }
}
