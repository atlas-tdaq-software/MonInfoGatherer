//--------------------------------------------------------------------------
// Description:
//	Test suite case for normalized histogram bin-by-bin adding.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_algo_normadd
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

namespace {

// Fixture that uses HistoAlgorithm
struct HistoAlgoFixture {
    template <typename T>
    void operate(oh::ObjectBase<T>& src, oh::ObjectBase<T>& dst) {
        ConfigParameters cfg({"Normalized=1"});
        HistoAlgorithm algo(cfg);
        LabelMap lblMap;
        algo.operate(src, dst, lblMap);
    }
};

}


BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmNoErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<float>(oh::Histogram::D1, "h1d", 30, axes, {1, 2, 3, 4, 5});
    auto histo2 = makeHist<float>(oh::Histogram::D1, "h1d", 20, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 30);
    BOOST_CHECK_EQUAL(histo2->entries, 50);
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK(histo1->errors.empty());
    BOOST_CHECK(histo2->errors.empty());
    float expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrors, HistoAlgoFixture)
{
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<float>(oh::Histogram::D1, "h1d", 30, axes, {1, 2, 3, 4, 5}, {1., 2., 3., 4., 5.});
    auto histo2 = makeHist<float>(oh::Histogram::D1, "h1d", 20, axes, {0, 0, 3, 3, 1}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 30);
    BOOST_CHECK_EQUAL(histo2->entries, 50);
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo1->errors.size(), 5);
    BOOST_CHECK_EQUAL(histo2->errors.size(), 5);
    float expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
    }
    double expecterr[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->errors[i], expecterr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrorsMixed1, HistoAlgoFixture)
{
    // one of the two histograms has errors array
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<float>(oh::Histogram::D1, "h1d", 30, axes, {1, 2, 3, 4, 5});
    auto histo2 = makeHist<float>(oh::Histogram::D1, "h1d", 20, axes, {0, 0, 3, 3, 1}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_TEST(histo1->entries == 30);
    BOOST_TEST(histo2->entries == 50);
    BOOST_TEST(histo1->get_bins_size() == 5);
    BOOST_TEST(histo2->get_bins_size() == 5);
    BOOST_TEST(histo1->errors.empty());
    BOOST_TEST(histo2->errors.size() == 5);
    float expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->get_bins_array()[i] == expect[i]);
    }
    std::vector<double> expecterr({0., 0., 1.2, 1.2, .4});
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmWithErrorsMixed2, HistoAlgoFixture)
{
    // one of the two histograms has errors array
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeHist<float>(oh::Histogram::D1, "h1d", 30, axes, {1, 2, 3, 4, 5}, {1., 2., 3., 4., 5.});
    auto histo2 = makeHist<float>(oh::Histogram::D1, "h1d", 20, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_TEST(histo1->entries == 30);
    BOOST_TEST(histo2->entries == 50);
    BOOST_TEST(histo1->get_bins_size() == 5);
    BOOST_TEST(histo2->get_bins_size() == 5);
    BOOST_TEST(histo1->errors.size() == 5);
    BOOST_TEST(histo2->errors.size() == 5);
    float expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->get_bins_array()[i] == expect[i]);
    }
    std::vector<double> expecterr({.6, 1.2, 1.8, 2.4, 3.});
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(histo2->errors[i] == expecterr[i], tt::tolerance(0.000001));
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmProfNoErrorsNew, HistoAlgoFixture)
{
	// Profile case with new algo, should do normalized
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeProf(oh::Histogram::D1, "h1d", 30, axes, {1., 2., 3., 4., 5.}, {1., 2., 3., 4., 5.});
    auto histo2 = makeProf(oh::Histogram::D1, "h1d", 20, axes, {0., 0., 3., 3., 1.}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->entries, 30);
    BOOST_CHECK_EQUAL(histo2->entries, 50);
    BOOST_CHECK(histo1->errors.empty());
    BOOST_CHECK(histo2->errors.empty());
    BOOST_CHECK_EQUAL(histo1->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo1->bin_entries.size(), 5);
    BOOST_CHECK_EQUAL(histo2->bin_entries.size(), 5);

    double expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(histo2->bin_entries[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_AddAlgorithmEfficiency, HistoAlgoFixture)
{
	// Profile case with new algo, should do normalized
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto histo1 = makeEffi(oh::Histogram::D1, "h1d", 30, axes, {1., 2., 3., 4., 5.}, {1., 2., 3., 4., 5.});
    auto histo2 = makeEffi(oh::Histogram::D1, "h1d", 20, axes, {0., 0., 3., 3., 1.}, {0., 0., 3., 3., 1.});

    BOOST_REQUIRE_NO_THROW(this->operate(*histo1, *histo2));

    BOOST_CHECK_EQUAL(histo1->total.entries, 30);
    BOOST_CHECK_EQUAL(histo2->total.entries, 50);
    BOOST_CHECK_EQUAL(histo2->passed.entries, 50);
    BOOST_CHECK(histo1->total.errors.empty());
    BOOST_CHECK(histo2->total.errors.empty());
    BOOST_CHECK_EQUAL(histo1->total.get_bins_size(), 5);
    BOOST_CHECK_EQUAL(histo2->total.get_bins_size(), 5);

    double expect[] = {0.6, 1.2, 3, 3.6, 3.4};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(histo2->total.get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(histo2->passed.get_bins_array()[i], expect[i]);
    }
}
