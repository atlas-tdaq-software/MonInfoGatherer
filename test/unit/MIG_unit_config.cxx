//--------------------------------------------------------------------------
// Description:
//	Test suite case for the Config classes.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <iostream>
#include <chrono>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/Config.h"

using namespace MIG;

#define BOOST_TEST_MODULE MIG_unit_config
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

using namespace MIG;
namespace tt = boost::test_tools;

BOOST_TEST_DONT_PRINT_LOG_VALUE(std::chrono::seconds)
BOOST_TEST_DONT_PRINT_LOG_VALUE(std::chrono::milliseconds)


// ==============================================================

BOOST_AUTO_TEST_CASE(test_config_parameter_value)
{
    ConfigParameters cfg({"param1", "param2=22", "param3="});
    BOOST_TEST(cfg.parameters.size() == 3);
    BOOST_TEST(cfg.value("param1") == "");
    BOOST_TEST(cfg.value("param1", "default") == "default");
    BOOST_TEST(cfg.value("param2") == "22");
    BOOST_TEST(cfg.value("param3", "default") == "");
    BOOST_TEST(cfg.value("unparam") == "");
    BOOST_TEST(cfg.value("unparam", "default") == "default");
    std::cout << cfg;
}

BOOST_AUTO_TEST_CASE(test_config_parameter_values)
{
    ConfigParameters cfg({"param1=", "param2", "param3=x=y", "param1=22", "param1=33"});
    BOOST_TEST(cfg.parameters.size() == 5);
    auto values1 = cfg.values("param1");
    BOOST_TEST(values1 == std::vector<std::string>({"", "22", "33"}), tt::per_element());
    auto values2 = cfg.values("param2");
    BOOST_TEST(values2.empty());
    auto values3 = cfg.values("param3");
    BOOST_TEST(values3 == std::vector<std::string>({"x=y"}), tt::per_element());
    auto values4 = cfg.values("param4");
    BOOST_TEST(values4.empty());
    std::cout << cfg;
}

BOOST_AUTO_TEST_CASE(test_config_parameters_boolvalue)
{
    ConfigParameters cfg;
    BOOST_TEST(not cfg.boolValue("param1"));
    BOOST_TEST(cfg.boolValue("param1", true));
}

BOOST_AUTO_TEST_CASE(test_config_parameters_normalized)
{
    ConfigParameters cfg;

    cfg = ConfigParameters();
    BOOST_TEST(not cfg.Normalized());

    cfg = ConfigParameters({"Normalized=1"});
    BOOST_TEST(cfg.Normalized());

    cfg = ConfigParameters({"Normalized=0"});
    BOOST_TEST(not cfg.Normalized());

    cfg = ConfigParameters({"Normalized"});
    BOOST_TEST(cfg.Normalized());

    cfg = ConfigParameters({"Normalized=X"});
    BOOST_TEST(not cfg.Normalized());
}

BOOST_AUTO_TEST_CASE(test_config_parameters_concatenate)
{
    ConfigParameters cfg;

    cfg = ConfigParameters();
    BOOST_TEST(not cfg.Concatenate());

    cfg = ConfigParameters({"Concatenate=1"});
    BOOST_TEST(cfg.Concatenate());

    cfg = ConfigParameters({"Concatenate=0"});
    BOOST_TEST(not cfg.Concatenate());

    cfg = ConfigParameters({"Concatenate"});
    BOOST_TEST(cfg.Concatenate());

    cfg = ConfigParameters({"Concatenate=X"});
    BOOST_TEST(not cfg.Concatenate());
}

BOOST_AUTO_TEST_CASE(test_handler_config)
{
    HandlerConfig cfg(".*", "handler", {"param1", "param2=22", "param3="});
    BOOST_TEST(cfg.objectRe == ".*");
    BOOST_TEST(cfg.name == "handler");
    auto&& parameters = cfg.parameters;
    BOOST_TEST(parameters.parameters.size() == 3);
    BOOST_TEST(parameters.value("param1") == "");
    BOOST_TEST(parameters.value("param2") == "22");
    BOOST_TEST(parameters.value("param3") == "");
    BOOST_TEST(parameters.value("unparam") == "");
    std::cout << cfg;
}

BOOST_AUTO_TEST_CASE(test_handler_config_EOPRecoveryDelay)
{
    const auto default_delay = std::chrono::seconds(5);

    HandlerConfig cfg;

    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms=10"});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == std::chrono::milliseconds(10));

    // 0 is the same as default
    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms=0"});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms"});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms="});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms=sec"});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"EOPRecoveryDelay_ms=5sec"});
    BOOST_TEST(cfg.parameters.EOPRecoveryDelay() == default_delay);
}

BOOST_AUTO_TEST_CASE(test_handler_config_PerLBPublishDelay)
{
    const std::chrono::milliseconds default_delay(1500);

    HandlerConfig cfg;

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms=100"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == std::chrono::milliseconds(100));

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms=10000"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == std::chrono::seconds(10));

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms=0"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay().count() == 0);

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms="});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms=sec"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == default_delay);

    cfg = HandlerConfig(".*", "handler", {"PerLBPublishDelay_ms=5sec"});
    BOOST_TEST(cfg.parameters.PerLBPublishDelay() == default_delay);
}

BOOST_AUTO_TEST_CASE(test_config)
{
    HandlerConfig hcfg(".*", "handler", {"param1", "param2=22", "param3="});

    Config cfg("provider",
               "object.*",
               {"server1", "server2"},
               {"dst1"},
               {hcfg});
    BOOST_TEST(cfg.providerRe == "provider");
    BOOST_TEST(cfg.objectRe == "object.*");
    BOOST_TEST(cfg.srcServers.size() == 2);
    BOOST_TEST(cfg.dstServers.size() == 1);
    BOOST_TEST(cfg.handlerConfigs.size() == 1);
    std::cout << cfg;
}
