//------------------------------------------------------------------------
// Description:
//      Test suite for non-histogram handler
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <memory>
#include <vector>
#include <unistd.h>

#define BOOST_TEST_MODULE MIG_unit_handler_isdynany
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "../../src/containers/Scheduler.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace MIG {

// special class with full access to some internals
template <typename T=Scheduler>
class MIGUnitTestFriendHack {
public:
    static void executeAllScheduledTasks() {
        Scheduler::instance().executeAllTasks();
    }
};

}

namespace {

// This is used to set ISDynAnyPublishDelay_ms parameter
std::chrono::milliseconds publishDelay(5);

std::chrono::milliseconds bunchInclusionTimeout(10);
std::chrono::milliseconds sleepTime(12);

// Factory for HandlerConfig objects
struct HandlerConfigFactory {
    std::string name;
    std::vector<std::string> parameters;
    bool useNG = false;
    HandlerConfigFactory(const std::string& name_, std::vector<std::string> parameters_)
            : name(name_), parameters(parameters_) {}
    HandlerConfig operator()() {
        std::vector<std::string> params = parameters;
        if (not useNG) {
            params.push_back("EOPRecoveryDelay_ms="+std::to_string(bunchInclusionTimeout.count()));
            params.push_back("ISDynAnyNG=");
        } else {
            params.push_back("ISDynAnyPublishDelay_ms=" + std::to_string(publishDelay.count()));
        }
        HandlerConfig cfg("", name, params);
        return cfg;
    }
};

struct ISHandlerFactory : HandlerConfigFactory {
    ISHandlerFactory() : HandlerConfigFactory("Default", {}) {}
};

struct ISAvgHandlerFactory : HandlerConfigFactory {
    ISAvgHandlerFactory() :
        HandlerConfigFactory("Default", {"PublishSum=0",
                                         "PublishAverage=1",
                                         "SuffixAverage="}) {}
};

struct ISSumAvgHandlerFactory : HandlerConfigFactory {
    ISSumAvgHandlerFactory() :
        HandlerConfigFactory("Default", {"PublishAverage=1",
                                         "SuffixSum=_sum"}) {}
};

// Factory for ISInfoTest objects
struct DataFactory {
    ISInfoTest operator()(int providerId, int /*tag*/) {
        ISInfoTest data;
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        data.testValue = 1;
        return data;
    }
};

// Checks value of the merged results in ISDynAnyCont container
struct ISHandlerFixture : HandlerFixture<ISHandlerFactory, DataFactory> {
    void check(const std::string& name) {
        auto iter = results.find(name);
        BOOST_CHECK(iter != results.end());
        if (iter != results.end()) {
            double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
            double expected = nUpdates;
            BOOST_CHECK_MESSAGE(value == expected,
                    "value == expected [" << value << " == " << expected << "]");
        }
    }

    static const unsigned callbacks_per_update = 1;
};

// Checks value of the merged results in ISDynAnyAvgCont container
struct ISAvgHandlerFixture : HandlerFixture<ISAvgHandlerFactory, DataFactory> {
    void check(const std::string& name) {
        auto iter = results.find(name);
        BOOST_CHECK(iter != results.end());
        if (iter != results.end()) {
            double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
            // for average we should always have 1
            BOOST_CHECK_EQUAL(value, 1.);
        }
    }

    static const unsigned callbacks_per_update = 1;
};

// Checks value of the merged results in ISDynAnySumAvgCont container
struct ISSumAvgHandlerFixture : HandlerFixture<ISSumAvgHandlerFactory, DataFactory> {
    void check(const std::string& name) {

        auto iter = results.find(name+"_sum");
        BOOST_CHECK(iter != results.end());
        if (iter != results.end()) {
            double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
            double expected = nUpdates;
            BOOST_CHECK_MESSAGE(value == expected,
                    "value == expected [" << value << " == " << expected << "]");
        }

        iter = results.find(name+"_avg");
        BOOST_CHECK(iter != results.end());
        if (iter != results.end()) {
            double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
            // for average we should always have 1
            BOOST_CHECK_EQUAL(value, 1.);
        }
    }

    static const unsigned callbacks_per_update = 2;
};

typedef boost::mpl::vector<ISHandlerFixture, ISAvgHandlerFixture, ISSumAvgHandlerFixture> Fixtures;

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test, F, Fixtures, F)
{
    BOOST_REQUIRE(this->handler == nullptr);

    BOOST_CHECK_EQUAL(this->nCallbacks, 0);
    BOOST_CHECK_EQUAL(this->nUpdates, 0);
    this->publish("a", 1);

    BOOST_REQUIRE(this->handler != nullptr);

    this->publish("a", 2);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    this->publish("a", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    // moving to next cycle

    // repeating provider should cause result publishing
    this->publish("a", 1);
    BOOST_CHECK_EQUAL(this->nCallbacks, 1*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("a");

    this->publish("a", 2);
    BOOST_CHECK_EQUAL(this->nCallbacks, 1*F::callbacks_per_update);

    // all known providers, should cause result publishing
    this->publish("a", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 2*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("a");

    // wait a bit (longer than timeout)
    std::this_thread::sleep_for(sleepTime);

    // same provider twice
    this->publish("a", 1);
    this->publish("a", 1);
    BOOST_CHECK_EQUAL(this->nCallbacks, 3*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 1);
    this->check("a");
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test_ng, F, Fixtures, F)
{
    this->hConfigFactory.useNG = true;

    BOOST_REQUIRE(this->handler == nullptr);

    BOOST_CHECK_EQUAL(this->nCallbacks, 0);
    BOOST_CHECK_EQUAL(this->nUpdates, 0);
    this->publish("a", 1);

    BOOST_REQUIRE(this->handler != nullptr);

    this->publish("a", 2);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    this->publish("a", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    // scheduler fires up, should cause result publishing
    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 1*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("a");

    this->publish("a", 1);
    this->publish("a", 2);
    this->publish("a", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 2*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);

    // scheduler fires up, should cause result publishing
    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 2*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("a");

    // same provider twice
    this->publish("a", 1);
    this->publish("a", 1);

    // scheduler fires up, should cause result publishing
    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 3*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("a");
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(many_items, F, Fixtures, F)
{
    // Test for multiple objects

    this->publishRange({"a", "b", "c"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    std::this_thread::sleep_for(sleepTime);

    this->publishRange({"c", "a", "b"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 6*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");

    std::this_thread::sleep_for(sleepTime);

    this->publishRange({"a", "c", "b"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 9*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(many_items_ng, F, Fixtures, F)
{
    // Test for multiple objects

    this->hConfigFactory.useNG = true;

    this->publishRange({"a", "b", "c"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 3*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");

    this->publishRange({"c", "a", "b"}, 0, 6, true);

    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 6*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");

    this->publishRange({"a", "c", "b"}, 0, 6, true);

    std::this_thread::sleep_for(publishDelay * 2);
    MIGUnitTestFriendHack<>::executeAllScheduledTasks();

    BOOST_CHECK_EQUAL(this->nCallbacks, 9*F::callbacks_per_update);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");
}
