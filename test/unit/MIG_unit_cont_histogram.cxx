//--------------------------------------------------------------------------
// Description:
//      Test suite for HistogramCont class
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <memory>
#include <vector>
#include <string>
#include <unistd.h>

#define BOOST_TEST_MODULE MIG_unit_cont_histogram
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "../../src/containers/HistogramCont.h"
#include "MIG_test_clock.h"
#include "MIG_test_utils.h"
#include "oh/core/HistogramData.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

// needed for Boost to print in tt:per_element
std::ostream&
operator<<(std::ostream& out, std::pair<std::string, ISAnnotation> const& pair) {
    return out << '(' << pair.first << ", " << pair.second.data() << ')';
}

namespace {

// recovery delay in old algorithm
std::chrono::milliseconds bunchInclusionTimeout(2500);

// delay between publication cycles by providers
std::chrono::milliseconds providerCyclePeriod(5000);


using Histogram = oh::HistogramData<int>;
const unsigned entries_idx = 4;  // index of entries attribute in Histogram info object
const unsigned entries_per_hist = 15;

// Factory for Histogram objects
struct DataFactory {
    Histogram operator()(int providerId, int tag=-1) {
        std::vector<oh::Axis> axes({
            makeAxis(3, "X", {0., 10.})
        });
        auto data = *makeHist<int>(oh::Histogram::D1, "h1d", entries_per_hist, axes, {1, 2, 3, 4, 5});
        ISProxy<>::set_tag(data, tag);
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        if (do_final) {
            data.setAnnotation("FINAL", "FINAL");
        }
        unsigned level = 0;
        for (auto lvlCount: do_annotations) {
            std::string key = level == 0 ? "providers" : "mig.lvl"+std::to_string(level);
            data.setAnnotation(key, lvlCount);
            ++level;
        }
        // replace system current time with our clock time
        ISProxy<>::set_time(data, Clock::instance().now());
        return data;
    }

    std::vector<int> do_annotations;
    bool do_final = false;
};

// factory for HistogramCont objects
struct ContFactory {
    std::unique_ptr<HistogramCont<oh::Histogram>>
    operator()(InfoPublisher& publisher) {
        using Cont = HistogramCont<oh::Histogram>;
        using Algo = HistoAlgorithm;
        ConfigParameters parameters({"EOPRecoveryDelay_ms=" + std::to_string(bunchInclusionTimeout.count()),
                                     "PerLBHistogram=noauto"});
        return std::make_unique<Cont>("Histo", std::make_unique<Algo>(), publisher, parameters);
    }
};

using Fixture = TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<HistogramCont<oh::Histogram>, ContFactory, DataFactory>>>>;

// Checks for number of updates and publications
struct Check {

    template <typename Fixture>
    static void check(Fixture& fixture, int nCallbacks,
            unsigned numUpdatingProviders, unsigned numUpdatesSincePublish, int tag=-1) {
        BOOST_TEST(fixture.nCallbacks == nCallbacks);
        if (nCallbacks == 0) {
            BOOST_TEST(fixture.nUpdates == 0);
        } else {
            BOOST_TEST(fixture.nUpdates == numUpdatingProviders);
        }
        if (fixture.cont != nullptr) {
            BOOST_TEST(fixture.cont->numUpdatingProviders(tag) == numUpdatingProviders);
            BOOST_TEST(fixture.cont->numUpdatesSincePublish(tag) == numUpdatesSincePublish);
        }
    }
    template <typename Fixture>
    static void check(Fixture& fixture, int nCallbacks, int nUpdates,
            unsigned numUpdatingProviders, unsigned numUpdatesSincePublish, int tag) {
        BOOST_TEST(fixture.nCallbacks == nCallbacks);
        BOOST_TEST(fixture.nUpdates == nUpdates);
        if (fixture.cont != nullptr) {
            BOOST_TEST(fixture.cont->numUpdatingProviders(tag) == numUpdatingProviders);
            BOOST_TEST(fixture.cont->numUpdatesSincePublish(tag) == numUpdatesSincePublish);
        }
    }
};

// Checks entries in merged result in HistogramCont container
struct ResultCheck {
    template <typename Fixture>
    static void check(Fixture& fixture, int tag=-1) {
        auto iter = fixture.results.find("Histo");
        BOOST_REQUIRE(iter != fixture.results.end());
        double entries = iter->second.template getAttributeValue<decltype(oh::Histogram::entries)>(entries_idx);
        double expected = fixture.cont->numUpdatingProviders(tag) * entries_per_hist;
        BOOST_TEST(entries == expected);
    }
};

// Checks finalization process in merged result in HistogramCont container
struct FinalizingCheck {
    static void check(Fixture& fixture, int numFinalProviders, int tag=-1) {
        auto tag_cont = fixture.cont->tag_cont(tag);
        BOOST_REQUIRE(tag_cont != nullptr);
        BOOST_TEST(tag_cont->numFinalProviders() == numFinalProviders);
    }
};

// Helper structure to keep inputs and results for one publication
struct PubFin {
    int provider;
    bool isfinal;
    int nCallbacks;
    int nUpdates;
    int nUpdatingProviders;
    int nUpdatesSincePublish;
    int nFinals;
};

// Checks finalization process in merged result in HistogramCont container
template <typename FixtureClass>
struct PubFinFixture : public FixtureClass {

    void check(int numFinalProviders, int tag=-1) {
        FinalizingCheck::check(*this, numFinalProviders, tag);
    }

    void checkPubFins(const PubFin* begin, const PubFin* end) {
        for (auto iter = begin; iter != end; ++ iter) {
            auto pub = *iter;
            BOOST_TEST_MESSAGE("Publish " << (pub.isfinal ? "final" : "non-final") << " contribution, provider " << pub.provider);
            this->dataFactory.do_final = pub.isfinal;
            BOOST_CHECK_NO_THROW(this->publish(pub.provider));
            this->check_pubs()
                .nCallbacks(pub.nCallbacks)
                .nUpdates(pub.nUpdates)
                .nUpdatingProviders(pub.nUpdatingProviders)
                .nUpdatesSincePublish(pub.nUpdatesSincePublish);
            this->check(pub.nFinals);
        }
    }
};

using FinalizingFixture = PubFinFixture<Fixture>;

} // namespace

typedef boost::mpl::vector<Fixture> Fixtures;

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test, F, Fixtures, F)
{
    // simple test with single tag (-1)

    BOOST_CHECK(this->cont == nullptr);
    Check::check(*this, 0, -1, -1);
    BOOST_CHECK_NO_THROW(this->publish(1));
    Check::check(*this, 0, 1, 1);

    BOOST_CHECK_NO_THROW(this->publish(2));
    Check::check(*this, 0, 2, 2);

    BOOST_CHECK_NO_THROW(this->publish(3));
    Check::check(*this, 0, 3, 3);

    this->tclock += providerCyclePeriod;

    // repeating provider should cause result publishing
    BOOST_CHECK_NO_THROW(this->publish(1));
    Check::check(*this, 1, 3, 1);
    ResultCheck::check(*this);

    BOOST_CHECK_NO_THROW(this->publish(2));
    Check::check(*this, 1, 3, 2);

    // all known providers, should cause result publishing
    BOOST_CHECK_NO_THROW(this->publish(3));
    Check::check(*this, 2, 3, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    // same provider twice, number of providers is reset to 1
    BOOST_CHECK_NO_THROW(this->publish(1));
    this->tclock += providerCyclePeriod;
    BOOST_CHECK_NO_THROW(this->publish(1));
    Check::check(*this, 3, 1, 1);
    ResultCheck::check(*this);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(new_provider_1, F, Fixtures, F)
{
    // Test for new provider being added, single tag

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    Check::check(*this, 0, 7, 7);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    Check::check(*this, 2, 7, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    // add new provider, make sure it's not last in the bunch
    BOOST_CHECK_NO_THROW(this->publishRange(0, 2, true));
    BOOST_CHECK_NO_THROW(this->publish(7));
    BOOST_CHECK_NO_THROW(this->publishRange(3, 6, true));
    Check::check(*this, 3, 8, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 7, true));
    Check::check(*this, 4, 8, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 7, true));
    Check::check(*this, 5, 8, 0);
    ResultCheck::check(*this);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(miss_provider_1, F, Fixtures, F)
{
    // Test for provider going missing, single tag only

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    Check::check(*this, 0, 7, 7);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    Check::check(*this, 2, 7, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    // #6 is dead now, this cycle will not cause publication
    BOOST_CHECK_NO_THROW(this->publishRange(0, 5, true));
    Check::check(*this, 2, 7, 6);

    this->tclock += providerCyclePeriod;

    // here it should realize that one is missing
    BOOST_CHECK_NO_THROW(this->publishRange(0, 5, true));
    Check::check(*this, 4, 6, 0);
    ResultCheck::check(*this);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 5, true));
    Check::check(*this, 5, 6, 0);
    ResultCheck::check(*this);
}

/*
 * test case for the problem reported in
 * https://its.cern.ch/jira/browse/ATDSUPPORT-164
 * Reproduces the sequence similar to what is defined in tools/isPublish.py
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_atdsupport_164, F, Fixtures, F)
{
    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    BOOST_TEST_MESSAGE("First set");
    Check::check(*this, 0, 7, 7);
    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));
    BOOST_TEST_MESSAGE("second set, First in sequence next");
    Check::check(*this, 2, 7, 0);
    ResultCheck::check(*this);
    this->tclock += providerCyclePeriod;

    // should add new provider
    BOOST_CHECK_NO_THROW(this->publish(7));
    Check::check(*this, 2, 7, 8, 1, -1);
    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true));

    // should trigger publication
    Check::check(*this, 3, 8, 0);
    ResultCheck::check(*this);

    // in 2.5sec-cutoff implementation this will be ignored
    BOOST_CHECK_NO_THROW(this->publish(8));
    Check::check(*this, 3, 8, 9, 0, -1);
    BOOST_TEST_MESSAGE("Last in sequence above and Delayed transaction next");
    this->tclock += bunchInclusionTimeout / 2;

    // still ignored
    BOOST_CHECK_NO_THROW(this->publish(9));
    Check::check(*this, 3, 8, 10, 0, -1);
    BOOST_TEST_MESSAGE("third publish set");
    this->tclock += providerCyclePeriod;

    // full cycle with all providers, expect it to publish the result
    BOOST_CHECK_NO_THROW(this->publishRange(0, 9, true));
    Check::check(*this, 4, 10, 0);
    ResultCheck::check(*this);
    BOOST_TEST_MESSAGE("fourth publish");
    this->tclock += providerCyclePeriod;

    // full publication
    BOOST_CHECK_NO_THROW(this->publishRange(0, 9, true));
    Check::check(*this, 5, 10, 0);
    ResultCheck::check(*this);
    BOOST_TEST_MESSAGE("fifth publish,missing provider next");
    this->tclock += providerCyclePeriod;

    // missing provider, no publishing
    BOOST_CHECK_NO_THROW(this->publishRange(0, 8, true));
    Check::check(*this, 5, 10, 9);
    BOOST_TEST_MESSAGE("sixth publish one provider is missing");
    this->tclock += providerCyclePeriod;

    // same missing provider, should publish twice
    BOOST_CHECK_NO_THROW(this->publishRange(0, 8, true));
    Check::check(*this, 7, 9, 0);
    ResultCheck::check(*this);
    BOOST_TEST_MESSAGE("seventh publish same provider is missing, back on next");
    this->tclock += providerCyclePeriod;

    // re-introduce #9
    BOOST_CHECK_NO_THROW(this->publishRange(0, 4, true));
    BOOST_CHECK_NO_THROW(this->publish(9));
    Check::check(*this, 7, 9, 10, 6, -1);
    BOOST_CHECK_NO_THROW(this->publishRange(5, 8, true));
    Check::check(*this, 8, 10, 0);
    ResultCheck::check(*this);
    BOOST_TEST_MESSAGE("full publish");
    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 9, true));
    Check::check(*this, 9, 10, 0);
    ResultCheck::check(*this);
    BOOST_TEST_MESSAGE("full publish again");
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(multi_tag, F, Fixtures, F)
{
    int tag = 0;

    // first cycle with three provides
    BOOST_TEST_MESSAGE("publish providers 1-3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publishRange(1, 3, false, tag));
    Check::check(*this, 0, 3, 3, tag);

    this->tclock += providerCyclePeriod;

    // start next cycle, same tag
    BOOST_TEST_MESSAGE("publish provider 1 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 1, 3, 1, tag);
    ResultCheck::check(*this, tag);

    // complete second cycle
    BOOST_TEST_MESSAGE("publish providers 2-3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publishRange(2, 3, false, tag));
    Check::check(*this, 2, 3, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // next cycle with different tag, numUpdatingProviders
    // is a full set, taken from last tag
    tag = 1;
    BOOST_TEST_MESSAGE("publish provider 1 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 2, 3, 1, tag);

    BOOST_TEST_MESSAGE("publish provider 2 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(2, tag));
    Check::check(*this, 2, 3, 2, tag);

    BOOST_TEST_MESSAGE("publish provider 3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(3, tag));
    Check::check(*this, 3, 3, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // mix two tags together, and change pub order
    tag = 0;
    BOOST_TEST_MESSAGE("publish provider 2 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(2, tag));
    Check::check(*this, 3, 3, 1, tag);
    tag = 1;
    BOOST_TEST_MESSAGE("publish provider 3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(3, tag));
    Check::check(*this, 3, 3, 1, tag);

    this->tclock += providerCyclePeriod;

    tag = 0;
    BOOST_TEST_MESSAGE("publish provider 3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(3, tag));
    Check::check(*this, 3, 3, 2, tag);
    tag = 1;
    BOOST_TEST_MESSAGE("publish provider 1 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 3, 3, 2, tag);

    this->tclock += providerCyclePeriod;

    tag = 0;
    BOOST_TEST_MESSAGE("publish provider 1 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 4, 3, 0, tag);
    ResultCheck::check(*this, tag);
    tag = 1;
    BOOST_TEST_MESSAGE("publish provider 2 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publish(2, tag));
    Check::check(*this, 5, 3, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // complete cycle with different tag
    tag = 5;
    BOOST_TEST_MESSAGE("publish providers 1-3 tag " << tag);
    BOOST_CHECK_NO_THROW(this->publishRange(1, 3, true, tag));
    Check::check(*this, 6, 3, 0, tag);
    ResultCheck::check(*this, tag);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(multi_tag_new_provider, F, Fixtures, F)
{
    // Test for new provider being added, many tags

    // run couple of cycles with tag = 0
    int tag = 0;
    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true, tag));
    Check::check(*this, 0, 7, 7, tag);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true, tag));
    Check::check(*this, 2, 7, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // add new provider in a new tag, make it first in cycle
    tag = 1;
    BOOST_CHECK_NO_THROW(this->publish(7, tag));
    Check::check(*this, 2, 7, 8, 1, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true, tag));
    Check::check(*this, 3, 8, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // one more complete cycle
    BOOST_CHECK_NO_THROW(this->publishRange(0, 7, true, tag));
    Check::check(*this, 4, 8, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // add new provider with a new tag, in the middle of cycle
    tag = 2;
    BOOST_CHECK_NO_THROW(this->publishRange(0, 3, true, tag));
    Check::check(*this, 4, 8, 4, tag);
    BOOST_CHECK_NO_THROW(this->publish(8, tag));
    Check::check(*this, 4, 8, 9, 5, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(4, 7, true, tag));
    Check::check(*this, 5, 9, 0, tag);
    ResultCheck::check(*this, tag);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(multi_tag_miss_provider, F, Fixtures, F)
{
    // Test for new provider being removed, many tags

    // run couple of cycles with tag = 0
    int tag = 0;
    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true, tag));
    Check::check(*this, 0, 7, 7, tag);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publishRange(0, 6, true, tag));
    Check::check(*this, 2, 7, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // start new tag, provider #6 is not there, no update after first cycle
    tag = 1;
    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 2, 7, 1, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(1, 5, true, tag));
    Check::check(*this, 2, 7, 6, tag);

    this->tclock += providerCyclePeriod;

    // next cycle, numUpdatingProviders should now drop
    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 3, 6, 1, tag);
    ResultCheck::check(*this, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(1, 5, true, tag));
    Check::check(*this, 4, 6, 0, tag);
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    // next cycle with the new tag, drop provider #0
    // note that numUpdatingProviders is still at 7, i.e. it is equal to the total
    // number of providers seen so far, not "recently seen"
    tag = 2;
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 4, 6, 7, 1, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(2, 5, true, tag));
    Check::check(*this, 4, 6, 7, 5, tag);

    this->tclock += providerCyclePeriod;

    // next cycle, numUpdatingProviders should now drop
    BOOST_CHECK_NO_THROW(this->publish(1, tag));
    Check::check(*this, 5, 5, 1, tag);
    ResultCheck::check(*this, tag);
    BOOST_CHECK_NO_THROW(this->publishRange(2, 5, true, tag));
    Check::check(*this, 6, 5, 0, tag);
    ResultCheck::check(*this, tag);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(single_provider, F, Fixtures, F)
{
    // Test for the case with  a single provider (some ipc_proxies)

    int tag = 0;
    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 0, 1, 1, tag);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 1, 1, 1, tag);   // Should it be 2, 1, 0 ?
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 2, 1, 1, tag);   // Should it be 3, 1, 0 ?
    ResultCheck::check(*this, tag);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(this->publish(0, tag));
    Check::check(*this, 3, 1, 1, tag);   // Should it be 4, 1, 0 ?
    ResultCheck::check(*this, tag);
}

// === annotations tests are only for regular HistogramCont

BOOST_FIXTURE_TEST_CASE(annotations1, Fixture)
{
    // test for annotations, case when histograms come without annotations,
    // in that case gatherer should add single level of annotations

    dataFactory.do_annotations = {};

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    Check::check(*this, 0, 7, 7);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    Check::check(*this, 2, 7, 0);
    this->check_annotations({7});
}

BOOST_FIXTURE_TEST_CASE(annotations2, Fixture)
{
    // test for annotations, case when histograms come with annotations,
    // in that case gatherer should add extra level of annotations

    dataFactory.do_annotations = {100};

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    Check::check(*this, 0, 7, 7);

    this->tclock += providerCyclePeriod;

    BOOST_CHECK_NO_THROW(publishRange(0, 6, true));
    Check::check(*this, 2, 7, 0);
    this->check_annotations({700, 7});
}

BOOST_FIXTURE_TEST_CASE(annotations_misaligned1, Fixture)
{
    // test for annotations, case when different histograms have different
    // number of level of gathering.
    // In this test case histograms with deeper levels come first

    dataFactory.do_annotations = {100, 10};
    BOOST_CHECK_NO_THROW(publishRange(0, 3, true));
    dataFactory.do_annotations = {100};
    BOOST_CHECK_NO_THROW(publishRange(4, 7, true));
    Check::check(*this, 0, 8, 8);

    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {100, 10};
    BOOST_CHECK_NO_THROW(publish(0));
    Check::check(*this, 1, 8, 1);
    this->check_annotations({800, 40, 8});

    BOOST_CHECK_NO_THROW(publishRange(1, 3, true));
    dataFactory.do_annotations = {100};
    BOOST_CHECK_NO_THROW(publishRange(4, 7, true));
    Check::check(*this, 2, 8, 0);
    this->check_annotations({800, 40, 8});
}

BOOST_FIXTURE_TEST_CASE(annotations_misaligned2, Fixture)
{
    // test for annotations, case when different histograms have different
    // number of level of gathering.
    // In this test case histograms with deeper levels come last

    dataFactory.do_annotations = {100};
    BOOST_CHECK_NO_THROW(publishRange(0, 3, true));
    dataFactory.do_annotations = {100, 10};
    BOOST_CHECK_NO_THROW(publishRange(4, 7, true));
    Check::check(*this, 0, 8, 8);

    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {100};
    BOOST_CHECK_NO_THROW(publish(0));
    Check::check(*this, 1, 8, 1);
    this->check_annotations({800, 40, 8});

    BOOST_CHECK_NO_THROW(publishRange(1, 3, true));
    dataFactory.do_annotations = {100, 10};
    BOOST_CHECK_NO_THROW(publishRange(4, 7, true));
    Check::check(*this, 2, 8, 0);
    this->check_annotations({800, 40, 8});
}

BOOST_FIXTURE_TEST_CASE(annotations3, Fixture)
{
    // test for annotations, case for messed up alignments

    dataFactory.do_annotations = {124, 1}; publish(26001);
    dataFactory.do_annotations = {720, 30, 1}; publish(16001);
    dataFactory.do_annotations = {49, 1}; publish(23001);
    dataFactory.do_annotations = {744, 31, 1}; publish(22001);
    dataFactory.do_annotations = {744, 31, 1}; publish(19001);
    dataFactory.do_annotations = {744, 31, 1}; publish(25001);
    dataFactory.do_annotations = {744, 31, 1}; publish(21001);
    dataFactory.do_annotations = {744, 31, 1}; publish(17001);
    dataFactory.do_annotations = {744, 31, 1}; publish(18001);
    dataFactory.do_annotations = {648, 27, 1}; publish(62001);
    dataFactory.do_annotations = {744, 31, 1}; publish(20001);
    dataFactory.do_annotations = {744, 31, 1}; publish(24001);
    dataFactory.do_annotations = {74, 1}; publish(61001);
    dataFactory.do_annotations = {888, 37, 1}; publish(59001);
    dataFactory.do_annotations = {936, 39, 1}; publish(84001);
    dataFactory.do_annotations = {48, 1}; publish(75001);
    dataFactory.do_annotations = {936, 39, 1}; publish(73001);
    dataFactory.do_annotations = {936, 39, 1}; publish(63001);
    dataFactory.do_annotations = {936, 39, 1}; publish(74001);
    dataFactory.do_annotations = {936, 39, 1}; publish(90001);
    dataFactory.do_annotations = {936, 39, 1}; publish(55001);
    dataFactory.do_annotations = {936, 39, 1}; publish(81001);
    dataFactory.do_annotations = {936, 39, 1}; publish(56001);
    dataFactory.do_annotations = {936, 39, 1}; publish(87001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(51001);
    dataFactory.do_annotations = {936, 39, 1}; publish(80001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(44001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(53001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(54001);
    dataFactory.do_annotations = {936, 39, 1}; publish(57001);
    dataFactory.do_annotations = {936, 39, 1}; publish(85001);
    dataFactory.do_annotations = {936, 39, 1}; publish(86001);
    dataFactory.do_annotations = {936, 39, 1}; publish(58001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(49001);
    dataFactory.do_annotations = {936, 39, 1}; publish(88001);
    dataFactory.do_annotations = {936, 39, 1}; publish(89001);
    dataFactory.do_annotations = {936, 39, 1}; publish(82001);
    dataFactory.do_annotations = {936, 39, 1}; publish(60001);
    dataFactory.do_annotations = {936, 39, 1}; publish(70001);
    dataFactory.do_annotations = {936, 39, 1}; publish(76001);
    dataFactory.do_annotations = {936, 39, 1}; publish(71001);
    dataFactory.do_annotations = {936, 39, 1}; publish(79001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(52001);
    dataFactory.do_annotations = {936, 39, 1}; publish(72001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(46001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(45001);
    dataFactory.do_annotations = {936, 39, 1}; publish(77001);
    dataFactory.do_annotations = {936, 39, 1}; publish(83001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(47001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(48001);
    dataFactory.do_annotations = {1064, 38, 1}; publish(50001);
    Check::check(*this, 0, 51, 51);

    // start next cycle
    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {480, 20, 1}; publish(62001);
    Check::check(*this, 1, 51, 1);
    this->check_annotations({43887, 1749, 47, 51});

    dataFactory.do_annotations = {668, 1}; publish(26001);
    dataFactory.do_annotations = {745, 1}; publish(23001);
    dataFactory.do_annotations = {744, 31, 1}; publish(18001);
    dataFactory.do_annotations = {744, 31, 1}; publish(22001);
    dataFactory.do_annotations = {744, 31, 1}; publish(21001);
    dataFactory.do_annotations = {920, 1}; publish(61001);
    dataFactory.do_annotations = {936, 39, 1}; publish(87001);
    dataFactory.do_annotations = {720, 30, 1}; publish(16001);
    dataFactory.do_annotations = {936, 39, 1}; publish(74001);
    dataFactory.do_annotations = {744, 31, 1}; publish(19001);
    dataFactory.do_annotations = {744, 31, 1}; publish(25001);
    dataFactory.do_annotations = {936, 39, 1}; publish(73001);
    dataFactory.do_annotations = {936, 39, 1}; publish(85001);
    dataFactory.do_annotations = {936, 39, 1}; publish(90001);
    dataFactory.do_annotations = {936, 39, 1}; publish(58001);
    dataFactory.do_annotations = {936, 39, 1}; publish(84001);
    dataFactory.do_annotations = {744, 31, 1}; publish(24001);
    dataFactory.do_annotations = {961, 1}; publish(75001);
    dataFactory.do_annotations = {936, 39, 1}; publish(77001);
    dataFactory.do_annotations = {1064, 38, 1}; publish(50001);
    dataFactory.do_annotations = {888, 37, 1}; publish(59001);
    dataFactory.do_annotations = {744, 31, 1}; publish(20001);
    dataFactory.do_annotations = {744, 31, 1}; publish(17001);
    dataFactory.do_annotations = {936, 39, 1}; publish(83001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(52001);
    dataFactory.do_annotations = {936, 39, 1}; publish(79001);
    dataFactory.do_annotations = {936, 39, 1}; publish(86001);
    dataFactory.do_annotations = {936, 39, 1}; publish(80001);
    dataFactory.do_annotations = {936, 39, 1}; publish(82001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(53001);
    dataFactory.do_annotations = {936, 39, 1}; publish(63001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(45001);
    dataFactory.do_annotations = {936, 39, 1}; publish(88001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(47001);
    dataFactory.do_annotations = {936, 39, 1}; publish(71001);
    dataFactory.do_annotations = {936, 39, 1}; publish(76001);
    dataFactory.do_annotations = {936, 39, 1}; publish(89001);
    dataFactory.do_annotations = {936, 39, 1}; publish(81001);
    dataFactory.do_annotations = {936, 39, 1}; publish(72001);
    dataFactory.do_annotations = {936, 39, 1}; publish(56001);
    dataFactory.do_annotations = {936, 39, 1}; publish(70001);
    dataFactory.do_annotations = {936, 39, 1}; publish(57001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(49001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(46001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(54001);
    dataFactory.do_annotations = {936, 39, 1}; publish(60001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(44001);
    dataFactory.do_annotations = {936, 39, 1}; publish(55001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(51001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(48001);
    Check::check(*this, 2, 51, 0);
    this->check_annotations({46718, 1742, 47, 51});

    // start next cycle
    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {768, 32, 1}; publish(62001);
    dataFactory.do_annotations = {766, 767, 767}; publish(26001);
    dataFactory.do_annotations = {763, 764, 764}; publish(23001);
    dataFactory.do_annotations = {744, 31, 1}; publish(24001);
    dataFactory.do_annotations = {969, 970, 970}; publish(61001);
    dataFactory.do_annotations = {744, 31, 1}; publish(18001);
    dataFactory.do_annotations = {744, 31, 1}; publish(17001);
    dataFactory.do_annotations = {744, 31, 1}; publish(22001);
    dataFactory.do_annotations = {720, 30, 1}; publish(16001);
    dataFactory.do_annotations = {955, 956, 956}; publish(75001);
    dataFactory.do_annotations = {888, 37, 1}; publish(59001);
    dataFactory.do_annotations = {936, 39, 1}; publish(55001);
    dataFactory.do_annotations = {936, 39, 1}; publish(74001);
    dataFactory.do_annotations = {936, 39, 1}; publish(60001);
    dataFactory.do_annotations = {936, 39, 1}; publish(70001);
    dataFactory.do_annotations = {936, 39, 1}; publish(84001);
    dataFactory.do_annotations = {744, 31, 1}; publish(20001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(54001);
    dataFactory.do_annotations = {936, 39, 1}; publish(80001);
    dataFactory.do_annotations = {744, 31, 1}; publish(21001);
    dataFactory.do_annotations = {936, 39, 1}; publish(56001);
    dataFactory.do_annotations = {744, 31, 1}; publish(25001);
    dataFactory.do_annotations = {744, 31, 1}; publish(19001);
    dataFactory.do_annotations = {936, 39, 1}; publish(73001);
    dataFactory.do_annotations = {936, 39, 1}; publish(88001);
    dataFactory.do_annotations = {936, 39, 1}; publish(72001);
    dataFactory.do_annotations = {936, 39, 1}; publish(76001);
    dataFactory.do_annotations = {936, 39, 1}; publish(81001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(51001);
    dataFactory.do_annotations = {936, 39, 1}; publish(82001);
    dataFactory.do_annotations = {936, 39, 1}; publish(79001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(52001);
    dataFactory.do_annotations = {936, 39, 1}; publish(58001);
    dataFactory.do_annotations = {936, 39, 1}; publish(85001);
    dataFactory.do_annotations = {1064, 38, 1}; publish(50001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(49001);
    dataFactory.do_annotations = {936, 39, 1}; publish(90001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(46001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(48001);
    dataFactory.do_annotations = {936, 39, 1}; publish(87001);
    dataFactory.do_annotations = {936, 39, 1}; publish(89001);
    dataFactory.do_annotations = {936, 39, 1}; publish(71001);
    dataFactory.do_annotations = {936, 39, 1}; publish(86001);
    dataFactory.do_annotations = {936, 39, 1}; publish(77001);
    dataFactory.do_annotations = {936, 39, 1}; publish(63001);
    dataFactory.do_annotations = {936, 39, 1}; publish(83001);
    dataFactory.do_annotations = {936, 39, 1}; publish(57001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(45001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(44001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(47001);
    dataFactory.do_annotations = {1092, 39, 1}; publish(53001);
    Check::check(*this, 3, 51, 0);
    this->check_annotations({47165, 5207, 3504, 51});
}

BOOST_FIXTURE_TEST_CASE(annotations4, Fixture)
{
    // test for annotations, case for messed up alignments

    dataFactory.do_annotations = {74}; publish(26001);

    Check::check(*this, 0, 1, 1);

    // start next cycle
    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {920}; publish(26001);
    Check::check(*this, 1, 1, 1);        // I'd want it to be (2, 1, 0)
    this->check_annotations({74, 1});

    // start next cycle
    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {969, 969}; publish(26001);
    Check::check(*this, 2, 1, 1);
    this->check_annotations({920, 1});

    // start next cycle
    this->tclock += providerCyclePeriod;

    dataFactory.do_annotations = {973, 973}; publish(26001);
    Check::check(*this, 3, 1, 1);
    this->check_annotations({969, 969, 1});
}

BOOST_FIXTURE_TEST_CASE(finalization, Fixture)
{
    // Test for finalization, once "FINAL" annotation appears in
    // histogram then finalization for that tag starts.

    dataFactory.do_annotations = {100};

    BOOST_TEST_MESSAGE("Publish 5 non-final contributions");
    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    this->check_pubs().nCallbacks(0).nUpdates(0).nUpdatingProviders(5).nUpdatesSincePublish(5);
    FinalizingCheck::check(*this, 0);

    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Publish 5 non-final contributions");
    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(5).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 0);

    this->tclock += providerCyclePeriod;

    // start finalizing
    BOOST_TEST_MESSAGE("Publish 1 final contribution, provider 0");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publish(0));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(4).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 1);

    // non-final update
    BOOST_TEST_MESSAGE("Publish 1 non-final contribution, provider 1");
    dataFactory.do_final = false;
    BOOST_CHECK_NO_THROW(publish(1));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(4).nUpdatesSincePublish(1);
    FinalizingCheck::check(*this, 1);

    // final update for the same provider
    BOOST_TEST_MESSAGE("Publish 1 final contribution, provider 1");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publish(1));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(3).nUpdatesSincePublish(1);
    FinalizingCheck::check(*this, 2);

    BOOST_TEST_MESSAGE("Publish 1 final contribution, provider 2");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publish(2));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(2).nUpdatesSincePublish(1);
    FinalizingCheck::check(*this, 3);

    // remaining providers, "finalizing" flag is reset after publication
    BOOST_TEST_MESSAGE("Publish final contributions, providers 3, 4");
    BOOST_CHECK_NO_THROW(publishRange(3, 4, true));
    // final publication should have all 5 contributions, but
    // internal counters will all reset to 0 after publish()
    this->check_pubs().nCallbacks(3).nUpdates(5).nUpdatingProviders(0).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 0);
}

BOOST_FIXTURE_TEST_CASE(finalization2, Fixture)
{
    // Test for finalization, once "FINAL" annotation appears in
    // histogram then finalization for that tag starts.

    dataFactory.do_annotations = {100};

    BOOST_TEST_MESSAGE("Publish 5 non-final contributions");
    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    this->check_pubs().nCallbacks(0).nUpdates(0).nUpdatingProviders(5).nUpdatesSincePublish(5);
    FinalizingCheck::check(*this, 0);

    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Publish 5 non-final contributions");
    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(5).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 0);

    this->tclock += providerCyclePeriod;

    // start finalizing
    BOOST_TEST_MESSAGE("Publish 1 final contribution, provider 0");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publish(0));
    this->check_pubs().nCallbacks(2).nUpdates(5).nUpdatingProviders(4).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 1);

    // non-final update
    BOOST_TEST_MESSAGE("Publish 4 non-final contribution, providers 1-4");
    dataFactory.do_final = false;
    BOOST_CHECK_NO_THROW(publishRange(1, 4, true));
    this->check_pubs().nCallbacks(3).nUpdates(5).nUpdatingProviders(4).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 1);

    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Publish 4 non-final contribution, providers 1-4");
    dataFactory.do_final = false;
    BOOST_CHECK_NO_THROW(publishRange(1, 4, true));
    this->check_pubs().nCallbacks(4).nUpdates(5).nUpdatingProviders(4).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 1);

    BOOST_TEST_MESSAGE("Publish 1 final contribution, provider 4");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publish(4));
    this->check_pubs().nCallbacks(4).nUpdates(5).nUpdatingProviders(3).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 2);

    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Publish 3 non-final contribution, providers 1-3");
    dataFactory.do_final = false;
    BOOST_CHECK_NO_THROW(publishRange(1, 3, true));
    this->check_pubs().nCallbacks(5).nUpdates(5).nUpdatingProviders(3).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 2);

    // remaining providers, "finalizing" flag is reset after publication
    BOOST_TEST_MESSAGE("Publish last final contributions, providers 1-3");
    dataFactory.do_final = true;
    BOOST_CHECK_NO_THROW(publishRange(1, 3, true));
    // final publication should have all 5 contributions, but
    // internal counters will all reset to 0 after publish()
    this->check_pubs().nCallbacks(6).nUpdates(5).nUpdatingProviders(0).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 0);

    this->tclock += providerCyclePeriod * 3;

    // re-start normal publishing
    BOOST_TEST_MESSAGE("Re-start, publish 5 non-final contributions");
    dataFactory.do_final = false;
    BOOST_CHECK_NO_THROW(publishRange(0, 4, true));
    this->check_pubs().nCallbacks(7).nUpdates(5).nUpdatingProviders(5).nUpdatesSincePublish(0);
    FinalizingCheck::check(*this, 0);
}

BOOST_FIXTURE_TEST_CASE(finalization3_1, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - all 3 providers publish non-final contribution
    //  - after that all 3 providers publish final
    //  - expect one publication

    ::PubFin data[] = {
        {0, false, 0, 0, 1, 1, 0},
        {1, false, 0, 0, 2, 2, 0},
        {2, false, 0, 0, 3, 3, 0},
        {1,  true, 0, 0, 2, 3, 1},
        {0,  true, 0, 0, 1, 3, 2},
        {2,  true, 1, 3, 0, 0, 0},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

BOOST_FIXTURE_TEST_CASE(finalization3_2, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - 2 providers publish non-final contribution
    //  - then one of them pushes final
    //  - remaining one pushes non-final
    //  - 2 remaining publish final
    //  - expect one publication

    ::PubFin data[] = {
        {0, false, 0, 0, 1, 1, 0},
        {1, false, 0, 0, 2, 2, 0},
        {1,  true, 0, 0, 1, 2, 1},
        {2, false, 0, 0, 2, 3, 1},
        {0,  true, 0, 0, 1, 3, 2},
        {2,  true, 1, 3, 0, 0, 0},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

BOOST_FIXTURE_TEST_CASE(finalization3_3, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - 2 providers publish non-final contribution  (0+1)
    //  - after that all 3 providers publish final, in specific order  (0+2+1)
    //  - expect one publication

    ::PubFin data[] = {
        {0, false, 0, 0, 1, 1, 0},
        {1, false, 0, 0, 2, 2, 0},
        {0,  true, 0, 0, 1, 2, 1},
        {2,  true, 0, 0, 1, 2, 2},
        {1,  true, 1, 3, 0, 0, 0},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

BOOST_FIXTURE_TEST_CASE(finalization3_4, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - 2 providers publish non-final contribution
    //  - then the same 2 providers publish final contribution
    //  - after that remaining publishes non-final and then final
    //  - as there is no delay, expect one final publication

    ::PubFin data[] = {
        {0, false, 0, 0, 1, 1, 0},
        {1, false, 0, 0, 2, 2, 0},
        {0,  true, 0, 0, 1, 2, 1},
        {1,  true, 1, 2, 0, 0, 0},
        {2, false, 1, 2, 3, 1, 0},
        {2,  true, 1, 2, 2, 1, 1},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

BOOST_FIXTURE_TEST_CASE(finalization3_5, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - 2 providers publish non-final contribution
    //  - then the same 2 providers publish final contribution
    //  - after that remaining publishes final, which is ignored
    //  - expect one final publication

    // provider; isfinal; nCallbacks; nUpdates; nUpdatingProviders; nUpdatesSincePublish; nFinals;
    ::PubFin data[] = {
        {0, false, 0, 0, 1, 1, 0},
        {1, false, 0, 0, 2, 2, 0},
        {0,  true, 0, 0, 1, 2, 1},
        {1,  true, 1, 2, 0, 0, 0},
        {2,  true, 1, 2, 0, 0, 1},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

BOOST_FIXTURE_TEST_CASE(finalization3_6, FinalizingFixture)
{
    // Test for short run:
    //  - 3 providers total
    //  - all 3 providers do final publications only
    //  - nothing should be published as a result

    // provider; isfinal; nCallbacks; nUpdates; nUpdatingProviders; nUpdatesSincePublish; nFinals;
    ::PubFin data[] = {
        {0,  true, 0, 0, 0, 0, 1},
        {1,  true, 0, 0, 0, 0, 2},
        {2,  true, 0, 0, 0, 0, 3},
    };

    this->checkPubFins(std::begin(data), std::end(data));
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing duplicate subscription
 */
BOOST_FIXTURE_TEST_CASE(ticket_adhi_4578_dups, Fixture)
{
    BOOST_TEST_MESSAGE("First cycle");
    publishRange(0, 3, true);
    Check::check(*this, 0, 4, 4);
    // duplicate notification from provider 1
    publish(1);
    Check::check(*this, 0, 4, 4);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Second cycle");
    publishRange(0, 3, true);
    Check::check(*this, 2, 4, 0);
    // duplicate notification from provider 1
    publish(1);
    Check::check(*this, 2, 4, 0);

    BOOST_TEST_MESSAGE("Sleep before next cycle");
    this->tclock += providerCyclePeriod;

    BOOST_TEST_MESSAGE("Finalization cycle with one final");
    publishRange(0, 3, true);
    Check::check(*this, 3, 4, 0);
    // duplicate publication from provider 0, final
    dataFactory.do_final = true;
    publish(0);
    Check::check(*this, 3, 4, 3, 0, -1);
    // duplicate finalization
    publish(0);
    Check::check(*this, 3, 4, 3, 0, -1);
    // finish finalizing
    FinalizingCheck::check(*this, 1);
    publishRange(1, 3, true);
    Check::check(*this, 4, 4, 0, 0, -1);

}
