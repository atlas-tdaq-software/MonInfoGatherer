//--------------------------------------------------------------------------
// Description:
//	Test suite for the SmallIntSet class.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <sstream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/SmallIntSet.h"

using namespace MIG;

#define BOOST_TEST_MODULE mig_unit_smallintset
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

using namespace MIG;

// ==============================================================

BOOST_AUTO_TEST_CASE(test_default)
{
    SmallIntSet set;
    BOOST_TEST(set.size() == 0);
    BOOST_TEST(set.empty());
}

BOOST_AUTO_TEST_CASE(test_add)
{
    SmallIntSet set;
    BOOST_TEST(set.size() == 0);
    BOOST_TEST(set.empty());

    set.add(2);
    BOOST_TEST(set.size() == 1);
    BOOST_TEST(!set.empty());

    set.add(5);
    BOOST_TEST(set.size() == 2);
    BOOST_TEST(!set.empty());

    set.add(5);
    BOOST_TEST(set.size() == 2);
    BOOST_TEST(!set.empty());

    set.add(120);
    BOOST_TEST(set.size() == 3);
    BOOST_TEST(!set.empty());
}

BOOST_AUTO_TEST_CASE(test_contains)
{
    SmallIntSet set;
    for (auto num: {0, 5, 13, 32, 120}) {
        set.add(num);
    }
    BOOST_TEST(set.size() == 5);
    for (auto num: {0, 5, 13, 32, 120}) {
        BOOST_TEST(set.contains(num));
    }
    for (auto num: {1, 2, 7, 64, 121}) {
        BOOST_TEST(!set.contains(num));
    }
}

BOOST_AUTO_TEST_CASE(test_remove)
{
    SmallIntSet set;
    for (auto num: {0, 5, 13, 32, 120}) {
        set.add(num);
    }
    BOOST_TEST(set.size() == 5);

    set.remove(0);
    BOOST_TEST(set.size() == 4);
    BOOST_TEST(!set.contains(0));

    set.remove(100);  // non-existing is OK
    BOOST_TEST(set.size() == 4);

    for (auto num: {0, 5, 13, 32, 120}) {
        set.remove(num);
    }
    BOOST_TEST(set.empty());
}

BOOST_AUTO_TEST_CASE(test_print)
{
    SmallIntSet set;
    std::ostringstream str;
    str << set;
    BOOST_TEST(str.str() == "{}");

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set.add(num);
    }
    std::ostringstream str2;
    str2 << set;
    BOOST_TEST(str2.str() == "{0,5,13-15,32-33,120}");
}

BOOST_AUTO_TEST_CASE(test_isdisjoint)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // empty sets are disjoint
    BOOST_TEST(set1.isdisjoint(set2));
    BOOST_TEST(set2.isdisjoint(set1));

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST(set1.isdisjoint(set2));
    BOOST_TEST(set2.isdisjoint(set1));

    for (auto num: {2, 6, 7, 9}) {
        set2.add(num);
    }
    BOOST_TEST(set1.isdisjoint(set2));
    BOOST_TEST(set2.isdisjoint(set1));

    set2.add(5);
    BOOST_TEST(!set1.isdisjoint(set2));
    BOOST_TEST(!set2.isdisjoint(set1));
}

BOOST_AUTO_TEST_CASE(test_equal)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // empty sets are equal
    BOOST_TEST(set1 == set2);
    BOOST_TEST(set2 == set1);

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST(set1 == set1);
    BOOST_TEST(set1 != set2);
    BOOST_TEST(set2 != set1);

    set2 = set1;
    BOOST_TEST(set1 == set2);
    BOOST_TEST(set2 == set1);
}

BOOST_AUTO_TEST_CASE(test_issubset)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // empty sets are subsets of other empty sets
    BOOST_TEST(set1.issubset(set2));
    BOOST_TEST(set2.issubset(set1));

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST(set2.issubset(set1));
    BOOST_TEST(!set1.issubset(set2));

    for (auto num: {5, 13, 14, 15, 32, 33}) {
        set2.add(num);
    }
    BOOST_TEST(set2.issubset(set1));
    BOOST_TEST(!set1.issubset(set2));

    set2.add(0);
    set2.add(120);
    BOOST_TEST(set2.issubset(set1));

    set2.add(121);
    BOOST_TEST(!set2.issubset(set1));
}

BOOST_AUTO_TEST_CASE(test_issuperset)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // empty sets are supersets of other empty sets
    BOOST_TEST(set1.issuperset(set2));
    BOOST_TEST(set2.issuperset(set1));

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set2.add(num);
    }
    BOOST_TEST(set2.issuperset(set1));
    BOOST_TEST(!set1.issuperset(set2));

    for (auto num: {5, 13, 14, 15, 32, 33}) {
        set1.add(num);
    }
    BOOST_TEST(set2.issuperset(set1));
    BOOST_TEST(!set1.issuperset(set2));

    set1.add(0);
    set1.add(120);
    BOOST_TEST(set2.issuperset(set1));

    set1.add(121);
    BOOST_TEST(!set2.issuperset(set1));
}

BOOST_AUTO_TEST_CASE(test_union)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // union of two empty sets
    BOOST_TEST((set1 | set2).empty());

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST((set1 | set2).size() == set1.size());

    for (auto num: {2, 6, 7, 9}) {
        set2.add(num);
    }
    BOOST_TEST((set1 | set2).size() == set1.size() + set2.size());

    set1 |= set2;
    BOOST_TEST(set1.issuperset(set2));
}

BOOST_AUTO_TEST_CASE(test_intersect)
{
    SmallIntSet set1;
    SmallIntSet set2;

    // intersect of two empty sets
    BOOST_TEST((set1 & set2).empty());

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST((set1 & set2).empty());

    for (auto num: {2, 5, 32, 64}) {
        set2.add(num);
    }
    BOOST_TEST((set1 & set2).size() == 2);

    set1 &= set2;
    BOOST_TEST(set1.issubset(set2));
}

BOOST_AUTO_TEST_CASE(test_difference)
{
    SmallIntSet set1;
    SmallIntSet set2;

    BOOST_TEST((set1 - set2).empty());

    for (auto num: {0, 5, 13, 14, 15, 32, 33, 120}) {
        set1.add(num);
    }
    BOOST_TEST((set1 - set2).size() == set1.size());

    for (auto num: {2, 5, 32, 64}) {
        set2.add(num);
    }
    BOOST_TEST((set1 - set2).size() == set1.size() - 2);

    set1 -= set2;
    BOOST_TEST(set1.size() == 6);
    BOOST_TEST(set1.isdisjoint(set2));
}

BOOST_AUTO_TEST_CASE(test_minmax)
{
    SmallIntSet set;
    BOOST_TEST(set.min_element() == -1);
    BOOST_TEST(set.max_element() == -1);

    set.add(0);
    BOOST_TEST(set.min_element() == 0);
    BOOST_TEST(set.max_element() == 0);

    set.clear();
    set.add(3);
    BOOST_TEST(set.min_element() == 3);
    BOOST_TEST(set.max_element() == 3);

    set.clear();
    set.add(63);
    BOOST_TEST(set.min_element() == 63);
    BOOST_TEST(set.max_element() == 63);

    set.clear();
    set.add(128);
    BOOST_TEST(set.min_element() == 128);
    BOOST_TEST(set.max_element() == 128);

    set.clear();
    for (auto num: {3, 5, 9}) {
        set.add(num);
    }
    BOOST_TEST(set.min_element() == 3);
    BOOST_TEST(set.max_element() == 9);

    set.clear();
    for (auto num: {0, 63}) {
        set.add(num);
    }
    BOOST_TEST(set.min_element() == 0);
    BOOST_TEST(set.max_element() == 63);

    set.clear();
    for (auto num: {128, 511}) {
        set.add(num);
    }
    BOOST_TEST(set.min_element() == 128);
    BOOST_TEST(set.max_element() == 511);
}
