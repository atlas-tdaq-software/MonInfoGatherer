#ifndef MIG_TEST_UTILS_H
#define MIG_TEST_UTILS_H
//----------------------------------------------------
// Bunch of utility methods to be used by (unit) tests
//----------------------------------------------------

// Note that this header uses BOOST test macros but it does not include
// headers, this is intentional as BOOST needs special macros defined
// before any header is included. Include this file after BOOST headers.
//
// !!! DO NOT include this file into any C++ file that can become a part of
// a library. It is only intended to be used by unit tests in this folder.

//-----------------
// C/C++ Headers --
//-----------------
#include <numeric>
#include <memory>
#include <random>
#include <stdexcept>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "is/infoany.h"
#include "is/infodynany.h"
#include "is/infoT.h"
#include "oh/core/Axis.h"
#include "oh/core/EfficiencyData.h"
#include "oh/core/HistogramData.h"
#include "oh/core/ProfileData.h"
#include "MonInfoGatherer/Clock.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/HandlerBase.h"
#include "MonInfoGatherer/HandlerManager.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/LibraryManager.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

// Hack! ISProxy is declared as a friend for ISInfo class, we use this to hack
// a support for updating few internal members of ISInfo. Default template
// type is selected so that it should not collide with any other ISProxy
// specialization.
template <class T=MIG::Config>
class ISProxy {
public:
    static void set_tag(ISInfo& info, int tag) {
        info.m_tag = tag;
    }
    // set time to specific number of mksec
    static void set_time(ISInfo& info, uint64_t mksec) {
        BOOST_TEST_MESSAGE("ISProxy: set info time: " << mksec/1000 << " msec");
        info.m_time = OWLTime(mksec / 1000000, mksec % 1000000);
    }
    // set time to clock time
    static void set_time(ISInfo& info, const MIG::Clock::time_point& tp) {
        auto tpe = tp.time_since_epoch();
        auto tpe_mksec = std::chrono::duration_cast<std::chrono::microseconds>(tpe);
        auto mksec = tpe_mksec.count();
        BOOST_TEST_MESSAGE("ISProxy: set info time: " << mksec/1000 << " msec");
        info.m_time = OWLTime(mksec / 1000000, mksec % 1000000);
    }
};


namespace MIG {
namespace test {

// special IS data object for testing only
class ISInfoTest: public ISInfo {
public:
    double testValue;

    enum {testval_pos = 0}; // index of testValue attribute in ISInfoTest info object

    ISInfoTest() : ISInfo("ISInfoTest") {
        testValue = 0;
    }

    static const ISType & type() {
        static const ISType type_ = ISInfoTest().ISInfo::type();
        return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
        ISInfo::print(out);
        return out << "testValue: " << testValue << "\t// Some arbitrary value" << std::endl;
    }

protected:
    void publishGuts(ISostream& out) {
        out << testValue;
    }
    void refreshGuts(ISistream& in) {
        in >> testValue;
    }
};

inline std::ostream & operator<<(std::ostream& out, const ISInfoTest& info) {
    return info.print( out );
}

/**
 * Factory method for ISInfoT<T> instances.
 */
template <typename T>
inline
std::unique_ptr<ISInfoT<T>>
makeInfo(T value=0)
{
    auto info = std::make_unique<ISInfoT<T>>(value);
    // this is necessary to initialize ISType object inside info
    static_cast<ISInfo&>(*info).type();
    return info;
}

/**
 * Convert ISInfo object into ISInfoDynAny.
 */
std::unique_ptr<ISInfoDynAny>
makeInfoDynAny(ISInfo& info)
{
    ISInfoAny any;
    any <<= info;
    auto dynany = std::make_unique<ISInfoDynAny>();
    any >>= *dynany;
    return dynany;
}

/**
 * Factory method for Axis instances.
 */
inline
oh::Axis makeAxis(int nbins,
        const std::string& title,
        const std::vector<double>& range,
        const std::vector<std::string>& labels=std::vector<std::string>(),
        const std::vector<int>& indices=std::vector<int>())
{
    oh::Axis axis;
    axis.kind = oh::Axis::Fixed;
    axis.nbins = nbins;
    axis.title = title;
    axis.range = range;
    axis.labels = labels;
    if (indices.empty()) {
        for (unsigned i = 0; i != labels.size(); ++ i) {
            axis.indices.push_back(i+1);
        }
    } else {
        axis.indices = indices;
    }
    return axis;
}

/**
 * Factory method for Axis instances of Variable kind
 */
inline
oh::Axis makeVarAxis(int nbins,
        const std::string& title,
        const std::vector<double>& range,
        const std::vector<std::string>& labels=std::vector<std::string>(),
        const std::vector<int>& indices=std::vector<int>())
{
    auto axis = makeAxis(nbins, title, range, labels, indices);
    axis.kind = oh::Axis::Variable;
    return axis;
}

/**
 * Factory method for oh::HistogramData<T> instances.
 */
template <typename T>
inline
std::unique_ptr<oh::HistogramData<T>>
makeHist(oh::Histogram::Dimension ndim,
        const std::string& title,
        unsigned entries,
        const std::vector<oh::Axis>& axes,
        const std::vector<T>& data=std::vector<T>(),
        const std::vector<double> errors=std::vector<double>())
{
    auto histo = std::make_unique<oh::HistogramData<T>>();
    histo->dimension = ndim;
    histo->id.title = title;
    histo->entries = entries;
    histo->errors = errors;
    histo->axes = axes;

    // copy data into owned array and pass it to histo
    auto dcopy = std::make_unique<T[]>(data.size());
    std::copy(data.begin(), data.end(), dcopy.get());
    histo->set_bins_array(dcopy.release(), data.size(), true);

    return histo;
}

/**
 * Factory method for oh::ProfileData<T> instances.
 */
inline
std::unique_ptr<oh::ProfileData>
makeProf(oh::Histogram::Dimension ndim,
        const std::string& title,
        unsigned entries,
        const std::vector<oh::Axis>& axes,
        const std::vector<double>& data=std::vector<double>(),
        const std::vector<double>& bin_entries=std::vector<double>(),
        const std::vector<double>& errors=std::vector<double>(),
        const std::vector<double>& bin_stat=std::vector<double>())
{
    auto histo = std::make_unique<oh::ProfileData>();
    histo->dimension = ndim;
    histo->id.title = title;
    histo->entries = entries;
    histo->axes = axes;
    histo->errors = errors;
    histo->bin_entries = bin_entries;
    histo->statistics = bin_stat;

    // copy data into owned array and pass it to histo
    auto dcopy = std::make_unique<double[]>(data.size());
    std::copy(data.begin(), data.end(), dcopy.get());
    histo->set_bins_array(dcopy.release(), data.size(), true);

    return histo;
}

/**
 * Factory method for oh::EfficiencyData instances.
 */
inline
std::unique_ptr<oh::EfficiencyData>
makeEffi(oh::Histogram::Dimension ndim,
        const std::string& title,
        unsigned entries,
        const std::vector<oh::Axis>& axes,
        const std::vector<double>& data_total,
        const std::vector<double>& data_passed)
{
    auto histo = std::make_unique<oh::EfficiencyData>();
    histo->id.title = title;
    histo->total.dimension = histo->passed.dimension = ndim;
    histo->total.axes = histo->passed.axes = axes;
    histo->total.entries = entries;
    histo->passed.entries = entries;
    histo->betaAlpha = 1.;
    histo->betaBeta = 1.;
    histo->confidenceLevel =  0.682689492137;
    histo->weight =  1.;

    // copy data into owned array and pass it to histo
    auto dcopy = std::make_unique<double[]>(data_total.size());
    std::copy(data_total.begin(), data_total.end(), dcopy.get());
    histo->total.set_bins_array(dcopy.release(), data_total.size(), true);

    dcopy = std::make_unique<double[]>(data_passed.size());
    std::copy(data_passed.begin(), data_passed.end(), dcopy.get());
    histo->passed.set_bins_array(dcopy.release(), data_passed.size(), true);

    return histo;
}

/**
 * Class defining few useful methods for publishing contributions from providers.
 */
class PublishBase {
public:

    virtual ~PublishBase() {}

    virtual void publish(int providerId, int tag=-1) = 0;

    // publish few structures from different providers
    void publishRange(int begin, int end, bool random=false, int tag=-1) {
        std::vector<int> v;
        for (int i = begin; i != end+1; ++ i) {
            v.push_back(i);
        }
        if (random) {
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(v.begin(), v.end(), g);
        }
        for (int i: v) {
            this->publish(i, tag);
        }
    }

    // publish few structures from different providers
    void publishList(std::initializer_list<int> providers, int tag=-1) {
        for (int i: providers) {
            this->publish(i, tag);
        }
    }
};


/**
 *  Implementation of InfoPublisher which records published data
 */
struct SimplePublisher : public InfoPublisher {

    // Callback called by container to publish accumulated data
    void publish(ISInfo& info, const std::string& name, int numUpdates) override {
        nCallbacks ++;
        nUpdates = numUpdates;
        ERS_LOG("callback: " << nCallbacks << " info: " << info << " name: " << name
                << " nUpdates: " << nUpdates);
        ISInfoAny any;
        any <<= info;
        any >>= results[name];
    }

    int nCallbacks = 0;  // number of times callback() was called
    int nUpdates = 0;  // number of updates in the last call to callback()
    std::map<std::string, ISInfoDynAny> results;  // latest info passed to callback()
};

/**
 *  Special fixture class for testing container types.
 */
template <typename Cont, typename ContFactory, typename DataFactory>
struct ContFixture : public PublishBase, public SimplePublisher {

    typedef Cont container_type;

    // publish one object for a given provider
    void publish(int providerId, int tag=-1) override {

        ISInfoAny info_any;
        auto data = dataFactory(providerId, tag);
        info_any <<= data;
        std::string provider = "provider" + std::to_string(providerId);
        ERS_LOG("Publishing " << provider);
        if (cont == nullptr) {
            cont = contFactory(*this);
        }
        cont->operate(info_any, provider);
    }

    std::unique_ptr<Cont> cont;
    ContFactory contFactory;
    DataFactory dataFactory;
};

/**
 * Fixture mix-in that checks number of publications and publishers.
 *
 * Typically used with ContFixture or its sub-class: PublishCheck<ContFixture<...>>
 */
template <typename Fixture>
struct PublishCheck : Fixture {

    struct CheckHelper {
        explicit CheckHelper(PublishCheck& fixture, int tag_=-1) : f(fixture), tag(tag_) {}

        CheckHelper& nCallbacks(int nCallbacks) {
            BOOST_TEST(f.nCallbacks == nCallbacks * f.callbacks_per_update);
            return *this;
        }
        CheckHelper& nUpdates(int nUpdates) {
            BOOST_TEST(f.nUpdates == nUpdates);
            return *this;
        }
        CheckHelper& nUpdatingProviders(int numUpdatingProviders) {
            if (f.cont != nullptr) {
                BOOST_TEST(f.cont->numUpdatingProviders(tag) == numUpdatingProviders);
            }
            return *this;
        }
        CheckHelper& nUpdatesSincePublish(int numUpdatesSincePublish) {
            if (f.cont != nullptr) {
                BOOST_TEST(f.cont->numUpdatesSincePublish(tag) == numUpdatesSincePublish);
            }
            return *this;
        }

        PublishCheck& f;
        int tag;
    };

    CheckHelper check_pubs(int tag=-1) { return CheckHelper(*this, tag); }

    void check_pubs(int nCallbacks, int nUpdates, int numUpdatingProviders,
                    int numUpdatesSincePublish, int tag=-1) {
        CheckHelper(*this, tag)
            .nCallbacks(nCallbacks)
            .nUpdates(nUpdates)
            .nUpdatingProviders(numUpdatingProviders)
            .nUpdatesSincePublish(numUpdatesSincePublish);
    }

    unsigned callbacks_per_update = 1;
};

/**
 * Fixture mix-in that checks annotations in published results.
 *
 * Typically used with ContFixture or its sub-class, requires `results`
 * data member defined as in SimplePublisher class.
 */
template <typename Fixture>
struct AnnotationsCheck : Fixture {
    void check_annotations(std::initializer_list<int> lvlCounts) {
        // only look at the first one, if there is more than one they are
        // supposed to be identical.
        auto iter = this->results.begin();
        BOOST_REQUIRE(iter != this->results.end());

        ISInfo::Annotations expected;
        unsigned level = 0;
        for (auto lvlCount: lvlCounts) {
            std::string key = level == 0 ? "providers" : "mig.lvl"+std::to_string(level);
            expected[key] = ISAnnotation(lvlCount);
            ++level;
        }

        auto&& anno = iter->second.annotations();
        BOOST_TEST(anno == expected, boost::test_tools::per_element());
    }
};

/**
 *  Special fixture class for testing handler types.
 */
template <typename HandlerConfigFactory, typename DataFactory>
struct HandlerFixture : public SimplePublisher {

    HandlerFixture() {
        const char* libs[] = {"libMIGContainers.so"};
        for (auto lib: libs) {
            LibraryManager::instance().loadLibrary(lib);
        }
    }

    void makeHandler() {
        handler = HandlerManager::instance().getHandler(hConfigFactory(), *this);
    }

    // publish few structures from different providers
    void publishRange(const std::string &name, int begin, int end,
                      bool random=false, int tag=-1) {
        std::vector<int> v;
        for (int i = begin; i != end+1; ++ i) {
            v.push_back(i);
        }
        if (random) {
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(v.begin(), v.end(), g);
        }
        for (int i: v) {
            publish(name, i, tag);
        }
    }

    void publishRange(const std::vector<std::string>& names, int begin, int end,
                      bool random=false, int tag=-1) {
        std::vector<int> v;
        for (int i = begin; i != end+1; ++ i) {
            v.push_back(i);
        }
        if (random) {
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(v.begin(), v.end(), g);
        }
        for (int i: v) {
            for (auto&& name: names) {
                publish(name, i, tag);
            }
        }
    }

    // publish one object for a given provider
    void publish(const std::string &name, int providerId, int tag=-1) {
        if (handler == nullptr) {
            makeHandler();
            if (handler == nullptr) {
                throw std::runtime_error("failed to make handler");
            }
        }
        ISInfoAny info_any;
        auto data = dataFactory(providerId, tag);
        info_any <<= data;
        std::string provider = "provider" + std::to_string(providerId);
        ERS_LOG("Publishing " << name << " from provider " << provider);
        handler->updateInfo(info_any, name, provider);
    }

    HandlerConfigFactory hConfigFactory;
    DataFactory dataFactory;
    std::shared_ptr<HandlerBase> handler;
};


}} // namespace MIG::test

#endif // MIG_TEST_UTILS_H
