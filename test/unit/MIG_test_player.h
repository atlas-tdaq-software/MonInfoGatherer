#ifndef MIG_TEST_PLAYER_H
#define MIG_TEST_PLAYER_H

#include <functional>
#include <string>
#include <vector>

#include "MIG_test_clock.h"
#include "../../src/containers/Scheduler.h"

namespace MIG {
namespace test {

struct Action {
    Action(Clock::time_point time_,
           std::function<void()> action_,
           std::string const& message_ = std::string())
        : time(time_), action(action_), message(message_) {}
    Action(Clock::duration time_,
           std::function<void()> action_,
           std::string const& message_ = std::string())
        : time(time_), action(action_), message(message_) {}

    Clock::time_point time;     // time when to execute an action
    std::function<void()> action; // function representing an action
    std::string message;        // message to print before execution
};


/**
 *
 */
class Player {
public:

    using duration = Clock::duration;
    using time_point = Clock::time_point;

    /**
     * @param task Task for periodic execution, may be nullptr
     */
    Player(TestClock& clock,
           std::function<void()> task = nullptr,
           duration task_period = duration::zero(),
           time_point task_start = time_point())
        : m_clock(clock),
          m_task(task),
          m_task_period(task_period),
          m_task_start(task_start)
    {}

    /**
     * Execute actions.
     *
     * @param actions Actions to execute.
     */
    void run(std::vector<Action> const& actions) {
        if (actions.empty()) {
            return;
        }
        // find starting point
        bool const run_task = m_task and m_task_period != duration::zero();
        time_point time = actions[0].time;
        time_point next_task_time = m_task_start;
        if (run_task and m_task_start < time) {
            next_task_time += int((time - m_task_start) / m_task_period) * m_task_period;
        }

        for (auto&& action: actions) {
            if (run_task) {
                while (next_task_time <= action.time) {
                    m_clock.set(next_task_time);
                    BOOST_TEST_MESSAGE(m_clock << ": execute periodic task");
                    m_task();
                    next_task_time += m_task_period;
                }
            }
            m_clock.set(action.time);
            BOOST_TEST_MESSAGE(m_clock << ": " << action.message);
            action.action();
        }
    }

private:

    TestClock& m_clock;
    std::function<void()> m_task;
    duration m_task_period;
    time_point m_task_start;
};

}} // namespace MIG::test

#endif // MIG_TEST_PLAYER_H
