//--------------------------------------------------------------------------
// Description:
//	Test suite for the PeriodFinder class.
//------------------------------------------------------------------------

#include <sstream>

#include "MonInfoGatherer/PeriodFinder.h"

#define BOOST_TEST_MODULE mig_unit_periodfinder
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace MIG;


namespace MIG::test {

template <>
class PeriodFinderFriendHack<int> {
public:
    size_t count(PeriodFinder& pf) const {return pf.m_count;}
    std::chrono::microseconds period_sum(PeriodFinder& pf) const {return pf.m_period_sum;}
    std::map<unsigned, size_t> const& histo(PeriodFinder& pf) const {return pf.m_histo;}
};

}

namespace {

struct PeriodFinderFixture {

    void init(PeriodFinder& pf) {
        pf.add(ts);
    }
    void add(PeriodFinder& pf, std::chrono::microseconds period, unsigned count=1) {
        for (unsigned i = 0; i != count; ++ i) {
            ts += period;
            pf.add(ts);
        }
    }

    std::chrono::microseconds ts{1000000000000UL};
    MIG::test::PeriodFinderFriendHack<int> hack;
};

} // namespace

// ==============================================================

BOOST_FIXTURE_TEST_CASE(test_simple, PeriodFinderFixture)
{
    // test with a fixed interval, all in one bin

    for (int interval: {2, 5, 10, 30, 180}) {
        PeriodFinder pf;
        this->init(pf);
        this->add(pf, std::chrono::seconds(interval), 1000);
        BOOST_TEST(this->hack.count(pf) == 1000);
        BOOST_TEST(this->hack.period_sum(pf).count() == 1000 * interval * 1000000UL);

        BOOST_TEST(pf.period().count() == interval);
    }
}

BOOST_FIXTURE_TEST_CASE(test_2bins, PeriodFinderFixture)
{
    // test with a fixed interval, two neighbor bins

    PeriodFinder pf;
    this->init(pf);

    this->add(pf, std::chrono::seconds(10), 300);
    this->add(pf, std::chrono::seconds(11), 500);

    BOOST_TEST(this->hack.count(pf) == 800);

    BOOST_TEST(pf.period().count() == 11);
}

BOOST_FIXTURE_TEST_CASE(test_3bins, PeriodFinderFixture)
{
    // test with a fixed interval, three neighbor bins

    PeriodFinder pf;
    this->init(pf);
    this->add(pf, std::chrono::seconds(10), 300);
    this->add(pf, std::chrono::seconds(11), 500);
    this->add(pf, std::chrono::seconds(12), 300);
    BOOST_TEST(this->hack.count(pf) == 1100);

    BOOST_TEST(pf.period().count() == 11);
}

BOOST_FIXTURE_TEST_CASE(test_lowstat, PeriodFinderFixture)
{
    // test with low counts, returns average and switches to mode

    PeriodFinder pf;
    this->init(pf);
    this->add(pf, std::chrono::seconds(1));
    this->add(pf, std::chrono::seconds(3));
    this->add(pf, std::chrono::seconds(7), 7);
    BOOST_TEST(this->hack.count(pf) == 9);
    BOOST_TEST(this->hack.period_sum(pf).count() == 53 * 1000000UL);
    BOOST_TEST(pf.period().count() == 6);

    // two more entries should trigger mode calculation
    this->add(pf, std::chrono::seconds(7), 2);
    BOOST_TEST(this->hack.count(pf) == 11);
    BOOST_TEST(this->hack.period_sum(pf).count() == 67 * 1000000UL);
    BOOST_TEST(pf.period().count() == 7);
}

BOOST_FIXTURE_TEST_CASE(test_avg, PeriodFinderFixture)
{
    // test case when there is no single peak

    PeriodFinder pf;
    this->init(pf);
    this->add(pf, std::chrono::seconds(1), 100);
    this->add(pf, std::chrono::seconds(7), 100);
    BOOST_TEST(this->hack.count(pf) == 200);
    BOOST_TEST(this->hack.period_sum(pf).count() == 800 * 1000000UL);
    BOOST_TEST(pf.period().count() == 4);

    // this also switches to average mode, resetting histo
    BOOST_TEST(this->hack.histo(pf).size() == 0);
}
