//--------------------------------------------------------------------------
// Description:
//	Test suite for ADHI-4688. Checks that automatic histograms axis resize
//  is supported by gatherer correctly.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <memory>
#include <TH1.h>
#include <TProfile.h>

#define BOOST_TEST_MODULE mig_unit_histo_resize
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infoany.h"
#include "oh/core/DataTypes.h"
#include "oh/OHRootProvider.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/DefaultHandler.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MIG_test_utils.h"

namespace tt = boost::test_tools;
namespace utf = boost::unit_test;
using namespace MIG;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace internal {

// this is defined in OHRootReceiver.cxx
template <class T>
TH1* convert( const std::string & name, const oh::HistogramData<T> & histogram);
TH1* convert( const std::string & name, const oh::ProfileData & pdata );
}

namespace {

uint32_t bunchInclusionTimeout_mksec = 10000;
uint32_t sleepTime_mksec = bunchInclusionTimeout_mksec + 2000;

class HistoFixtureBase : public InfoPublisher {
public:
    std::unique_ptr<ISInfoAny> histo2any(TH1& histo) {
        auto infoObj = OHRootProvider::convert(histo);
        ISProxy<>::set_tag(*infoObj, -1);
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(*infoObj).type();
        auto infoAny = std::make_unique<ISInfoAny>();
        *infoAny <<= *infoObj;
        return infoAny;
    }

    void check_histo(TH1& histo, int nbins, double xmin, double xmax, double mean, int entries) {
        BOOST_TEST(histo.GetNbinsX() == nbins);
        auto axis = histo.GetXaxis();
        BOOST_TEST(axis->GetXmin() == xmin);
        BOOST_TEST(axis->GetXmax() == xmax);
        BOOST_TEST(histo.GetMean() == mean);
        BOOST_TEST(histo.GetEntries() == entries);
    }
};

class HistoFixture : public HistoFixtureBase {
public:
    std::unique_ptr<TH1I> make_histo() {
        auto histogram = std::make_unique<TH1I>("th1i", "TH1I", 10, 0., 10.);
        histogram->SetDirectory(nullptr);
        histogram->SetCanExtend(TH1::kAllAxes);
        return histogram;
    }

    void fill(TH1I& histo, double x) {
        histo.Fill(x);
    }

    void check_bin_contents(TH1I& histo, std::vector<int> const& contents) {
        // checks include undeflow/overflow
        auto data = histo.GetArray();
        std::vector<int> bins(data, data+histo.GetSize());
        BOOST_TEST(bins == contents, tt::per_element());
    }

    void check_stats(TH1I& histo, std::vector<double> const& stats) {
        // checks include undeflow/overflow
        std::vector<double> hstats(4);
        histo.GetStats(hstats.data());
        BOOST_TEST(hstats == stats, tt::per_element());
    }

    std::unique_ptr<TH1I> info2h(ISInfo& info) {
        auto const& hdata = dynamic_cast<oh::HistogramData<int>&>(info);
        return std::unique_ptr<TH1I>(dynamic_cast<TH1I*>(internal::convert("name", hdata)));
    }

    // implements InfoPublisher interface
    void publish(ISInfo& info, const std::string& name, int nProv) override {
        BOOST_TEST_MESSAGE("HistoFixture::publish");
        ++ nPublish;
        sum = info2h(info);
    }

    static bool const is_profile = false;
    int nPublish = 0;
    std::unique_ptr<TH1I> sum;
};


class ProfileFixture : public HistoFixtureBase {
public:
    std::unique_ptr<TProfile> make_histo() {
        auto histogram = std::make_unique<TProfile>("tprof", "TProfile", 10, 0., 10.);
        histogram->SetDirectory(nullptr);
        histogram->SetCanExtend(TH1::kAllAxes);
        return histogram;
    }

    void fill(TProfile& histo, double x, double y=1.) {
        histo.Fill(x, y);
    }

    void check_bin_contents(TProfile& histo, std::vector<int> const& contents) {
        // checks include undeflow/overflow, assumes all y=1 in fill()
        double const* data = histo.GetArray();
        std::vector<int> bins(data, data+histo.GetSize());
        BOOST_TEST(bins == contents, tt::per_element());

        auto const& sumw2 = *histo.GetBinSumw2();
        if (sumw2.GetSize() != 0) {
            data = sumw2.GetArray();
            std::vector<int> sumw2bins(data, data+sumw2.GetSize());
            BOOST_TEST(sumw2bins == contents, tt::per_element());
        }

        std::vector<int> entries;
        for (int i = 0; i != histo.GetSize(); ++ i) {
            entries.push_back(int(histo.GetBinEntries(i)));
        }
        BOOST_TEST(entries == contents, tt::per_element());
    }

    void check_stats(TProfile& histo, std::vector<double> const& stats) {
        // checks include undeflow/overflow
        std::vector<double> hstats(6);
        histo.GetStats(hstats.data());
        BOOST_TEST(hstats == stats, tt::per_element());
    }

    std::unique_ptr<TProfile> info2h(ISInfo& info) {
        auto const& hdata = dynamic_cast<oh::ProfileData&>(info);
        return std::unique_ptr<TProfile>(dynamic_cast<TProfile*>(internal::convert("name", hdata)));
    }

    // implements InfoPublisher interface
    void publish(ISInfo& info, const std::string& name, int nProv) override {
        BOOST_TEST_MESSAGE("HistoFixture::publish");
        ++ nPublish;
        sum = info2h(info);
    }

    static bool const is_profile = true;
    int nPublish = 0;
    std::unique_ptr<TProfile> sum;
};

typedef boost::mpl::vector<HistoFixture, ProfileFixture> Fixtures2;

}


// ==============================================================

BOOST_TEST_DECORATOR(*utf::tolerance(0.0001))
BOOST_FIXTURE_TEST_CASE_TEMPLATE(test_root_histo_resize, F, Fixtures2, F)
{
    // this just tests how CanExtend works in ROOT

    auto histo1 = this->make_histo();
    BOOST_TEST_MESSAGE("Empty histogram has to have 10 bins");
    this->check_histo(*histo1, 10, 0., 10., 0., 0);

    BOOST_TEST_MESSAGE("Fill once in current range");
    this->fill(*histo1, 5.5);
    this->check_histo(*histo1, 10, 0., 10., 5.5, 1);
    this->check_bin_contents(*histo1, {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25});
    }

    BOOST_TEST_MESSAGE("Fill once in overflow");
    this->fill(*histo1, 15.5);
    this->check_histo(*histo1, 10, 0., 20., 10.5, 2);
    this->check_bin_contents(*histo1, {0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {2., 2., 21., 270.5, 2., 2.});
    } else {
        this->check_stats(*histo1, {2., 2., 21., 270.5});
    }

    BOOST_TEST_MESSAGE("Fill once more in overflow");
    this->fill(*histo1, 36.);
    this->check_histo(*histo1, 10, 0., 40., 19., 3);
    this->check_bin_contents(*histo1, {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {3., 3., 57., 1566.5, 3., 3.});
    } else {
        this->check_stats(*histo1, {3., 3., 57., 1566.5});
    }

    BOOST_TEST_MESSAGE("Fill once more in underflow");
    this->fill(*histo1, -1.);
    this->check_histo(*histo1, 10, -40., 40., 14., 4);
    this->check_bin_contents(*histo1, {0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {4., 4., 56., 1567.5, 4., 4.});
    } else {
        this->check_stats(*histo1, {4., 4., 56., 1567.5});
    }
}

BOOST_TEST_DECORATOR(*utf::tolerance(0.0001))
BOOST_FIXTURE_TEST_CASE_TEMPLATE(test_no_resize, F, Fixtures2, F)
{
    // Simplest case, both histograms are the same

    BOOST_TEST_MESSAGE("Empty histogram has to have 10 bins");
    auto histo1 = this->make_histo();
    this->fill(*histo1, 5.5);
    this->check_histo(*histo1, 10, 0., 10., 5.5, 1);
    this->check_bin_contents(*histo1, {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25});
    }

    auto histo2 = this->make_histo();
    this->fill(*histo2, 5.5);
    this->check_histo(*histo2, 10, 0., 10., 5.5, 1);
    this->check_bin_contents(*histo2, {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo2, {1., 1., 5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo2, {1., 1., 5.5, 30.25});
    }

    HandlerConfig hConfig(".*", "", {});
    DefaultHandler handler(hConfig, *this);

    auto infoAny = this->histo2any(*histo1);
    handler.updateInfo(*infoAny, "hname", "prov1");
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");

    // need to sleep to make sure
    usleep(sleepTime_mksec);

    // this should trigger publication
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");
    BOOST_TEST(this->nPublish == 1);
    this->check_histo(*this->sum, 10, 0., 10., 5.5, 2);
    this->check_bin_contents(*this->sum, {0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*this->sum, {2., 2., 11., 60.5, 2., 2.});
    } else {
        this->check_stats(*this->sum, {2., 2., 11., 60.5});
    }
}

BOOST_TEST_DECORATOR(*utf::tolerance(0.0001))
BOOST_FIXTURE_TEST_CASE_TEMPLATE(test_resize_right, F, Fixtures2, F)
{
    // one histogram is extended on right side
    BOOST_TEST_MESSAGE("Empty histogram has to have 10 bins");
    auto histo1 = this->make_histo();
    this->fill(*histo1, 5.5);
    this->check_histo(*histo1, 10, 0., 10., 5.5, 1);
    this->check_bin_contents(*histo1, {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25});
    }

    auto histo2 = this->make_histo();
    this->fill(*histo2, 15.5);
    this->check_histo(*histo2, 10, 0., 20., 15.5, 1);
    this->check_bin_contents(*histo2, {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo2, {1., 1., 15.5, 240.25, 1., 1.});
    } else {
        this->check_stats(*histo2, {1., 1., 15.5, 240.25});
    }

    HandlerConfig hConfig(".*", "", {});
    DefaultHandler handler(hConfig, *this);

    auto infoAny = this->histo2any(*histo1);
    handler.updateInfo(*infoAny, "hname", "prov1");
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");

    // need to sleep to make sure
    usleep(sleepTime_mksec);

    // this should trigger publication
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");
    BOOST_TEST(this->nPublish == 1);
    this->check_histo(*this->sum, 10, 0., 20., 10.5, 2);
    this->check_bin_contents(*this->sum, {0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*this->sum, {2., 2., 21., 270.5, 2., 2.});
    } else {
        this->check_stats(*this->sum, {2., 2., 21., 270.5});
    }

    infoAny = this->histo2any(*histo1);
    handler.updateInfo(*infoAny, "hname", "prov1");
    BOOST_TEST(this->nPublish == 2);
    this->check_histo(*this->sum, 10, 0., 20., 10.5, 2);
    this->check_bin_contents(*this->sum, {0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*this->sum, {2., 2., 21., 270.5, 2., 2.});
    } else {
        this->check_stats(*this->sum, {2., 2., 21., 270.5});
    }
}

BOOST_TEST_DECORATOR(*utf::tolerance(0.0001))
BOOST_FIXTURE_TEST_CASE_TEMPLATE(test_resize_left, F, Fixtures2, F)
{
    // one histogram is extended on left side
    BOOST_TEST_MESSAGE("Empty histogram has to have 10 bins");
    auto histo1 = this->make_histo();
    this->fill(*histo1, 5.5);
    this->check_histo(*histo1, 10, 0., 10., 5.5, 1);
    this->check_bin_contents(*histo1, {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo1, {1., 1., 5.5, 30.25});
    }

    auto histo2 = this->make_histo();
    this->fill(*histo2, -5.5);
    this->check_histo(*histo2, 10, -10., 10., -5.5, 1);
    this->check_bin_contents(*histo2, {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0});
    if (this->is_profile) {
        this->check_stats(*histo2, {1., 1., -5.5, 30.25, 1., 1.});
    } else {
        this->check_stats(*histo2, {1., 1., -5.5, 30.25});
    }

    HandlerConfig hConfig(".*", "", {});
    DefaultHandler handler(hConfig, *this);

    auto infoAny = this->histo2any(*histo1);
    handler.updateInfo(*infoAny, "hname", "prov1");
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");

    // need to sleep to make sure
    usleep(sleepTime_mksec);

    // this should trigger publication
    infoAny = this->histo2any(*histo2);
    handler.updateInfo(*infoAny, "hname", "prov2");
    BOOST_TEST(this->nPublish == 1);
    this->check_histo(*this->sum, 10, -10., 10., 0., 2);
    if (this->is_profile) {
        this->check_stats(*this->sum, {2., 2., 0., 60.5, 2., 2.});
    } else {
        this->check_stats(*this->sum, {2., 2., 0., 60.5});
    }

    infoAny = this->histo2any(*histo1);
    handler.updateInfo(*infoAny, "hname", "prov1");
    BOOST_TEST(this->nPublish == 2);
    this->check_histo(*this->sum, 10, -10., 10., 0., 2);
    if (this->is_profile) {
        this->check_stats(*this->sum, {2., 2., 0., 60.5, 2., 2.});
    } else {
        this->check_stats(*this->sum, {2., 2., 0., 60.5});
    }
}
