//--------------------------------------------------------------------------
// Description:
//      Test suite for ISDynAny*Cont classes
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <memory>
#include <vector>
#include <unistd.h>

#define BOOST_TEST_MODULE MIG_unit_cont_isdynanyng
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "../../src/algos/ISSum.h"
#include "../../src/containers/ISDynAnyNGCont.h"
#include "MIG_test_clock.h"
#include "MIG_test_player.h"
#include "MIG_test_utils.h"

#define TEST_MESSAGE(X) BOOST_TEST_MESSAGE(X << " (line:" << __LINE__ << ")")

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;
using namespace std::chrono_literals;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace MIG {

// special class with full access to container internals
template <typename T=int>
class MIGUnitTestFriendHack {
public:
    template <typename Cont>
    static void mess_up_providerMap(Cont* cont) {
        if (cont != nullptr) {
            BOOST_TEST_MESSAGE("messing up provider map with extra entries");
            cont->m_providerMap.index(".");
            cont->m_providerMap.index("*");
        }
    }
};

}

// needed for Boost to print in tt:per_element
std::ostream&
operator<<(std::ostream& out, std::pair<std::string, ISAnnotation> const& pair) {
    return out << '(' << pair.first << ", " << pair.second.data() << ')';
}

namespace {

// This is used to set ISDynAnyPublishDelay_ms parameter
auto publishDelay = 1500ms;

// This is used to call Task::execute() after this delay,
// should be longer than publishDelay to trigger result publishing
auto taskSchedulerDelay = 1000ms;

// delay between publication cycles by providers
auto providerCyclePeriod = 30s;

// recovery delay in old algorithm
auto bunchInclusionTimeout = 2500ms;

// Factory for ISInfoTest objects
struct DataFactory {
    ISInfoTest operator()(int providerId, int tag) {
        ISInfoTest data;
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        data.testValue = 1;
        // replace system current time with our clock time
        ISProxy<>::set_time(data, Clock::instance().now());
        if (do_final) {
            data.setAnnotation("FINAL", "FINAL");
        }
        for (unsigned lvl = 0; lvl != do_annotations.size(); ++lvl) {
            auto lvlCount = do_annotations[lvl];
            if (lvl == 0) {
                data.setAnnotation("providers", std::to_string(lvlCount));
            } else {
                data.setAnnotation("mig.lvl" + std::to_string(lvl), std::to_string(lvlCount));
            }
        }
        return data;
    }
    std::vector<int> do_annotations;
    bool do_final = false;
};

// factory for ISDynAnyNGCont objects
struct ContFactory {
    std::unique_ptr<ISDynAnyNGCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({
            "ISDynAnyPublishDelay_ms=" + std::to_string(publishDelay.count())});
        return std::make_unique<ISDynAnyNGCont>("ISInfoTest", ISInfoTest::type(),
                                                std::make_unique<ISSum>(),
                                                publisher, parameters);
    }
};

// factory for ISDynAnyNGCont which publishes averages
struct ContFactoryAvg {
    std::unique_ptr<ISDynAnyNGCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({
            "ISDynAnyPublishDelay_ms=" + std::to_string(publishDelay.count()),
            "PublishSum=0",
            "PublishAverage=1",
            "SuffixAverage="});
        return std::make_unique<ISDynAnyNGCont>("ISInfoTest", ISInfoTest::type(),
                                                std::make_unique<ISSum>(),
                                                publisher, parameters);
    }
};

// factory for ISDynAnyNGCont which publishes both sums and averages
struct ContFactorySumAvg {
    std::unique_ptr<ISDynAnyNGCont>
    operator()(InfoPublisher& publisher) {
        ConfigParameters parameters({
            "ISDynAnyPublishDelay_ms=" + std::to_string(publishDelay.count()),
            "PublishSum=1",
            "PublishAverage=1",
            "SuffixSum=_sum",
            "SuffixAverage=_avg"});
        return std::make_unique<ISDynAnyNGCont>("ISInfoTest", ISInfoTest::type(),
                                                std::make_unique<ISSum>(),
                                                publisher, parameters);
    }
};

// Checks value of the merged results in ISDynAnyNGCont container
struct ISDynAnyFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyNGCont, ContFactory, DataFactory>>>> {
    void check_result() {
        auto iter = results.find("ISInfoTest");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        double expected = cont->numUpdatingProviders();
        BOOST_TEST(value == expected);
    }
};

// Checks value of the merged results in ISDynAnyAvgCont container
struct ISDynAnyAvgFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyNGCont, ContFactoryAvg, DataFactory>>>> {
    void check_result() {
        auto iter = results.find("ISInfoTest");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        // for average we should always have 1
        BOOST_TEST(value == 1.);
    }
};

// Checks value of the merged results in ISDynAnySumAvgCont container
struct ISDynAnySumAvgFixture : TClockFixture<AnnotationsCheck<PublishCheck<ContFixture<ISDynAnyNGCont, ContFactorySumAvg, DataFactory>>>> {

    ISDynAnySumAvgFixture() { callbacks_per_update = 2; }

    void check_result() {

        auto iter = results.find("ISInfoTest_sum");
        BOOST_REQUIRE(iter != results.end());
        double value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        double expected = cont->numUpdatingProviders();
        BOOST_TEST(value == expected);

        iter = results.find("ISInfoTest_avg");
        BOOST_REQUIRE(iter != results.end());
        value = iter->second.getAttributeValue<double>(ISInfoTest::testval_pos);
        // for average we should always have 1
        BOOST_TEST(value == 1.);
    }
};

typedef boost::mpl::vector<ISDynAnyFixture, ISDynAnyAvgFixture, ISDynAnySumAvgFixture> Fixtures;

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test, F, Fixtures, F)
{
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                BOOST_CHECK(this->cont == nullptr);
                this->check_pubs()
                    .nCallbacks(0)
                    .nUpdates(0);
            },
            "Initial check"
        }, {
            time0,
            [this]() {
                this->publish(1);
                BOOST_CHECK(this->cont != nullptr);
                this->check_pubs(0, 0, 1, 1);
            },
            "Publish provider 1"
        }, {
            time0,
            [this]() {
                this->publish(2);
                this->check_pubs(0, 0, 2, 2);
            },
            "Publish provider 2"
        }, {
            time0,
            [this]() {
                this->publish(3);
                this->check_pubs(0, 0, 3, 3);
            },
            "Publish provider 3"
        }, {
            time0 + ::taskSchedulerDelay,
            [this]() { this->check_pubs().nCallbacks(0); },
            "Verify no callbacks"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 3, 3, 0);
                this->check_result();
            },
            "Verify first callback"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(1);
                this->check_pubs(1, 3, 3, 1);
            },
            "Publish provider 1"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(2);
                this->check_pubs(1, 3, 3, 2);
            },
            "Publish provider 2"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(3);
                this->check_pubs(2, 3, 3, 0);
                this->check_result();
            },
            "Publish provider 3, triggers merge"
        }, {
            time0 + ::providerCyclePeriod + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(2, 3, 3, 0);
            },
            "No changes"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publish(1);
                this->publish(1);
                this->check_pubs(2, 3, 3, 2);
            },
            "Publish provider 1 twice"
        }, {
            time0 + 2 * ::providerCyclePeriod + ::taskSchedulerDelay,
            [this]() { this->check_pubs(2, 3, 3, 2); },
            "No merge yet"
        }, {
            time0 + 2 * ::providerCyclePeriod + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(3, 3, 3, 0);
                this->check_result();
            },
            "Merge with provider1"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(new_provider_1, F, Fixtures, F)
{
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_result();
            },
            "Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_result();
            },
            "Publish providers 0-6 again"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                // add new provider, make sure it's not last in the bunch
                this->publishRange(0, 2, true);
                this->publish(7);
                this->publishRange(3, 6, true);
                this->check_pubs(3, 8, 8, 0);
                this->check_result();
            },
            "Publish providers 0-7"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 7, true);
                this->check_pubs(4, 8, 8, 0);
                this->check_result();
            },
            "Publish providers 0-7 again"
        }, {
            time0 + 4 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 7, true);
                this->check_pubs(5, 8, 8, 0);
                this->check_result();
            },
            "Publish providers 0-7 once again"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(miss_provider_1, F, Fixtures, F)
{
    // Test for provider going missing, in current implementation
    // its latest publication is used for for few update cycles
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_result();
            },
            "Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_result();
            },
            "Publish providers 0-6 again"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 5, true);
                this->check_pubs(2, 7, 7, 6);
            },
            "Publish providers 0-5, 6 is dead"
        }, {
            time0 + 2 * ::providerCyclePeriod + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(3, 7, 7, 0);
                this->check_result();
            },
            "Verify next merge, still 7 providers"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 5, true);
                this->check_pubs(3, 7, 7, 6);
            },
            "Publish providers 0-5, no merge"
        }, {
            time0 + 3 * ::providerCyclePeriod + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(4, 7, 7, 0);
                this->check_result();
            },
            "Verify next merge, still 7 providers"
        }, {
            time0 + 4 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 5, true);
            },
            "Publish providers 0-5, still 7 providers"
        }, {
            time0 + 4 * ::providerCyclePeriod + + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(5, 6, 6, 0);
                this->check_result();
            },
            "Should realize provider 6 is dead"
        }, {
            time0 + 5 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 5, true);
                this->check_pubs(6, 6, 6, 0);
                this->check_result();
            },
            "Publish providers 0-5"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(miss_provider_2, F, Fixtures, F)
{
    // check that non-synchronized publishing does not cause missing providers

    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_result();
            },
            "Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_result();
            },
            "Publish providers 0-6 again"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publish(0);
                this->check_pubs().nCallbacks(2);
            },
            "Publish provider 0"
        }, {
            time0 + 2 * ::providerCyclePeriod + 2 * ::taskSchedulerDelay,
            [this]() {
                this->publish(1);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 1"
        }, {
            time0 + 2 * ::providerCyclePeriod + 4 * ::taskSchedulerDelay,
            [this]() {
                this->publish(2);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 2"
        }, {
            time0 + 2 * ::providerCyclePeriod + 6 * ::taskSchedulerDelay,
            [this]() {
                this->publish(3);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 3"
        }, {
            time0 + 2 * ::providerCyclePeriod + 8 * ::taskSchedulerDelay,
            [this]() {
                this->publish(4);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 4"
        }, {
            time0 + 2 * ::providerCyclePeriod + 10 * ::taskSchedulerDelay,
            [this]() {
                this->publish(5);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 5"
        }, {
            time0 + 2 * ::providerCyclePeriod + 12 * ::taskSchedulerDelay,
            [this]() {
                this->publish(6);
                this->check_pubs().nCallbacks(3);
            },
            "Publish provider 6"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->check_pubs().nCallbacks(4);
            },
            "Verify next merge"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(5, 7, 7, 0);
                this->check_result();
            },
            "Publish providers 0-6"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(single_provider, F, Fixtures, F)
{
    // Test for the case with  a single provider (some ipc_proxies)

    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publish(0);
                this->check_pubs(0, 0, 1, 1);
            },
            "Publish provider 0"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 1, 1, 0);
                this->check_result();
            },
            "Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(0);
                this->check_pubs(2, 1, 1, 0);
                this->check_result();
            },
            "Publish provider 0"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publish(0);
                this->check_pubs(3, 1, 1, 0);
                this->check_result();
            },
            "Publish provider 0"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

/*
 * test case for the problem reported in
 * https://its.cern.ch/jira/browse/ATDSUPPORT-164 Reproduces the sequence
 * similar to what is defined in tools/isPublish.py
 *
 * TODO: This test probably does not matter anymore with the latest
 * implementation, maybe best to drop it entirely.
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_atdsupport_164, F, Fixtures, F)
{
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_result();
            },
            "Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_result();
            },
            "Publish providers 0-6"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                // should add new provider
                this->publish(7);
                this->check_pubs(2, 7, 8, 1);
            },
            "Publish provider 7"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                // causes immediate publication
                this->publishRange(0, 6, true);
                this->check_pubs(3, 8, 8, 0);
                this->check_result();
            },
            "Publish providers 0-6"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publish(8);
                this->check_pubs(3, 8, 9, 1);
            },
            "Publish provider 8"
        }, {
            time0 + 2 * ::providerCyclePeriod + ::bunchInclusionTimeout / 2,
            [this]() {
                ERS_LOG("Last in sequence above and Delayed transaction next");
                this->publish(9);
                this->check_pubs(3, 8, 10, 2);
            },
            "Publish provider 9"
        }, {
            time0 + 2 * ::providerCyclePeriod + 2 * ::taskSchedulerDelay,
            [this]() {
                this->publish(9);
                this->check_pubs(3, 8, 10, 3);
            },
            "Publish provider 9"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->check_pubs(4, 10, 10, 0);
                this->check_result();
            },
            "Verify next merge"
        }, {
            time0 + 3 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 9, true);
                this->check_pubs(5, 10, 10, 0);
                this->check_result();
            },
            "Publish providers 0-9"
        }, {
            time0 + 4 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 9, true);
                this->check_pubs(6, 10, 10, 0);
                this->check_result();
            },
            "Publish providers 0-9"
        }, {
            time0 + 5 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 9, true);
                this->check_pubs(7, 10, 10, 0);
                this->check_result();
            },
            "Publish providers 0-9"
        }, {
            time0 + 6 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 8, true);
                this->check_pubs(7, 10, 10, 9);
            },
            "Publish providers 0-8, 9 is missing"
        }, {
            time0 + 7 * ::providerCyclePeriod,
            [this]() {
                this->check_pubs(8, 10, 10, 0);
                this->publishRange(0, 8, true);
                this->check_pubs(8, 10, 10, 9);
            },
            "Publish providers 0-8"
        }, {
            time0 + 8 * ::providerCyclePeriod,
            [this]() {
                this->check_pubs(9, 10, 10, 0);
                this->publishRange(0, 8, true);
                this->check_pubs(9, 10, 10, 9);
            },
            "Publish providers 0-8"
        }, {
            time0 + 9 * ::providerCyclePeriod,
            [this]() {
                this->check_pubs(10, 9, 9, 0);
                this->publishRange(0, 8, true);
                this->check_pubs(11, 9, 9, 0);
            },
            "Publish providers 0-8"
        }, {
            time0 + 10 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 4, true);
                this->publish(9);
                this->check_pubs(11, 9, 10, 6);
                this->publishRange(5, 8, true);
                this->check_pubs(12, 10, 10, 0);
            },
            "Publish providers 0-4 + 9, then 5-8"
        }, {
            time0 + 9 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 9, true);
                this->check_pubs(13, 10, 10, 0);
            },
            "Publish providers 0-9"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing negative time difference
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_timeshift, F, Fixtures, F)
{
    std::chrono::microseconds mksec_off = - bunchInclusionTimeout / 2;

    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 3, true);
                this->check_pubs(0, 0, 4, 4);
            },
            "Cycle0: Publish providers 0-3"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 4, 4, 0);
                this->check_result();
            },
            "Cycle0: Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(0);
                this->check_pubs(1, 4, 4, 1);
            },
            "Cycle1: Publish provider 0"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(4);
                this->check_pubs(1, 4, 5, 2);
            },
            "Cycle1: Publish provider 4"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(1, 3, true);
                this->check_pubs(2, 5, 5, 0);
            },
            "Cycle1: Publish providers 1-3"
        }, {
            time0 + ::providerCyclePeriod - mksec_off,
            [this]() {
                this->publish(5);
                this->check_pubs(2, 5, 6, 1);
            },
            "Cycle1: New provider 5 (negative offset triggers a bug in old code)"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishList({6, 7});
                this->check_pubs(2, 5, 8, 3);
            },
            "Cycle1: Publish providers 6-7"
        }, {
            // this have to wait for > half period
            time0 + ::providerCyclePeriod + ::providerCyclePeriod / 2 + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(3, 8, 8, 0);
                this->check_result();
            },
            "Verify merge"
        }, {
            time0 + 2 * ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(3, 8, 8, 7);
                this->publish(7);
                this->check_pubs(4, 8, 8, 0);
                this->check_result();
            },
            "Cycle2: Publish providers 0-6 + 7"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing providers counting
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_providercount, F, Fixtures, F)
{
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 3, true);
                this->check_pubs(0, 0, 4, 4);
            },
            "Cycle0: Publish providers 0-3"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 4, 4, 0);
                this->check_result();
            },
            "Cycle0: Verify first merge"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                MIGUnitTestFriendHack<>::mess_up_providerMap(this->cont.get());
            },
            "Cycle1: mess_up_providerMap"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(4);
                this->check_pubs(1, 4, 5, 1);
            },
            "Cycle1: Publish provider 4"
        }, {
            time0 + ::providerCyclePeriod + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(2, 5, 5, 0);
                this->check_result();
            },
            "Cycle1: Verify merge"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

/*
 * test case for the problem in
 * https://its.cern.ch/jira/browse/ADHI-4578,
 * testing duplicate subscription
 */
BOOST_FIXTURE_TEST_CASE_TEMPLATE(ticket_adhi_4578_dups, F, Fixtures, F)
{
    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 3, true);
                this->check_pubs(0, 0, 4, 4);
            },
            "Cycle0: Publish providers 0-3"
        }, {
            time0,
            [this]() {
                this->publish(1);
                this->check_pubs(1, 4, 4, 0);
                this->check_result();
            },
            "Cycle0: Publish provider 1 (duplicate notification)"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 3, true);
                this->check_pubs(2, 4, 4, 0);
                this->check_result();
            },
            "Cycle1: Publish providers 0-3"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publish(1);
                this->check_pubs(2, 4, 4, 1);
            },
            "Cycle1: Publish provider 1 (duplicate notification)"
        }, {
            time0 + ::providerCyclePeriod + ::providerCyclePeriod / 2 + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(3, 4, 4, 0);
                this->check_result();
            },
            "Cycle1: Verify merge"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(annotations1, F, Fixtures, F)
{
    // test for annotations, case when objects come without annotations,
    // in that case gatherer should add single level of annotations

    this->dataFactory.do_annotations = {};

    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_annotations({7});
            },
            "Verify 1"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_annotations({7});
            },
            "Publish providers 0-6"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(annotations2, F, Fixtures, F)
{
    // test for annotations, case when objects come with annotations,
    // in that case gatherer should add extra level of annotations

    this->dataFactory.do_annotations = {100, 10};

    Clock::time_point const time0(1000s);

    std::vector<Action> actions({
        {
            time0,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(0, 0, 7, 7);
            },
            "Publish providers 0-6"
        }, {
            time0 + ::publishDelay + ::taskSchedulerDelay,
            [this]() {
                this->check_pubs(1, 7, 7, 0);
                this->check_annotations({700, 70, 7});
            },
            "Verify 1"
        }, {
            time0 + ::providerCyclePeriod,
            [this]() {
                this->publishRange(0, 6, true);
                this->check_pubs(2, 7, 7, 0);
                this->check_annotations({700, 70, 7});
            },
            "Publish providers 0-6"
        }
    });

    auto periodic_task = [this]() {
        if (this->cont) this->cont->execute();
    };
    Player player(this->tclock, periodic_task, ::taskSchedulerDelay, time0 - 100ms);
    player.run(actions);
}
