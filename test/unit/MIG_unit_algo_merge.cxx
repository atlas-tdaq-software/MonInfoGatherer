//--------------------------------------------------------------------------
// Description:
//	Test suite case for many-to-one bin merging.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>

#define BOOST_TEST_MODULE MIG_unit_algo_merge
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MonInfoGatherer/LabelMap.h"
#include "../../src/algos/HistoAlgorithm.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

// Fixture that uses HistoAlgorithm
struct HistoAlgoFixture {
    void operate(oh::ObjectBase<oh::Histogram>& src, oh::ObjectBase<oh::Histogram>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap;
        algo.operate(src, dst, lblMap);
    }
    void operate(oh::ObjectBase<oh::Efficiency>& src, oh::ObjectBase<oh::Efficiency>& dst) {
        HistoAlgorithm algo;
        LabelMap lblMap;
        algo.operate(src, dst, lblMap);
    }
};

}


BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_same_bins, HistoAlgoFixture)
{
    // same axes, that should just use AddAlgorithm.inc
    // which sums underflow/overflow bins
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_same_bins_with_err, HistoAlgoFixture)
{
    // same axes, that should just use AddAlgorithm.inc
    // which sums underflow/overflow bins
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1}, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1, 2, 6, 7, 6};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_same_bins_mixed_err1, HistoAlgoFixture)
{
    // same axes, that should just use AddAlgorithm.inc
    // which sums underflow/overflow bins
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1}, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1, 2, 6, 7, 6};
    int expectErr[] = {0, 0, 3, 3, 1};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expectErr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_same_bins_mixed_err2, HistoAlgoFixture)
{
    // same axes, that should just use AddAlgorithm.inc
    // which sums underflow/overflow bins
    std::vector<oh::Axis> axes({
        makeAxis(3, "X", {0., 10.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes, {0, 0, 3, 3, 1});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1, 2, 6, 7, 6};
    int expectErr[] = {1, 2, 3, 4, 5};
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expectErr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which does not sum underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_with_err, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which does not sum underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->errors.size(), 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_mixed_err_1, HistoAlgoFixture)
{
    // same as previous but only one histo has errors
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 15);
    BOOST_TEST(dst->entries == 22);
    BOOST_TEST(src->get_bins_size() == 8);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.empty());
    BOOST_TEST(dst->errors.size() == 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i]);
    }
    double expecterr[] = {1000, 0, 3, 3, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_src_mixed_err_2, HistoAlgoFixture)
{
    // same as previous but only one histo has errors
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 15);
    BOOST_TEST(dst->entries == 22);
    BOOST_TEST(src->get_bins_size() == 8);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.size() == 8);
    BOOST_TEST(dst->errors.size() == 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i]);
    }
    double expecterr[] = {0, 5, 9, 13, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_src, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 7);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {0, 5, 12, 16, 0};           // under/overflow is reset
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_with_err, HistoAlgoFixture)
{
    // src bins are wider, this should use actual merge algo
    // which also resets underflow/overflow bins,
    // dst histogram contents will be replaced to have wider bins
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_src, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 7);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {0, 5, 12, 16, 0};           // under/overflow is reset
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_mixed_err_1, HistoAlgoFixture)
{
    // same as previous but only one histo has errors
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_src, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 7);
    BOOST_TEST(dst->entries == 22);
    BOOST_TEST(src->get_bins_size() == 5);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.empty());
    BOOST_TEST(dst->errors.size() == 5);
    int expect[] = {0, 5, 12, 16, 0};           // under/overflow is reset
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i]);
    }
    double expecterr[] = {0, 5, 9, 13, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_1D_merge_dst_mixed_err_2, HistoAlgoFixture)
{
    // same as previous but only one histo has errors
    std::vector<oh::Axis> axes_src({
        makeAxis(3, "X", {0., 10.})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(6, "X", {0., 5.})
    });
    auto src = makeHist<int>(oh::Histogram::D1, "h1d", 7, axes_src, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});
    auto dst = makeHist<int>(oh::Histogram::D1, "h1d", 15, axes_dst, {1, 2, 3, 4, 5, 6, 7, 8});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_TEST(src->entries == 7);
    BOOST_TEST(dst->entries == 22);
    BOOST_TEST(src->get_bins_size() == 5);
    BOOST_TEST(dst->get_bins_size() == 5);
    BOOST_TEST(src->errors.size() == 5);
    BOOST_TEST(dst->errors.size() == 5);
    int expect[] = {0, 5, 12, 16, 0};           // under/overflow is reset
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->get_bins_array()[i] == expect[i]);
    }
    double expecterr[] = {0, 0, 3, 3, 0};
    for (int i = 0; i != 5; ++i) {
        BOOST_TEST(dst->errors[i] == expecterr[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_Prof1D_merge_src, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which does not sum underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeProf(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeProf(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->bin_entries[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_Efficiency1D_merge_src, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which does not sum underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeEffi(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeEffi(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->total.entries, 15);
    BOOST_CHECK_EQUAL(dst->total.entries, 22);
    BOOST_CHECK_EQUAL(dst->passed.entries, 22);
    BOOST_CHECK_EQUAL(src->total.get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->total.get_bins_size(), 5);
    BOOST_CHECK_EQUAL(dst->passed.get_bins_size(), 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->total.get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->passed.get_bins_array()[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_Prof1D_merge_src_with_err, HistoAlgoFixture)
{
    // dst bins are wider, this should use actual merge algo
    // which does not sum underflow/overflow bins
    std::vector<oh::Axis> axes_src({
        makeAxis(6, "X", {0., 5.})   // nbins, name, {low_ege, bin_width}
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(3, "X", {0., 10.})   // nbins, name, {low_ege, bin_width}
    });
    auto src = makeProf(oh::Histogram::D1, "h1d", 15, axes_src, {1, 2, 3, 4, 5, 6, 7, 8},
                        {1, 2, 3, 4, 5, 6, 7, 8}, {1, 2, 3, 4, 5, 6, 7, 8});
    auto dst = makeProf(oh::Histogram::D1, "h1d", 7, axes_dst, {1000, 0, 3, 3, 2000},
                        {1000, 0, 3, 3, 2000}, {1000, 0, 3, 3, 2000});

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));

    BOOST_CHECK_EQUAL(src->entries, 15);
    BOOST_CHECK_EQUAL(dst->entries, 22);
    BOOST_CHECK_EQUAL(src->get_bins_size(), 8);
    BOOST_CHECK_EQUAL(dst->get_bins_size(), 5);
    int expect[] = {1000, 5, 12, 16, 2000};     // under/overflow is preserved
    for (int i = 0; i != 5; ++i) {
        BOOST_CHECK_EQUAL(dst->get_bins_array()[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->bin_entries[i], expect[i]);
        BOOST_CHECK_EQUAL(dst->errors[i], expect[i]);
    }
}

BOOST_FIXTURE_TEST_CASE(test_MergeAlgorithm_2D_with_labels, HistoAlgoFixture)
{
    // test case for ADHI-4823, 2D histo with Y axis labelled and
    // X axis with kCanRebin option, same number of bins in both but
    // one histo X bin width is 2**n wider than other.

    unsigned const nbins_x = 100;
    unsigned const nbins_y = 3;

    std::vector<oh::Axis> axes_src({
        makeAxis(nbins_x, "X", {0., .1}),
        makeAxis(nbins_y, "Y", {-0.5, nbins_y-0.5}, {"A", "B", "C"})
    });
    std::vector<oh::Axis> axes_dst({
        makeAxis(nbins_x, "X", {0., .1 * 128}),
        makeAxis(nbins_y, "Y", {-0.5, nbins_y-0.5}, {"A", "B", "C"})
    });

    unsigned const nbins = (nbins_x + 2) * (nbins_y + 2);
    std::vector<double> data(nbins);
    auto src = makeHist<double>(oh::Histogram::D2, "h2d", 0, axes_src, data);
    auto dst = makeHist<double>(oh::Histogram::D2, "h2d", 0, axes_dst, data);

    BOOST_REQUIRE_NO_THROW(this->operate(*src, *dst));
}
