//--------------------------------------------------------------------------
// Description:
//	Test suite case for the MIG IS algos.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

#define BOOST_TEST_MODULE MIG_unit_algo_is
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "../../src/algos/ISSum.h"
#include "MIG_test_utils.h"

using namespace MIG;
using namespace MIG::test;
namespace tt = boost::test_tools;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

// ==============================================================

BOOST_AUTO_TEST_CASE(test_ISSum1)
{
    using Item = int;
    auto info1 = makeInfo<Item>(2);
    auto dyn1 = makeInfoDynAny(*info1);
    auto info2 = makeInfo<Item>(3);
    auto dyn2 = makeInfoDynAny(*info2);

    ISSum algo;
    BOOST_REQUIRE_NO_THROW(algo.operate(*dyn1, *dyn2));
    BOOST_CHECK_EQUAL(dyn1->getAttributeValue<Item>(0), 2);
    BOOST_CHECK_EQUAL(dyn2->getAttributeValue<Item>(0), 5);
}

BOOST_AUTO_TEST_CASE(test_ISSum2)
{
    // strings are not "summed" by ISSum
    using Item = std::string;
    auto info1 = makeInfo<Item>("ab");
    auto dyn1 = makeInfoDynAny(*info1);
    auto info2 = makeInfo<Item>("cd");
    auto dyn2 = makeInfoDynAny(*info2);

    ISSum algo;
    BOOST_REQUIRE_NO_THROW(algo.operate(*dyn1, *dyn2));
    BOOST_CHECK_EQUAL(dyn1->getAttributeValue<Item>(0), "ab");
    BOOST_CHECK_EQUAL(dyn2->getAttributeValue<Item>(0), "cd");
}

// ISSum does not detect any error conditions, it will just crash

BOOST_AUTO_TEST_CASE(test_ISSum_NoConcat)
{
    using Item = std::vector<int>;
    auto info1 = makeInfo<Item>(Item({1, 2, 3}));
    auto dyn1 = makeInfoDynAny(*info1);
    auto info2 = makeInfo<Item>(Item({4, 5, 6}));
    auto dyn2 = makeInfoDynAny(*info2);

    ConfigParameters cfg({"Concatenate=0"});
    ISSum algo(cfg);
    BOOST_REQUIRE_NO_THROW(algo.operate(*dyn1, *dyn2));
    BOOST_TEST(dyn1->getAttributeValue<Item>(0) == Item({1, 2, 3}), tt::per_element());
    BOOST_TEST(dyn2->getAttributeValue<Item>(0) == Item({4, 5, 6}), tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_ISSum_Concat)
{
    using Item = std::vector<int>;
    auto info1 = makeInfo<Item>(Item({1, 2, 3}));
    auto dyn1 = makeInfoDynAny(*info1);
    auto info2 = makeInfo<Item>(Item({4, 5, 6}));
    auto dyn2 = makeInfoDynAny(*info2);

    ConfigParameters cfg({"Concatenate=1"});
    ISSum algo(cfg);
    BOOST_REQUIRE_NO_THROW(algo.operate(*dyn1, *dyn2));
    BOOST_TEST(dyn1->getAttributeValue<Item>(0) == Item({1, 2, 3}), tt::per_element());
    BOOST_TEST(dyn2->getAttributeValue<Item>(0) == Item({4, 5, 6, 1, 2, 3}), tt::per_element());
}
