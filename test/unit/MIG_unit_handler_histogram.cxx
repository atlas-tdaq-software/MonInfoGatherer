//------------------------------------------------------------------------
// Description:
//      Test suite for Histogram handler classes
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <memory>
#include <vector>
#include <string>
#include <unistd.h>

#define BOOST_TEST_MODULE MIG_unit_handler_histogram
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "MonInfoGatherer/HandlerBase.h"
#include "MIG_test_clock.h"
#include "MIG_test_utils.h"
#include "oh/core/HistogramData.h"

using namespace MIG;
using namespace MIG::test;

/**
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

// recovery delay in old algorithm
std::chrono::milliseconds bunchInclusionTimeout(2500);

// delay between publication cycles by providers
std::chrono::milliseconds providerCyclePeriod(5000);

using Histogram = oh::HistogramData<int>;
const unsigned entries_idx = 4;  // index of entries attribute in Histogram info object
const unsigned entries_per_hist = 15;


// Factory for HandlerConfig objects
struct HandlerConfigFactory {
    std::string name;
    HandlerConfigFactory(const std::string& name_="Default") : name(name_) {}
    HandlerConfig operator()() {
        std::vector<std::string> parameters;
        parameters.push_back("EOPRecoveryDelay_ms="+std::to_string(bunchInclusionTimeout.count()));
        // tests here only work with old-style ISDynAnyCont, so disable NG thingy
        parameters.push_back("ISDynAnyNG=");
        HandlerConfig cfg("", name, parameters);
        return cfg;
    }
};


// Factory for Histogram objects
struct DataFactory {
    Histogram operator()(int providerId, int tag=-1) {
        std::vector<oh::Axis> axes({
            makeAxis(3, "X", {0., 10.})
        });
        auto data = *makeHist<int>(oh::Histogram::D1, "h1d", entries_per_hist, axes, {1, 2, 3, 4, 5});
        ISProxy<>::set_tag(data, tag);
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        if (do_final) {
            data.setAnnotation("FINAL", "FINAL");
        }
        unsigned level = 0;
        for (auto lvlCount: do_annotations) {
            std::string key = level == 0 ? "providers" : "mig.lvl"+std::to_string(level);
            data.setAnnotation(key, lvlCount);
            ++level;
        }
        // replace system current time with our clock time
        ISProxy<>::set_time(data, Clock::instance().now());
        return data;
    }

    std::vector<int> do_annotations;
    bool do_final = false;
};

// Factory for ISInfoTest objects
struct ISDataFactory {
    ISInfoTest operator()(int providerId, int /*tag*/) {
        ISInfoTest data;
        // this is necessary to initialize ISType object inside info
        static_cast<ISInfo&>(data).type();
        data.testValue = 1;
        // replace system current time with our clock time
        ISProxy<>::set_time(data, Clock::instance().now());
        return data;
    }
};

// Checks entries in merged result in HistogramCont container
template <typename Fixture>
struct ResultCheck : Fixture {
    void check(const std::string& name, int tag=-1) {
        auto iter = this->results.find(name);
        BOOST_CHECK(iter != this->results.end());
        if (iter != this->results.end()) {
            double entries = iter->second.template getAttributeValue<decltype(oh::Histogram::entries)>(entries_idx);
            double expected = this->nUpdates * entries_per_hist;
            BOOST_CHECK_MESSAGE(entries == expected,
                    "entries == expected [" << entries << " == " << expected << "]");
        }
    }
};

// Checks value of the merged results in ISDynAnyCont container
template <typename Fixture>
struct ISResultCheck : Fixture{
    void check(const std::string& name) {
        auto iter = this->results.find(name);
        BOOST_CHECK(iter != this->results.end());
        if (iter != this->results.end()) {
            double value = iter->second.template getAttributeValue<double>(ISInfoTest::testval_pos);
            double expected = this->nUpdates;
            BOOST_CHECK_MESSAGE(value == expected,
                    "value == expected [" << value << " == " << expected << "]");
        }
    }
};

using HistogramHandlerFixture = TClockFixture<ResultCheck<HandlerFixture<HandlerConfigFactory, DataFactory>>>;
using HistogramHandlerISFixture = TClockFixture<ISResultCheck<HandlerFixture<HandlerConfigFactory, ISDataFactory>>>;

typedef boost::mpl::vector<HistogramHandlerFixture, HistogramHandlerISFixture> Fixtures;

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(basic_test, F, Fixtures, F)
{
    // simple test with one histogram
    BOOST_REQUIRE(this->handler == nullptr);

    BOOST_CHECK_EQUAL(this->nCallbacks, 0);
    BOOST_CHECK_EQUAL(this->nUpdates, 0);
    this->publish("/histo1", 1);

    BOOST_REQUIRE(this->handler != nullptr);

    this->publish("/histo1", 2);
    this->publish("/histo1", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    this->tclock += providerCyclePeriod;

    // repeating provider should cause result publishing
    this->publish("/histo1", 1);
    BOOST_CHECK_EQUAL(this->nCallbacks, 1);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("/histo1");

    this->publish("/histo1", 2);
    BOOST_CHECK_EQUAL(this->nCallbacks, 1);

    this->tclock += providerCyclePeriod;

    // all known providers, should cause result publishing
    this->publish("/histo1", 3);
    BOOST_CHECK_EQUAL(this->nCallbacks, 2);
    BOOST_CHECK_EQUAL(this->nUpdates, 3);
    this->check("/histo1");
}

BOOST_FIXTURE_TEST_CASE_TEMPLATE(many_items, F, Fixtures, F)
{
    // Test for multiple objects

    this->publishRange({"a", "b", "c"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 0);

    this->tclock += providerCyclePeriod;

    this->publishRange({"c", "a", "b"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 6);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");

    this->tclock += providerCyclePeriod;

    this->publishRange({"a", "c", "b"}, 0, 6, true);
    BOOST_CHECK_EQUAL(this->nCallbacks, 9);
    BOOST_CHECK_EQUAL(this->nUpdates, 7);
    this->check("a");
    this->check("b");
    this->check("c");
}
