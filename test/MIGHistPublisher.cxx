/*
 root_provider -> cxx

 A simple utility which can publish root histograms from a file or
 a randomly generated histogram to the OH.

 Fredrik Sandin, Nov 2001
 Monika Braczyk, Sep 2002
 Herbert Kaiser, Aug 2006  (added DrawOption)
 Modified to make it partition aware and publish histograms as long as it is running
 Sami Kama May 2011
 */

#include <iostream>
#include <map>
#include <algorithm>
#include <iomanip>
#include <sys/prctl.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <condition_variable>
#include <mutex>
#include <chrono>
#include <ctime>

//using namespace std;

// ROOT include files
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TKey.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TApplication.h"
#include "TClass.h"
#include "TRegexp.h"
#include "TSystem.h"

#include <boost/program_options.hpp>
#include <ipc/core.h>
#include <oh/OHRootProvider.h>
#include <oh/OHRootReceiver.h>

namespace boostPOpt = boost::program_options;
bool doPublish = true;
TRandom3* mtRand = 0;
int strip;
int wait;
std::string histogram_name;
std::string provider_name;
std::string providerName;
int tag;
std::vector<std::pair<std::string, std::string>> ann;

long search_directory(TDirectory * d, std::map<std::string, TObject*>& ld, std::string basename = "",
                      int spaces = 0)
{
    //    std::cout << "Searching for histograms in the file..." << std::endl;
    std::string space;

    for (int i = 0; i < spaces; i++)
        space += " ";
    long count = 0;
    TIter it(d->GetListOfKeys());
    TKey* key;
    TObject* obj;
    while ((key = (TKey*) it())) {
        obj = d->Get(key->GetName());
        //std::cout<<space<< "Found an object with name " << key->GetName() << " and type " << obj->IsA( )->GetName( ) << "... "<<std::endl;
        if (obj->IsA()->InheritsFrom(TH1::Class())) {
            std::string name = basename + "/" + obj->GetName();
            ld[name] = obj;
            ++count;
        } else if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
            count += search_directory((TDirectory*) obj, ld, std::string(basename + "/" + obj->GetName()),
                                      spaces + 2);
        }
    }
    return count;
}

void set_labels(TAxis * axis, bool allbins = true)
{
    if (!axis)
        return;
    for (int i = 1; i < axis->GetNbins(); ++i) {
        if (allbins || mtRand->Rndm() < 0.5) {
            char label[32];
            sprintf(label, "label %d", i);
            axis->SetBinLabel(i, label);
        }
    }
}


int main(int argc, char ** argv)
{
    std::string partition_name;
    std::string app;
    std::string uniqueId;
    bool iMode = false;
    std::string segname;
    std::string parentname;
    std::string threads;
    std::string server_name;
    std::string option;
    int tag;
    int bins_number;
    bool graphics;
    bool verbose;
    bool labels;
    bool sparseLabels;
    std::string drawoption;
    std::string filesDirectory;
    bool mixedLabels;
    bool oneFile;
    bool TagHistos = false;
    int multiPublish;
    int numForks = 14;
    std::vector < std::string > annotations;
    try {
        std::cout << "Input parameters\n";
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"\n";
        }

        boost::program_options::options_description desc("This program publishes histograms with different properties to given OH servers.");
        desc.add_options()
            ("partition,p", boostPOpt::value<std::string>(&partition_name), "Partition Name")
            ("name,N", boostPOpt::value<std::string>(&app), "application name")
            ("uniquename,n", boostPOpt::value<std::string>(&uniqueId), "Unique application id")
            ("interactiveMode,i", boostPOpt::value<bool>(&iMode)->default_value(false), "interactive mode")
            ("segment,s", boostPOpt::value<std::string>(&segname), "Name of the segment")
            ("parentname,P", boostPOpt::value<std::string>(&parentname), "Parent name")
            ("threads,m", boostPOpt::value<std::string>(&threads), "Max server threads per connection.")
            ("server,S", boostPOpt::value<std::string>(&server_name), "OH (IS) server name.")
            ("provider,M", boostPOpt::value<std::string>(&provider_name), "Provider name for the application.")
            ("histogram,h", boostPOpt::value<std::string>(&histogram_name), "Name of the published histograms")
            ("option,o", boostPOpt::value<std::string>(&option)->default_value(""), "\"1d\",\"2d\",\"3d\",\"1p\",\"2p\", or the name of a file which contains histograms.")
            ("tag,t", boostPOpt::value<int>(&tag)->default_value(-1), "Tag number for histogram (<0 disabled)")
            ("bins,b", boostPOpt::value<int>(&bins_number)->default_value(100), "number of bins in each axis")
            ("graphics,g", boostPOpt::value<bool>(&graphics)->default_value(false), "display histogram in graphics mode")
            ("verbose,v", boostPOpt::value<bool>(&verbose)->default_value(false), "Print histogram contents")
            ("labels,l", boostPOpt::value<bool>(&labels)->default_value(false), "Add bin labels to histograms")
            ("sparselabels,L", boostPOpt::value<bool>(&sparseLabels)->default_value(false), "add bin labels sparsely. implies \"-l\"")
            ("drawoption,d", boostPOpt::value<std::string>(&drawoption), "give a string for the ROOT DrawOption for the histogram")
            ("strip,e", boostPOpt::value<int>(&strip)->default_value(0), "strip n tokens from histogram name")
            ("wait,w", boostPOpt::value<int>(&wait)->default_value(60000), "number of milliseconds to wait between publishes.")
            ("filesDirectory,D", boostPOpt::value<std::string>(&filesDirectory), "path to directory containing the root files to loop over.")
            ("mixedlabels,X", boostPOpt::value<bool>(&mixedLabels)->default_value(false), "add labels only one of the axes. implies \"-l\"a ")
            ("oneFile,O", boostPOpt::value<bool>(&oneFile)->default_value(false)->zero_tokens(), "if not enough files, use first one")
            ("multi-publish,U", boostPOpt::value<int>(&multiPublish)->default_value(1), "publish multiple histograms.")
            ("annotations,a", boostPOpt::value<std::vector<std::string> >(&annotations), "annotations.")
            ("Tags,T", boostPOpt::value<bool>(&TagHistos)->default_value(false)->zero_tokens(), "Automatically increment tags at each publish cycle")
            ("numForks,f", boostPOpt::value<int>(&numForks)->default_value(18), "Number of forks")
            ("help", "Print help message");

        boostPOpt::basic_parsed_options<char> bop = boostPOpt::parse_command_line(argc, argv, desc);

        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                          << bop.options.at(t).value.at(k) << "\n";
            }
        }
        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::parse_command_line(argc, argv, desc), vm);
        boostPOpt::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return 0;
        }
        if (vm.count("partition") == 0) {
            std::cout << "you must give partition name" << std::endl;
            return 5;
        }
        // if(vm.count("name")==0) {
        //   std::cout << "you must give application name" << std::endl;
        //   return 5;
        // }
        // if(vm.count("uniquename")==0) {
        //   std::cout << "you must give unique application id" << std::endl;
        //   return 5;
        // }
        if (vm.count("server") == 0) {
            std::cout << "you must give server name" << std::endl;
            return 5;
        }
        if (vm.count("provider") == 0) {
            std::cout << "you must give provider name" << std::endl;
            return 5;
        }
        if (vm.count("option") == 0 && vm.count("filesDirectory") == 0) {
            std::cerr << "Either an option or a path to a directory must be given" << std::endl;
            return 1;
        }
    } catch (const std::exception& ex) {
        std::cerr << "Caught Exception " << ex.what() << std::endl;
    }

    if (sparseLabels)
        labels = true;
    if (!option.empty() && option == "2d" && mixedLabels)
        labels = true;
    //  TApplication theApp("App", 0, 0);

    mtRand = new TRandom3();
    if (wait < 0)
        wait = -wait;
    //get list of files in the directory
    void * histoDir = 0;
    std::vector<std::string> rootFileNames;
    if (!filesDirectory.empty()) {
        histoDir = gSystem->OpenDirectory(filesDirectory.c_str());
        if (histoDir == 0) {
            std::cerr << "Can't open root file directory \"" << filesDirectory << "\".\n";
            return 1;
        }
        TRegexp fileRE(".root");
        const char * fname = 0;
        while ((fname = gSystem->GetDirEntry(histoDir))) {
            if (fname[0] == '.')
                continue;    //skip .* files
            TString s = fname;
            if (s.Index(fileRE) == kNPOS)
                continue;
            rootFileNames.push_back(fname);
        }
        gSystem->FreeDirectory(histoDir);    // we dont need it anymore
        if (rootFileNames.size() == 0) {
            std::cerr << "Root file directory \"" << filesDirectory << "\" doesn't contain any root files!"
            << std::endl;
            return 3;
        } else {
            std::sort(rootFileNames.begin(), rootFileNames.end());
        }
    }

    int numProviders = 1;
    //int myPos=1;
    //find out position in the all positions
    //int filesPerProvider=1;
    if (rootFileNames.size() > 0) {
        numProviders = numForks;
        if (numForks > (int) rootFileNames.size() && !oneFile) {
            std::cerr << "Error! Root files directory \"" << filesDirectory << "\" contains "
                      << rootFileNames.size() << " files but " << numProviders << " providers defined in the partition."
                      << std::endl;
            std::cerr << "At least 1 file is needed per provider!" << std::endl;
            return 4;
        } else if (oneFile) {
            //filesPerProvider=1;
            //myPos=0;
        } else {
            //filesPerProvider=rootFileNames.size()/numProviders;
        }
        //    return 1; //sami
    }

    IPCCore::init(argc, argv);
    std::unique_ptr<OHRootProvider> provider;
    try {
        provider = std::make_unique<OHRootProvider>(IPCPartition(partition_name), server_name, providerName);
    } catch (const daq::oh::Exception& ex) {
        ers::fatal(ex);
        return 1;
    }

    TH1 * histo = 0;

    // create DrawOption annotation if wanted
    //std::vector< std::pair<std::string,std::string> > ann;

    if (drawoption.empty() && annotations.size() == 0) {
        ann = oh::util::EmptyAnnotation;
    } else {
        if (!drawoption.empty()) {
            ann.resize(1);
            ann[0].first = "DrawOption";
            ann[0].second = drawoption;
        }
        if (annotations.size() > 0) {
            size_t currCount = ann.size();
            ann.resize(currCount + annotations.size());
            std::cout << "Setting annotations" << std::endl;
            for (uint k = 0; k < annotations.size(); k++) {
                std::string currAnn = annotations.at(k);
                if (currAnn.find('=') == std::string::npos) {
                    ann[k + currCount].first = currAnn;
                    ann[k + currCount].second = "";
                } else {
                    ann[k + currCount].first = currAnn.substr(0, currAnn.find('='));
                    ann[k + currCount].second = currAnn.substr(currAnn.find('=') + 1);
                }
                std::cout << " " << k + currCount << " \"" << ann[k + currCount].first << "\", \""
                          << ann[k + currCount].second << "\"" << std::endl;
            }
        }
    }

    // Create and publish the histogram(s)
    std::string opt;
    if (!option.empty())
        opt = option;
    if (opt == "1d") {
        histo = new TH1F(histogram_name.c_str(), histogram_name.c_str(), bins_number, -2, 2);
        histo->GetXaxis()->SetTitle("X Axis");
        histo->GetYaxis()->SetTitle("Y Axis");
        histo->GetZaxis()->SetTitle("Z Axis");
    } else if (opt == "2d") {
        histo = new TH2F(histogram_name.c_str(), histogram_name.c_str(), bins_number / 2, -2, 2,
                         bins_number / 2, -4, 4);
        histo->GetXaxis()->SetTitle("X Axis");
        histo->GetYaxis()->SetTitle("Y Axis");
    } else if (opt == "3d") {
        histo = new TH3F(histogram_name.c_str(), histogram_name.c_str(), bins_number / 3, -2, 2,
                         bins_number / 3, -4, 4, bins_number / 3, -6, 6);
        histo->GetXaxis()->SetTitle("X Axis");
        histo->GetYaxis()->SetTitle("Y Axis");
        histo->GetZaxis()->SetTitle("Z Axis");
    } else if (opt == "1p") {
        histo = new TProfile(histogram_name.c_str(), histogram_name.c_str(), 10, -200, 200);
        histo->GetXaxis()->SetTitle("X Axis");
    } else if (opt == "2p") {
        histo = new TProfile2D(histogram_name.c_str(), histogram_name.c_str(), 10, -200, 200, 20, -100, 100);

        histo->GetXaxis()->SetTitle("X Axis");
        histo->GetYaxis()->SetTitle("Y Axis");
    } else {
        // option should be a filename, try to open the file

        //int currFile,firstFile,lastFile;

        // firstFile=myPos*filesPerProvider;
        // lastFile=(myPos+1)*filesPerProvider;
        // currFile=firstFile;
        std::string currDirPath = std::string(filesDirectory) + "/";
        for (auto &f : rootFileNames)
            f = currDirPath + f;
        for (int pos = 0; pos < numForks; pos++) {
            pid_t t = fork();
            if (t != 0) {    //mother

            } else {
                //if(logEnv)logsPath=logEnv;
                auto tnow = std::chrono::system_clock::now();
                long tepoch = std::chrono::duration_cast < std::chrono::seconds
                        > (tnow.time_since_epoch()).count();
                const int bufflen = 8192;
                char buff[bufflen];
                std::string logsPath = "/tmp";
                pid_t m_myPid = getpid();
                snprintf(buff, bufflen, "%s:%02d", provider_name.c_str(), pos);
                //setenv("TDAQ_APPLICATION_NAME",buff,1);
                snprintf(buff, bufflen, "%s/%s:%02d-%d-%ld.out", logsPath.c_str(), provider_name.c_str(), pos,
                         m_myPid, tepoch);
                std::cerr << "I am the child # " << pos << " with pid " << m_myPid
                << ". Redirecting stdout to " << buff << std::endl;
                fflush (stdout);
                freopen(buff, "a", stdout);
                snprintf(buff, bufflen, "%s/%s:%02d-%d-%ld.err", logsPath.c_str(), provider_name.c_str(), pos,
                         m_myPid, tepoch);
                std::cerr << "I am the child # " << pos << " with pid " << m_myPid
                          << ". Redirecting stderr to " << buff << std::endl;

                fflush (stderr);
                freopen(buff, "a", stderr);
                //sleep(30);
                int dummc = 0;
                char *dummv = 0;
                try {
                    IPCCore::init(dummc, &dummv);
                    std::cerr << "******************************  IPC INIT SUCCEEDED  ******************************\n";
                    std::cerr.flush();
                    //sleep(60);
                } catch (const std::exception& ex) {
                    std::cerr << "******************************   ERROR  ******************************\n"
                              << "IPC Reinitialization failed for pid=" << getpid() << " with " << ex.what() << "\n"
                              << "******************************   ERROR  ******************************\n";
                    std::cerr.flush();
                }

                std::cerr << "IPC reinitialization done " << m_myPid << std::endl;
                //printOpenFDs();
                prctl(PR_SET_PDEATHSIG, SIGTERM);
                ::signal(SIGCHLD, SIG_DFL);
                ::signal(SIGKILL, SIG_DFL);
                ::signal(SIGTERM, SIG_DFL);

                ERS_LOG("I am the child # " << pos << " with, pid=" << m_myPid << ". Redirection is completed");
                snprintf(buff, bufflen, "%s-%02d", provider_name.c_str(), pos);
                providerName = buff;

            }
        }
        if (int(rootFileNames.size()) >= numForks) {

        }
        return 0;
    }
    if (labels) {
        if (mixedLabels && opt == "2d") {
            set_labels(histo->GetYaxis(), !sparseLabels);
        } else {
            set_labels(histo->GetXaxis(), !sparseLabels);
            set_labels(histo->GetYaxis(), !sparseLabels);
            set_labels(histo->GetZaxis(), !sparseLabels);
        }
    }
    histo->FillRandom("gaus", gRandom->Integer(100000) + 1);
    if (verbose) {
        if (graphics) {
            TCanvas* canvas = new TCanvas(histo->GetName(), histo->GetTitle());
            if (drawoption.empty())
                histo->Draw();
            else
                histo->Draw(drawoption.c_str());
            canvas->Update();
        } else {
            histo->Print("all");
        }
    }

    try {
        if (multiPublish > 1) {
            std::string formatString = histogram_name + "_%06d";
            char *histNameBuff = new char[formatString.size() + 10];
            for (int k = 0; k < multiPublish; k++) {
                sprintf(histNameBuff, formatString.c_str(), k);
                provider->publish(*histo, histNameBuff, tag, ann);
            }
            delete[] histNameBuff;
        } else {
            provider->publish(*histo, histogram_name, tag, ann);
        }
    } catch (const daq::oh::Exception& ex) {
        ers::error(ex);
        return 1;
    }
    std::cout << "publish successful " << std::endl;

    if (graphics) {
        std::cout << "Entering ROOT message loop...\n"
                  << "Click 'Quit ROOT' on the file menu to quit" << std::endl;
        //    theApp.Run( );
    }
    delete histo;
}
