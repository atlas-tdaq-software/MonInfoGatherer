/**
 * Bunch of test utilities/classes.
 */

#include <string>
#include <sys/types.h>
#include <unistd.h>

#include <boost/process.hpp>

#include "ers/ers.h"
#include "ipc/partition.h"
#include "is/server/repository.h"


namespace MIG {
namespace Test {

/**
 * Make test partition whose lifetime is the lifetime of the instance
 * of this class.
 */
class Partition {
public:
    // make partition with random name
    Partition() : m_name(make_unique_name()) {
        _run();
    }

    // make partition with specified name
    explicit Partition(std::string const& name) : m_name() {
        _run();
    }

    // kills partition 
    ~Partition() {
        _stop();
    }

    Partition(Partition const&) = delete;
    Partition& operator=(Partition const&) = delete;

    // return partition name
    std::string name() const { return m_name; }

    // return partition object
    IPCPartition partition() const { return IPCPartition(m_name); }

private:

    static std::string make_unique_name() {
        static std::atomic<unsigned> counter;
        static pid_t pid = ::getpid();
        auto seq = ++ counter;
        return "test-part-" + std::to_string(pid) + "-" + std::to_string(seq);
    }

    void _run() {
        ERS_LOG("Making temporary test partition with name " << m_name);
        auto exec = boost::process::search_path("ipc_server");
        m_ipc_server = boost::process::child(exec, "-p", m_name);
    }

    void _stop() {
        if (m_ipc_server.running()) {
            ERS_LOG("Destroying temporary test partition with name " << m_name);
            m_ipc_server.terminate();
            m_ipc_server.wait();
        }
    }

    std::string const m_name;
    boost::process::child m_ipc_server;
};


/**
 * Implementation of is:repository which prints received objects
 */
class ISRepoPrint : public ISRepository {
public:

    using ISRepository::ISRepository;

    void checkin(const ::is::info& i, ::CORBA::Boolean keep_history, ::CORBA::Boolean use_tag) override {
        printInfo(i, "checkin");
        ISRepository::checkin(i, keep_history, use_tag);
    }
    void create(const ::is::info& i) override {
        printInfo(i, "create");
        ISRepository::create(i);
    }
    void update(const ::is::info& i, ::CORBA::Boolean keep_history, ::CORBA::Boolean use_tag) override {
        printInfo(i, "update");
        ISRepository::update(i, keep_history, use_tag);
    }
private:
    void printInfo(const ::is::info& i, const char* method) {
        auto& annotations = i.value.attr.ann;
        std::string ann;
        for (unsigned j = 0; j < annotations.length(); ++ j) {
            if (j != 0) ann += ",";
            ann += annotations[j];
        }
        ERS_LOG(method << ": name=" << i.name << " type=" << i.type.name << " annotations=[" << ann << "]");
    }
};



}} // namespace MIG::Test

