#!/usr/bin/env tdaq_python
"""
PartitionMaker script to generate standalone partition which includes MIG
and few test applications to simulate running at P1. One of the applications
is LumiBlock generator which updates LumiBlock IS object. Another application is
OH provider which generates and publishes histograms.
"""

import argparse
from configparser import ConfigParser
import os
import sys

import pm
import pm.project
from config.dal import module as dal_module


# default values for some parameters

# host name where to run partition
_host_name = os.environ.get('HOSTNAME', '')

# Tag name
_tag_name = os.environ['CMTCONFIG']


def main():
    """run PM to create partition for MIG
    """

    parser = argparse.ArgumentParser(description='Generate segment or partition for MIG')
    parser.add_argument('-c', '--config', metavar='PATH', type=argparse.FileType(),
                        required=True, help='Configuration file')
    parser.add_argument('output_file', help='The name of the output file')
    args = parser.parse_args()

    output_file = args.output_file

    config = ConfigParser(dict(Tag=_tag_name, Host=_host_name))
    config.read_file(args.config)

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    includes = ['daq/schema/core.schema.xml']

    tag = config.get("DEFAULT", "Tag")
    try:
        tags = pm.project.Project("daq/sw/tags.data.xml")
        tag_dal = tags.getObject("Tag", tag)
        includes += ["daq/sw/tags.data.xml"]
    except RuntimeError:
        tag_dal = dal.Tag(tag)

    # Computer
    host = config.get("DEFAULT", "Host")
    if host:
        try:
            hosts = pm.project.Project("daq/hw/hosts.data.xml")
            host = hosts.getObject("Computer", host)
            comp = host
            includes += ["daq/hw/hosts.data.xml"]
        except RuntimeError:
            hw_tag = '-'.join(tag.split('-')[:2])
            comp = dal.Computer(host, HW_Tag=hw_tag)

    local_repo = make_local_repo(config, tag_dal, includes)

    is_repo = local_repo if config.getboolean("SrcISServer", "UseLocalRepo") else None
    src_is = make_is_server(config.get("SrcISServer", "Name"), is_repo, includes)
    is_servers = [src_is]
    if config.get("DstISServer", "Name") != config.get("SrcISServer", "Name"):
        is_repo = local_repo if config.getboolean("DstISServer", "UseLocalRepo") else None
        dst_is = make_is_server(config.get("DstISServer", "Name"), is_repo, includes)
        is_servers += [dst_is]
    else:
        dst_is = src_is

    mig_repo = local_repo if config.getboolean("MIGSegment", "UseLocalRepo") else None
    mig_app = make_mig_app(config, src_is, dst_is, mig_repo, includes)

    mig_seg = make_segment(config, [mig_app], is_servers, [], includes)
    extras = []
    hmake_seg = make_hmaker_segment(config, config.get("SrcISServer", "Name"), tag_dal, local_repo, includes, extras)

    part = make_partition(config, [mig_seg, hmake_seg], tag_dal, comp, includes)

    includes.sort()
    save_db = pm.project.Project(output_file, includes)
    save_db.addObjects([part] + extras)


def make_local_repo(config, tag_dal, includes):
    """Make SW_Repository instance for lcoal repo
    """

    repo_path = config.get("SW_Repository", "InstallationPath")
    if repo_path:
        dal = dal_module('dal', 'daq/schema/core.schema.xml')
        repo = pm.project.Project('daq/sw/repository.data.xml')
        tag = tag_dal.HW_Tag + "-" + tag_dal.SW_Tag
        pp1 = dal.SW_PackageVariable('PYTHONPATH-local', Name='PYTHONPATH', Suffix='share/lib/python')
        pp2 = dal.SW_PackageVariable('PYTHONPATH-Local-SOLIB', Name='PYTHONPATH', Suffix=tag + '/lib')
        localrepo = dal.SW_Repository('Local-Repository',
                                      Name="Local software repository",
                                      InstallationPath=repo_path,
                                      SW_Objects=[],
                                      Tags=[tag_dal],
                                      Uses=[repo.getObject("SW_Repository", "Online")],
                                      ProcessEnvironment=[],
                                      AddProcessEnvironment=[pp1, pp2])
        return localrepo


def get_mig_binary(config, local_repo, includes):
    """For now it should come from tdaq release
    """

    if local_repo:
        dal = dal_module('dal', 'daq/schema/core.schema.xml')
        binary = dal.Binary('MonInfoGathererLocal',
                            BinaryName='MonInfoGatherer',
                            BelongsTo=local_repo)
        local_repo.SW_Objects.append(binary)
    else:
        repo_inc = 'daq/sw/repository.data.xml'
        includes += [repo_inc]
        repo = pm.project.Project(repo_inc)
        binary = repo.getObject("Binary", "MonInfoGatherer")
    return binary


def get_is_binary(local_repo, includes):
    """For now it should come from tdaq release
    """

    if local_repo:
        binaries = [b for b in local_repo.SW_Objects if b.id == "is_server_local"]
        if binaries:
            binary = binaries[0]
        else:
            dal = dal_module('dal', 'daq/schema/core.schema.xml')
            binary = dal.Binary('is_server_local',
                                BinaryName='is_server',
                                BelongsTo=local_repo)
            local_repo.SW_Objects.append(binary)
    else:
        repo_inc = 'daq/sw/repository.data.xml'
        includes += [repo_inc]
        repo = pm.project.Project(repo_inc)
        binary = repo.getObject("Binary", "is_server")
    return binary


def make_is_server(name, local_repo, includes):

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    binary = get_is_binary(local_repo, includes)

    isparam = "-s -p ${TDAQ_PARTITION} -n env(TDAQ_APPLICATION_NAME) " \
              "-b ${TDAQ_BACKUP_PATH}/env(TDAQ_APPLICATION_NAME).backup"
    iss = dal.InfrastructureApplication(name,
                                        Parameters=isparam,
                                        RestartParameters=isparam,
                                        Program=get_is_binary(local_repo, includes),
                                        RestartableDuringRun=1)
    return iss


def make_mig_app(config, src_server, dst_server, local_repo, includes):

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    migdal = dal_module('dal', 'daq/schema/MonInfoGatherer.schema.xml')
    includes += ['daq/schema/MonInfoGatherer.schema.xml']

    mig_config = migdal.MIGConfiguration("test_config",
                                         ProviderRegExp=".*",
                                         ObjectRegExp=".*",
                                         SourceServers=[src_server],
                                         DestinationServers=[dst_server])
    if config.getboolean("MIGConfig", "UseMIGMatchHandler"):
        parameters = config.get("MIGConfig", "Parameters").strip().split()
        mig_config.MatchHandlers = [migdal.MIGMatchHandler("handler1",
                                                           Name="DefaultHandler",
                                                           ObjectRegExp="/DEBUG/.*",
                                                           Parameters=parameters),
                                    migdal.MIGMatchHandler("handler2",
                                                           Name="DefaultHandler",
                                                           ObjectRegExp="/SHIFT/SFO/.*",
                                                           Parameters=parameters),
                                    migdal.MIGMatchHandler("handlerAll",
                                                           Name="DefaultHandler",
                                                           ObjectRegExp=".*",
                                                           Parameters=parameters)]

    env = []
    for varstr in config.get("MIGConfig", "Environment").strip().split():
        varname, sep, value = varstr.partition("=")
        env += [dal.Variable("MIG-" + varname,
                             Name=varname,
                             Value=value)]

    app = migdal.MIGApplication('MIG-OH',
                                InterfaceName="rc/commander",
                                InitTimeout=60,
                                ExitTimeout=5,
                                RestartableDuringRun=1,
                                RunsOn="FirstHost",
                                Program=get_mig_binary(config, local_repo, includes),
                                Configurations=[mig_config],
                                ProcessEnvironment=env,
                                LibraryPaths=[])

    return app


def make_segment(config, resources, infrastructure, apps, includes):

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    repo = pm.project.Project('daq/sw/repository.data.xml')

    segment_name = config.get("MIGSegment", "Name")
    rcapp = dal.RunControlApplication(segment_name + "RCApp",
                                      InterfaceName="rc/commander",
                                      InitTimeout=30,
                                      Program=repo.getObject("Binary", "rc_controller"))
    segment = dal.Segment(segment_name,
                          IsControlledBy=rcapp,
                          Applications=apps,
                          Infrastructure=infrastructure,
                          Resources=resources,
                          Hosts=[])

    return segment


def make_monsvc_config(src_is_name):
    monsvcdal = dal_module('dal', 'daq/schema/monsvc_config.schema.xml')

    # copied from HLT OKS (including misspelled stuff)
    pubpar = monsvcdal.ISPublishingParameters("HLTMPPUShortISPublishingParamters",
                                                PublishInterval=10,
                                                NumberOfSlots=1,
                                                ISServer="${TDAQ_IS_SERVER=DF}")
    rule1 = monsvcdal.ConfigurationRule("HLTMPPUShortISPublishingRule",
                                        IncludeFilter="^PU_.*",
                                        ExcludeFilter="",
                                        Name="FastIS",
                                        Parameters=pubpar)
    pubpar = monsvcdal.ISPublishingParameters("HLTMPPULongISPublishingParameters",
                                                PublishInterval=60,
                                                NumberOfSlots=1,
                                                ISServer="${TDAQ_IS_SERVER=DF}")
    rule2 = monsvcdal.ConfigurationRule("HLTMPPULongISPublishingRule",
                                        IncludeFilter=".*",
                                        ExcludeFilter="^PU_.*",
                                        Name="SlowIS",
                                        Parameters=pubpar)
    pubpar = monsvcdal.OHPublishingParameters("HLTMPPUSlowOHPublishingParameter",
                                                PublishInterval=60,
                                                NumberOfSlots=6,
                                                OHServer=src_is_name,
                                                ROOTProvider="${TDAQ_APPLICATION_NAME}")
    rule3 = monsvcdal.ConfigurationRule("HLTMPPUSlowPublishingOHRule",
                                        IncludeFilter=".*",
                                        ExcludeFilter="^.SHIFT.TrigSteer_HLT.*$",
                                        Name="SlowOH",
                                        Parameters=pubpar)
    pubpar = monsvcdal.OHPublishingParameters("HLTMPPUFastOHPublishingParameter",
                                                PublishInterval=10,
                                                NumberOfSlots=1,
                                                OHServer=src_is_name,
                                                ROOTProvider="${TDAQ_APPLICATION_NAME}")
    rule4 = monsvcdal.ConfigurationRule("HLTMPPUFastPublishingOHRule",
                                        IncludeFilter="^.SHIFT.TrigSteer_HLT.*$",
                                        ExcludeFilter="",
                                        Name="FastOH",
                                        Parameters=pubpar)
    rules = monsvcdal.ConfigurationRuleBundle("HLTMPPUConfigurationRuleBundle",
                                                Rules=[rule1, rule2, rule3, rule4])

    return rules

def make_hmaker_segment(config, src_is_name, tag_dal, local_repo, includes, extras):

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    repo = pm.project.Project('daq/sw/repository.data.xml')
    binary = repo.getObject("Binary", "MonInfoGatherer")
    tag = tag_dal.HW_Tag + "-" + tag_dal.SW_Tag

    try:
        exrepo = pm.project.Project("daq/sw/tdaq-common-external-repository.data.xml")
        pyhome = exrepo.getObject("Variable", "TDAQ_PYTHON_HOME")
        includes += ["daq/sw/tdaq-common-external-repository.data.xml"]
    except RuntimeError:
        pyhome = dal.Variable("MIG-TDAQ_PYTHON_HOME",
                              Name="TDAQ_PYTHON_HOME",
                              Value=os.environ['TDAQ_PYTHON_HOME'])

    if config.getboolean("SW_Repository", "DoPythonPath"):
        ppath = ["${TDAQ_INST_PATH}/" + tag + "/lib",
                 "${TDAQ_INST_PATH}/share/lib/python",
                 "${TDAQC_INST_PATH}/" + tag + "/lib",
                 "${TDAQC_INST_PATH}/share/lib/python",
                 "${ROOT_HOME}/" + tag + "/lib"]
        ppath = dal.Variable("MIG-PYTHONPATH",
                             Name="PYTHONPATH",
                             Value=":".join(ppath),
                             Description="PYTHONPATH with tdaq and tdaq-common releases")
    else:
        commonenv = pm.project.Project("daq/segments/common-environment.data.xml")
        includes += ["daq/segments/common-environment.data.xml"]
        ppath = commonenv.getObject("Variable", "PYTHONPATH_NO_TAGS")

    uses = [local_repo] if local_repo else []
    replay_file = config.get("HistoSegment", "REPLAY_FILE")
    replay_params = config.get("HistoSegment", "HReplayParams")
    napps = config.getint("HistoSegment", "NumPublishers")
    apps = []
    if config.getboolean("HistoSegment", "UsePythonHreplay"):

        replay_file = dal.Variable("MDA_REPLAY_FILE",
                                   Name="REPLAY_FILE",
                                   Value=replay_file)
        for i in range(napps):
            params = replay_params + " -M MDAReplay@MonInfoGatherer.test.hreplay"
            replay = dal.RunControlApplication('ReplayHisto{}'.format(i + 1),
                                            InterfaceName="rc/commander",
                                            InitTimeout=30,
                                            Parameters=params,
                                            RestartParameters=params,
                                            Program=repo.getObject("Binary", "rc_pyrunner"),
                                            ProcessEnvironment=[pyhome, ppath, replay_file],
                                            Uses=uses)
            apps.append(replay)

    else:

        monsvcdal = dal_module('dal', 'daq/schema/monsvc_config.schema.xml')
        includes += ["daq/schema/monsvc_config.schema.xml"]

        binary = dal.Binary('MIGHReplayLocal',
                            BinaryName='MIGHReplay',
                            BelongsTo=local_repo)
        local_repo.SW_Objects.append(binary)

        rules = make_monsvc_config(src_is_name)

        for i in range(napps):

            appid = 'MIGHReplay{}'.format(i + 1)
            params = replay_params + " --root-file " + replay_file
            interval = config.getint("HistoSegment", "ReplayProbeInterval")
            replay = dal.RunControlApplication(appid,
                                               InterfaceName="rc/commander",
                                               InitTimeout=30,
                                               ProbeInterval=interval,
                                               Parameters=params,
                                               RestartParameters=params,
                                               Program=binary,
                                               ProcessEnvironment=[],
                                               Uses=uses)
            apps.append(replay)

            # this is not really an application, its ID has to be the same as real app
            publisher = monsvcdal.PublishingApplication(appid,
                                                        ConfigurationRules=rules)
            extras.append(publisher)


    if config.getboolean("HistoSegment", "MakeLBGenerator"):
        ini_lbn = dal.Variable("LBGEN_INITIAL_LBN",
                            Name="INITIAL_LBN",
                            Value=config.get("HistoSegment", "INITIAL_LBN"))
        r4p_lbn = dal.Variable("LBGEN_R4P_LBN",
                            Name="R4P_LBN",
                            Value=config.get("HistoSegment", "R4P_LBN"))
        params = "-M LBGen@MonInfoGatherer.test.lb_gen"
        interval = config.getint("HistoSegment", "ProbeInterval")
        lb_gen = dal.RunControlApplication('LBGenerator',
                                        InterfaceName="rc/commander",
                                        InitTimeout=30,
                                        ProbeInterval=interval,
                                        Parameters=params,
                                        RestartParameters=params,
                                        Program=repo.getObject("Binary", "rc_pyrunner"),
                                        ProcessEnvironment=[pyhome, ppath, ini_lbn, r4p_lbn],
                                        Uses=uses)
        apps.append(lb_gen)

    env = []
    for varstr in config.get("HistoSegment", "Environment").strip().split():
        varname, sep, value = varstr.partition("=")
        env += [dal.Variable("MIG-HM-" + varname,
                             Name=varname,
                             Value=value)]

    segment_name = config.get("HistoSegment", "Name")
    rcapp = dal.RunControlApplication(segment_name + "RCApp",
                                      InterfaceName="rc/commander",
                                      InitTimeout=30,
                                      Program=repo.getObject("Binary", "rc_controller"))
    segment = dal.Segment(segment_name,
                          IsControlledBy=rcapp,
                          Applications=apps,
                          Hosts=[],
                          ProcessEnvironment=env)

    return segment


def make_partition(config, segments, tag_dal, comp, includes):

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    common_env = pm.project.Project("daq/segments/common-environment.data.xml")
    setup = pm.project.Project("daq/segments/setup.data.xml")
    includes += ['daq/schema/core.schema.xml',
                 "daq/segments/common-environment.data.xml",
                 "daq/segments/setup.data.xml"]

    # make partition object
    part = dal.Partition(config.get("Partition", "Name"),
                         OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
                         Segments=segments,
                         DefaultHost=comp,
                         DefaultTags=[tag_dal],
                         LogRoot=config.get("Partition", "LogRoot"),
                         WorkingDirectory=config.get("Partition", "WorkingDirectory"),
                         ProcessEnvironment=[common_env.getObject('VariableSet', 'CommonEnvironment')],
                         Parameters=[common_env.getObject('VariableSet', 'CommonParameters')])

    return part


if __name__ == '__main__':
    sys.exit(main())
