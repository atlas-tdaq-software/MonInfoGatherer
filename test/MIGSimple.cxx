/**
 *  MonInfoGatherer stand-alone application.
 */

#include <dlfcn.h>
#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <exception>
#include <memory>
#include <mutex>
#include <condition_variable>

#include <boost/program_options.hpp>
#include "ers/ers.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/DefaultHandler.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/LibraryManager.h"
#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "oh/OHUtil.h"
#include "oh/OHRootProvider.h"

namespace boostPOpt = boost::program_options;
using namespace MIG;

class CBhandler: public InfoPublisher {
public:
    CBhandler() {}
    ~CBhandler() {}

    void publish(ISInfo& info, const std::string& name, int nProv) override {
        // std::cout<<"publish callback"<<destName+name<<std::endl;
        m_dict->checkin(destName + name, info.tag(), info);
    }

    void receiveIS(ISCallbackInfo* cbinfo) {
        std::string fullname = cbinfo->name();
        if (cbinfo->reason() == ISInfo::Deleted)
            return;
        std::string publisher = oh::util::get_provider_name(fullname);
        if (publisher == m_myProviderName)
            return;
        std::string name = oh::util::get_histogram_name(fullname);
        ISInfoAny info;
        try {
            cbinfo->value(info);
        } catch (const daq::is::RepositoryNotFound&) {
            return;
        } catch (const daq::is::InfoNotFound&) {
            return;
        } catch (const daq::is::InfoNotCompatible&) {
            return;
        }
        m_myHistHandler->updateInfo(info, name, publisher);
    }

    std::shared_ptr<ISInfoDictionary> m_dict;
    std::string destName;
    std::string m_myProviderName;
    std::shared_ptr<DefaultHandler> m_myHistHandler;
    std::shared_ptr<OHRootProvider> m_prov;
};

std::mutex s_mutex;
std::condition_variable s_cond;
bool finishRunning;

void sahandler(int sig, siginfo_t * si, void* /*vp*/)
{
//    std::cerr << " Got signal " << sig << std::endl;
//    if (!si) {
//        std::cerr << __PRETTY_FUNCTION__ << " siginfo_t is NULL " << std::endl;
//        return;
//    }
    // TODO: should not use thread primitives here, better use semaphores or pipes.
    std::unique_lock<std::mutex> lock(s_mutex);
    finishRunning = true;
    s_cond.notify_all();
}

int main(int argc, char* argv[])
{
    std::string partition;
    std::string srcserver;
    std::string dstserver;
    std::string regex;
    std::string appname;

    boost::program_options::options_description desc(
            "This is a simplified version of MIG that can be configured from commandline.");
    try {
        desc.add_options()
            ("partition,p", boostPOpt::value<std::string>(&partition)->default_value(""), "Partition")
            ("source-server,s", boostPOpt::value<std::string>(&srcserver)->default_value(""), "Source Server")
            ("destination-server,d", boostPOpt::value<std::string>(&dstserver)->default_value(""), "Destination Server")
            ("regex,r", boostPOpt::value<std::string>(&regex)->default_value(".*"), "Subscription regex")
            ("provider-name,n", boostPOpt::value<std::string>(&appname)->default_value("MIGSimple"), "provider name")
            ("version,v", "Print MIGSimple version")
            ("help,h", "Print help message");

        std::cout << std::endl << "Input parameters are " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << " " << i << " \"" << argv[i] << "\"" << std::endl;
        }
        std::cout << std::endl << "Command to run " << std::endl;
        for (int i = 0; i < argc; i++) {
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;
        boostPOpt::variables_map vm;
        boostPOpt::store(boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        boostPOpt::notify(vm);
        std::cout << "Got input parameters " << std::endl;
        //boostPOpt::basic_parsed_options<char> bop=boostPOpt::basic_command_line_parser(argc, argv, desc);
        boostPOpt::basic_parsed_options<char> bop =
                boostPOpt::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
        for (size_t t = 0; t < bop.options.size(); t++) {
            for (size_t k = 0; k < bop.options.at(t).value.size(); k++) {
                std::cout << " " << bop.options.at(t).string_key << " [" << k << "] = "
                          << bop.options.at(t).value.at(k) << std::endl;
            }
        }

        if (vm.count("help")) {
            std::cout << "MIGBench" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }
        if (vm.count("version")) {
            std::cout << MonInfVersion << std::endl;
            return EXIT_SUCCESS;
        }
        if (partition.empty()) {
            std::cerr << "Need partition" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
        if (srcserver.empty()) {
            std::cerr << "Need source-server" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
        if (dstserver.empty()) {
            std::cerr << "Need destination-server" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

    } catch (const std::exception& ex) {
        std::cerr << "Caught exception " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Program parameters:\n"
              << "  Partition           = " << partition << "\n"
              << "  Source Server       = " << srcserver << "\n"
              << "  Destination Server  = " << dstserver << "\n"
              << "  Regex               = '" << regex << "'" << "\n"
              << "  Provider name       = " << appname << "\n";

    struct sigaction act;
    memset(&act, '\0', sizeof(act));
    act.sa_sigaction = &sahandler;
    act.sa_flags = SA_SIGINFO;
    if (sigaction(SIGTERM, &act, NULL) < 0) {
        std::cerr << "Error setting signal handler for SIGTERM" << std::endl;
        return EXIT_FAILURE;
    }

    try {
        IPCCore::init(argc, argv);
        IPCPartition p(partition);
        auto isr = std::make_shared<ISInfoReceiver>(p, false); //Parallel callbacks
        auto isdict = std::make_shared<ISInfoDictionary>(p);
        ISCriteria crit(regex);
        HandlerConfig hConfig(".*", "", {});

        CBhandler hnd;
        hnd.destName = dstserver + "." + appname + ".";
        hnd.m_dict = isdict;
        hnd.m_myProviderName = appname;
        auto handler = std::make_shared<DefaultHandler>(hConfig, hnd);
        hnd.m_myHistHandler = handler;
        hnd.m_prov = std::make_shared<OHRootProvider>(p, dstserver, appname);
        isr->subscribe(srcserver, crit, &CBhandler::receiveIS, &hnd);

        std::unique_lock<std::mutex> lock(s_mutex);
        finishRunning = false;
        ERS_LOG("Waiting for callback");
        s_cond.wait(lock, [] {return finishRunning;});
        ERS_LOG("Exiting ");
        isr->unsubscribe(srcserver, crit, true);

    } catch (const std::exception& ex) {
        std::cerr << "Caught exception " << ex.what() << std::endl;
    }
    return EXIT_SUCCESS;
}
