/**
 *  Stand-alone app which tests publication and gathering of simple IS data.
 */

#include <chrono>
#include <iostream>
#include <stdlib.h>
#include <thread>

#include <boost/program_options.hpp>
#include <boost/process.hpp>

#include "ers/ers.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "is/infoT.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/InfoHandlerWrapper.h"
#include "MonInfoGatherer/LibraryManager.h"

#include "MIGTestUtils.h"

namespace bpo = boost::program_options;
using namespace MIG;

namespace {

Config make_gatherer_config(const std::string& serverName) {
    HandlerConfig hc(".*", "Default", {});
    Config cfg(".*", ".*", {serverName}, {serverName}, {hc});
    return cfg;
}

} // namespace


int main(int argc, char* argv[])
try {
    std::string partition;
    std::string is_server_name;
    int numCycles;
    int delayMillisec;

    boost::program_options::options_description desc(
            "Stand-alone app which test IS object gathering.");
    desc.add_options()
        ("partition,p", bpo::value<std::string>(&partition)->default_value(""), "Partition")
        ("is-server,s", bpo::value<std::string>(&is_server_name)->default_value("isrepo"), "IS server name")
        ("cycles,n", bpo::value<int>(&numCycles)->default_value(100), "Number of publication cycles")
        ("delay-msec,d", bpo::value<int>(&delayMillisec)->default_value(5000), "Delay between publication cycles, millisec")
        ("help,h", "Print help message");

    bpo::variables_map vm;
    bpo::store(bpo::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    bpo::notify(vm);

    if (vm.count("help")) {
        std::cout << argv[0] << std::endl;
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    std::unique_ptr<MIG::Test::Partition> test_part;
    if (partition.empty()) {
        test_part = std::make_unique<MIG::Test::Partition>();
        partition = test_part->name();
        // wait until it starts
        sleep(2);
    }

    // make sure TDAQ_PARTITION is set
    setenv("TDAQ_PARTITION", partition.c_str(), 1);

    std::list<std::pair<std::string, std::string>> options;
    IPCCore::init(options);

    IPCPartition ipc_part(partition);

    ERS_LOG("starting is server " << is_server_name);
    auto is_exec = boost::process::search_path("is_server");
    auto is_server = boost::process::child(is_exec, "-p", partition, "-n", is_server_name);
    std::this_thread::sleep_for(std::chrono::seconds(2));

    LibraryManager::instance().loadLibrary("libMIGContainers.so");
    Config cfg = make_gatherer_config(is_server_name);
    auto receiver = std::make_unique<ISInfoReceiver>(ipc_part, false);
    auto handler = std::make_unique<InfoHandlerWrapper>(cfg, "gatherer", ipc_part);

    handler->subscribe(*receiver);

    ISInfoDictionary dict(ipc_part);

    for (int cycle = 0; cycle <= numCycles; ++cycle) {

        ERS_LOG("start publication cycle " << cycle);


        for (int prov = 0; prov < 5; ++ prov) {
            auto provider = "Provider" + std::to_string(prov);

            ISInfoT<int> infoI(cycle*10 + prov);
            dict.checkin(is_server_name + "." + provider + ".infoI", infoI);

            ISInfoT<double> infoD(cycle*10 + prov);
            dict.checkin(is_server_name + "." + provider + ".dataD", infoD);
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(delayMillisec));
    }


    handler->unsubscribe(*receiver);
    handler.reset();
    receiver.reset();

    if (is_server.running()) {
        ERS_LOG("Stopping IS server");
        is_server.terminate();
        is_server.wait();
    }


} catch (std::exception const& exc) {
    ERS_LOG("Exception:" << exc.what());
    return 1;
}
