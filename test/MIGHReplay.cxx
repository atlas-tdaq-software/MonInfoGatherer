/**
 *  This is re-implementation of hreplay Python module - it reads MDA
 *  histograms from ROOT file and publishes them to OH server. Difference
 *  from hreplay is that this application uses monsvc for publishing and it
 *  tries to simulate histogram publishing as it happens in HLTMPPU.
 */

#include <cstdlib>
#include <ctime>
#include <memory>
#include <set>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <sys/time.h>

#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "TTCInfo/LumiBlock.h"
#include "ers/ers.h"
#include "is/infodictionary.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"

namespace bpo = boost::program_options;

namespace {
    ERS_DECLARE_ISSUE(errors, UncaughtException, "Uncaught exception, terminating.", )
    ERS_DECLARE_ISSUE(errors, CommandLineError, "Command line error", )
    ERS_DECLARE_ISSUE(errors, FileOpenError, "Failed to open file " << fileName, ((std::string)fileName))
    ERS_DECLARE_ISSUE(errors, ObjectReadError, "Failed to read object from a file " << objectName, ((std::string)objectName))
    ERS_DECLARE_ISSUE(errors, ISError, "Error retrieving IS object " << objectName, ((std::string)objectName))

    // labels to use for dynamic labelling
    std::vector<std::string> labels({
        "A", "B", "C", "D", "E", "F"
    });
}

class MIGHReplayControllable : public daq::rc::Controllable {
public:
    explicit MIGHReplayControllable(const std::string& rootFilePath,
                                    unsigned bufferSize,
                                    bool dynLabels,
                                    unsigned pubInterval)
        : m_rootFilePath(rootFilePath),
          m_bufferSize(bufferSize),
          m_dynLabels(dynLabels),
          m_pubInterval(pubInterval)
    {
        struct timeval tv;
        gettimeofday(&tv, nullptr);
        int seed = tv.tv_usec;
        ERS_LOG("seed: " << seed);
        std::srand(seed);
        m_npubsDynLabels = std::rand() % 2 + 3;
    }

    void prepareForRun(const daq::rc::TransitionCmd& cmd) override;
    void stopROIB(const daq::rc::TransitionCmd& cmd) override;
    void publish() override;

private:

    void _open_file();

    static void _list_names(TDirectory& tdir, std::string const& dirPath, std::set<std::string>& names);

    using LB2PathMap = std::map<int, std::string>;

    std::string const m_rootFilePath;
    unsigned const m_bufferSize;
    bool const m_dynLabels;
    unsigned const m_pubInterval;
    std::unique_ptr<TFile> m_tfile;
    std::string const m_mda_server = "Histogramming";  // server name in MDA file
    std::string const m_mda_provider = "TopMIG-OH_HLT";  // provider name in MDA file
    std::string const m_is_lb_object = "RunParams.LumiBlock";  // IS object to check for LBN (attr LumiBlockNumber)
    std::map<std::string, std::string> m_regular_histo;  // names->path of non-LB histograms;
    std::map<std::string, LB2PathMap> m_perlb_histo;  // names->tag map of per-LB histograms;
    std::unique_ptr<monsvc::PublishingController> m_pub;
    int m_prevLB = -1;
    std::vector<int> m_publishedLBs;
    std::unique_ptr<ISInfoDictionary> m_dict;
    time_t m_dynPubTime = 0;
    unsigned m_npubsDynLabels;
};

void
MIGHReplayControllable::prepareForRun(const daq::rc::TransitionCmd& cmd)
{
    ERS_LOG("MIGHReplayControllable::prepareForRun");

    // open file and read all histo names
    _open_file();

    auto& msvc = monsvc::MonitoringService::instance();

    // register all regular histograms in advance
    for (auto&& item: m_regular_histo) {
        TObject* tobj = m_tfile->Get(item.second.c_str());
        if (tobj == nullptr) {
            ers::error(::errors::ObjectReadError(ERS_HERE, item.second));
            continue;
        }
        // have to remove histogram from its directory to avoid sharing mess
        dynamic_cast<TH1*>(tobj)->SetDirectory(nullptr);
        msvc.register_object(item.first, tobj, true);
    }

    // make few dynamically-labelled histos
    if (m_dynLabels) {

        TH1I* h = new TH1I("/DEBUG/HReplay/DynLab1", "DynLab 1D", 1, -0.5, 0.5);
        h->SetDirectory(nullptr);
        h->SetCanExtend(TH1::kAllAxes);
        msvc.remove_object(h->GetName(), -1);
        msvc.register_object(h->GetName(), -1, h, true);

        TH2I* h2 = new TH2I("/DEBUG/HReplay/DynLab2x", "DynLab 2D (X only)", 1, -0.5, 0.5, 10, 0., 10.);
        h2->SetDirectory(nullptr);
        h2->SetCanExtend(TH1::kAllAxes);
        msvc.remove_object(h2->GetName(), -1);
        msvc.register_object(h2->GetName(), -1, h2, true);

        h2 = new TH2I("/DEBUG/HReplay/DynLab2y", "DynLab 2D (Y only)", 10, 0., 10., 1, -0.5, 0.5);
        h2->SetDirectory(nullptr);
        h2->SetCanExtend(TH1::kAllAxes);
        msvc.remove_object(h2->GetName(), -1);
        msvc.register_object(h2->GetName(), -1, h2, true);

        h2 = new TH2I("/DEBUG/HReplay/DynLab2both", "DynLab 2D (X and Y)", 1, -0.5, 0.5, 1, -0.5, 0.5);
        h2->SetDirectory(nullptr);
        h2->SetCanExtend(TH1::kAllAxes);
        msvc.remove_object(h2->GetName(), -1);
        msvc.register_object(h2->GetName(), -1, h2, true);

        m_dynPubTime = std::time(nullptr);
    }

    // setup PublishingController and start publishing
    auto& onlineSvc = daq::rc::OnlineServices::instance();
    m_pub = std::make_unique<monsvc::PublishingController>();
    m_pub->add_configuration_rules(onlineSvc.getConfiguration());
    m_pub->start_publishing();

    // setup infodict
    m_dict = std::make_unique<ISInfoDictionary>(onlineSvc.getIPCPartition());
}

void MIGHReplayControllable::stopROIB(const daq::rc::TransitionCmd& cmd)
{
    ERS_LOG("MIGHReplayControllable::stopROIB");

    m_dict.reset();

    auto& msvc = monsvc::MonitoringService::instance();

    // stop publishing and do "final" publishing
    ERS_LOG("MIGHReplayControllable::stopROIB: stop_publishing");
    m_pub->stop_publishing();
    ERS_LOG("MIGHReplayControllable::stopROIB: publishing_all");
    m_pub->publish_all();

    ERS_LOG("MIGHReplayControllable::stopROIB: unregistering histograms");
    // un-register all regular histograms
    for (auto&& item: m_regular_histo) {
        msvc.remove_object(item.first);
    }
    // un-register all per-LBN histograms
    for (auto&& item: m_perlb_histo) {
        msvc.remove_object(item.first);
    }
    ERS_LOG("MIGHReplayControllable::stopROIB: done");
}

void MIGHReplayControllable::publish()
{
    ERS_LOG("MIGHReplayControllable::publish");

    // abusing publish() for simple periodic task - check whether new 
    // LB has started and regiser new per-LB histograms

    if (m_dict == nullptr) return;

    int lbn = -1;
    try {
        LumiBlock luminosity_info;
        m_dict->getValue(m_is_lb_object, luminosity_info);
        lbn = luminosity_info.LumiBlockNumber;
    } catch (const std::exception &ex) {
        ers::error(::errors::ISError(ERS_HERE, m_is_lb_object, ex));
        return;
    }

    auto& msvc = monsvc::MonitoringService::instance();

    if (m_dynLabels) {

        time_t now = std::time(nullptr);

        ERS_LOG("m_dynPubTime: " << m_dynPubTime << " npubs: " << m_npubsDynLabels
                    << " m_pubInterval: " << m_pubInterval << " now: " << now);

        if (now >= m_dynPubTime + m_npubsDynLabels*m_pubInterval) {

            std::string lblX = ::labels[std::rand() % ::labels.size()];
            std::string lblY = ::labels[std::rand() % ::labels.size()];
            ERS_LOG("publishing labels: " << lblX << " " << lblY);

            double rnd = (std::rand() % 100 - 50) / 7.;

            msvc.find_object<TH1I>("/DEBUG/HReplay/DynLab1", -1)->Fill(lblX.c_str(), 1.);
            msvc.find_object<TH2I>("/DEBUG/HReplay/DynLab2x", -1)->Fill(lblX.c_str(), rnd, 1.);
            msvc.find_object<TH2I>("/DEBUG/HReplay/DynLab2y", -1)->Fill(rnd, lblY.c_str(), 1.);
            msvc.find_object<TH2I>("/DEBUG/HReplay/DynLab2both", -1)->Fill(lblX.c_str(), lblY.c_str(), 1.);

            m_dynPubTime = now;
        }
    }

    if (lbn == m_prevLB) {
        // nothing to do yet
        return;
    }

    // may need to unregister old tags
    while (m_publishedLBs.size() >= m_bufferSize and m_bufferSize > 0) {
        int const tag = m_publishedLBs.front();
        m_publishedLBs.erase(m_publishedLBs.begin());
        ERS_DEBUG(1, "will remove old tag: " << tag);
        for (auto&& item: m_perlb_histo) {
            msvc.remove_object(item.first, tag);
        }
    }

    ERS_DEBUG(1, "will publish new tag: " << lbn);

    // remember it
    m_prevLB = lbn;
    m_publishedLBs.push_back(lbn);

    // register new tag
    for (auto&& item: m_perlb_histo) {
        LB2PathMap const& lb2path = item.second;
        auto iter = lb2path.find(lbn);
        if (iter == lb2path.end()) {
            // cannot find tag, use arbitrary tag
            iter = lb2path.begin();
        }
        // read histogram
        TObject* tobj = m_tfile->Get(iter->second.c_str());
        if (tobj == nullptr) {
            ers::error(::errors::ObjectReadError(ERS_HERE, iter->second));
            continue;
        }
        // have to remove histogram from its directory to avoid sharing mess
        dynamic_cast<TH1*>(tobj)->SetDirectory(nullptr);
        msvc.register_object(item.first, lbn, tobj, true);
    }
}

void MIGHReplayControllable::_open_file() 
{
    m_tfile.reset(TFile::Open(m_rootFilePath.c_str(), "READ"));
    if (m_tfile == nullptr or m_tfile->IsZombie()) {
        throw ::errors::FileOpenError(ERS_HERE, m_rootFilePath);
    }

    // get all names in a file
    std::set<std::string> names;
    _list_names(*m_tfile, "", names);

    // scan names and split them into per-LB and non-LB
    for (auto&& hpath: names) {
        ERS_DEBUG(1, "hpath: " << hpath);

        std::vector<std::string> pieces;
        boost::algorithm::split(pieces, hpath, boost::algorithm::is_any_of("/"));
        if (pieces.size() < 3) {
            // odd number of components
            continue;
        }

        // first strip run_NN/lb_MM part and remember LBN
        int lbn = -1;
        if (pieces[0].compare(0, 4, "run_") == 0) {
            if (pieces[1].compare(0, 3, "lb_") != 0) {
                continue;
            }
            lbn = std::stoi(pieces[1].substr(3));
            if (pieces.size() - 2 < 3) {
                // odd number of components
                continue;
            }
            pieces.erase(pieces.begin(), pieces.begin()+2);
        }

        // path should start with server name and provider name
        if (pieces[0] == m_mda_server and pieces[1] == m_mda_provider) {
            std::string hname;
            for (auto iter = pieces.begin()+2; iter != pieces.end(); ++ iter) {
                hname += '/';
                hname += *iter;
            }
            // remember names and paths
            ERS_DEBUG(1, "adding: " << lbn << " -> " << hpath);
            if (lbn == -1) {
                m_regular_histo[hname] = hpath;
            } else {
                m_perlb_histo[hname][lbn] = hpath;
            }
        }
    }

    // in MDA per-LB histograms appear at top level too, drop those
    for (auto&& item: m_perlb_histo) {
        m_regular_histo.erase(item.first);
    }
}

// recursively scan ROOT folders and build full set of names
void MIGHReplayControllable::_list_names(TDirectory& tdir, std::string const& dirPath, std::set<std::string>& names)
{
    TIter iter = tdir.GetListOfKeys();
    for (TObject* key = iter(); key != nullptr; key = iter()) {
        std::string const name = key->GetName();
        if (name[0] == '.') {
            // skip special folders
            continue;
        }

        std::string const path = dirPath.empty() ? name : dirPath + "/" + name;

        if (key->IsFolder()) {
            std::unique_ptr<TObject> tobj(tdir.Get(key->GetName()));
            _list_names(dynamic_cast<TDirectory&>(*tobj), path, names);
        } else {
            names.insert(path);
        }

    }
}

int
main(int argc, char **argv)
try {

    std::string rootFilePath;
    unsigned bufferSize = 2;
    unsigned pubInterval = 10;
    bool dynLabels = false;

    bpo::options_description desc("Application-specific options");
    desc.add_options()
        ("root-file", bpo::value<std::string>(&rootFilePath), "Path to MDA file with histograms.")
        ("buffer-size", bpo::value<unsigned>(&bufferSize)->default_value(2), "Number of per-LB histograms in a buffer.")
        ("pub-interval", bpo::value<unsigned>(&pubInterval)->default_value(10), "Monsvc publishing interval.")
        ("dyn-labels", bpo::bool_switch(&dynLabels)->default_value(false), "Make few dynamically-labelled histograms too.");

    try {
        bpo::variables_map vm;
        bpo::store(bpo::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        bpo::notify(vm);

        daq::rc::CmdLineParser cmdline(argc, argv, true);
        auto ctrl = std::make_shared<MIGHReplayControllable>(rootFilePath, bufferSize, dynLabels, pubInterval);
        daq::rc::ItemCtrl myItem(cmdline, ctrl);
        myItem.init();
        myItem.run();

        return 0;

    } catch (const daq::rc::CmdLineHelp& ex) {
        std::cout << "Replay histogram publishing from MDA file.\n\n";
        std::cout << ex.what() << '\n';
        std::cout << desc << '\n';
        return 0;
    }

} catch (const daq::rc::CmdLineError& ex) {
    ers::error(errors::CommandLineError(ERS_HERE, ex));
    return 1;
} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
