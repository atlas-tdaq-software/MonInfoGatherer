#!/usr/bin/env tdaq_python

"""Script which dumps info about labels in histogram axis
"""

from __future__ import print_function

import argparse
import logging
import sys

import ROOT


def _setLogging(lvl):
    """ Initialize logging """
    lmap = {0: logging.WARNING, 1: logging.INFO}
    logging.basicConfig(level=lmap.get(lvl, logging.DEBUG),
                        format="%(levelname)-8s %(message)s")


def _rootHistGen(rootdir, dirname=""):
    """Generator for names of all non-directory objects in ROOT file"""

    for key in rootdir.GetListOfKeys():

        cls = key.GetClassName()
        if cls in ['TDirectoryFile', 'TDirectory']:
            subdirname = dirname + key.GetName() + '/'
            subdir = key.ReadObj()
            for name in _rootHistGen(subdir, subdirname):
                yield name
        else:
            yield dirname + key.GetName()


def _checkLabels(histo, hname, do_full):
    """Check labels and possibly print some info
    """
    ndim = histo.GetDimension()
    axes = [("X", histo.GetXaxis())]
    if ndim > 1:
        axes += [("Y", histo.GetYaxis())]
    if ndim > 2:
        axes += [("Z", histo.GetZaxis())]

    for aname, axis in axes:
        nbins = axis.GetNbins()
        # labels = axis.GetLabels()
        labels = [(bin, axis.GetBinLabel(bin)) for bin in range(1, nbins+1)]
        labels = [(bin, label) for bin, label in labels if label]
        nlabels = len(labels)
        logging.debug("  axis: %s  nbins: %s  nlabels: %s", aname, nbins, len(labels))
        if nlabels > 0 and nlabels != nbins:
            print("partially labeled {aname} nbins={nbins} nlabels={nlabels} {hname} ({type})".format(
                aname=aname, nbins=nbins, nlabels=nlabels, hname=hname, type=histo.ClassName()
            ))
            labelstr = ", ".join(["{}:{}".format(bin, lbl) for bin, lbl in labels])
            print("    labels={labels}".format(labels=labelstr))
        if do_full and nlabels > 0 and nlabels == nbins:
            print("fully labeled {aname} nbins={nbins} nlabels={nlabels} {hname} ({type})".format(
                aname=aname, nbins=nbins, nlabels=nlabels, hname=hname, type=histo.ClassName()
            ))

def main() :

    # define options
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="increase verbosity")
    parser.add_argument('-f', '--full', action='store_true', default=False,
                        help="print info for fully-labeled axes")
    parser.add_argument('file_name', metavar="PATH", help="file with histograms")
    args = parser.parse_args()

    _setLogging(args.verbose)

    logging.info("opening ROOT file %s", args.file_name)
    froot = ROOT.TFile.Open(args.file_name, 'READ')
    if not froot:
        raise IOError("Failed to open file " + args.file_name)

    for hname in _rootHistGen(froot):
        if hname.startswith(".meta/"):
            continue
        histo = froot.Get(hname)
        if not histo.InheritsFrom("TH1"):
            logging.info("unexpected type %s (%s)", histo.ClassName(), hname)
            continue
        logging.debug("checking histogram %s (%s)", hname, histo.ClassName())
        _checkLabels(histo, hname, args.full)


if __name__ == "__main__" :
    sys.exit(main())
