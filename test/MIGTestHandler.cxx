/**
 *  Test application which reads a histogram from a ROOT file, converts
 *  it to OH histogram, and calls a handler multiple times with that single
 *  histogram. Primary purpose of this is to stress container/algo for
 *  profiling (profiling would be done with a tool like perf/oprofile)
 */

#include <algorithm>
#include <atomic>
#include <iostream>
#include <random>
#include <boost/program_options.hpp>

#include "ers/ers.h"
#include "is/infoany.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/DefaultHandler.h"
#include "MonInfoGatherer/InfoPublisher.h"
#include "MonInfoGatherer/Timers.h"
#include "oh/OHRootProvider.h"
#include "TFile.h"

namespace bpo = boost::program_options;
using namespace MIG;

namespace {

// Implementation of publisher that does not publish
class DummyPublisher : public InfoPublisher {
public:

    ~DummyPublisher() {
        ERS_LOG("DummyPublisher: publish count: " << m_publishCount);
    }

    // implements InfoPublisher interface
    void publish(ISInfo& info, const std::string& name, int nProv) override {
        // just count callbacks
        //ERS_LOG("DummyPublisher: publish");
        ++ m_publishCount;
    }

private:
    std::atomic<unsigned> m_publishCount{0};  // counts publish callbacks
};

void doShuffleLabels(oh::Histogram& hist) {
    std::random_device rd;
    std::mt19937 g(rd());
    for (auto&& axis: hist.axes) {
        // ERS_LOG("Shuffling label axis " << axis.title << " nbins=" << axis.nbins);
        std::shuffle(axis.indices.begin(), axis.indices.end(), g);
        std::shuffle(axis.labels.begin(), axis.labels.end(), g);
    }
}

} // namespace

int main(int argc, char* argv[])
{
    std::string fpath;
    std::string hname;
    std::string algName;
    std::string tpath;
    int numProviders;
    int numCycles;
    int delayMillisec;
    unsigned timingSize;
    bool shuffleLabels = false;

    // make all command line options and parse
    try {
        boost::program_options::options_description desc("Test application for DefaultHandler");
        desc.add_options()
            ("root-file,f", bpo::value<std::string>(&fpath)->required(), "ROOT file path")
            ("histo-path,H", bpo::value<std::string>(&hname)->required(), "Histogram name path")
            ("algorithm,a", bpo::value<std::string>(&algName)->default_value("LabelledMerge"), "Name of the algorithm to use")
            ("providers,p", bpo::value<int>(&numProviders)->default_value(24), "Number of providers")
            ("cycles,n", bpo::value<int>(&numCycles)->default_value(100), "Number of publication cycles")
            ("delay-mksec,d", bpo::value<int>(&delayMillisec)->default_value(1), "Delay between publication cycles, millisec")
            ("timing-file,t", bpo::value<std::string>(&tpath)->default_value(""), "timing file path")
            ("timing-size,T", bpo::value<unsigned>(&timingSize)->default_value(10000), "timing history size")
            ("shuffle-labels,l", "shuffle labels on every publication")
            ("help,h", "Print help message");

        bpo::variables_map vm;
        bpo::store(bpo::command_line_parser(argc, argv).options(desc).run(), vm);
        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }
        bpo::notify(vm);

        shuffleLabels = vm.count("shuffle-labels") > 0;

    } catch (const std::exception& ex) {
        std::cerr << "Caught exception " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    // open file
    TFile tfile(fpath.c_str(), "READ");
    if (!tfile.IsOpen()) {
        std::cerr << "Failed to open Root file " << fpath << std::endl;
        return EXIT_FAILURE;
    }

    // find histogram
    TH1* histo = dynamic_cast<TH1*>(tfile.Get(hname.c_str()));
    if (histo == nullptr) {
        std::cerr << "Failed to find histogram " << hname << std::endl;
        return EXIT_FAILURE;
    }
    ERS_LOG("ROOT histo errors: " << histo->GetSumw2());

    // convert it into OH object
    auto infoObj = OHRootProvider::convert(*histo);
    if (infoObj == nullptr) {
        std::cerr << "Failed to convert histogram into OH object" << std::endl;
        return EXIT_FAILURE;
    }
    // this is necessary to initialize ISType object inside info
    static_cast<ISInfo&>(*infoObj).type();

    // make a list of provider names
    std::vector<std::string> providers;
    for (int i = 0; i != numProviders; ++ i) {
        providers.push_back("provider-" + std::to_string(i));
    }

    // instanciate handler with all parameters
    HandlerConfig hConfig(".*", "",
                          {"EOPRecoveryDelay_ms=" + std::to_string(delayMillisec/2)});
    DummyPublisher publisher;
    DefaultHandler handler(hConfig, publisher);

    TimingEntries::setHistorySize(timingSize);

    // do publishing now
    for (int cycle=0; cycle != numCycles; ++ cycle) {
        ERS_LOG("Starting publication cycle #" << cycle);

        for (auto&& prov: providers) {
            if (shuffleLabels) {
                ::doShuffleLabels(*infoObj);
            }
            ISInfoAny infoAny;
            infoAny <<= *infoObj;
            handler.updateInfo(infoAny, hname, prov);
        }

        usleep(delayMillisec*1000);
    }

    // save timing info to file(s)
    if (!tpath.empty()) {
        ERS_LOG("Saving timing info to " << tpath);
        TimingSaver saver(tpath);
        handler.saveTiming(saver);
        saver.close();
    }

    return EXIT_SUCCESS;
}
