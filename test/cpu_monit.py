#!/usr/bin/env python

from __future__ import print_function, division

import argparse
import os
import psutil
import re
import time

def main():

    parser = argparse.ArgumentParser(description='CPU monitoring.')
    parser.add_argument("-f", dest="full", default=False, action="store_true",
                        help="Match regexp against whole command line")
    parser.add_argument("-i", dest="interval", default=1, type=int,
                        help="Number of seconds between quering process state")
    parser.add_argument("regex", nargs="+",
                        help="Regular expression for process name or command line")
    args = parser.parse_args()

    regexes = []
    for restr in args.regex:
        regexes += [re.compile(restr)]

    mypid = os.getpid()

    processes = []
    for proc in psutil.process_iter():
        if proc.pid == mypid:
            continue
        if args.full:
            name = ' '.join(proc.cmdline)
        else:
            name = proc.name
        for regex in regexes:
            if regex.search(name):
                processes += [proc]

    # header
    print(",".join(["time"]+[proc.name for proc in processes]))

    while True:
        
        now = time.time()
        next = (int(now + 0.1) // args.interval + 1) * args.interval
        wait = next - now
        # print ("sleep %f sec" % wait)
        time.sleep(wait)
        
        now = int(time.time() + 0.1)
        cpus = ["%.2f" % proc.get_cpu_percent() for proc in processes]
        row = [str(now)] + cpus
        print(",".join(row))

if __name__ == "__main__":
    main()
