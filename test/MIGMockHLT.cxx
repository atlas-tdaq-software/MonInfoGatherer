/**
 *  Stand-alone app which simulates HLTMPPU monitoring.
 */

#include <iostream>
#include <sstream>
#include <stdlib.h>

#include <boost/algorithm/string/replace.hpp>
#include <boost/dll.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "ers/ers.h"
#include "hltinterface/IInfoRegister.h"
#include "is/inforeceiver.h"
#include "MonInfoGatherer/Config.h"
#include "MonInfoGatherer/InfoHandlerWrapper.h"
#include "MonInfoGatherer/LibraryManager.h"
#include "TH1.h"

#include "MIGTestUtils.h"

namespace bpo = boost::program_options;
namespace bpt = boost::property_tree;
using namespace MIG;

namespace {

char const MonSvcConfig[] = R"raw(
<HLTMonInfoImpl>
    <ConfigurationRules>
        <ConfigurationRuleBundle>
            <UID>HLTMPPUConfigurationRuleBundle</UID>
            <Rules>
                <ConfigurationRule>
                    <UID>HLTMPPUSlowPublishingOHRule</UID>
                    <Name>SlowOH</Name>
                    <IncludeFilter>.*</IncludeFilter>
                    <ExcludeFilter></ExcludeFilter>
                    <Parameters>
                        <UID></UID>
                        <OHPublishingParameters>
                            <PublishInterval>60</PublishInterval>
                            <NumberOfSlots>6</NumberOfSlots>
                            <OHServer>{OHServer}</OHServer>
                            <ROOTProvider>{OHProvider}</ROOTProvider>
                        </OHPublishingParameters>
                    </Parameters>
                </ConfigurationRule>
                <ConfigurationRule>
                    <UID>HLTMPPUFastOHPublishingParameter</UID>
                    <Name>FastOH</Name>
                    <IncludeFilter></IncludeFilter>
                    <ExcludeFilter></ExcludeFilter>
                    <Parameters>
                        <UID></UID>
                        <OHPublishingParameters>
                            <PublishInterval>10</PublishInterval>
                            <NumberOfSlots>1</NumberOfSlots>
                            <OHServer>{OHServer}</OHServer>
                            <ROOTProvider>{OHProvider}</ROOTProvider>
                        </OHPublishingParameters>
                    </Parameters>
                </ConfigurationRule>
            </Rules>
        </ConfigurationRuleBundle>
    </ConfigurationRules>
</HLTMonInfoImpl>
)raw";

bpt::ptree make_monsvc_config(std::string const& is_server_name, std::string const& provider_name) {
    bpt::ptree config;
    std::string cfg_str = ::MonSvcConfig;
    boost::algorithm::replace_all(cfg_str, "{OHServer}", is_server_name);
    boost::algorithm::replace_all(cfg_str, "{OHProvider}", provider_name);
    std::istringstream istr(cfg_str);
    bpt::read_xml(istr, config);
    return config;
}


std::unique_ptr<hltinterface::IInfoRegister> makeInfoService() {
    ERS_LOG("Instantiating info service");
    using isvccreator = hltinterface::IInfoRegister* ();
    boost::dll::shared_library infoservice_library =
        boost::dll::shared_library("libMonSvcInfoService.so",
                                   boost::dll::load_mode::type::search_system_folders |
                                   boost::dll::load_mode::type::rtld_global);
    std::function<isvccreator> isc = infoservice_library.get<isvccreator>("create_hltmp_infoservice");
    return std::unique_ptr<hltinterface::IInfoRegister>(isc());
}

std::vector<TH1*>
makeHistograms(hltinterface::IInfoRegister& infoSvc) {
    std::vector<TH1*> histos;
    histos.push_back(new TH1I("th1", "th1", 10, 0., 10.));
    for (auto h: histos) {
        infoSvc.registerTObject("service", h->GetName(), h);
    }
    return histos;
}

Config make_gatherer_config(const std::string& serverName) {
    HandlerConfig hc(".*", "Default", {});
    Config cfg(".*", ".*", {serverName}, {serverName}, {hc});
    return cfg;
}

} // namespace 


int main(int argc, char* argv[])
try {
    std::string partition;
    std::string is_server_name;

    boost::program_options::options_description desc(
            "Stand-alone app which simulates HLTMPPU monitoring.");
    desc.add_options()
        ("partition,p", bpo::value<std::string>(&partition)->default_value(""), "Partition")
        ("is-server,s", bpo::value<std::string>(&is_server_name)->default_value("isrepo"), "IS server name")
        ("help,h", "Print help message");

    bpo::variables_map vm;
    bpo::store(bpo::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    bpo::notify(vm);

    if (vm.count("help")) {
        std::cout << argv[0] << std::endl;
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    std::unique_ptr<MIG::Test::Partition> test_part;
    if (partition.empty()) {
        test_part = std::make_unique<MIG::Test::Partition>();
        partition = test_part->name();
        // wait until it starts
        sleep(2);
    }

    // make sure TDAQ_PARTITION is set
    setenv("TDAQ_PARTITION", partition.c_str(), 1);

    std::list<std::pair<std::string, std::string>> options;
    IPCCore::init(options);

    IPCPartition ipc_part(partition);

    auto repository = new MIG::Test::ISRepoPrint(ipc_part, is_server_name);
    repository->publish();


    LibraryManager::instance().loadLibrary("libMIGContainers.so");
    Config cfg = make_gatherer_config(is_server_name);
    auto receiver = std::make_unique<ISInfoReceiver>(ipc_part, false);
    auto handler = std::make_unique<InfoHandlerWrapper>(cfg, "gatherer", ipc_part);

    const int Nworkers = 3;     // In reality there can be only one
    std::vector<bpt::ptree> configs;
    for (int w = 0; w < Nworkers; ++ w) {
        std::string provider_name = "HLTMPPU-" + std::to_string(w);
        configs.push_back(make_monsvc_config(is_server_name, provider_name));
    }

    ERS_LOG("Configuring MonSvc");
    std::vector<std::unique_ptr<hltinterface::IInfoRegister>> infoSvcs;
    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs.push_back(makeInfoService());
    }

    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs[w]->configure(configs[w]);
    }
    // Registry is singleton, can only register histogram once
    std::vector<TH1*> histos = makeHistograms(*infoSvcs[0]);

    ERS_LOG("MonSvc prepareForRun");
    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs[w]->prepareForRun(configs[w]);
    }

    handler->subscribe(*receiver);

    std::vector<bpt::ptree> wconfigs;
    for (int w = 0; w < Nworkers; ++ w) {
        boost::property_tree::ptree conf;
        conf.put("appName", "HLTMPPU-" + std::to_string(w));
        wconfigs.push_back(conf);
    }

    ERS_LOG("MonSvc prepareWorker");
    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs[w]->prepareWorker(wconfigs[w]);
    }

    sleep(200);

    ERS_LOG("MonSvc finalizeWorker");
    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs[w]->finalizeWorker(wconfigs[w]);
    }

    ERS_LOG("MonSvc finalize");
    for (int w = 0; w < Nworkers; ++ w) {
        infoSvcs[w]->finalize(wconfigs[w]);
    }

    handler->unsubscribe(*receiver);
    handler.reset();
    receiver.reset();

    infoSvcs.clear();

    repository->withdraw();
    repository->_destroy(true);

} catch (std::exception const& exc) {
    ERS_LOG("Exception:" << exc.what());
    return 1;
}
