
# MonInfoGatherer

Alternative implementation for merging non-histogram data (ADHI-4842). This
should resolve most of the timing issues we have seen with DCM data in the
past. It is enabled by default but can be disabled with `ISDynAnyNG`
configuration parameter (see `ConfigParameters.md`).

## tdaq-09-02-01

Fix for the merge algorithm with large scale factor (ADHI-4823).

## tdaq-09-02-00

Removed copy of lz4 library from sources.

## tdaq-09-01-00

Add support for dynamically labelled axes (ADHI-4769).
