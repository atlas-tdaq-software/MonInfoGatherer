
# Configuration parameters

In the recent update of the MIG configuration schema in OKS many parameters
that control MIG functions are now stored in a `Parameters` attribute of
`MIGMatchHandler` configuration class. That attribute is a simple list of
strings, the reason for it being free-format is that we still support
dynamic loading of handlers and parameters obviously depend on handler
implementation (and container classes that they) use so it is not possible
to configure in advance something that we do not know.

Some parameters in that list can be just names signifying some boolean flag,
other parameters can be names with associated value(s). Latter case is
represented by simple conversion oa string in a form "Name=Value". For
parameters with multiple values regular convention is to vae several
strings in a list with the same parameter name. Here is an example of
possible configuration:

    <obj class="MIGMatchHandler" id="handlerAll">
      <attr name="ObjectRegExp" type="string">".*"</attr>
      <attr name="Name" type="string">"DefaultHandler"</attr>
      <attr name="Parameters" type="string" num="4">
        "DoSomethingFancy"          -- boolean flag
        "Timeout_ms=500"            -- parameter with value
        "PerLBHistogram=auto"       -- parameter with two values
        "PerLBHistogram=/LB/.*"
      </attr>
    </obj>

Of single-value parameter appears twice in the list only its first value
is used.

Boolean parameters can usually be specified one of three formats:
- `"Parameter=1"` -- to set parameter to "true" or "enabled" state
- `"Parameter"` -- same as `"Parameter=1"`
- `"Parameter=0"` -- to set parameter to "false" or "disabled" state

Below is the list of known parameters used by the code in this package.


## CompressTargetPercent

Integer parameter defining target compression ratio in percent. If after
compression the size exceeds this target value (fraction of the original size)
then uncompressed data is stored. Default value is `75`.

## Concatenate

Used by non-histogram merging algorithm, if this parameter is enabled then
arrays in IS objects are concatenated, otherwise arrays are not summed (array
content will be undefined). Disabled by default.

## EOPRecoveryDelay_ms

Specifies time in milliseconds for a publication to be considered a part of
preceding publication cycle. Used by a "recovery" algorithm which re-enables
providers that had previously disappeared. Default value is `5000` (5 seconds).

## FinalPublishDeadband_sec

Specifies time in seconds for a deadband applied to FINAL publications. This is
used to suppress "publication storms" for a very short runs when the first
publication from a provider is also a FINAL publication.

## ISDynAnyCyclesMissing

Integer (unsigned) parameter used by new implementation of IS publishing. If a
contribution from a provider is missing for this number of publication cycles
then provider is assumed to be dead and its contribution is removed. Default
value is `3`.

## ISDynAnyNG

This parameters defines when new ISDynAny algorithm is to be used. New
algorithm is supposed to work better and be less sensitive to timing issues
than the old one, but it is also less tested than the old one. If new algorithm
needs to be disabled then this parameter provides means for that. By default
new algorithm is used for all non-histogram IS objects. `ISDynAnyNG` can be
used to specify regular expression (one or few) to match object names, and only
the matching names will use new algorithm. To disable new algorithm completely
one has to provide empty value for this parameter. The parameter is
multi-valued, meaning that it can appear few times in the list of parameters.

Few examples:

    # same as default configuration
    <attr name="Parameters" type="string" num="1">
        "ISDynAnyNG=.*"
    </attr>

    # this disables new algorithm completely
    <attr name="Parameters" type="string" num="1">
        "ISDynAnyNG="
    </attr>

    # this enables NG algorithm only for matching IS object names,
    # all other names will use old algorithm.
    <attr name="Parameters" type="string" num="1">
        "ISDynAnyNG=DCM.*"
        "ISDynAnyNG=SFO.*"
    </attr>


## ISDynAnyPublishDelay_ms

Used by new implementation of IS publishing during ramp-up period when
publication rate cannot be determined. IS object is published if there are no
updates within this delay. Default value is `2500` (2.5 seconds). Actual delay
can be longer as this condition is only checked twice every second.


## MinCompressSizeBytes

Integer parameter defining minimum size of data (bin arrays) in bytes for
compression. Compression is only performed on per-LB histograms. Default
size is `16384` (16kB).

## Normalized

This boolean parameter is used by histogram summing algorithm, if parameter
is enabled the algorithm uses normalized/weighed summing of bin data.
Disabled by default.

## PeriodCutoff_sec

Integer parameter specifying an upper limit for the estimate of the
publication period. Used by `ISDynAnyNG` when it guesses publication rates.
Default value is `300` (5 minutes).

## PerLBHistogram

This defines which histograms are considered per-LB histograms. By default any
non-negative tag is treated as per-LB histogram (per-LB histograms are not
supposed to be published with tag=-1). Using this parameter one can change
matching criteria. The parameter is multi-valued, meaning that it can appear
few times in the list of parameters. If parameter is specified then default
tag-based matching is disabled unless one of the values is `auto`. Values
that are not `auto` or `noauto` are used as regular expressions for matching
histogram names.

Few examples:

    # same as default configuration
    <attr name="Parameters" type="string" num="1">
        "PerLBHistogram=auto"
    </attr>

    # disable tag-based matching but use all names starting with /LB/ or /LBN/
    <attr name="Parameters" type="string" num="2">
        "PerLBHistogram=/LB/.*"
        "PerLBHistogram=/LBN/.*"
    </attr>

    # use both tag-based matching and names starting with /LB/
    <attr name="Parameters" type="string" num="2">
        "PerLBHistogram=/LB/.*"
        "PerLBHistogram=auto"
    </attr>

    # completely disable per-LB histograms
    <attr name="Parameters" type="string" num="1">
        "PerLBHistogram=noauto"
    </attr>

## PerLBPublishDelay_ms

Used by per-LB histogram publishing thread, histogram is published if there
are no updates within this delay. Default value is `1500` (1.5 seconds).
Actual delay can be longer as this condition is only checked once every
second.

## PublishAverage

Boolean parameter used by non-histogram container to enable publishing of the
accumulated averages. Disabled by default.

## PublishSum

Boolean parameter used by non-histogram container to enable publishing of the
accumulated sum. Enabled by default.

## SuffixAverage

Suffix added to the name of the published object for accumulated average.
Default: "_avg".

## SuffixSum

Suffix added to the name of the published object for accumulated sum.
Default: "".

## TagExpiration_sec

Used by cleanup algorithm that removes histogram tags that are not updated,
it removes the tags that were not updated within that number of seconds from
the most recent publication of any tag (of the same histogram). Default value
is `297` (almost 5 minutes).

## TimingHistorySize

Determines history size limit for timing information stored in memory. This is
only meaningful when MIG is compiled with `MIG_TIMING_ENABLE` preprocessor
macro set to non-zero. Timing information is saved to the files on
`Unconfigure` transition if `MIG_TIMING_FILE` envvar is defined as non-empty
string containint an initial path name of the file names. This string can
contain literal `{time}` which is replaced with a current timestamp (seconds
since UNIX epoch).
